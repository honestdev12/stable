#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER stable_dev WITH PASSWORD 'hello_stable' CREATEDB;
    CREATE DATABASE stable_dev;
    GRANT ALL PRIVILEGES ON DATABASE stable_dev TO stable_dev;
EOSQL
