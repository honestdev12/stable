#!/usr/bin/env bash

until cd src
do
    echo "Waiting for django volume..."
done

until python3.6 manage.py migrate
do
    echo "Waiting for postgres ready..."
    sleep 2
done

python3.6 manage.py loaddata /django/src/fixtures/User_fixtures /django/src/fixtures/Market_fixtures /django/src/fixtures/Category_fixtures /django/src/fixtures/Partner_fixtures /django/src/fixtures/Index_fixtures
python3.6 manage.py runserver 0.0.0.0:8000
