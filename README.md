
# StablePrice Dashboard & ChatBot Rest APIs


## Retrieve code

* `$ git clone https://github.com/theaox/aox_dr.git`


## Installation

You have two ways of running this project: Using the Dockers scripts or running directly in the console.

### Running NO DOCKER

**Compile and run project**

* `$ virtualenv -p /usr/bin/python3 virtualenv`
* `$ source virtualenv/bin/activate`
* `$ pip install -r py-requirements/dev.txt`

* `$ cd src`
* `$ python manage.py migrate`
* `$ python manage.py loaddata fixtures.json`
* `$ python manage.py runserver`

Then open your browser the page: http://HOST_URL:8000/ If all goes ok you should see Django Rest Framework Welcome page.

### Running DOCKER

* Install [Docker](https://www.docker.com/products/overview) and [Docker Compose](https://docs.docker.com/compose/install/).
* `$ docker-compose build`
* `$ docker-compose up`

To stop the development server:

* `$ docker-compose stop`

Stop Docker development server and remove containers, networks, volumes, and images created by up.

* `$ docker-compose down`

You can access shell in a container

* `$ docker ps  # get the name from the list of running containers`
* `$ docker exec -i -t DOCKER_CONTAINER_NAME /bin/bash`

The database can be accessed @localhost:5433

* `$ psql -h localhost -p 5433 -U POSTGRES_USER POSTGRES_DB_NAME`

**Install wkhtmltopdf library for email sending**

Run `aoxdr_backend_1` docker container
* `docker exec -i -t aoxdr_backend_1 /bin/bash`

Download and install wkhtmltopdf library

* `cd ~`
* `wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz`
* `tar vxf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz`
* `cp wkhtmltox/bin/* /usr/bin/`
