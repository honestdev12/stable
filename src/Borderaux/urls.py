from rest_framework.routers import DefaultRouter

from Borderaux import views

router = DefaultRouter()

router.register(r'risks', views.RiskBorderauxViewSet)
router.register(r'claims', views.ClaimBorderauxViewSet)

urlpatterns = router.get_urls()
