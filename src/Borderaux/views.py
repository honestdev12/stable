from django_filters.rest_framework import (
    DjangoFilterBackend,
    FilterSet,
)
from rest_condition import Or
from rest_framework.permissions import (
    IsAdminUser,
    IsAuthenticated,
)
from rest_framework.settings import api_settings
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework_csv.renderers import CSVRenderer

from Borderaux.permissions import IsUnderwriter
from .models import (
    ClaimBorderaux,
    RiskBorderaux,
)
from .serializer import (
    ClaimBorderauxSerializer,
    RiskBorderauxSerializer,
)


class RiskBorderauxFilter(FilterSet):
    class Meta:
        model = RiskBorderaux
        fields = '__all__'


class ClaimBorderauxFilter(FilterSet):
    class Meta:
        model = ClaimBorderaux
        fields = '__all__'


class RiskBorderauxViewSet(ReadOnlyModelViewSet):

    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (CSVRenderer,)
    permission_classes = (IsAuthenticated, Or(IsUnderwriter, IsAdminUser))
    queryset = RiskBorderaux.objects.all()
    serializer_class = RiskBorderauxSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = RiskBorderauxFilter


class ClaimBorderauxViewSet(ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated, Or(IsUnderwriter, IsAdminUser))
    queryset = ClaimBorderaux.objects.all()
    serializer_class = ClaimBorderauxSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ClaimBorderauxFilter
