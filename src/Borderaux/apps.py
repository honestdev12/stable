from django.apps import AppConfig


class BorderauxConfig(AppConfig):
    name = 'Borderaux'
