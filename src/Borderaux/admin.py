from __future__ import unicode_literals

from django.contrib import admin

from . import models


class RiskBorderauxAdmin(admin.ModelAdmin):
    list_display = ['transaction', 'transaction_risk_type', 'reporting_period_end', 'certificate_reference', 'in_force']
    raw_id_fields = ('transaction',)


class ClaimBorderauxAdmin(admin.ModelAdmin):
    list_display = ['cover_holder', 'reporting_period_end', 'certificate_reference', 'insured_full_name']


admin.site.register(models.ClaimBorderaux, ClaimBorderauxAdmin)
admin.site.register(models.RiskBorderaux, RiskBorderauxAdmin)
