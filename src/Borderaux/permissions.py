from rest_framework.permissions import BasePermission


class IsUnderwriter(BasePermission):

    def has_permission(self, request, view):
        return request.user.is_underwriter

    def has_object_permission(self, request, view, obj):
        return request.user.is_underwriter
