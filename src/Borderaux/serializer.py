from django.conf import settings
from rest_framework import serializers

from CRM.models import Transaction
from . import models


class BaseBorderauxSerializer(serializers.ModelSerializer):
    unique_market_reference = serializers.CharField(default=settings.UNIQUE_MARKET_REFERRENCE)

    claim_reference = serializers.PrimaryKeyRelatedField(source='transaction', read_only=True)
    customer_id = serializers.PrimaryKeyRelatedField(source='transaction.farmer.pk', read_only=True)
    certificate_reference = serializers.UUIDField(read_only=True)
    contract_type = serializers.SerializerMethodField()

    insured_first_name = serializers.CharField(source="transaction.farmer.first_name", default="")
    insured_name = serializers.SerializerMethodField()
    insured_address = serializers.CharField(source="transaction.farmer.address_1")
    insured_subdivision = serializers.CharField(source="transaction.farmer.county")
    insured_postcode = serializers.CharField(source="transaction.farmer.post_code")
    insured_country = serializers.CharField(source="transaction.country")
    insured_or_reinsured_country = serializers.CharField(source="transaction.country")

    risk_address = serializers.CharField(source="transaction.farmer.address_1")
    risk_county = serializers.CharField(source="transaction.farmer.address_1")
    risk_subdivision = serializers.CharField(source="transaction.farmer.address_2")
    risk_postcode = serializers.CharField(source="transaction.farmer.post_code")
    risk_country = serializers.CharField(source="transaction.country")

    effective_date_of_transaction = serializers.CharField(default="")
    expiry_date_of_transaction = serializers.CharField(default="")

    total_sum_insured = serializers.DecimalField(
        source='transaction.potential_loss',
        max_digits=24,
        decimal_places=8
    )

    deductible_excess_currency = serializers.CharField(default="")
    deductible_excess_amount = serializers.CharField(default="")
    deductible_excess_basis = serializers.CharField(default="")
    coverage_limit = serializers.CharField(default="")

    gross_premium_paid_this_time = serializers.DecimalField(
        source='transaction.monthly_cost',
        decimal_places=8,
        max_digits=24
    )
    total_gross_written_original_currency = serializers.DecimalField(
        source='transaction.total_cost',
        decimal_places=8,
        max_digits=24
    )
    total_taxes_and_levies = serializers.DecimalField(
        source='transaction.tax',
        decimal_places=8,
        max_digits=24
    )
    commission_percent = serializers.DecimalField(
        source='transaction.commission',
        default=20,
        decimal_places=8,
        max_digits=24
    )

    coverholder_commission_amount = serializers.DecimalField(
        source='transaction.commission',
        default=20,
        decimal_places=8,
        max_digits=24
    )

    net_premium_to_london_original_currency = serializers.DecimalField(
        source='transaction.premium',
        decimal_places=8,
        max_digits=24
    )
    net_premium_to_london_settlement_currency = serializers.DecimalField(
        source='transaction.premium',
        decimal_places=8,
        max_digits=24
    )

    number_of_installments = serializers.IntegerField(source='installments')
    installment_basis = serializers.CharField(source='get_installment_basis_display')

    other_fee_description = serializers.CharField(default="")
    other_fee_amount = serializers.CharField(default="")
    total_survey_fee_amount = serializers.CharField(default="")
    total_admin_fee_amount = serializers.CharField(default="")

    occupation = serializers.CharField(default="Farmer")
    sic_code = serializers.SerializerMethodField()
    occupation_description = serializers.SerializerMethodField()

    product_type = serializers.CharField(source='transaction.category.name')
    existing_customer = serializers.SerializerMethodField()
    reporting_period_start_date = serializers.DateField(source='transaction.start_date')
    reporting_period_end_date = serializers.DateField(source='transaction.stop_date')

    def get_insured_address(self, obj):
        return ""

    def get_insured_subdivision(self, obj):
        return ""

    def get_insured_postcode(self, obj):
        return ""

    def get_insured_country(self, obj):
        return ""

    def get_risk_address(self, obj):
        return ""

    def get_risk_county(self, obj):
        return ""

    def get_risk_subdivision(self, obj):
        return ""

    def get_risk_postcode(self, obj):
        return ""

    def get_risk_country(self, obj):
        return ""

    def get_occupation(self, obj):
        return "Farmer"

    def get_sic_code(self, obj):
        return "1500"

    def get_occupation_description(self, obj):
        return ""

    def get_contract_type(self, obj):
        return 'Average' if obj.transaction.protection_style == 'Asian' else 'Spot'

    def get_insured_name(self, obj):
        return f"{obj.transaction.farmer.first_name} {obj.transaction.farmer.last_name}"

    def get_insured_or_reinsured_country(self, obj):
        return obj.transaction.country

    def get_product_type(self, obj):
        return f"{obj.transaction.category.name}"

    def get_existing_customer(self, obj):
        return Transaction.objects.filter(
            farmer=obj.transaction.farmer,
            category=obj.transaction.category,
            datetime_created__lt=obj.transaction.datetime_created
        ).exclude(
            pk=obj.pk
        ).exists()


class RiskBorderauxSerializer(BaseBorderauxSerializer):
    policy_or_group_reference = serializers.PrimaryKeyRelatedField(source='transaction', read_only=True)

    risk_cancellation_date = serializers.SerializerMethodField()
    reason_for_cancellation = serializers.SerializerMethodField()

    class Meta:
        model = models.RiskBorderaux
        fields = (
            'unique_market_reference',
            'reporting_period_start_date',
            'reporting_period_end_date',
            'customer_id',
            'policy_or_group_reference',
            'product_type',
            'contract_type',
            'certificate_reference',
            'insured_first_name',
            'insured_name',
            'insured_address',
            'insured_subdivision',
            'insured_postcode',
            'insured_country',
            'existing_customer',
            'risk_inception_date',
            'risk_expiry_date',
            'risk_address',
            'risk_county',
            'risk_subdivision',
            'risk_postcode',
            'risk_country',
            'effective_date_of_transaction',
            'expiry_date_of_transaction',
            'total_sum_insured',

            'deductible_excess_currency',
            'deductible_excess_amount',
            'deductible_excess_basis',
            'coverage_limit',

            'gross_premium_paid_this_time',
            'total_gross_written_original_currency',
            'total_taxes_and_levies',
            'commission_percent',
            'coverholder_commission_amount',
            'net_premium_to_london_original_currency',
            'net_premium_to_london_settlement_currency',
            'number_of_installments',
            'installment_basis',
            'notes',

            'other_fee_description',
            'other_fee_amount',
            'total_survey_fee_amount',
            'total_admin_fee_amount',

            'occupation',
            'sic_code',
            'occupation_description',
            'in_force',
            'risk_cancellation_date',
            'reason_for_cancellation',
            'policy_issuance_date',
        )

    def get_risk_cancellation_date(self, obj):
        if obj.transaction.cancelled:
            return obj.risk_cancellation_date
        else:
            return ""

    def get_reason_for_cancellation(self, obj):
        if obj.transaction.cancelled:
            return obj.reason_for_cancellation
        else:
            return ""



class ClaimBorderauxSerializer(BaseBorderauxSerializer):
    class Meta:
        model = models.ClaimBorderaux
        fields = '__all__'
