from uuid import uuid4

from django.conf import settings
from django.db import models

TRANSACTION_RISK_TYPE_CHOICES = (
    ('new', 'New'),
    ('renewal', 'Renewal'),
    ('cancellation', 'Cancellation'),
    ('amendment', 'Amendment'),
)

BORDERAUX_INSTALLMENT_BASIS_CHOICES = (
    ('monthly', 'Monthly'),
    ('in_full', 'In Full'),
    ('quarterly', 'Quarterly'),
    ('bi_annual', 'Bi-Annually')
)

BORDERAUX_CANCELLATION_REASON_CHOICES =(
    ('unsuitable', 'Product Unsuitable / Misunderstood'),
    ('expensive', 'Product too Expensive'),
    ('alternative', 'Alternative product purchased'),
    ('pre_existing', 'Cover Overlapped with Pre-Existing Cover'),
    ('cooling_off', 'Product Cancelled within Cooling Off Period'),
    ('underwriter', 'Product Cancelled by Underwriter'),
    ('complaint', 'Poor service / Complaint'),
    ('not_needed', 'Cover No Longer Required'),
    ('other', 'Other (Administrative Reason)'),
    ('unknown', 'Unknown'),
)


class RiskBorderaux(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    unique_market_reference = models.TextField(null=True, blank=True, default=settings.UNIQUE_MARKET_REFERRENCE)
    reporting_period_start = models.DateField()
    reporting_period_end = models.DateField()
    transaction = models.ForeignKey('CRM.Transaction', on_delete=models.PROTECT)
    risk_inception_date = models.DateField()
    risk_expiry_date = models.DateField()
    transaction_risk_type = models.TextField(choices=TRANSACTION_RISK_TYPE_CHOICES, default='new')
    installments = models.IntegerField()
    installment_basis = models.TextField(choices=BORDERAUX_INSTALLMENT_BASIS_CHOICES, default='monthly')
    notes = models.TextField()
    risk_cancellation_date = models.DateTimeField(null=True)
    reason_for_cancellation = models.TextField(choices=BORDERAUX_CANCELLATION_REASON_CHOICES, default='unknown')
    sic_code = models.TextField(default="1500")
    sic_description = models.TextField(default="Mixed Farming")
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.transaction} {self.get_transaction_risk_type_display()}"

    @property
    def policy_reference(self):
        return self.transaction.pk

    @property
    def certificate_reference(self):
        return self.transaction.pdf_id

    @property
    def policy_issuance_date(self):
        return self.transaction.start_date

    @property
    def in_force(self):
        return not self.transaction.cancelled


CLAIM_BORDERAUX_STATE_CHOICES = (
    ('inactive', 'Inactive'),
    ('due', 'Due'),
    ('paid', 'Paid'),
)


class ClaimBorderauxManager(models.Manager):

    def inactive(self):
        return self.get_queryset().filter(state='inactive')

    def active(self):
        return self.get_queryset().filter(state__in=['due', 'paid'])

    def paid(self):
        return self.get_queryset().filter(state='paid')

    def due(self):
        return self.get_queryset().filter(state='due')


class ClaimBorderaux(models.Model):

    state = models.CharField(choices=CLAIM_BORDERAUX_STATE_CHOICES, default='inactive', max_length=20)

    transaction = models.ForeignKey('CRM.Transaction', on_delete=models.PROTECT)
    cover_holder = models.TextField(default='Stable')
    unique_market_reference = models.TextField(null=True, blank=True)
    binder_contract_inception = models.TextField(null=True, blank=True)
    reporting_period_start = models.DateField()
    reporting_period_end = models.DateField()
    business_class = models.TextField(default='Contingency')
    original_currency = models.TextField(default='GBP')
    insured_full_name = models.TextField()
    insured_country = models.TextField(default='UK')
    loss_location = models.TextField(default='UK')
    date_claim_first_advised = models.DateField()
    claim_status = models.TextField(default='Open')
    paid_month_indemnity = models.DecimalField(default=0.0, max_digits=24, decimal_places=8)
    paid_month_fees = models.DecimalField(blank=True, null=True, max_digits=24, decimal_places=8)
    paid_prev_indemnity = models.DecimalField(default=0.0, max_digits=24, decimal_places=8)
    paid_prev_fees = models.DecimalField(blank=True, null=True, max_digits=24, decimal_places=8)
    reserve_indemnity = models.DecimalField(blank=True, null=True, max_digits=24, decimal_places=8)
    reserve_fees = models.DecimalField(blank=True, null=True, max_digits=24, decimal_places=8)
    total_incurred_indemnity = models.DecimalField(blank=True, null=True, max_digits=24, decimal_places=8)
    total_incurred_fees = models.DecimalField(blank=True, null=True, max_digits=24, decimal_places=8)
    paid_month_tpa = models.DecimalField(null=True, blank=True, max_digits=24, decimal_places=8)
    paid_prev_tpa = models.DecimalField(null=True, blank=True, max_digits=24, decimal_places=8)
    reserve_tpa = models.DecimalField(null=True, blank=True, max_digits=24, decimal_places=8)
    date_claim_opened = models.DateField()
    date_coverage_confirmed = models.DateField(null=True, blank=True)
    date_claim_amount_agreed = models.DateField()
    date_claim_paid_final = models.DateField()
    date_fees_paid_final = models.DateField()
    date_reopened = models.DateField(null=True, blank=True)
    date_subrogation = models.DateField(null=True, blank=True)
    date_claim_denied = models.DateField(null=True, blank=True)
    denied_reason = models.TextField(null=True, blank=True)
    amount_claimed = models.DecimalField(default=0.0, null=True, blank=True, max_digits=24, decimal_places=8)
    date_claim_withdrawn = models.DateField(null=True, blank=True)

    objects = ClaimBorderauxManager()

    @property
    def claim_reference(self):
        return self.transaction_id

    @property
    def certificate_reference(self):
        return self.transaction_id
