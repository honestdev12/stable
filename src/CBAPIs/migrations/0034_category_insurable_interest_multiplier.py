# Generated by Django 2.0 on 2018-11-20 11:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CBAPIs', '0033_auto_20181113_1556'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='insurable_interest_multiplier',
            field=models.DecimalField(decimal_places=2, default=5.0, max_digits=10),
            preserve_default=False,
        ),
    ]
