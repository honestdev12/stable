# Generated by Django 2.0 on 2018-11-01 15:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CBAPIs', '0029_auto_20181002_1723'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatbot',
            name='partner_phone',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
