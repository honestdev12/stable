# Generated by Django 2.0 on 2019-01-28 09:31
from pprint import pprint

import django.db.models.deletion
from django.db import (
    migrations,
    models,
)


def link_categories_to_partner_categories(apps, schema_editor):
    PartnerCategory = apps.get_model('CBAPIs.PartnerCategory')
    Category = apps.get_model('CBAPIs.Category')
    Market = apps.get_model('CBAPIs.Market')
    Chatbot = apps.get_model('CBAPIs.ChatBot')
    for partner in PartnerCategory.objects.all():
        country = Chatbot.objects.get(unique_id=partner.chatbot_unique_id).country
        partner.category, _ = Category.objects.get_or_create(
            name=partner.name,
            country=country,
            defaults={
                'name': partner.name,
                'image': partner.image,
                'market': Market.objects.get(name=partner.market.name),
                'country': country,
                'currency': partner.currency,
                'percent': partner.percent,
                'unit': partner.unit,
                'index': partner.index,
                'display': partner.display,
                'single_month': partner.single_month,
                'multi_month': partner.multi_month,
                'min_contract_length': partner.min_contract_length,
                'max_contract_length': partner.max_contract_length,
            }
        )
        partner.save()
    pprint(list(PartnerCategory.objects.values()))


class Migration(migrations.Migration):

    dependencies = [
        ('CBAPIs', '0041_auto_20190124_1605'),
    ]

    operations = [
        migrations.AddField(
            model_name='partnercategory',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='CBAPIs.Category'),
        ),
        migrations.RunPython(link_categories_to_partner_categories),
        migrations.AlterField(
            model_name='partnercategory',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CBAPIs.Category'),
        ),
    ]
