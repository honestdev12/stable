import datetime
import json
from decimal import Decimal

import gocardless_pro
import stripe
from celery.worker.control import revoke
from django.conf import settings
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils import timezone
from django_twilio.client import twilio_client
from raven.contrib.django.models import client
from rest_auth.views import LoginView
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from administration.models import AuthUser
from aox_dr.exception import (
    StableAPIException,
    StableAuthException,
    StableFarmerRegisterException,
)
from CRM.models import (
    Farmer,
    Transaction as Purchase,
)
from CRM.serializer import FarmerSerializer
from CRM.tasks import (
    go_card_less_cancellation,
    set_invoice_as_void,
    set_policy_runs_out,
    set_quote_timeout,
)
from Partner.models import (
    Farmers,
)
from payment.tasks import (
    send_payment_cancellation_email,
    success_payment,
)
from pricing.models import (
    PaymentStatusLog,
    PricingResults,
)
from pricing.utils import (
    convert_string_to_date,
    custom_strftime,
    get_stop_date,
    str_to_date,
    str_to_str,
)
from sdk.utils import get_availability_percent
from .models import (
    ChatBot,
    Farm,
    FunFact,
    Index,
    PartnerCategory,
    PartnerMarket,
)
from .serializer import (
    CBFarmSerializer,
    CBFunFactSerializer,
)
from .tasks import (
    send_register_farmer_mail
)
from .utils import (
    cancel_this_wrapper,
    confirm_to_sell_wrapper,
    decrypt_password,
    get_category_info_view,
    sell_this_wrapper,
    send_forgot_password_farmer_mail,
    send_quote_mail,
)


class MarketsView(APIView):

    @staticmethod
    def get(request):
        chat_bot_id = request.GET.get('chatbot_ID') if 'chatbot_ID' in request.GET else None
        if not chat_bot_id:
            return Response({'error': 'ChatBot not defined'}, status=status.HTTP_200_OK)

        markets_arr = PartnerMarket.objects.filter(display=True, chatbot_id=chat_bot_id).order_by('id')
        markets_arr_new = []

        for item in markets_arr:
            markets_arr_new.append(
                {
                    "id": str(item.id),
                    "image": item.image,
                    "name": item.name,
                    "orderNumber": item.order_number
                }
            )
        return Response(markets_arr_new, status=status.HTTP_200_OK)


class CategoriesView(APIView):

    @staticmethod
    def get(request):
        chat_bot_id = request.GET.get('chatbot_ID') if 'chatbot_ID' in request.GET else None
        if not chat_bot_id:
            return Response({'error': 'ChatBot not defined'}, status=status.HTTP_200_OK)

        chatbot = ChatBot.objects.get(pk=chat_bot_id)
        country = chatbot.country

        market_id = request.GET.get('marketId') if 'marketId' in request.GET else None
        if not market_id:
            categories = PartnerCategory.objects.filter(chatbot=chatbot, display=True)
        else:
            market = PartnerMarket.objects.get(pk=market_id)
            categories = PartnerCategory.objects.filter(
                market=market,
                chatbot=chatbot,
                display=True
            )

        availability_list = get_availability_percent(country)

        """
        Probably some performance gains here to be made! If statement is ugly but not got time now. (Sorry)
        """
        category_arr_new = []

        for category in categories:
            index_item = Index.objects.get(id=category.index_id)
            available_percent = 0
            available_amount = 0
            for available in availability_list:
                if available['Commodity'] == 'GB_' + index_item.name.replace(" ", ""):
                    available_amount = available['available_amount']
                    available_percent = available['available_percent']

            new_index_item = {
                'id': index_item.id,
                'name': index_item.name,
                'definition': index_item.definition,
                'url': index_item.url,
                'percent': index_item.percent,
                'owner': index_item.owner,
                'note': index_item.note,
                'last_index_date': index_item.last_index_date,
                'last_index_price': index_item.last_index_price,
                'low_index_price': index_item.low_index_price,
                'high_index_price': index_item.high_index_price,
            }
            new_item = {
                "id": str(category.id),
                "image": category.image,
                "name": category.name,
                "currency": category.currency,
                "percent": category.percent,
                "unit": category.unit,
                "amount": category.amount,
                "available_amount": available_amount,
                "available_percent": available_percent,
                "spent": category.spent,
                "index": new_index_item,
                'single_month': category.single_month,
                'multi_month': category.multi_month,
                'min_contract_length': category.min_contract_length,
                'max_contract_length': category.max_contract_length,
                "insurable_interest_multiplier": category.category.insurable_interest_multiplier
            }

            category_arr_new.append(new_item)
        return Response(category_arr_new, status=status.HTTP_200_OK)


class DirectionView(APIView):

    @staticmethod
    def get(request, **kwargs):
        category_id = kwargs.get('id', 'Default Value if not there')
        category = PartnerCategory.objects.get(id=category_id)
        if category.available < 50000:
            out_of_money = True
        else:
            out_of_money = False
        res_arr = {
            'fall': True,
            'rise': False,
            'outOfMoney': out_of_money
        }
        return Response(res_arr, status=status.HTTP_200_OK)


class CategoryInfoView(APIView):

    @staticmethod
    def post(request):
        try:
            chat_bot_id = request.data.get('chatbot_ID', None)
            country = request.data.get('country', None)
            if not chat_bot_id and not country:
                return Response({'error': 'Neither ChatBot or country were defined'}, status=status.HTTP_200_OK)

            market_name = request.data.get('marketName')
            index_name = request.data.get('indexName')
            policy_start_date = request.data.get('policyStartDate')
            policy_end_date = request.data.get('policyEndDate')
            option_type = request.data.get('optionType')
            quantity = request.data.get('quantity')

            if not index_name or not policy_start_date or not policy_end_date or not option_type or not quantity:
                return Response({'error': 'Please provide all required params'}, status=status.HTTP_200_OK)

            context = get_category_info_view(
                index_name,
                option_type,
                market_name,
                policy_end_date,
                policy_start_date,
                quantity,
                request.user,
                country=country,
                chat_bot_id=chat_bot_id
            )

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class CategoryPolicyDateView(APIView):

    @staticmethod
    def post(request):
        try:
            chat_bot_id = request.data['chatbot_ID'] if 'chatbot_ID' in request.data else None
            if not chat_bot_id:
                return Response({'error': 'ChatBot not defined'}, status=status.HTTP_200_OK)

            chatbot = ChatBot.objects.get(pk=chat_bot_id)
            country = chatbot.country

            category_name = request.data['indexName'] if "indexName" in request.data else None
            if not category_name:
                return Response({'error': 'IndexName not defined'}, status=status.HTTP_200_OK)

            option_type = request.data['optionType'] if 'optionType' in request.data else None
            if not option_type:
                return Response({'error': 'OptionType not defined'}, status=status.HTTP_200_OK)

            pricing_results = PricingResults.objects.filter(
                prod_name=category_name,
                style=option_type,
                country=country,
            )
            policy_start_date = pricing_results[0].policy_start_date
            policy_end_date = pricing_results.values('policy_end_date').distinct()
            policy_end_date_list = [end_date['policy_end_date'] for end_date in policy_end_date]
            policy_end_date_list.sort(key=lambda end_date: str_to_date(end_date))
            policy_end_date_list = [policy_date for policy_date in policy_end_date_list]

            context = {
                'policy_start_date': policy_start_date,
                'policy_end_date': policy_end_date_list
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PrepareView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            chat_bot_id = request.data['chatbot_ID'] if 'chatbot_ID' in request.data else None
            if not chat_bot_id:
                return Response({'error': 'ChatBot not defined'}, status=status.HTTP_200_OK)

            chat_bot = ChatBot.objects.get(pk=chat_bot_id)

            country = chat_bot.country

            partner = chat_bot.sales_partner
            tax = partner.tax

            if partner.is_pause or not partner.is_active:
                return Response(
                    {
                        'error': 'Company does not exist: {} or '
                                 'Maybe not accepted by Admin yet.'.format(partner.name)
                    },
                    status=status.HTTP_200_OK
                )

            category_name = request.data['categoryName'] if "categoryName" in request.data else None
            if not category_name:
                return Response({'error': 'Category Name should provided.'}, status=status.HTTP_200_OK)
            else:
                partner_category = PartnerCategory.objects.get(name=category_name, chatbot_id=chat_bot_id)
                commission = partner_category.commission
                farmer_savings = partner_category.farmers_save

            protection_style = request.data['protectionStyle'] \
                if "protectionStyle" in request.data else None
            if not protection_style:
                return Response({'error': 'Protection Style should provided.'}, status=status.HTTP_200_OK)

            protection_duration = request.data['protectionDuration'] if "protectionDuration" in request.data else None
            if not protection_duration:
                return Response({'error': 'Protection Duration should provided.'}, status=status.HTTP_200_OK)

            stop_price = Decimal(request.data['stopPrice']) if "stopPrice" in request.data else None
            if not stop_price:
                return Response({'error': 'Stop Price should provided.'}, status=status.HTTP_200_OK)

            start_price = Decimal(request.data['startPrice']) if "startPrice" in request.data else None
            if not start_price:
                return Response({'error': 'Start Price should provided.'}, status=status.HTTP_200_OK)

            quantity = int(request.data['quantity']) if "quantity" in request.data else None
            if not quantity:
                return Response({'error': 'Quantity should provided.'}, status=status.HTTP_200_OK)

            reference_id = request.data['reference_id'] if "reference_id" in request.data else None

            start_date = request.data['startDate'] if "startDate" in request.data else None
            if not start_date:
                return Response({'error': 'startDate should provided.'}, status=status.HTTP_200_OK)

            closing_date = request.data['endDate'] if "endDate" in request.data else None
            if not closing_date:
                return Response({'error': 'endDate should provided.'}, status=status.HTTP_200_OK)

            closing_date = get_stop_date(closing_date)

            if category_name in ["Red Diesel", "AN Fertiliser"]:
                protection_direction = "RISE"
            else:
                protection_direction = "FALL"

            multiplier = Decimal('1.0')
            multiplier_defined = settings.CURRENCY_CONVERSION_RULE.get(
                country
            ).get(category_name).get('multiplier')

            if not multiplier_defined:
                multiplier_defined = multiplier
            else:
                multiplier_defined = Decimal(multiplier_defined)

            start_price = start_price * Decimal(multiplier_defined)
            stop_price = stop_price * Decimal(multiplier_defined)

            if protection_style == 'Asian' and protection_direction == 'FALL':
                style = 'Asian Put BearSpread'
            elif protection_style == 'Asian' and protection_direction == 'RISE':
                style = 'Asian Call BullSpread'
            elif protection_style == 'European' and protection_direction == 'FALL':
                style = 'European Put BearSpread'
            else:
                style = 'European Call BullSpread'

            price_obj = PricingResults.objects.filter(
                prod_name=category_name,
                country=country,
                style=protection_style,
                stop=stop_price,
                strike=start_price,
                policy_start_date=start_date,
                policy_end_date=datetime.datetime.strftime(closing_date, "%B %Y"),
            )

            if not price_obj.exists():
                return Response(
                    {
                        'error', 'Does not exist Prices for this case'
                    },
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )

            price_obj = price_obj.first()
            strike_ratio = price_obj.strike_ratio

            price_per_unit_raw = price_obj.price
            price_per_unit_discount = price_per_unit_raw * farmer_savings / 100
            price_per_unit = (price_per_unit_raw - price_per_unit_discount) * (1 + tax)

            total_cost = quantity * price_per_unit
            monthly_cost = total_cost / int(protection_duration)
            premium = quantity * (price_per_unit_raw - price_per_unit_discount)
            commission = premium * commission / 100
            farmer_saving = price_per_unit_discount * quantity
            potential_loss = abs(start_price - stop_price) * quantity

            quote_end_time = timezone.now() + timezone.timedelta(hours=72)

            purchase = Purchase(
                farmer=request.user,
                partner=partner,
                start_date=convert_string_to_date(start_date),
                stop_date=closing_date,
                market=partner_category.category.market,
                category=partner_category.category,
                quantity=quantity,
                metric=partner_category.unit,
                protection_duration=protection_duration,
                protection_style=protection_style,
                protection_direction=protection_direction,
                strike_ratio=strike_ratio,
                start_price=start_price,
                stop_price=stop_price,
                price_per_unit=price_per_unit,
                price_per_unit_raw=price_per_unit_raw,
                tax=tax,
                monthly_cost=monthly_cost,
                total_cost=total_cost,
                farmer_saving=farmer_saving,
                commission=commission,
                premium=premium,
                potential_loss=potential_loss,
                payment_style='All',
                country=country,
                reference_id=reference_id,
                quote_expiration_date=quote_end_time
            )

            purchase.save()

            # tagging recent sales partner to farmer
            user = request.user
            user.company_name = partner.name
            user.save()

            # tagging multiple sales partners to farmer
            farmer = Farmer.objects.get(user=request.user)
            if partner.id in farmer.tagged_partners:
                farmer.tagged_partners.remove(partner.id)
                farmer.tagged_partners.append(partner.id)
            else:
                farmer.tagged_partners.append(partner.id)

            send_quote_mail(
                first_name=request.user.first_name,
                farmer_email=request.user.email,
                quantity=quantity,
                unit=partner_category.unit,
                market=partner_category.market.name,
                category=partner_category.category,
                index_owner=partner_category.index.owner,
                direction=protection_direction,
                start_date=start_date,
                closing_date=closing_date,
                quote_date=purchase.quote_date,
                current_cost=total_cost / 100,
                monthly_cost=monthly_cost / 100,
                price_per_quantity=purchase.price_per_unit,
                start_price=start_price,
                stop_price=stop_price,
                premium=purchase.premium,
                tax=Decimal(purchase.tax) * Decimal(purchase.premium),
                potential_loss=purchase.potential_loss,
                protection_duration=purchase.protection_duration,
                transaction=purchase
            )

            res_obj = {
                "purchaseId": str(purchase.id),
                "amountToPay": purchase.total_cost / 100,
                "quantity": purchase.quantity,
                "quote": purchase.id,
                "closingDate": closing_date,
                "startPrice": purchase.start_price / multiplier_defined,
                "pricePerQuantity": purchase.price_per_unit / 100,
                "pricePerQuantityRaw": purchase.price_per_unit_raw / 100,
                "monthlyCost": purchase.monthly_cost / 100,
                "monthCount": purchase.protection_duration,
                "potentialLoss": purchase.potential_loss / 100,
                'premium': purchase.premium / 100,
                'tax': purchase.tax,
                'country': country,
                'reference_id': reference_id,
                'amount_saved': farmer_saving / 100
            }

            set_quote_timeout.apply_async(args=[purchase.id], eta=quote_end_time)

            return Response(res_obj, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class InvoicePaymentView(APIView):

    @staticmethod
    def post(request):
        try:
            policy_id = request.data['PolicyID'] if 'PolicyID' in request.data else None
            if not policy_id:
                return Response({'error': 'PolicyID should be provided.'}, status=status.HTTP_200_OK)

            email = request.data['email'] if 'email' in request.data else None
            if not email:
                return Response({'error': 'Email should be provided.'}, status=status.HTTP_200_OK)

            transaction = Purchase.objects.get(id=policy_id)
            if transaction.payment_style == 'Invoice':
                return Response({'error': 'Already tagged with invoice.'}, status=status.HTTP_200_OK)
            if transaction.payment_style == 'Void':
                return Response({'error': 'Invoice already runs out 48 hours.'}, status=status.HTTP_200_OK)

            transaction.payment_style = 'Invoice'
            transaction.invoice_end_date = timezone.now() + timezone.timedelta(hours=48)
            transaction.save()

            set_invoice_as_void.apply_async(args=[transaction.id], eta=transaction.invoice_end_date)

            return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class EmailUniqueView(APIView):

    @staticmethod
    def post(request):
        try:
            email = request.data.get('email')
            if not email:
                return Response({'error': 'Email is not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            user = AuthUser.objects.get(email=email)

            context = {'ID': user.id}

        except Exception as e:
            print(e)
            context = {
                'ID': None
            }
        return Response(context, status=status.HTTP_200_OK)


class ForgotView(APIView):

    @staticmethod
    def get(request):
        mobile_number = request.GET.get('mobile_number')
        try:
            user = AuthUser.objects.get(phone=mobile_number)
            password = user.password
            twilio_client.messages.create(
                to=mobile_number,
                from_="447400329427",
                body="Hello from Stable! Your Password is: " + password
            )
            return Response(True, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(False, status=status.HTTP_200_OK)


class FarmerLoginView(LoginView):

    def post(self, request, *args, **kwargs):
        self.request = request
        data = self.request.data.copy()
        data['password'] = decrypt_password(self.request.data['password']).decode("utf-8")
        self.serializer = self.get_serializer(data=data,
                                              context={'request': request})
        login_valid = self.serializer.is_valid(raise_exception=False)
        if not login_valid:
            raise StableAuthException()

        try:
            user = AuthUser.objects.get(email=data['email'])
        except AuthUser.objects.DoesNotExist:
            raise StableAuthException()

        is_chat_bot = True if request.data.get('chat_bot', 'NO') == 'YES' else False
        if is_chat_bot:
            chat_bot_id = request.data.get('chat_bot_id')
            if not chat_bot_id:
                return Response({"error": 'ChatBot ID not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            chat_bot = ChatBot.objects.get(pk=chat_bot_id)
            if chat_bot.membership_id:
                farmer = Farmer.objects.get(user=user)
                membership_id = farmer.membership_id

                chatbot_members = Farmers.objects.filter(chatbot=chat_bot, reference_id=membership_id)

                if not chatbot_members.exists():
                    return Response(
                        {
                            'error': '<div>We are afraid your details no longer match our membership records for'
                                     ' <b>{sales_partner}</b>. </br></br> <ul style="text-align: left"><li>Click '
                                     '<a href="/#/register/name">'
                                     'back</a> to re-enter your details.</li> <li>Contact {sales_partner} directly '
                                     'to confirm your details or enquire about membership.'
                                     '<small>(Their number is <b>{enquiry_number}</b>)</small></li> '
                                     '<li>Follow this <a href="https://stableprice.com">link</a> to '
                                     'access the Stable insurance through our other Partners.'
                                     '</li></ul>'.format(sales_partner=chat_bot.sales_partner.name,
                                                         enquiry_number=chat_bot.partner_phone)
                        },
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR
                    )

        self.login()
        return self.get_response()


class CurrentView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        farmer = FarmerSerializer(Farmer.objects.get(user=request.user))
        return Response(farmer.data, status=status.HTTP_200_OK)


class UsersFirstRegisterView(APIView):

    @staticmethod
    def post(request):
        try:
            first_name = request.data['firstName'] if "firstName" in request.data else ""
            last_name = request.data['lastName'] if "lastName" in request.data else ""
            if "email" in request.data:
                email = request.data['email']
            else:
                return Response({"error": "Email should provided."})

            if AuthUser.objects.filter(email=email).exists():
                return Response({"error": "An account with that email address already exists."})

            chat_bot_id = request.data['chat_bot_id'] if 'chat_bot_id' in request.data else None
            if not chat_bot_id:
                return Response({'error': 'ChatBot ID not defined'}, status=status.HTTP_200_OK)

            chat_bot = ChatBot.objects.get(pk=chat_bot_id)

            membership_id = request.data['membership_ID'] if 'membership_ID' in request.data else None
            if chat_bot.membership_id and not membership_id:
                return Response({'error': 'Membership ID not defined'}, status=status.HTTP_200_OK)

            phone_number = request.data['phoneNumber'] if 'phoneNumber' in request.data else None
            if not phone_number:
                return Response({'error': 'Phone Number has not been defined'}, status=status.HTTP_200_OK)

            if "county" in request.data:
                county = request.data['county']
            else:
                return Response({"error": "County should provided."})

            address_1 = request.data.get('address_1')
            if not address_1:
                return Response({"error": "Address should provided."})

            address_2 = request.data.get('address_2')

            town = request.data.get('town')

            post_code = request.data.get('postCode')
            if not post_code:
                return Response({'error': 'Post Code should provided.'})

            date_of_birth = request.data['date_of_birth'] if 'date_of_birth' in request.data else None
            if not date_of_birth:
                return Response({'error': 'A date of birth was not supplied'})
            date_of_birth = datetime.datetime.strptime(date_of_birth, "%d/%m/%Y").date()

            # New API Part

            if 'farmSize' in request.data:
                farm_size = request.data['farmSize']
            else:
                return Response({"error": "farmSize should provided."})

            zip_code = request.data['farm_postcode'] if "farm_postcode" in request.data else None
            if not zip_code:
                return Response({"error": "Postcode has not been defined"})

            farm_name = request.data['farmName'] if 'farmName' in request.data else None
            if not farm_name:
                return Response({"error": "Farm Name has not been defined"})

            dairy_herd_size = request.data['dairyHerdSize'] if 'dairyHerdSize' in request.data else 0

            cattle = request.data['cattle'] if 'cattle' in request.data else 0

            breeding_sows = request.data['breedingSows'] if 'breedingSows' in request.data else 0

            barley = request.data['barley'] if 'barley' in request.data else 0

            rapeseed = request.data['rapeseed'] if 'rapeseed' in request.data else 0

            other_crops = request.data['otherCrops'] if 'otherCrops' in request.data else 0

            other_livestock = request.data['otherLivestock'] if 'otherLivestock' in request.data else 0

            potatoes = request.data['potatoes'] if 'potatoes' in request.data else 0

            wheat = request.data['wheat'] if 'wheat' in request.data else 0

            oats = request.data['oats'] if 'oats' in request.data else 0

            sheep = request.data['sheep'] if 'sheep' in request.data else 0

            suckler_herd_size = request.data['sucklerHerdSize'] if 'sucklerHerdSize' in request.data else 0

            if "password" in request.data:
                password = request.data['password']
                password = decrypt_password(password)
            else:
                return Response({"error": "password should provided."})

            memberships = Farmers.objects.filter(chatbot=chat_bot, reference_id=membership_id)
            if chat_bot.membership_id and not memberships.exists():
                if chat_bot.partner_phone:
                    return Response(
                        {
                            'error': '<div>We are unable to validate your membership with'
                                     ' <b>{sales_partner}</b>. </br></br> <ul style="text-align: left"><li>Click '
                                     '<a href="/#/register/name">'
                                     'back</a> to re-enter your details.</li> <li>Contact {sales_partner} directly '
                                     'to confirm your details or enquire about membership.'
                                     '<small>(Their number is <b>{enquiry_number}</b>)</small></li> '
                                     '<li>Follow this <a href="https://stableprice.com">StablePrice.com</a> to '
                                     'access the Stable insurance through our other Partners.</li></ul> Finally '
                                     'you could purchase stable insurance by logging into your account on the '
                                     '<a href="{dashboard_url}">Stable platform</a>'
                                     '</div>'.format(sales_partner=chat_bot.sales_partner.name,
                                                     enquiry_number=chat_bot.partner_phone,
                                                     dashboard_url=settings.DASHBOARD_URL)
                        },
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR
                    )
                else:
                    return Response(
                        {
                            'error': '<div>We are unable to validate your membership with'
                                     ' <b>{sales_partner}</b>. </br></br> <ul style="text-align: left"><li>Click '
                                     '<a href="/#/register/name">'
                                     'back</a> to re-enter your details.</li> <li>Contact {sales_partner} directly '
                                     'to confirm your details or enquire about membership.</li> '
                                     '<li>Follow this <a href="https://stableprice.com">StablePrice.com</a> to '
                                     'access the Stable insurance through our other Partners.</li></ul> Finally '
                                     'you could purchase stable insurance by logging into your account on the '
                                     '<a href="{dashboard_url}">Stable platform</a>'
                                     '</div>'.format(sales_partner=chat_bot.sales_partner.name,
                                                     dashboard_url=settings.DASHBOARD_URL)
                        },
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR
                    )

            with transaction.atomic():

                new_user, created = AuthUser.objects.get_or_create(
                    email=email,
                    defaults={
                        'first_name': first_name,
                        'last_name': last_name,
                        'username': email,
                        'phone': phone_number,
                        'address_1': address_1,
                        'address_2': address_2,
                        'town': town,
                        'county': county,
                        'post_code': post_code,
                        'is_active': True,
                        'is_farmer': True,
                        'is_two_factor': False,
                        'company_name': chat_bot.sales_partner.name,
                        'date_of_birth': date_of_birth
                    }
                )
                if created:
                    new_user.set_password(password)
                    new_user.save()

                if not Token.objects.filter(user=new_user).exists():
                    Token.objects.create(user=new_user)

                new_farmer, created = Farmer.objects.get_or_create(
                    user=new_user,
                    defaults={
                        'membership_id': membership_id,
                        'tagged_partners': [],
                        'zip': zip_code,
                        'farm_size': farm_size,
                        'farm_name': farm_name,
                        'dairy_herd_size': dairy_herd_size,
                        'cattle': cattle,
                        'breeding_sows': breeding_sows,
                        'barley': barley,
                        'rapeseed': rapeseed,
                        'other_crops': other_crops,
                        'other_livestock': other_livestock,
                        'potatoes': potatoes,
                        'wheat': wheat,
                        'oats': oats,
                        'sheep': sheep,
                        'suckler_herd_size': suckler_herd_size
                    }
                )

                new_farmer.tagged_partners.append(chat_bot.sales_partner.id)
                membership = memberships.first()
                if membership is not None:
                    new_farmer.partners.add(membership)

                new_farmer.save()

            send_register_farmer_mail.delay(
                farmer_email=new_user.email,
                first_name=new_user.first_name
            )

            res_obj = {
                "ID": str(new_user.id),
                "firstName": new_user.first_name,
                "lastName": new_user.last_name,
                "email": new_user.email,
                "country": new_farmer.country,
                "phoneNumber": new_user.phone,
            }

            return Response(res_obj, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableFarmerRegisterException(e)


class StripeView(APIView):
    permission_classes = (IsAuthenticated,)

    def check_sdk(self, request, purchase: Purchase):
        """
        Ask the Risk Engine SDK to check the purchase and then confirm it
        """
        response = sell_this_wrapper(purchase.id, request.user.id)
        if response.get('if_confirmed', None) is not None:
            purchase.unique_trade_id = response.get('_unique_trade_id')
            if purchase.unique_trade_id is not None:
                confirm_to_sell_wrapper(purchase.unique_trade_id, purchase_id=purchase.pk)
                return True, "Success", purchase.unique_trade_id
            else:
                return False, "We didn't get a trade id from the SDK", None
        return False, response.get('comment', "Trade was denied by the SDK"), None

    def pay_with_stripe(self, request, purchase, token, chatbot_id, trade_id):
        stripe.api_key = settings.STRIPE_API_KEY
        try:
            stripe_res = stripe.Charge.create(
                amount=int(purchase.total_cost),
                currency="gbp",
                source=token,
                description=f"New payment from Chat Bot {str(chatbot_id)} for "
                f"user {str(request.user.id)} and policy {str(purchase.pk)}",
                idempotency_key=str(purchase.idempotency_key),
                receipt_email=request.user.email
            )
        except Exception as e:
            cancel_this_wrapper.delay(trade_id)
            raise IOError(e)
        if stripe_res.get('status', 'failed') == 'failed':
            cancel_this_wrapper.delay(trade_id)
            raise ValueError()

        purchase.payment_id = stripe_res.id
        purchase.unique_trade_id = trade_id
        return purchase

    def post(self, request):
        purchase_id = request.data['purchaseId'] if "purchaseId" in request.data else None
        if not purchase_id:
            return Response({'error': 'purchase ID is not defined'}, status=status.HTTP_200_OK)

        chat_bot_id = request.data['chatbot_ID'] if 'chatbot_ID' in request.data else None
        if not chat_bot_id:
            return Response({'error': 'ChatBot not defined'}, status=status.HTTP_200_OK)

        token = request.data['token'] if "token" in request.data else None
        if not token:
            return Response({'error': 'Token not defined'}, status=status.HTTP_200_OK)

        purchase = Purchase.objects.get(id=purchase_id)
        if purchase.purchased:
            return Response({'error': 'This policy already purchased'}, status=status.HTTP_200_OK)

        if request.user != purchase.farmer:
            return Response(
                {
                    'error': 'You are trying to buy a policy that does not belong to you'
                },
                status=status.HTTP_200_OK
            )

        # if((category.available - purchase.amount_investor_pay) <= 0):
        #     return Response("not enough money in category", status=status.HTTP_200_OK)
        purchase.purchased = True
        purchase.paid_at = datetime.datetime.now()
        purchase.payment_provider = 'stripe'

        """
        Query Simon's Systems to make sure purchase is okay!
        """
        try:
            trade_allowed, comment, trade_id = self.check_sdk(request, purchase)
        except Exception as e:
            client.captureException()
            return Response({
                "error": "Sorry, something went wrong with your payment, don't worry you have"
                         " not been charged for any transaction. "
                         "Please check your details and try again",
                "detail": str(e)
            }, status=status.HTTP_200_OK)

        if trade_allowed:
            try:
                purchase = self.pay_with_stripe(
                    request=request,
                    purchase=purchase,
                    token=token,
                    chatbot_id=chat_bot_id,
                    trade_id=trade_id
                )
            except ValueError:
                client.captureException()
                return Response({
                    "error": "Our attempt to take payment for this policy failed, please check you entered your "
                             "details correctly and maybe try again with a different card"
                }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            except IOError:
                client.captureException()
                return Response({
                    "error": "We experienced an issue with our payment provider while attempting to take payment. "
                             "As we were unable to confirm the status of payment, we were forced to automatically "
                             "cancel the purchase of the policy It is possible that you were still charged by our "
                             "provider and if this is the case please contact us on 0203 859 9390 or "
                             "via email {}.".format(settings.STABLE_SUPPORT_MAIL)
                }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            policy_end_date = purchase.stop_date + timezone.timedelta(days=31)
            task_response = set_policy_runs_out.apply_async(args=[purchase.id], eta=policy_end_date)

            if task_response.task_id:
                purchase.owed_status = 'Running'
                purchase.contract_running_task_id = task_response.task_id

            purchase.underwriters = {"underwriter": "Ascot", "amount": "100%"}
            purchase.unique_trade_id = trade_id

            purchase.save()

            success_payment(request.user.id, purchase.id)

            return Response("success", status=status.HTTP_200_OK)
        else:
            return Response({"error": comment}, status=status.HTTP_200_OK)


class GoCardLessPrepareView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        token = request.auth.key
        user = request.user
        chatbot_id = request.GET.get('chatbot_id', None)

        if not chatbot_id or chatbot_id == 'undefined':
            return Response({'error': 'Chatbot ID was not provided'})

        farmer = Farmer.objects.get(user=user)
        chatbot = get_object_or_404(ChatBot, pk=chatbot_id)

        redirectUrl = settings.GO_CARD_LESS_REDIRECT_URL.format(
            chatbot_id=chatbot.pk
        )
        go_client = gocardless_pro.Client(
            access_token=settings.GO_CARD_LESS_ACCESS_TOKEN,
            environment=settings.GO_CARD_LESS_ENVIRONMENT
        )
        redirect_flow = go_client.redirect_flows.create(
            params={
                "description": "Insurance for " + user.first_name + " " + user.last_name,
                "session_token": token,
                "success_redirect_url": redirectUrl,
                "prefilled_customer": {
                    "given_name": user.first_name,
                    "family_name": user.last_name,
                    "email": user.email,
                    "postal_code": farmer.zip if farmer.zip else ''
                }
            }
        )

        res_obj = {
            "redirectUrl": redirect_flow.redirect_url,
            "token": redirect_flow.id,
            "session_token": token
        }

        return Response(res_obj, status=status.HTTP_200_OK)


class GoCardLessMakeView(APIView):
    permission_classes = (IsAuthenticated,)

    def check_sdk(self, purchase: Purchase, user_id: int):
        """
        Ask the Risk Engine SDK to check the purchase and then confirm it
        """
        try:
            response = sell_this_wrapper(purchase.pk, user_id)
        except Exception:
            client.captureException()
            return False, "We were unable to confirm the policy creation at this time. " \
                          "Don't worry, you will not be charged. Please try again later.", None

        if response.get('if_confirmed', None) is not None:
            purchase.unique_trade_id = response.get('_unique_trade_id', None)

            if purchase.unique_trade_id is not None:
                confirm_to_sell_wrapper(purchase.unique_trade_id, purchase_id=purchase.pk)
                return True, "Success", purchase.unique_trade_id
            else:
                return False, "We didn't get a trade id from the SDK", None
        return False, response.get('comment', "Trade was denied by the SDK"), None

    def make_subscription(self, redirect_flow_id: str, jwt_token: str, purchase: Purchase):
        try:
            go_client = gocardless_pro.Client(
                access_token=settings.GO_CARD_LESS_ACCESS_TOKEN,
                environment=settings.GO_CARD_LESS_ENVIRONMENT
            )
            redirect_flow = go_client.redirect_flows.complete(
                redirect_flow_id,
                params={
                    "session_token": jwt_token,
                }
            )
        except Exception:
            client.captureException()
            return False, "We are unable to contact our payment provider to complete the subscription process. " \
                          "You will not be charged, please try again later."
        try:
            subscription = go_client.subscriptions.create(
                params={
                    "amount": int(purchase.monthly_cost),
                    "currency": "GBP",
                    "interval_unit": "monthly",
                    "day_of_month": "-1",
                    "count": int(purchase.protection_duration),
                    "links": {
                        "mandate": redirect_flow.links.mandate
                    },
                },
                headers={
                    'Idempotency-Key': str(purchase.idempotency_key)
                }
            )
        except Exception:
            client.captureException()
            return False, "We were unable to confirm your subscription with our payment provider, it is possible that " \
                          "you were still subscribed by our provider and if this is the case please contact us on " \
                          "0203 859 9390 or via email {}.".format(settings.STABLE_SUPPORT_MAIL)
        purchase.purchased = True
        purchase.paid_at = datetime.datetime.now()
        purchase.payment_style = 'Monthly'
        purchase.payment_id = subscription.id
        purchase.payment_provider = 'gocardless'
        purchase.underwriters = {"underwriter": "Ascot", "amount": "100%"}
        try:
            purchase.save()
        except Exception:
            client.captureException()
            return False, "Your subscription was created successfully, " \
                          "however we encountered an error when storing the details. Please contact us on " \
                          "0203 859 9390 or via email {}.".format(settings.STABLE_SUPPORT_MAIL)
        policy_end_date = purchase.stop_date + timezone.timedelta(days=31)

        task_response = set_policy_runs_out.apply_async(args=[purchase.id], eta=policy_end_date)
        if task_response.task_id:
            purchase.owed_status = 'Running'
            purchase.contract_running_task_id = task_response.task_id

        try:
            purchase.save()
        except Exception:
            client.captureException()
            return False, "Your subscription was created successfully, " \
                          "however we encountered an error when storing the details. Please contact us on " \
                          "0203 859 9390 or via email {}.".format(settings.STABLE_SUPPORT_MAIL)
        return True, "Success"

    def post(self, request):
        jwt_token = request.auth.key
        purchase_id = request.data['purchaseId'] if "purchaseId" in request.data else ""
        token = request.data['token'] if "token" in request.data else ""

        purchase = get_object_or_404(Purchase, id=purchase_id)
        if purchase.purchased:
            return Response({'error': 'This policy already purchased'}, status=status.HTTP_200_OK)

        if request.user != purchase.farmer:
            return Response(
                {
                    'error': 'You are trying to buy a policy that does not belong to you'
                },
                status=status.HTTP_200_OK
            )

        allowed, comment, trade_id = self.check_sdk(purchase, request.user.pk)
        if not allowed:
            return Response({'error': comment})

        purchase.unique_trade_id = trade_id

        success, failure_reason = self.make_subscription(token, jwt_token, purchase)
        if not success:
            return Response({'error': failure_reason})

        success_payment(request.user.id, purchase.id, monthly=True)
        return Response("success", status=status.HTTP_200_OK)


class GoCardLessRestoreView(APIView):

    @staticmethod
    def post():
        return Response("res_obj", status=status.HTTP_200_OK)


class GoCardLessWebHookView(APIView):

    @staticmethod
    def post(request):
        data = json.loads(request.body)
        resource_type = data.get('events')[0].get('resource_type')
        action = data.get('events')[0].get('action')

        if action == 'failed' and resource_type == 'payments':
            payment_id = data.get('events')[0].get('links').get('payment')
            go_client = gocardless_pro.Client(
                access_token=settings.GO_CARD_LESS_ACCESS_TOKEN,
                environment=settings.GO_CARD_LESS_ENVIRONMENT
            )
            try:
                go_client.payments.retry(payment_id)
            except Exception as e:
                print(e)
                try:
                    payment = go_client.payments.get(payment_id)
                    subscription_id = payment.links.subscription
                    if not subscription_id:
                        return Response(
                            {
                                'success': 'This payment is not assigned to subscription'
                            },
                            status=status.HTTP_200_OK
                        )
                    else:
                        if Purchase.objects.filter(payment_id=subscription_id, payment_style='Monthly').exists():
                            transaction = Purchase.objects.get(payment_id=subscription_id, payment_style='Monthly')
                            if transaction.cancel_status != 'Cancelled':
                                transaction.cancel_status = 'Cancelled'
                                transaction.cancelled = datetime.datetime.now()
                                transaction.owed_status = 'Terminated'
                                task_id = transaction.contract_running_task_id
                                if task_id:
                                    revoke(task_id, terminate=True)

                                cancel_this_wrapper(transaction.unique_trade_id)

                                transaction.save()

                                subscription = go_client.subscriptions.get(subscription_id)
                                months_over = ""
                                for up_payments in subscription.upcoming_payments:
                                    months_over += '{}, '.format(
                                        str_to_str(
                                            up_payments.get('charge_date')
                                        )
                                    )

                                send_payment_cancellation_email.delay(
                                    transaction_id=transaction.pk,
                                    first_name=transaction.farmer.first_name,
                                    email=transaction.farmer.email,
                                    quote=transaction.id,
                                    month_unpaid=str_to_str(subscription.upcoming_payments[0].get('charge_date')),
                                    months_over=months_over,
                                    date_3_days=custom_strftime(
                                        "{S} %B %Y",
                                        timezone.now() + timezone.timedelta(days=3)
                                    ),
                                    market=transaction.market.name,
                                    category=transaction.category.name,
                                    index=transaction.category.index.name,
                                    protection_direction=transaction.protection_direction,
                                    start_date=datetime.date.strftime(transaction.start_date, "%B %Y"),
                                    stop_date=datetime.date.strftime(transaction.stop_date, "%B %Y"),
                                    start_price=transaction.start_price,
                                    stop_price=transaction.stop_price,
                                    quantity=transaction.quantity,
                                    unit=transaction.metric,
                                    total_cost=transaction.total_cost,
                                    montly_cost=transaction.monthly_cost,
                                    months=transaction.protection_duration,
                                    premium=transaction.premium,
                                    tax=Decimal(transaction.tax) * Decimal(transaction.premium),
                                    potential_loss=transaction.potential_loss,
                                    protection_duration=transaction.protection_duration
                                )

                                # Cancellation for GoCardLess monthly subscription after 7 days
                                if transaction.payment_style == 'Monthly' and transaction.cancel_status != 'Cancelled' \
                                        and transaction.owed_status != 'Terminated':
                                    cancel_date = timezone.now() + timezone.timedelta(days=7)
                                    go_card_less_cancellation.apply_async(args=[transaction.id], eta=cancel_date)

                                payment_status_log = PaymentStatusLog(
                                    content='Cancelled by Payment Failed Web hook',
                                    date=datetime.datetime.now(),
                                    policy=int(transaction.id)
                                )
                                payment_status_log.save()
                        else:
                            return Response(
                                {
                                    'error': 'Contract with this subscription id - {} is not '
                                             'exist'.format(subscription_id)
                                },
                                status=status.HTTP_200_OK
                            )
                except Exception as e:
                    raise StableAPIException(e)

        return Response({'success': 'success'}, status=status.HTTP_200_OK)


class PaymentWebHookEventView(APIView):

    @staticmethod
    def post(request):
        try:
            payment_id = request.data['id'] if 'id' in request.data else None
            if not payment_id:
                return Response({'error': 'Payment ID not defined'}, status=status.HTTP_200_OK)

            event_type = request.data['type'] if 'type' in request.data else None
            if not event_type:
                return Response({'error': 'Event Type not defined'}, status=status.HTTP_200_OK)

            Purchase.objects.get(payment_id=payment_id)

        except Exception as e:
            raise StableAPIException(e)


class FarmView(APIView):

    @staticmethod
    def get(request):
        post_code_prefix = request.GET.get('postcodeprefix')
        json_farm = CBFarmSerializer(Farm.objects.filter(post_code=post_code_prefix), many=True)

        return Response(json_farm.data, status=status.HTTP_200_OK)


class ReCalculateByVoucher(APIView):

    @staticmethod
    def post(request):
        try:
            purchase_id = request.data['purchaseId'] if 'purchaseId' in request.data else None
            if not purchase_id:
                return Response('Purhcase ID not defined.', status=status.HTTP_200_OK)

            total_cost = Decimal(request.data['amountToPay']) if 'amountToPay' in request.data else None
            if not total_cost:
                return Response('amountToPay not defined.', status=status.HTTP_200_OK)

            discount = Decimal(request.data['discount']) if 'discount' in request.data else None
            if not discount:
                return Response('discount not defined.', status=status.HTTP_200_OK)

            discount_type = request.data['discountType'] if 'discountType' in request.data else None
            if not discount_type:
                return Response('discountType not defined.', status=status.HTTP_200_OK)
            if discount_type != 'PERCENT' and discount_type != 'AMOUNT':
                return Response('discountType is not correct.', status=status.HTTP_200_OK)
            else:
                if discount_type == 'PERCENT':
                    discount = str(discount) + '%'
                else:
                    discount = '£' + str(discount)

            premium = request.data['premium'] if 'premium' in request.data else None
            if not premium:
                return Response('premium is not defined.', status=status.HTTP_200_OK)

            purchase = Purchase.objects.get(id=purchase_id)
            monthly_cost = total_cost / purchase.protection_duration
            purchase.discount = discount
            purchase.monthly_cost = monthly_cost
            purchase.total_cost = total_cost
            purchase.premium = premium
            purchase.save()
            context = {
                'amountToPay': purchase.total_cost,
                'monthlyCost': purchase.monthly_cost
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class SendResetPasswordLink(APIView):

    authentication_classes = []

    @staticmethod
    def get(request):
        email = request.GET.get('email')
        chat_bot_id = request.GET.get('chat_bot_id')
        try:
            user = AuthUser.objects.get(email=email)
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

            payload = {
                'email': user.email
            }
            jwt_token = jwt_encode_handler(payload)
            send_forgot_password_farmer_mail(
                email,
                user.first_name,
                f'{settings.CHAT_BOT_URL}/p/{chat_bot_id}/resetpassword/{jwt_token}'
            )
        except Exception as e:
            print(e)
            return Response({
                'status': 'error',
                'message': 'Sorry we were unable to find this account.'
            }, status=status.HTTP_200_OK)
        return Response({
        'status': 'success',
        'message': 'We have sent an email to this account.'
        } , status=status.HTTP_200_OK)


class ValidateToken(APIView):

    @staticmethod
    def get(request):
        token = request.GET.get('token')
        try:
            jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
            payload = jwt_decode_handler(token)
            user = AuthUser.objects.get(email=payload['email'])
            if user is None:
                res = False
            else:
                res = True
        except Exception as e:
            print(e)
            return Response(False, status=status.HTTP_200_OK)
        return Response(res, status=status.HTTP_200_OK)


class ResetPassword(APIView):

    @staticmethod
    def post(request):
        token = request.data.get('token')
        jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
        payload = jwt_decode_handler(token.encode('utf-8'))
        try:
            user = AuthUser.objects.get(email=payload['email'])
        except AuthUser.DoesNotExist:
            return Response({
                'error': 'We could not find a user with this email address, maybe the account was deleted or possibly '
                         'it never existed in the first place. Either way, you need an account to use our service so '
                         'why not register?'
            })
        except AuthUser.MultipleObjectsReturned:
            return Response({
                'error': 'Wow, somehow you have ended up with more than one account with the same email address. '
                         'Please call us at 0203 859 9390 or email us at support@stableprice.com'
            })
        password = request.data.get('password')
        try:
            validate_password(password, user)
        except ValidationError as e:
            return Response({
                'error': str(e)
            })
        user.set_password(password)
        user.save()
        return Response({
            'success': 'You successfully changed your password, you can now login with your new details.'
        })


class PurchasesOrder(APIView):

    @staticmethod
    def post(request):
        # if "purchaseID" in request.data:
        #     purchaseID = request.data['purchaseID']
        # else:
        #     purchaseID = None
        #
        # if 'partnerCompany' in request.data:
        #     partnerCompany = request.data['partnerCompany']
        # else:
        #     partnerCompany = None
        #
        # if 'partnerID' in request.data:
        #     partnerID = request.data['partnerID']
        # else:
        #     partnerID = None
        #
        # if 'partnerFirstName' in request.data:
        #     partnerFirstName = request.data['partnerFirstName']
        # else:
        #     partnerFirstName = None
        #
        # if 'partnerLastName' in request.data:
        #     partnerLastName = request.data['partnerLastName']
        # else:
        #     partnerLastName = None
        #
        # if 'partnerEmail' in request.data:
        #     partnerEmail = request.data['partnerEmail']
        # else:
        #     partnerEmail = None
        #
        # if 'partnerPhone' in request.data:
        #     partnerPhone = request.data['partnerPhone']
        # else:
        #     partnerPhone = None
        #
        # if 'firstName' in request.data:
        #     firstName = request.data['firstName']
        # else:
        #     firstName = None
        #
        # if 'lastName' in request.data:
        #     lastName = request.data['lastName']
        # else:
        #     lastName = None
        #
        # if 'email' in request.data:
        #     email = request.data['email']
        # else:
        #     email = None
        #
        # if 'phone' in request.data:
        #     phone = request.data['phone']
        # else:
        #     phone = None
        #
        # if 'farmName' in request.data:
        #     farmName = request.data['farmName']
        # else:
        #     farmName = None
        #
        # if 'monthlyCost' in request.data:
        #     monthlyCost = request.data['monthlyCost']
        # else:
        #     monthlyCost = None
        #
        # if 'premium' in request.data:
        #     premium = request.data['premium']
        # else:
        #     premium = None
        #
        # if 'markets' in request.data:
        #     markets = request.data['markets']
        # else:
        #     markets = None
        #
        # if 'category' in request.data:
        #     category = request.data['category']
        # else:
        #     category = None
        #
        # if 'startPrice' in request.data:
        #     startPrice = request.data['startPrice']
        # else:
        #     startPrice = None
        #
        # if 'stopPrice' in request.data:
        #     stopPrice = request.data['stopPrice']
        # else:
        #     stopPrice = None
        #
        # if 'purchasePrice' in request.data:
        #     purchasePrice = request.data['purchasePrice']
        # else:
        #     purchasePrice = None
        #
        # if 'optionType' in request.data:
        #     optionType = request.data['optionType']
        # else:
        #     optionType = None
        #
        # if 'endDate' in request.data:
        #     endDate = request.data['endDate']
        # else:
        #     endDate = None
        #
        # if 'startDate' in request.data:
        #     startDate = request.data['startDate']
        # else:
        #     startDate = None

        return Response("Received", status=status.HTTP_200_OK)


class FunFactView(APIView):

    @staticmethod
    def get(request):
        chat_bot_id = request.GET.get('chatbot_ID') if 'chatbot_ID' in request.GET else None
        if not chat_bot_id:
            return Response({'error': 'ChatBot not defined'}, status=status.HTTP_200_OK)

        chat_bot = get_object_or_404(ChatBot, pk=chat_bot_id)

        json_fun_fact = CBFunFactSerializer(FunFact.objects.filter(country=chat_bot.country), many=True)

        return Response(json_fun_fact.data, status=status.HTTP_200_OK)


class PartnerInformationView(APIView):

    @staticmethod
    def get(request):
        chat_bot_id = request.GET.get('chatbot_ID') if 'chatbot_ID' in request.GET else None
        if not chat_bot_id:
            return Response({'error': 'Chatbot not defined'}, status=status.HTTP_200_OK)

        try:
            chat_bot = ChatBot.objects.get(pk=chat_bot_id)
        except:
            return Response({'error': 'No Chatbot exists with this ID'}, status=status.HTTP_200_OK)

        membership_id = chat_bot.membership_id
        country = chat_bot.country
        context = {
            'membership_id': membership_id,
            'country': country
        }

        return Response(context, status=status.HTTP_200_OK)


class PartnerStatusView(APIView):

    @staticmethod
    def get(request):
        chat_bot_id = request.GET.get('chatbot_ID') if 'chatbot_ID' in request.GET else None
        if not chat_bot_id:
            return Response({'error': 'No Chatbot ID defined'}, status=status.HTTP_200_OK)

        try:
            chat_bot = ChatBot.objects.get(pk=chat_bot_id)
        except:
            return Response({'error': 'No Chat bot exists with this ID'}, status=status.HTTP_200_OK)

        partner = chat_bot.sales_partner
        partner_paused = partner.is_pause

        try:
            support_number = partner.support_number
        except:
            support_number = "None"

        context = {
            'partner_status': partner_paused,
            'partner_name': partner.name,
            'support_number': support_number
        }

        return Response(context, status=status.HTTP_200_OK)


class FarmerSavingsView(APIView):

    @staticmethod
    def get(request):
        chat_bot_id = request.GET.get('chatbot_ID') if 'chatbot_ID' in request.GET else None
        if not chat_bot_id:
            return Response({'error': 'No Chatbot ID defined'}, status=status.HTTP_200_OK)

        commodity = request.GET.get('commodity') if 'commodity' in request.GET else None
        if not commodity:
            return Response({'error': 'No Commodity defined'}, status=status.HTTP_200_OK)

        try:
            partner_category = PartnerCategory.objects.filter(chatbot_id=chat_bot_id, name=commodity)
            commission = partner_category.values('commission')
            farmer_saving = partner_category.values('farmers_save')
        except Exception as e:
            print(e)
            return Response(
                {
                    'error': 'Something went wrong fetching commission and farmer savings values'
                },
                status=status.HTTP_200_OK
            )

        # category = PartnerCategory.objects.filter(unique_id=chat_bot_id, name=commodity)

        context = {
            'farmer_saving': farmer_saving,
            'commission': commission
        }

        return Response(context, status=status.HTTP_200_OK)


class CheckBrokerIDView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_farmer:
                return Response({'error': 'Farmer can request this data'}, status=status.HTTP_200_OK)

            chat_bot_id = request.data.get('chat_bot_id')
            broker_id = request.data.get('broker_id')

            if not chat_bot_id or not broker_id:
                return Response({'error': 'ChatBot or Broker ID not provided'}, status=status.HTTP_200_OK)

            chat_bot = ChatBot.objects.get(pk=chat_bot_id)
            partner = chat_bot.sales_partner
            broker = partner.company_admin

            if broker.broker_id != broker_id:
                return Response(
                    {
                        'error': 'Your Broker ID has not been recognised by this chat bot, please try again.'
                    },
                    status=status.HTTP_200_OK
                )
            else:
                return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)
