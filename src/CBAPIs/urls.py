from django.conf.urls import url
from CBAPIs import views as cb_api_views

urlpatterns = [
    url(r'^markets/', cb_api_views.MarketsView.as_view()),

    url(r'^categories/(?P<id>\d+)/directions/', cb_api_views.DirectionView.as_view()),
    url(r'^categories/categoryInfo', cb_api_views.CategoryInfoView.as_view()),
    url(r'^categories/policyDate', cb_api_views.CategoryPolicyDateView.as_view()),
    url(r'^categories', cb_api_views.CategoriesView.as_view()),

    url(r'^purchases/preparePaymentGoCardless', cb_api_views.GoCardLessPrepareView.as_view()),
    url(r'^purchases/makePaymentGoCardless', cb_api_views.GoCardLessMakeView.as_view()),
    url(r'^purchases/makeRestorePaymentGoCardless', cb_api_views.GoCardLessRestoreView.as_view()),
    url(r'^purchases/prepare', cb_api_views.PrepareView.as_view()),
    url(r'^purchases/makePaymentStripe', cb_api_views.StripeView.as_view()),
    url(r'^purchases/invoice', cb_api_views.InvoicePaymentView.as_view()),
    url(r'^purchases/events', cb_api_views.PaymentWebHookEventView.as_view()),

    url(r'^users/registerOne', cb_api_views.UsersFirstRegisterView.as_view()),
    url(r'^users/login/', cb_api_views.FarmerLoginView.as_view()),
    url(r'^users/current', cb_api_views.CurrentView.as_view()),
    url(r'^users/isEmailUnique', cb_api_views.EmailUniqueView.as_view()),
    url(r'^users/forgot', cb_api_views.ForgotView.as_view()),

    url(r'^farms', cb_api_views.FarmView.as_view()),

    url(r'^recalculatebyvoucher', cb_api_views.ReCalculateByVoucher.as_view()),

    url(r'^requestResetPassword', cb_api_views.SendResetPasswordLink.as_view()),
    url(r'^validateToken', cb_api_views.ValidateToken.as_view()),
    url(r'^resetPassword', cb_api_views.ResetPassword.as_view()),

    url(r'^purchases/order', cb_api_views.PurchasesOrder.as_view()),

    url(r'^funfacts', cb_api_views.FunFactView.as_view()),
    url(r'^partnerinfo', cb_api_views.PartnerInformationView.as_view()),
    url(r'^partnerstatus', cb_api_views.PartnerStatusView.as_view()),
    url(r'^partnerdiscount', cb_api_views.FarmerSavingsView.as_view()),

    url(r'^check_broker_id/', cb_api_views.CheckBrokerIDView.as_view()),

    url(r'^gocardless_webhook/', cb_api_views.GoCardLessWebHookView.as_view())
]
