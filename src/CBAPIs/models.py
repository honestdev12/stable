import datetime
from uuid import uuid4

from django.db import models

from Partner.models import Partner


def company_logo_directory_path(instance, filename):
    # file will be uploaded to a hidden directory at
    return 'company_logo/' + str(instance.name) + '/' + filename


def chat_bot_name():
    return 'ChatBot - {}'.format(datetime.datetime.now())


class Market(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=True, verbose_name='ID')
    name = models.CharField(max_length=255)
    image = models.CharField(max_length=255)
    order_number = models.IntegerField()
    display = models.BooleanField(default=False)
    country = models.CharField(max_length=255, default='UK')

    def __str__(self):
        return f"{self.name} ({self.country})"


class Category(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=True, verbose_name='ID')
    name = models.CharField(max_length=255)
    image = models.CharField(max_length=255)
    market = models.ForeignKey('CBAPIs.Market', on_delete=models.CASCADE)
    currency = models.CharField(max_length=255)
    percent = models.BooleanField(default=False)
    unit = models.CharField(max_length=255)
    index = models.ForeignKey('CBAPIs.Index', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=19, decimal_places=10, null=True)
    available = models.DecimalField(max_digits=19, decimal_places=10, null=True)
    spent = models.DecimalField(max_digits=19, decimal_places=10, null=True)
    display = models.BooleanField(default=False)
    single_month = models.BooleanField(default=False)
    multi_month = models.BooleanField(default=False)
    min_contract_length = models.IntegerField(default=3)
    max_contract_length = models.IntegerField(default=12)
    country = models.CharField(max_length=255, default='UK')
    insurable_interest_multiplier = models.DecimalField(max_digits=10, decimal_places=2, null=False)

    def __str__(self):
        return f"{self.name} ({self.country})"


class PartnerMarket(models.Model):
    market = models.ForeignKey('CBAPIs.Market', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    image = models.CharField(max_length=255)
    order_number = models.IntegerField()
    display = models.BooleanField(default=False)
    chatbot = models.ForeignKey('CBAPIs.ChatBot', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"{self.name}"


class PartnerCategory(models.Model):
    name = models.CharField(max_length=255)
    image = models.CharField(max_length=255)
    market = models.ForeignKey('CBAPIs.PartnerMarket', on_delete=models.CASCADE)
    category = models.ForeignKey('CBAPIs.Category', on_delete=models.CASCADE)
    currency = models.CharField(max_length=255)
    percent = models.BooleanField(default=False)
    unit = models.CharField(max_length=255)
    index = models.ForeignKey('CBAPIs.Index', on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=19, decimal_places=10, null=True)
    available = models.DecimalField(max_digits=19, decimal_places=10, null=True)
    spent = models.DecimalField(max_digits=19, decimal_places=10, null=True)
    display = models.BooleanField(default=False)
    single_month = models.BooleanField(default=False)
    multi_month = models.BooleanField(default=False)
    min_contract_length = models.IntegerField(default=3)
    max_contract_length = models.IntegerField(default=12)
    chatbot = models.ForeignKey('CBAPIs.ChatBot', on_delete=models.CASCADE, null=True)
    commission = models.DecimalField(default=0.0, max_digits=24, decimal_places=8)
    farmers_save = models.DecimalField(default=0.0, max_digits=24, decimal_places=8)

    def __str__(self):
        return f"{self.name}"


class Index(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=True, verbose_name='ID')
    name = models.CharField(max_length=255)    
    definition = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    percent = models.BooleanField(default=False)
    owner = models.CharField(max_length=255)
    note = models.CharField(max_length=255)
    last_index_date = models.DateField()
    last_index_price = models.DecimalField(max_digits=19, decimal_places=10)
    low_index_price = models.DecimalField(max_digits=19, decimal_places=10, null=True)
    high_index_price = models.DecimalField(max_digits=19, decimal_places=10, null=True)

    def __str__(self):
        return self.name


class reg_log(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    firstName = models.CharField(max_length=40, default="None")
    lastName = models.CharField(max_length=40, default="None")
    email = models.CharField(max_length=80, default="None")
    country = models.CharField(max_length=20, default="None")
    phoneNumber = models.CharField(max_length=20, default="None")
    birthday = models.CharField(max_length=20, default="None")
    registeredAt = models.CharField(max_length=20, default="None")
    zip = models.CharField(max_length=20, default="None")
    farmSize = models.CharField(max_length=20, default="None")
    userID = models.CharField(max_length=40, default='id-3399786')

    def __str__(self):
        return self.lastName + ' ' + self.firstName


class purchase_log(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    marketName = models.CharField(max_length=40, default="None")
    indexName = models.CharField(max_length=40, default="None")
    protectionDuration = models.IntegerField(default=0)
    quantity = models.IntegerField(default=0)
    companyName = models.CharField(max_length=40, default="None")
    strikePrice = models.DecimalField(default=0, max_digits=24, decimal_places=8)
    stopPrice = models.DecimalField(default=0, max_digits=24, decimal_places=8)
    purchasePrice = models.DecimalField(default=0, max_digits=24, decimal_places=8)
    quoteDate = models.DateTimeField()
    paymentDate = models.DateTimeField()
    close_at = models.DateTimeField()
    purchaseCounter = models.IntegerField(default=0)
    userID = models.CharField(max_length=40, default="None")
    paymentRef = models.CharField(max_length=80, default="None")
    totalPaymentAmount = models.DecimalField(default=0, max_digits=24, decimal_places=8)
    underwriter = models.CharField(max_length=100, default="None")

    def __str__(self):
        return self.indexName + ' ' + self.marketName


class Farm(models.Model):
    farm_name = models.CharField(default='None', max_length=255)
    post_code = models.CharField(default='None', max_length=255)
    farm_size = models.CharField(default='None', max_length=255)


class FunFact(models.Model):
    slide_name = models.CharField(default='None', max_length=255)
    funfact_text = models.TextField(default='None')
    display = models.BooleanField(default=True)
    country = models.CharField(max_length=255, default='UK')


class ChatBot(models.Model):
    uuid = models.UUIDField(default=uuid4, db_index=True, primary_key=True)
    name = models.CharField(max_length=255, default=chat_bot_name)
    sales_partner = models.ForeignKey(Partner, on_delete=models.CASCADE)
    partner_phone = models.CharField(max_length=255, null=True, blank=True)
    membership_id = models.BooleanField(default=True)
    country = models.CharField(max_length=255, default='UK')
    default_commission = models.DecimalField(default=0, null=True, blank=True, max_digits=24, decimal_places=8)
    default_farmer_savings = models.DecimalField(default=0, null=True, blank=True, max_digits=24, decimal_places=8)
