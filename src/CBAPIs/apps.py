from django.apps import AppConfig


class CbapisConfig(AppConfig):
    name = 'CBAPIs'
