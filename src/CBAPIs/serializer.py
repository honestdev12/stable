from rest_framework import serializers

from CRM.models import Farmer
from .models import (
    Category,
    ChatBot,
    Farm,
    FunFact,
    Market,
    PartnerCategory,
    PartnerMarket,
)


class CategorySerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = Category
        fields = '__all__'


class MarketSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = Market
        fields = '__all__'


class PartnerCategorySerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = PartnerCategory
        fields = '__all__'


class PartnerMarketSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = PartnerMarket
        fields = '__all__'


class CBFarmSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = Farm
        fields = ('key', 'id', 'farm_name', 'farm_size', 'post_code')


class CBFarmProdSerializer(serializers.ModelSerializer):

    dairy = serializers.CharField(source='dairy_herd_size')
    sucklers = serializers.CharField(source='suckler_herd_size')
    breeding_ewes = serializers.CharField(source='sheep', label='Breeding Ewes')
    breeding_sows = serializers.CharField(label='Breeding Sows')

    class Meta:
        model = Farmer
        fields = ('dairy', 'cattle', 'breeding_sows', 'barley', 'rapeseed', 'wheat', 'breeding_ewes', 'sucklers')


class CBFunFactSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = FunFact
        fields = '__all__'


class ChatBotSerializer(serializers.ModelSerializer):
    key = serializers.UUIDField(source='uuid')

    class Meta:
        model = ChatBot
        fields = '__all__'
