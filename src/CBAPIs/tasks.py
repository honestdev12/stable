from django.conf import settings

from aox_dr.celery import app
from aox_dr.utils import send_provider_agnostic_email


@app.task
def send_register_farmer_mail(farmer_email, first_name):
    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'FIRST_NAME': first_name,
            'EMAIL': farmer_email,
        }
    else:
        template_id = 'register-farmer-template'
        merge_vars = {
            'FIRST_NAME': first_name,
            'EMAIL': farmer_email,
        }

    send_provider_agnostic_email(
        "Welcome to Stable",
        to_email=farmer_email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=[]
    )
