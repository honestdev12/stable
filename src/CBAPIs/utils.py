import datetime
import io
from base64 import b64decode
from decimal import Decimal
from uuid import UUID

import pdfkit
import pendulum
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.contrib.humanize.templatetags.humanize import intcomma
from django.contrib.sites.shortcuts import get_current_site
from django.core.files.storage import default_storage
from django.db.models import (
    Avg,
    Max,
    Min,
    Sum,
)
from django.db.models.functions import Coalesce
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from rest_framework import status
from rest_framework.response import Response
from retrying import retry

from administration.models import AuthUser
from aox_dr.celery import app
from aox_dr.exception import StableAPIException
from aox_dr.utils import send_provider_agnostic_email
from CRM.models import (
    Farmer,
    Transaction,
    Transaction as Purchase,
)
from Partner.models import Partner
from payment.utils import render_policy_schedule
from pricing.models import (
    IndexPriceData,
    PricingResults,
)
from pricing.utils import (
    convert_string_to_date,
    custom_strftime,
    decimal_format,
)
# backend sdk import from simon's team
from sdk.back_end_sdk import (
    AlgoSDKErrors,
    cancel_this,
    confirm_to_sell,
    how_much_left,
    name_this_risk_pool,
    sell_this,
    settle_this,
)
from .models import (
    Category,
    ChatBot,
    Index,
    PartnerCategory,
)


def send_forgot_password_farmer_mail(
        farmer_email: str,
        first_name: str,
        link: str
):
    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'first_name': first_name,
            'link': link,
        }
    else:
        template_id = 'reset-password-template'
        merge_vars = {
            'FIRST_NAME': first_name,
            'LINK': link,
        }

    send_provider_agnostic_email(
        "Reset your Stable password",
        to_email=farmer_email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=[]
    )


def send_quote_mail(
        first_name: str,
        farmer_email: str,
        quantity: int,
        unit: str,
        market: str,
        category: Category,
        index_owner: str,
        start_date: datetime.date,
        closing_date: datetime.date,
        quote_date: datetime.date,
        direction: str,
        current_cost: Decimal,
        monthly_cost: Decimal,
        price_per_quantity: Decimal,
        start_price: Decimal,
        stop_price: Decimal,
        premium: Decimal,
        tax: Decimal,
        potential_loss: Decimal,
        protection_duration: str,
        transaction: Transaction,
):
    farmer = AuthUser.objects.get(email=farmer_email)
    farm = Farmer.objects.get(user=farmer) if farmer.is_farmer else None
    index_price = IndexPriceData.objects.filter(prod_name=category.name).first().price

    if market != 'Arable':
        current_start_price = start_price
        stop_price = stop_price
        price_per_quantity = price_per_quantity
        pound = False
        penny = True
    else:
        current_start_price = start_price / 100
        stop_price = stop_price / 100
        price_per_quantity = price_per_quantity / 100
        pound = True
        penny = False

    # Convert specific prices to Pounds
    potential_loss = decimal_format(Decimal(potential_loss) / 100)
    premium = decimal_format(Decimal(premium) / 100)
    tax = decimal_format(Decimal(tax) / 100)

    attachment_pdf_html = render_policy_schedule(farmer, transaction)

    schedule_content = pdfkit.from_string(attachment_pdf_html, False)
    default_storage.save(f'email_attachment/Policy Schedule.pdf', io.BytesIO(schedule_content))
    # default_storage.save(f'email_attachment/Policy Schedule.html', io.StringIO(attachment_pdf_html))

    # attach stable policy insurance pdf
    policy_content = default_storage.open('email_attachment/Stable_Insurance_Policy.pdf').read()

    # attach IPID pdf
    ipid_content = default_storage.open('email_attachment/IPID.pdf').read()

    attachments = [
        # ('Policy Schedule.pdf', schedule_content, 'application/pdf'),
        ('Stable Insurance Policy.pdf', policy_content, 'application/pdf'),
        ('IPID.pdf', ipid_content, 'application/pdf')
    ]

    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'first_name': first_name.title(),
            'pound': pound,
            'penny': penny,
            'market': market,
            'category': category.name,
            'index': index_owner,
            'direction': direction.title(),
            'closing_date': datetime.date.strftime(closing_date, "%B %Y"),
            'quote_date': custom_strftime("{S} %B %Y", quote_date),
            'current_start_price': decimal_format(current_start_price, comma_format=True),
            'stop_price': decimal_format(stop_price, comma_format=True),
            'quantity': intcomma(quantity),
            'unit': unit.title(),
            'singular_unit': unit[:-1].title(),
            'current_cost': decimal_format(current_cost, comma_format=True),
            'monthly_pay': decimal_format(monthly_cost, comma_format=True),
            'price_per_quantity': decimal_format(price_per_quantity, comma_format=True)
        }
    else:
        template_id = 'Quote'
        merge_vars = {
            'FIRST_NAME': first_name.title(),
            'POUND': pound,
            'PENNY': penny,
            'MARKET': market,
            'CATEGORY': category.name,
            'INDEX': index_owner,
            'DIRECTION': direction.title(),
            'CLOSING_DATE': datetime.date.strftime(closing_date, "%B %Y"),
            'QUOTE_DATE': custom_strftime("{S} %B %Y", quote_date),
            'CURRENT_START_PRICE': decimal_format(current_start_price, comma_format=True),
            'STOP_PRICE': decimal_format(stop_price, comma_format=True),
            'QUANTITY': intcomma(quantity),
            'UNIT': unit.title(),
            'SINGULAR_UNIT': unit[:-1].title(),
            'CURRENT_COST': decimal_format(current_cost, comma_format=True),
            'MONTHLY_PAY': decimal_format(monthly_cost, comma_format=True),
            'PRICE_PER_QUANTITY': decimal_format(price_per_quantity, comma_format=True)
        }

    send_provider_agnostic_email(
        "Your Stable Volatility Insurance Quote",
        to_email=farmer_email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=attachments
    )


def send_broker_quote_mail(
        user: AuthUser,
        purchase_id: int,
        quantity: int,
        unit: str,
        market: str,
        category: Category,
        index_owner: str,
        closing_date: datetime.date,
        direction: str,
        current_cost: Decimal,
        price_per_quantity: Decimal,
        start_price: Decimal,
        stop_price: Decimal
):
    if market != 'Arable':
        current_start_price = decimal_format(start_price)
        stop_price = decimal_format(stop_price)
        pound = False
        penny = True
    else:
        current_start_price = decimal_format(start_price / 100)
        stop_price = decimal_format(stop_price / 100)
        pound = True
        penny = False

    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'first_name': user.first_name.title(),
            'pound': pound,
            'penny': penny,
            'link': 'http://{}/login?broker={}&token={}&policy={}'.format(  # todo: Bad url for login
                settings.DASHBOARD_URL,
                urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                default_token_generator.make_token(user),
                urlsafe_base64_encode(force_bytes(purchase_id)).decode()
            ),
            'market': market,
            'category': category.name,
            'index': index_owner,
            'direction': direction.title(),
            'closing_date': closing_date,
            'current_start_price': decimal_format(current_start_price, comma_format=True),
            'stop_price': decimal_format(stop_price, comma_format=True),
            'quantity': decimal_format(quantity, comma_format=True),
            'unit': unit.title(),
            'singular_unit': unit[:-1].title(),
            'current_cost': decimal_format(current_cost, comma_format=True),
            'price_per_quantity': decimal_format(price_per_quantity, comma_format=True)
        }
    else:
        template_id = 'Broker Farmer Quote'
        merge_vars = {
            'FIRST_NAME': user.first_name.title(),
            'POUND': pound,
            'PENNY': penny,
            'LINK': 'http://{}/login?broker={}&token={}&policy={}'.format(  # todo: Bad url for login
                settings.DASHBOARD_URL,
                urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                default_token_generator.make_token(user),
                urlsafe_base64_encode(force_bytes(purchase_id)).decode()
            ),
            'MARKET': market,
            'CATEGORY': category.name,
            'INDEX': index_owner,
            'DIRECTION': direction.title(),
            'CLOSING_DATE': closing_date,
            'CURRENT_START_PRICE': decimal_format(current_start_price, comma_format=True),
            'STOP_PRICE': decimal_format(stop_price, comma_format=True),
            'QUANTITY': decimal_format(quantity, comma_format=True),
            'UNIT': unit.title(),
            'SINGULAR_UNIT': unit[:-1].title(),
            'CURRENT_COST': decimal_format(current_cost, comma_format=True),
            'PRICE_PER_QUANTITY': decimal_format(price_per_quantity, comma_format=True)
        }

    send_provider_agnostic_email(
        subject="Your Stable Quote",
        to_email=user.email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=[]
    )


def send_broker_policy_mail(
        request,
        first_name: str,
        email: str,
        password: str
):
    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'link': 'http://{}/login'.format(
                get_current_site(request).domain.replace(':8000', ':4200')
            ),
            'first_name': first_name,
            'username': email,
            'password': password
        }
    else:
        template_id = 'Broker Farmer Policy'
        merge_vars = {
            'LINK': 'http://{}/login'.format(
                get_current_site(request).domain.replace(':8000', ':4200')
            ),
            'FIRST_NAME': first_name,
            'USERNAME': email,
            'PASSWORD': password
        }

    send_provider_agnostic_email(
        subject="Your Stable Insurance Policy",
        to_email=email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=[]
    )


def get_next_month(dt: datetime.datetime = None):
    if dt is None:
        date_time = pendulum.today()
    else:
        date_time = pendulum.instance(dt)
    return date_time.add(months=1)


def decrypt_password(
        string: str
):
    text_file = default_storage.open('decryption/private_key.pem', "r")
    key_string = text_file.read()
    text_file.close()

    key = RSA.importKey(key_string)
    cipher = PKCS1_OAEP.new(key, hashAlgo=SHA256)
    decrypted_message = cipher.decrypt(b64decode(string))

    return decrypted_message


def cal_max_availability(
        user: AuthUser,
        index_name: str,
        country: str,
        policy_end_date: datetime.date,
        policy_start_date: datetime.date,
        option_type: str,
        quantity: int,
):
    category = Category.objects.get(
        name=index_name,
        country=country
    )
    insurable_interest_multiplier = category.insurable_interest_multiplier
    produce = 0
    min = get_min(index_name)

    if index_name == "Feed Wheat" or index_name == "Milling Wheat":
        produce = Farmer.objects.get(user=user).wheat
    elif index_name == "Feed Barley":
        produce = Farmer.objects.get(user=user).barley
    elif index_name == "Rapeseed":
        produce = Farmer.objects.get(user=user).rapeseed
    elif index_name == "Milk":
        produce = Farmer.objects.get(user=user).dairy_herd_size
    elif index_name == "Beef":
        produce = Farmer.objects.get(user=user).cattle + \
                  Farmer.objects.get(user=user).suckler_herd_size
    elif index_name == "Lamb":
        sheep = Farmer.objects.get(user=user).sheep
        produce = sheep * Decimal('1.47')
    elif index_name == "Pork":
        produce = Farmer.objects.get(user=user).breeding_sows

    # Input Costs

    elif index_name == "Red Diesel":

        farmer = Farmer.objects.get(user=user)
        arable = farmer.wheat + farmer.barley + farmer.rapeseed
        arable_multiplied = arable * Decimal('122.2')

        if farmer.breeding_sows > 0:
            pork = farmer.farm_size - arable
            pork_multiplied = pork * Decimal('173.1')
            produce = arable_multiplied + pork_multiplied

        elif farmer.dairy_herd_size > 0:
            dairy = farmer.farm_size - arable
            dairy_multiplied = dairy * Decimal('151.2')
            produce = arable_multiplied + dairy_multiplied

        elif farmer.farm_size > 0:
            rest = farmer.farm_size - arable
            rest_multiplied = rest * Decimal('56.6')
            produce = arable_multiplied + rest_multiplied

        else:
            produce = arable_multiplied

    elif index_name == "AN Fertiliser":
        arable = Farmer.objects.get(user=user).wheat + \
                 Farmer.objects.get(user=user).barley + \
                 Farmer.objects.get(user=user).rapeseed
        arable_multiplied = arable * Decimal('195')

        if Farmer.objects.get(user=user).dairy_herd_size > 0:
            dairy = Farmer.objects.get(user=user).farm_size - arable
            dairy_multiplied = dairy * Decimal('154')
            produce = arable_multiplied + dairy_multiplied
        elif Farmer.objects.get(user=user).breeding_sows > 0:
            pork = Farmer.objects.get(user=user).farm_size - arable
            pork_multiplied = pork * Decimal('120')
            produce = arable_multiplied + pork_multiplied
        elif Farmer.objects.get(user=user).farm_size > 0:
            rest = Farmer.objects.get(user=user).farm_size - arable
            rest_multiplied = rest * Decimal('57.5')
            produce = arable_multiplied + rest_multiplied
        else:
            produce = arable_multiplied

    # Farmer Stated the do not create the commodity they wish to insure - amount is therefore 0
    if not produce:
        return {'error': 'SCENARIO_1', 'max_availability': produce}
    else:
        max_availability = Decimal(insurable_interest_multiplier) * produce

        if max_availability < Decimal(quantity):
            if max_availability < min:
                # Farmer is trying to insure more than they have said they farm (Single Policy) - but have given a value
                # that is greater than the minimum base requirement we set
                return {'error': 'SCENARIO_3', 'max_availability': max_availability}
            else:
                # Farmer is trying to insure more than they have said they farm (Single Policy) -
                # but have given a value that is greater than the minimum base requirement we set
                return {'error': 'SCENARIO_2', 'max_availability': max_availability}

    current_contracts_asian = Purchase.objects.filter(
        farmer=user,
        owed_status="Running",
        category=category,
        protection_style="Asian"
    )

    if option_type == "Asian":
        # Check running Asian contracts that will compete with Asian options
        current_contracts_asian = current_contracts_asian.exclude(
            stop_date=convert_string_to_date(policy_start_date)
        ).aggregate(
            amount=Coalesce(
                Sum('quantity'),
                0
            )
        )

        max_availability = max_availability - current_contracts_asian['amount']

    elif option_type == "European":
        # Check running Asian contracts that will compete with European options
        current_contracts_asian = current_contracts_asian.filter(
            stop_date__gte=convert_string_to_date(policy_end_date)
        ).aggregate(
            amount=Coalesce(
                Sum('quantity'),
                0
            )
        )

        max_availability = max_availability - current_contracts_asian['amount']

    # Check running European contracts that will compete with Asian + European option
    current_contracts_euro = Purchase.objects.filter(
        farmer=user,
        owed_status="Running",
        category=category,
        protection_style="European",
        stop_date=convert_string_to_date(policy_end_date)
    ).aggregate(
        amount=Coalesce(
            Sum('quantity'),
            0
        )
    )

    max_availability = max_availability - current_contracts_euro['amount']

    if max_availability < Decimal(quantity):
        if max_availability < Decimal(min):
            # Farmer is trying to insure more than they have said they farm (Multiple Policy) -
            # The amount they can insure lies below the minimum value
            return {'error': 'SCENARIO_4', 'max_availability': max_availability}
        else:
            # Farmer is trying to insure more than they have said they farm (Multiple Policy) -
            # but have given a value that is greater than the minimum base requirement we set
            return {'error': 'SCENARIO_5', 'max_availability': max_availability}

    return {'max_availability': max_availability if max_availability else 0}


def get_min(
        commodity: str
):
    if commodity in ['Milk']:
        return 10000
    elif commodity in ['Feed Wheat', 'Feed Barley', 'Milling Wheat', 'Rapeseed', 'AN Fertiliser']:
        return 10
    elif commodity in ['Beef', 'Lamb', 'Pork']:
        return 1000
    elif commodity in ['Red Diesel']:
        return 100
    else:
        return 1000


@retry(stop_max_attempt_number=3, wait_fixed=2000)
def get_availability_quantity(
        category_name: str,
        market_name: str,
        country: str
):
    # API fetching Availability for commodity from Simon's DB

    # risk_pools = []
    availability = []

    index_id = Category.objects.get(name=category_name, country=country).index_id

    multiplier_defined = settings.CURRENCY_CONVERSION_RULE.get(
        country
    ).get(
        category_name
    ).get('multiplier')

    if country == 'UK':
        country = 'GB'

    index_name = Index.objects.get(id=index_id).name
    risk_pool = name_this_risk_pool(market_name, index_name, country, '')

    """
    Call + Put options are different for working max_quantity.
    Price Rise and Price Fall.
    """

    if not multiplier_defined:
        multiplier_defined = Decimal(1)
    else:
        multiplier_defined = Decimal(multiplier_defined)

    if market_name != 'Input Costs':
        price = PricingResults.objects.filter(
            prod_name=category_name,
        ).aggregate(
            Max('strike'),
            Max('stop')
        )

        maximum_liability = abs((price['strike__max'] / multiplier_defined) - (price['stop__max'] / multiplier_defined))
    else:
        price = PricingResults.objects.filter(
            prod_name=category_name,
        ).aggregate(
            Min('strike'),
            Max('stop')
        )

        maximum_liability = abs((price['strike__min'] / multiplier_defined) - (price['stop__max'] / multiplier_defined))

    how_much = Decimal(how_much_left(risk_pool)[0]['Available Risk Capitals'])
    max_quantity = (how_much / maximum_liability)
    availability.append({'risk_pool': risk_pool, 'available_amount': max_quantity})

    return availability


@retry(stop_max_attempt_number=3, wait_fixed=2000)
def sell_this_wrapper(
        purchase_id: int, user_id: int
):
    # Variables Needed

    purchase = Purchase.objects.get(pk=purchase_id)
    User = get_user_model()
    user = User.objects.get(pk=user_id)

    quotation_time = datetime.datetime.now().strftime("%H:%M:%S %B-%d-%Y %Z")

    SOURCE_DATA_VERSION_MONTH = datetime.datetime.strftime(PricingResults.objects.latest().issuing_date, "%B %Y")

    market = purchase.market

    country = purchase.country

    # This category needs to be index name !
    category = purchase.category
    index_name = category.index.name

    multiplier = 1
    multiplier_defined = settings.CURRENCY_CONVERSION_RULE.get(
        country
    ).get(category.name).get('multiplier')

    if not multiplier_defined:
        multiplier_defined = multiplier
    else:
        multiplier_defined = Decimal(multiplier_defined)

    if country == 'UK':
        country = 'GB'

    # Style - derived from direction + style
    if purchase.protection_style == 'Asian' and purchase.protection_direction == 'FALL':
        style = 'Asian Put BearSpread'
    elif purchase.protection_style == 'Asian' and purchase.protection_direction == 'RISE':
        style = 'Asian Call BullSpread'
    elif purchase.protection_style == 'European' and purchase.protection_direction == 'FALL':
        style = 'European Put BearSpread'
    else:
        style = 'European Call BullSpread'

    shown_maturity = purchase.protection_duration

    strike = purchase.start_price / multiplier_defined
    stop = purchase.stop_price / multiplier_defined

    strike_ratio = purchase.strike_ratio
    strike_ratio = decimal_format(strike_ratio)
    # Stop_ratio
    if purchase.protection_direction == 'FALL':
        stop_ratio = Decimal('0.5')
    else:
        stop_ratio = Decimal('1.5')

    spread_price = purchase.price_per_unit_raw / multiplier_defined

    # Testing part
    start_asset_price = PricingResults.objects.filter(
        prod_name=category.name).latest().start_asset_price / multiplier_defined

    policy_end_date = datetime.datetime.strftime(purchase.stop_date, "%B %Y")
    policy_real_start_date = datetime.datetime.strftime(purchase.start_date, "%B %Y")

    units = 'Ton'
    if category.unit == 'TONNES':
        units = 'Ton'
    if category.unit == 'LITRES':
        units = 'Litre'
    if category.unit == 'KILOS':
        units = 'Kg'

    currency = category.currency
    ipt_rate = Decimal('0.12')
    ipt_amount = ipt_rate * spread_price * purchase.quantity
    quantity_requested = purchase.quantity
    total_price_with_ipt = purchase.total_cost / multiplier_defined
    total_price_without_ipt = purchase.premium / multiplier_defined
    discount_amount_from_voucher = purchase.farmer_saving / multiplier_defined
    total_amount_to_pay = total_price_with_ipt
    user_id = user.id
    policy_id = purchase.id

    dictionary = {
        'Quotation Time': quotation_time,
        'Market': market.name,
        'Product': index_name,
        'Country Code': country,
        'Style': style,
        'Shown Maturity': int(shown_maturity),
        'Strike Ratio': float(str(strike_ratio)),
        'Strike': float(str(strike)),
        'Stop': float(str(stop)),
        'Stop Ratio': float(str(stop_ratio)),
        'Spread Price': float(str(spread_price)),
        'Start Asset Price': float(str(start_asset_price)),
        'Policy End Date': policy_end_date,
        'Policy Shown Start Date': policy_real_start_date,
        'Units': units,
        'Currency': currency,
        'IPT Rate': float(str(ipt_rate)),
        'IPT Amount': float(str(ipt_amount)),
        'Quantity Requested': float(str(quantity_requested)),
        'Total Price With IPT': float(str(total_price_with_ipt)),
        'Total Price Without IPT': float(str(total_price_without_ipt)),
        'Discount Amount From Voucher': float(str(discount_amount_from_voucher)),
        'Total Amount to Pay': float(str(total_amount_to_pay)),  # This is correct currently
        'user_id': user_id,
        'policy_id': policy_id
    }
    print(dictionary)
    try:
        output = sell_this(dictionary, SOURCE_DATA_VERSION_MONTH)
    except Exception as e:
        AlgoSDKErrors(
            f'We attempted to call `sell_this` with the following values {dictionary}'
            f'It raised an error, can you please investigate?'
        )
        raise StableAPIException(e)

    return output


@retry(stop_max_attempt_number=3, wait_fixed=2000)
def confirm_to_sell_wrapper(
        unique_trade_id: str,
        purchase_id: int
):
    purchase = Purchase.objects.get(pk=purchase_id)
    try:
        response = confirm_to_sell(unique_trade_id)
    except Exception as e:
        raise AlgoSDKErrors(
            'We attempted to confirm purchase of policy {}, but \
            it has failed without returning a normal response. Please investigate'.format(unique_trade_id)
        )
        # return Response({
        #     "error": "Sorry, something went wrong with our system, don't worry you have"
        #              " not been charged for any transaction. "
        #              "We are aware of the problem and working to fix it"
        # }, status=status.HTTP_200_OK)

    if not response['if_confirmed']:
        return response, Response({
            "error": "Sorry, but it seems something is incorrect with the contract details \
        provided, we are investigating this issue currently."
        }, status=status.HTTP_200_OK)
    return response, None


@app.task(bind=True, retry_kwargs={'max_retries': 6}, retry_backoff=True)
def cancel_this_wrapper(
        self,
        unique_trade_id: str
):
    try:
        cancellation = cancel_this(unique_trade_id)
        if 'settlement' in cancellation:
            raise AlgoSDKErrors(str(cancellation))
    except Exception as e:
        if self.request.retries == 6:
            AlgoSDKErrors(
                'We have cancelled policy {} on the Dashboard, '
                'but you failed to cancel in Algo Database'.format(unique_trade_id)
            )
        self.retry(countdown=60)
    return


@app.task(bind=True, max_retries=6, retry_backoff=True)
def settle_this_wrapper(
        self,
        unique_trade_id: str,
        amount: Decimal):
    try:
        settle_this(unique_trade_id, float(str(amount)))
    except Exception as e:
        if self.request.retries == 6:
            AlgoSDKErrors(
                'We have settled policy {} on the Dashboard for amount {}, '
                'but you failed to settle in Algo Database'.format(unique_trade_id, float(str(amount)))
            )
        self.retry(countdown=60, exc=e)
    return


def get_category_info_api_availability_error(
        availability_error: str,
        index_name: str,
        produce: str,
        index_unit: str
):
    if availability_error == 'SCENARIO_1':
        max_reason = (
            f'<p><b>You did not enter a value for {index_name} when you registered so we are '
            f'unable to provide insurance this commodity if you do not produce it.</b></p>'
            f'<ul style="text-align: left;"><li>If you need to change the details relating to what you produce on your '
            f'farm please follow this <a href="{settings.DASHBOARD_URL}/profile">link</a> to the platform, '
            f'login (see your email called "Welcome to Stable" for details), select "profile" in the menu and then '
            f'click on "Farm Details" to edit your details.</li><li>If you do not want to insure {index_name} '
            f'please click back to and select another commodity</li></ul>'
        )
    elif availability_error == 'SCENARIO_2':
        max_reason = (
            f'<p><b>Our records suggest you are trying to insure more than you stated you '
            f'produce. The maximum you can insure for this period is {decimal_format(produce)} {index_unit}.'
            f'</b></p>There are a number of options...<br><br><ul style="text-align: left;">'
            f'<li>Edit the details of what you farm by following this '
            f'<a href="{settings.DASHBOARD_URL}/profile">link</a>.</li>'
            f'<li>If you think we have made a mistake please contact us at {settings.STABLE_SUPPORT_MAIL}</li></ul>'
        )
    elif availability_error == 'SCENARIO_3':
        max_reason = (
            f'<p><b>We are sorry, but according to our records you do not produce '
            f'enough {decimal_format(produce)} to be able to take out a policy.'
            f'</b></p><br><br><ul style="text-align: left;">'
            f'<li>If this was a mistake please click back to re-enter the details of what you farm. </li>'
            f'<li>Otherwise please follow this link to choose another product to insure.</li></ul>'
        )
    elif availability_error == 'SCENARIO_4':
        max_reason = (
            f'<p><b>Given the other policies that you currently have active, you are '
            f'trying to insure more than you produce for this period. '
            f'The maximum you can insure for this period is {decimal_format(produce)} {index_unit}.'
            f'</b></p>There are a number of options...<br><br><ul style="text-align: left;">'
            f'<li>Cancel any existing policies and create a new one</li>'
            f'<li>Edit the details of what you farm by following this '
            f'<a href="{settings.DASHBOARD_URL + "/profile"}">link</a>.</li>'
            f'<li>If you think we have made a mistake please contact us at {settings.STABLE_SUPPORT_MAIL}</li></ul>'
        )
    else:
        max_reason = (
            f'<p><b>Given the other policies that you currently have active, you are '
            f'trying to insure more than you produce for this period. '
            f'The maximum you can insure for this period is {decimal_format(produce)} {index_unit}.'
            f'</b></p>There are a number of options...<br><br><ul style="text-align: left;">'
            f'<li>Change the quantity to {decimal_format(produce)} {index_unit} or below</li>'
            f'<li>Edit the details of what you farm by following this '
            f'<a href="{settings.DASHBOARD_URL + "/profile"}">link</a>.</li>'
            f'<li>If you think we have made a mistake please contact us at {settings.STABLE_SUPPORT_MAIL}</li></ul>'
        )
    return max_reason


def get_category_info_view(
        category_name: str,
        option_type: str,
        market_name: str,
        policy_end_date: datetime.date,
        policy_start_date: datetime.date,
        quantity: int,
        user: AuthUser,
        country: str = None,
        chat_bot_id: UUID = None
):
    farmer = Farmer.objects.filter(user=user).first()
    if chat_bot_id:
        category = PartnerCategory.objects.get(name=category_name, chatbot_id=chat_bot_id)
        country = category.chatbot.country
        farmer_savings = Decimal(category.farmers_save)
        index_unit = category.unit
    elif farmer is not None:
        partners = Partner.objects.filter(pk__in=farmer.tagged_partners)
        chatbots = ChatBot.objects.filter(sales_partner__in=partners)
        partner_categories = PartnerCategory.objects.filter(
            name=category_name,
            chatbot__in=chatbots
        )

        if not partner_categories:

            category = Category.objects.get(name=category_name, country=country)
            farmer_savings = 0
            index_unit = category.unit
        else:
            category = partner_categories.order_by('-farmers_save').first()
            farmer_savings = Decimal(category.farmers_save)
            index_unit = category.unit
    else:
        category = Category.objects.get(name=category_name, country=country)
        farmer_savings = 0
        index_unit = category.unit

    multiplier_defined = settings.CURRENCY_CONVERSION_RULE.get(country).get(category_name).get('multiplier')

    if not multiplier_defined:
        multiplier_defined = Decimal(1)  # fallback value
    else:
        multiplier_defined = Decimal(multiplier_defined)

    if chat_bot_id or user.is_farmer:
        max_availability = cal_max_availability(
            user,
            category_name,
            country,
            policy_end_date,
            policy_start_date,
            option_type,
            quantity
        )

        if max_availability.get('error', False):
            availability_error = max_availability.get('error')
            produce = max_availability.get('max_availability')
            max_reason = get_category_info_api_availability_error(
                availability_error,
                category_name,
                produce,
                index_unit
            )
            return {'error': max_reason}

        max_availability = max_availability.get('max_availability', 0)

        availability = get_availability_quantity(category_name, market_name, country)
        if availability[0]['available_amount'] < max_availability:
            max_availability = round(Decimal(availability[0]['available_amount']), 0)

        if Decimal(quantity) > Decimal(max_availability):
            #  The availability we currently have is less than the amount they wish to insure against

            max_reason = (
                f'<p><b>We are sorry but there has been a very high demand for {category_name} price insurance. '
                f'You can currently insure a maximum of {decimal_format(max_availability)} {index_unit}</b></p>'
                f'Please amend the quantity you would like to insure to this amount (or less).'
                f'Please continue to check back as availability will increase soon.'
            )
            return {'error': max_reason}

    pricing_results = PricingResults.objects.filter(
        prod_name=category_name,
        style=option_type,
        policy_start_date=policy_start_date,
        policy_end_date=policy_end_date,
        country=country
    ).order_by('strike_ratio')
    index_results = IndexPriceData.objects.filter(
        prod_name=category_name,
        is_active='Live',
        country=country
    ).order_by('-date')[:pricing_results[0].maturity]

    if len(pricing_results) == 0 or len(index_results) == 0:
        return Response('error: Category not exist.')

    strike_list = [Decimal(entry.strike) / multiplier_defined for entry in pricing_results]

    price_list_multiplier = [Decimal(entry.price) / multiplier_defined for entry in pricing_results]
    price_list_farmers_saving = [price - price * farmer_savings / 100 for price in price_list_multiplier]
    price_list = [price + price * Decimal(0.12) for price in price_list_farmers_saving]

    stop_list = pricing_results[0].stop / multiplier_defined
    maturity = pricing_results[0].maturity

    total_list = [Decimal(price) * Decimal(quantity) for price in price_list]
    monthly_list = [Decimal(price) / int(maturity) for price in total_list]
    potential_list = [abs(price - stop_list) * Decimal(quantity) for price in strike_list]

    if multiplier_defined == 1:
        total_list = [price / 100 for price in total_list]
        monthly_list = [price / 100 for price in monthly_list]
        potential_list = [price / Decimal(100) for price in potential_list]

    metric = category.unit
    currency = category.currency
    last_index_month = datetime.datetime.strftime(index_results[0].date, '%B %Y')
    start_index_month = datetime.datetime.strftime(
        index_results[int(pricing_results[0].maturity) - 1].date,
        '%B %Y'
    )
    last_index_price = index_results[0].price

    year_aggregate = index_results.aggregate(
        Max('price'),
        Min('price'),
        Avg('price'),
    )

    current_date = datetime.datetime.now().strftime('%Y-%M-%D')

    context = {
        "marketName": market_name,
        'optionType': option_type,
        'policyStartDate': policy_start_date,
        'policyEndDate': policy_end_date,
        "strikePrice": strike_list,
        "purchasePrice": price_list,
        "total_list": total_list,
        "monthly_list": monthly_list,
        "potential_list": potential_list,
        "stopPrice": stop_list,
        "date": current_date,
        "indexName": category_name,
        "protectionDuration": maturity,
        "latestIndexMonth": last_index_month,
        'startIndexMonth': start_index_month,
        "latestIndexPrice": last_index_price,
        "high12month": year_aggregate['price__max'],
        "low12month": year_aggregate['price__min'],
        "average12month": year_aggregate['price__avg'],
        "currency": currency,
        "metric": metric,
        "flag": "check",
        "country": country,
    }

    return context
