from __future__ import unicode_literals

from django.contrib import admin

from . import models


class ChatBotAdmin(admin.ModelAdmin):
    list_display = ['pk', 'sales_partner', 'country', 'membership_id', 'country', 'partner_phone']


class MarketAdmin(admin.ModelAdmin):
    list_display = ['name', 'image', 'order_number', 'display', 'country']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'image', 'currency', 'unit', 'available', 'display',
                    'single_month', 'multi_month', 'min_contract_length', 'max_contract_length', 'country']


class PartnerMarketAdmin(admin.ModelAdmin):
    list_display = ['name', 'image', 'order_number', 'display', 'chatbot_id']


class PartnerCategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'image', 'currency', 'unit', 'available', 'display',
                    'single_month', 'multi_month', 'min_contract_length',
                    'max_contract_length', 'chatbot_id']


class FarmAdmin(admin.ModelAdmin):
    search_fields = ['farm_name', 'post_code']
    list_display = ['farm_name', 'post_code', 'farm_size']


class IndexDataAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'definition', 'owner']


class FunFactAdmin(admin.ModelAdmin):
    list_display = ['slide_name', 'funfact_text', 'display']


admin.site.register(models.ChatBot, ChatBotAdmin)
admin.site.register(models.Market, MarketAdmin)
admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.PartnerMarket, PartnerMarketAdmin)
admin.site.register(models.PartnerCategory, PartnerCategoryAdmin)
admin.site.register(models.Farm, FarmAdmin)
admin.site.register(models.Index, IndexDataAdmin)
admin.site.register(models.FunFact, FunFactAdmin)
