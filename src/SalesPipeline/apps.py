from django.apps import AppConfig


class SalespipelineConfig(AppConfig):
    name = 'SalesPipeline'
