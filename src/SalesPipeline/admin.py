from __future__ import unicode_literals
from django.contrib import admin
from . import models


class SalesPipelineAdmin(admin.ModelAdmin):
    list_display = ['company', 'country', 'contact_name', 'contact_email', 'country']


admin.site.register(models.SalesPipeline, SalesPipelineAdmin)