from django.conf.urls import url
from SalesPipeline import views

urlpatterns = [
    url(r'^get_sales_pipeline/', views.SalesPipelineView.as_view()),
    url(r'^update_sales_pipeline/', views.SalesPipelineView.as_view()),
]
