from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from aox_dr.exception import StableAPIException
from .models import SalesPipeline
from .serializer import SalesPipelineSerializer


class SalesPipelineView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'Stable Admin can see this data'}, status=status.HTTP_200_OK)

            sales_pipeline = SalesPipeline.objects.all().order_by('company')

            # page = request.GET.get('pagination[page]') if 'pagination[page]' in request.GET else 1
            # per_page = request.GET.get('pagination[perpage]') if 'pagination[perpage]' in request.GET else 20
            #
            # total = sales_pipeline.count()
            # paginator = Paginator(sales_pipeline, per_page)

            # try:
            #     sales_pipeline = paginator.page(page)
            # except PageNotAnInteger:
            #     # If page is not an integer, deliver first page.
            #     sales_pipeline = paginator.page(1)
            # except EmptyPage:
            #     # If page is out of range (e.g. 9999), deliver last page of results.
            #     sales_pipeline = paginator.page(paginator.num_pages)

            all_sales_pipeline = SalesPipelineSerializer(sales_pipeline, many=True)
            # all_sales_pipeline = SalesPipelineSerializer(SalesPipeline.objects.all().order_by('company'), many=True)

            context = {
                'data': all_sales_pipeline.data,
                # 'meta': {
                #     'page': page,
                #     'perpage': per_page,
                #     'total': total
                # }
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'Stable Admin can edit this data'}, status=status.HTTP_200_OK)

            sales_pipeline_id = request.data['id'] if 'id' in request.data else None
            if not sales_pipeline_id:
                sales_pipeline = SalesPipeline()
            else:
                sales_pipeline = SalesPipeline.objects.get(id=sales_pipeline_id)

            sales_pipeline.company = request.data['company'] if 'company' in request.data else None
            sales_pipeline.commodity = request.data['commodity'] if 'commodity' in request.data else None
            sales_pipeline.contact_name = request.data['contact_name'] if 'contact_name' in request.data else None
            sales_pipeline.contact_email = request.data['contact_email'] \
                if 'contact_email' in request.data else None
            sales_pipeline.country = request.data['country'] if 'country' in request.data else None
            sales_pipeline.current_finance_arm = request.data['current_finance_arm']\
                if 'current_finance_arm' in request.data else None
            sales_pipeline.deal_stage = request.data['deal_stage'] if 'deal_stage' in request.data else None
            sales_pipeline.direct_relation_with_farmer = request.data['direct_relation_with_farmer'] \
                if 'direct_relation_with_farmer' in request.data else None
            sales_pipeline.expected_revenue = request.data['expected_revenue']\
                if 'expected_revenue' in request.data else None
            sales_pipeline.note = request.data['note'] if 'note' in request.data else None
            sales_pipeline.present_else_where = request.data['present_else_where'] \
                if 'present_else_where' in request.data else None
            sales_pipeline.sector = request.data['sector'] if 'sector' in request.data else None
            sales_pipeline.size = request.data['size'] if 'size' in request.data else None
            sales_pipeline.uk_hq = request.data['uk_hq'] if 'uk_hq' in request.data else None

            sales_pipeline.save()

            return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)
