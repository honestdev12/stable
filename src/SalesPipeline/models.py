from django.db import models


class SalesPipeline(models.Model):
    company = models.CharField(max_length=255, default='')
    country = models.CharField(max_length=255, default='UK')
    deal_stage = models.CharField(max_length=255, default='')
    expected_revenue = models.CharField(max_length=255, default='')
    sector = models.CharField(max_length=255, null=True, blank=True)
    commodity = models.CharField(max_length=255, default='All')
    uk_hq = models.CharField(max_length=255, null=True, blank=True)
    present_else_where = models.CharField(max_length=255, null=True, blank=True)
    size = models.CharField(max_length=255, null=True, blank=True)
    direct_relation_with_farmer = models.CharField(max_length=255, null=True, blank=True)
    contact_name = models.CharField(max_length=255, null=True, blank=True)
    contact_email = models.CharField(max_length=255, null=True, blank=True)
    current_finance_arm = models.CharField(max_length=255, null=True, blank=True)
    note = models.TextField(null=True, blank=True)
