from rest_framework import serializers
from .models import SalesPipeline


class SalesPipelineSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = SalesPipeline
        fields = '__all__'
