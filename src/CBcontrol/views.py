import json

from django.conf import settings
from django.core.paginator import (
    EmptyPage,
    PageNotAnInteger,
    Paginator,
)
from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from aox_dr.exception import StableAPIException
from CBAPIs.models import (
    Category,
    ChatBot,
    Farm,
    FunFact,
    Market,
    PartnerCategory,
    PartnerMarket,
)
from CBAPIs.serializer import (
    CategorySerializer,
    CBFarmSerializer,
    CBFunFactSerializer,
    ChatBotSerializer,
    MarketSerializer,
    PartnerCategorySerializer,
    PartnerMarketSerializer,
)
from Partner.models import (
    Partner,
)


class GetCBCategoryMarketDataView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, country):
        try:
            category = CategorySerializer(Category.objects.filter(country=country).order_by('name'), many=True)
            market = MarketSerializer(Market.objects.filter(country=country).order_by('name'), many=True)
            context = {
                'success': 'success',
                'CategoryTableData': category.data,
                'MarketTableData': market.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class SalesPartnerCBDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def CBDataView(self, chatbot_unique_id, request):
        chat_bot = ChatBot.objects.get(pk=chatbot_unique_id)
        partner = chat_bot.sales_partner

        category_json = PartnerCategorySerializer(PartnerCategory.objects.filter(
            chatbot=chat_bot
        ).order_by('name'), many=True)
        market_json = PartnerMarketSerializer(PartnerMarket.objects.filter(
            chatbot=chat_bot
        ).order_by('name'), many=True)

        category_name_list = PartnerCategory.objects.filter(
            chatbot=chat_bot
        ).values_list('name', flat=True)

        markets = Market.objects.filter(country=chat_bot.country, display=True)
        default_commission = chat_bot.default_commission
        default_farmer_savings = chat_bot.default_farmer_savings

        commodity_array = [
            {
                'text': 'All Markets and Categories',
                'children': [],
                'state': {
                    'opened': True
                },
                'type': 'default'
            }
        ]

        commodity_tree_type = {}

        for i, market in enumerate(markets):
            market_id = market.id
            commodity_array[0]['children'].append(
                {
                    'id': 'market_{}'.format(market.id),
                    'text': market.name,
                    'children': [],
                    'state': {
                        'opened': True
                    },
                    'type': market.name
                }
            )
            commodity_tree_type[market.name] = {
                'icon': '{}/{}'.format(settings.MEDIA_URL_CUSTOM, market.image)
            }
            categories = Category.objects.filter(market_id=market_id, display=True)
            for category in categories:
                commodity_array[0]['children'][i]['children'].append(
                    {
                        'id': 'category_{}'.format(category.id),
                        'text': category.name,
                        'type': category.name,
                        'state': {
                            'selected': True if category.name in list(category_name_list) else False
                        }
                    }
                )
                commodity_tree_type[category.name] = {
                    'icon': '{}/{}'.format(settings.MEDIA_URL_CUSTOM, category.image)
                }

        context = {
            'success': 'ChatBot updated successfully!',
            'CategoryTableData': category_json.data,
            'MarketTableData': market_json.data,
            'CommodityTree': commodity_array,
            'max_savings': partner.max_savings,
            'CommodityTreeType': commodity_tree_type,
            'CB_Info': chat_bot.membership_id,
            'default_commission': default_commission,
            'default_farmer_savings': default_farmer_savings
        }
        return context

    def get(self, request):
        try:
            if not request.user.is_partner and not request.user.is_broker:
                return Response({'error': 'Sales Partner can see this data'}, status=status.HTTP_200_OK)

            chatbot_unique_id = request.GET.get('chatbot_id', None)
            if not chatbot_unique_id:
                return Response({'error': 'ChatBot ID not defined.'}, status=status.HTTP_200_OK)

            context = self.CBDataView(chatbot_unique_id, request)
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    def post(self, request):
        try:
            if not request.user.is_partner and not request.user.is_broker:
                return Response({'error': 'Sales Partner can update chat bot'}, status=status.HTTP_200_OK)

            chatbot_unique_id = request.data.get('chatbot_id', None)
            if not chatbot_unique_id:
                return Response({'error': 'ChatBot ID not defined.'}, status=status.HTTP_200_OK)

            selected_commodities = request.data.get('commodities')
            if not selected_commodities:
                return Response({'error': 'Commodities are not selected'}, status=status.HTTP_200_OK)
            else:
                selected_commodities = json.loads(selected_commodities)

            selected_markets = request.data.get('markets')
            if not selected_markets:
                return Response({'error': 'Markets are not selected'}, status=status.HTTP_200_OK)
            else:
                selected_markets = json.loads(selected_markets)

            chatbot = get_object_or_404(ChatBot, pk=chatbot_unique_id, sales_partner__company_admin=request.user)

            selected_market_ids = [market['id'] for market in selected_markets]
            selected_commodity_ids = [commodity['id'] for commodity in selected_commodities]
            selected_commodities = [
                (
                    commodity['id'],
                    commodity['commission'],
                    commodity['farmers_save']
                ) for commodity in selected_commodities
            ]

            PartnerCategory.objects.filter(chatbot=chatbot).exclude(category__id__in=selected_commodity_ids).delete()
            PartnerMarket.objects.filter(chatbot=chatbot).exclude(market__id__in=selected_market_ids).delete()

            for market_id in selected_market_ids:
                market = Market.objects.get(
                    pk=market_id,
                )
                PartnerMarket.objects.update_or_create(
                    name=market.name,
                    chatbot=chatbot,
                    defaults={
                        'market': market,
                        'name': market.name,
                        'image': market.image,
                        'order_number': market.order_number,
                        'display': market.display,
                        'chatbot': chatbot
                    }
                )

            for commodity_id, commission, farmers_save in selected_commodities:
                category = Category.objects.get(
                    pk=commodity_id
                )
                origin_market_name = category.market.name
                new_market = PartnerMarket.objects.get(
                    name=origin_market_name,
                    chatbot=chatbot
                )
                PartnerCategory.objects.update_or_create(
                    category=category,
                    chatbot=chatbot,
                    defaults={
                        'name': category.name,
                        'image': category.image,
                        'market': new_market,
                        'category': category,
                        'currency': category.currency,
                        'percent': category.percent,
                        'unit': category.unit,
                        'index_id': category.index_id,
                        'amount': category.amount,
                        'available': category.available,
                        'spent': category.spent,
                        'display': category.display,
                        'single_month': category.single_month,
                        'multi_month': category.multi_month,
                        'min_contract_length': category.min_contract_length,
                        'max_contract_length': category.max_contract_length,
                        'chatbot': chatbot,
                        'commission': commission,
                        'farmers_save': farmers_save
                })

                chatbot.default_commission = request.data.get('default_commission', None)
                if chatbot.default_commission == 'null':
                    chatbot.default_commission = None
                chatbot.default_farmer_savings = request.data.get('default_farmer_savings', None)
                if chatbot.default_farmer_savings == 'null':
                    chatbot.default_farmer_savings = None
                chatbot.save()

            context = self.CBDataView(chatbot_unique_id, request)
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class DeleteCBView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_partner and not request.user.is_broker:
                return Response({'error': 'Sales Partner can update chat bot'}, status=status.HTTP_200_OK)

            chat_bot_unique_id = request.data.get('chatbot_id', None)
            if not chat_bot_unique_id:
                return Response({'error': 'ChatBot ID not defined.'}, status=status.HTTP_200_OK)

            ChatBot.objects.get(pk=chat_bot_unique_id).delete()

            partner = Partner.objects.get(company_admin=request.user)
            chat_bots = ChatBot.objects.filter(sales_partner=partner)

            context = ChatBotSerializer(chat_bots, many=True).data

            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class UpdateCBMarketDisplayDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            data = request.data['data'] if 'data' in request.data else None
            if not data:
                return Response({'error': 'Market Data not changed.'}, status=status.HTTP_200_OK)

            country = request.data['country'] if 'country' in request.data else None
            if not country:
                return Response({'error': 'Country not defined.'}, status=status.HTTP_200_OK)

            market_data_changes = json.loads(data)
            if len(market_data_changes) <= 0:
                return Response({'error': 'Market Data not changed.'}, status=status.HTTP_200_OK)
            for change in market_data_changes:
                display_bool = change['display']
                market = Market.objects.get(id=change['key'])
                market.display = display_bool
                market.save()

            category = CategorySerializer(Category.objects.filter(country=country).order_by('name').all(), many=True)
            market = MarketSerializer(Market.objects.filter(country=country).order_by('name'), many=True)
            context = {
                'success': 'success',
                'CategoryTableData': category.data,
                'MarketTableData': market.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UpdateCBCategoryDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            data = request.data['data'] if 'data' in request.data else None
            if not data:
                return Response({'error': 'Category Data not changed.'}, status=status.HTTP_200_OK)

            country = request.data['country'] if 'country' in request.data else None
            if not country:
                return Response({'error': 'Country not changed.'}, status=status.HTTP_200_OK)

            category_data_changes = json.loads(data)
            if len(category_data_changes) <= 0:
                return Response({'error': 'Category Data not changed.'}, status=status.HTTP_200_OK)

            for change in category_data_changes:
                market = Category.objects.get(id=change['key'])
                if change['type'] == 'min_contract_length':
                    market.min_contract_length = change['value']
                elif change['type'] == 'max_contract_length':
                    market.max_contract_length = change['value']
                elif change['type'] == 'display':
                    market.display = change['value']
                elif change['type'] == 'single_month':
                    market.single_month = change['value']
                else:
                    market.multi_month = change['value']
                market.save()

            category = CategorySerializer(Category.objects.filter(country=country).order_by('name').all(), many=True)
            market = MarketSerializer(Market.objects.filter(country=country).order_by('name'), many=True)
            context = {
                'success': 'success',
                'CategoryTableData': category.data,
                'MarketTableData': market.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GetPageFarmDataView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            page = request.data.get('pagination[page]') if 'pagination[page]' in request.data else 1
            perpage = request.data.get('pagination[perpage]') if 'pagination[perpage]' in request.data else 20

            farms = Farm.objects.all()
            total = farms.count()
            paginator = Paginator(farms, perpage)

            try:
                farms = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                farms = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                farms = paginator.page(paginator.num_pages)

            farm = CBFarmSerializer(farms, many=True)

            context = {
                'data': farm.data,
                'meta': {
                    'page': page,
                    'perpage': perpage,
                    'total': total
                }
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UploadFarmDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            file = request.data['file']
            if not file.name.endswith('.csv'):
                return Response({'error': 'File is not CSV type'}, status=status.HTTP_200_OK)
            file_data = file.read().decode("utf-8")
            lines = file_data.split("\n")
            if len(lines):
                fields = lines[0].split(',')
                if fields[2] == 'BeneficiaryName_F201' and fields[3] == 'PostcodePrefix_F202B':
                    results = []
                    for idx, line in enumerate(lines):
                        print(idx)
                        if idx != 0 and line != '':
                            values = line.split(',')
                            if values[2] != '*******':
                                results.append(Farm(
                                    farm_name=values[2].strip(' \t\n\r'),
                                    post_code=values[3].strip(' \t\n\r').split(' ')[0],
                                ))
                    if results:
                        Farm.objects.all().delete()
                        Farm.objects.bulk_create(results)

                    context = {
                        'success': 'success',
                        'farm_data': CBFarmSerializer(Farm.objects.all(), many=True).data
                    }
                    return Response(context, status=status.HTTP_200_OK)
                else:
                    return Response(
                        {
                            'error': 'File not Farm data.'
                                     'Please upload correct farm data csv'
                        },
                        status=status.HTTP_200_OK
                    )
            else:
                return Response(
                    {
                        'error': 'File not Farm data.'
                                 'Please upload correct farm data csv'
                    },
                    status=status.HTTP_200_OK
                )
        except Exception as e:
            raise StableAPIException(e)


class GetFunFactTableDataView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, country):
        try:
            fun_fact = CBFunFactSerializer(FunFact.objects.filter(country=country).order_by('id'), many=True)
            context = {
                'fun_fact_data': fun_fact.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UpdateFunFactTableDataView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request, fun_fact_id):
        try:
            country = request.data['country'] if 'country' in request.data else None
            if not country:
                return Response({'error': 'Country not changed.'}, status=status.HTTP_200_OK)

            fun_fact = FunFact.objects.get(id=fun_fact_id, country=country)
            fun_fact.funfact_text = request.data['funfact_text']
            if request.data['display'] == 'true':
                fun_fact.display = True
            else:
                fun_fact.display = False
            fun_fact.save()

            fun_facts = CBFunFactSerializer(FunFact.objects.filter(country=country).order_by('id'), many=True)
            context = {
                'fun_fact_data': fun_facts.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)
