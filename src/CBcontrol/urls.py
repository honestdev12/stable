from django.conf.urls import url
from CBcontrol import views as cbcontrol_views

urlpatterns = [
    url(r'^get_category_market_data/(?P<country>[\w ]+)/$', cbcontrol_views.GetCBCategoryMarketDataView.as_view()),
    url(r'^update_market_display/', cbcontrol_views.UpdateCBMarketDisplayDataView.as_view()),
    url(r'^update_category_data/', cbcontrol_views.UpdateCBCategoryDataView.as_view()),

    url(r'^get_sales_partner_chat_bot_data/', cbcontrol_views.SalesPartnerCBDataView.as_view()),
    url(r'^update_sales_partner_chat_bot/', cbcontrol_views.SalesPartnerCBDataView.as_view()),
    url(r'^delete_sales_partner_chat_bot/', cbcontrol_views.DeleteCBView.as_view()),

    url(r'^get_paginated_farm_data/', cbcontrol_views.GetPageFarmDataView.as_view()),
    url(r'^upload_farm_data/', cbcontrol_views.UploadFarmDataView.as_view()),

    url(r'^get_fun_fact_data/(?P<country>[\w ]+)/$', cbcontrol_views.GetFunFactTableDataView.as_view()),
    url(r'^update_fun_fact_data/(?P<fun_fact_id>\d+)/$', cbcontrol_views.UpdateFunFactTableDataView.as_view()),
]
