from typing import Dict

from django.conf import settings
from django.core.mail import (
    EmailMessage,
    EmailMultiAlternatives,
)


def send_provider_agnostic_email(
        subject: str,
        to_email: str,
        template_id: str,
        merge_vars: Dict,
        attachments: list = None,
        categories: list = None
):

    if settings.EMAIL_BACKEND == "sgbackend.SendGridBackend":
        mail = EmailMultiAlternatives(
            subject=subject,
            from_email=settings.DEFAULT_FROM_MAIL,
            to=[to_email]
        )
        # Add template
        mail.template_id = template_id

        # Replace substitutions in sendgrid template
        mail.substitutions = merge_vars

        # Attach file
        mail.attachments = attachments

        # Add categories
        mail.categories = [] if categories is None else categories

        mail.send()

    else:

        msg = EmailMessage(
            subject=subject,
            from_email=settings.DEFAULT_FROM_MAIL,
            to=[to_email]
        )
        msg.template_name = template_id
        msg.global_merge_vars = merge_vars

        # add attachments
        msg.attachments = attachments

        msg.send()
