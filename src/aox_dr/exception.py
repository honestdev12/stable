from django.utils.translation import ugettext_lazy as _
from raven.contrib.django.raven_compat.models import client
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.views import exception_handler


class StableAPIException(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = _('API Failed.')
    default_code = 'api_failed'


class StableAuthException(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = _('Auth Failed.')
    default_code = 'authentication_failed'


class StableFarmerRegisterException(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = _('Registration Failed.')
    default_code = 'farmer_register_failed'


def custom_exception_handler(exc, context):
    client.captureException()
    response = exception_handler(exc, context)

    if not response:
        exc = StableAPIException(exc)
        response = exception_handler(exc, context)

    if response is not None:
        if exc.default_code == 'authentication_failed':
            error_str = 'We are sorry but this seems to be the wrong password. Please try again or click on ' \
                        '"Forgot Password?" to reset it.'
        elif exc.default_code == 'farmer_register_failed':
            error_str = 'We are sorry, but you have already registered and submitted your details. ' \
                        'You can change any of the details you submitted under settings on the ' \
                        '<a href="https://dash.stableprice.com">Stable Platform</a>. ' \
                        'To login use your email and the password you have just created.'
        else:
            error_str = 'We are sorry something appears to have gone wrong. ' \
                        'Please contact the Stable support team at support@stableprice.com quoting the ' \
                        'error codes at the bottom of the page (if applicable) and we will try and get ' \
                        'to the bottom of this as quickly as possible.'
        response.data['error'] = error_str
    return response
