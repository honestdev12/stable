from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def replace(value, arg):
    """Removes all values of arg from the given string"""
    args = arg.split(',')
    return value.replace(*args)
