from random import randint

from django.conf import settings
from django.core.cache import cache
from django_twilio.client import twilio_client
from rest_auth.views import (
    LoginView,
    PasswordResetView,
)
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from administration.models import AuthUser
from CBAPIs.models import ChatBot
from CBAPIs.utils import decrypt_password
from overview.models import TwoFactorTokenHistory
from Partner.models import Partner
from .exception import StableAPIException


class CustomLoginView(LoginView):

    def get_response(self):
        serializer_class = self.get_response_serializer()

        if getattr(settings, 'REST_USE_JWT', False):
            data = {
                'user': self.user,
                'token': self.token
            }
            serializer = serializer_class(instance=data,
                                          context={'request': self.request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            serializer = serializer_class(instance=self.token,
                                          context={'request': self.request})

            if self.user.is_superuser:
                current_authority = 'admin'
            else:
                if self.user.is_corporate:
                    if self.user.is_partner:
                        current_authority = 'corporate_sales_partner'
                    else:
                        current_authority = 'corporate'
                elif self.user.is_partner:
                    current_authority = 'partner'
                elif self.user.is_broker:
                    current_authority = 'broker'
                elif self.user.is_customer_supporter:
                    current_authority = 'customer_supporter'
                elif self.user.is_underwriter:
                    current_authority = 'underwriter'
                elif self.user.is_farmer:
                    current_authority = 'farmer'
                else:
                    current_authority = 'guest'

            if Partner.objects.filter(company_admin=self.user).exists():
                chat_bots = 'Yes' if ChatBot.objects.filter(
                    sales_partner=Partner.objects.get(company_admin=self.user)
                ).exists() else 'No'
            else:
                chat_bots = 'No'
            context = {
                'currentAuthority': current_authority,
                'key': serializer.data['key'],
                'chat_bots': chat_bots
            }
            return Response(context, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        self.request = request
        data = self.request.data.copy()
        data['password'] = decrypt_password(self.request.data['password']).decode("utf-8")
        self.serializer = self.get_serializer(data=data,
                                              context={'request': request})
        self.serializer.is_valid(raise_exception=True)

        self.login()
        return self.get_response()


class CustomPasswordResetView(PasswordResetView):

    def post(self, request, *args, **kwargs):
        # Create a serializer with request.data
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = request.data.get('email')
        if not AuthUser.objects.filter(email=email).exists():
            return Response({'error': 'User with this email does not exist'}, status=status.HTTP_200_OK)

        serializer.save()
        # Return the success message with OK HTTP status
        return Response(
            {
                "detail": "Password reset e-mail has been sent."
            },
            status=status.HTTP_200_OK
        )


class CheckCredentialsView(APIView):

    @staticmethod
    def post(request):
        auth_failure_key = 'LOGIN_FAILURES_AT_%s' % request.META.get('REMOTE_ADDR')
        auth_failures = cache.get(auth_failure_key) or 0
        # allow up to 3 failures per 3 minutes
        if auth_failures >= 3:
            cache.set(auth_failure_key, auth_failures + 1, 180)
            return Response(
                {
                    'error': 'Locked out, too many authentication failures',
                    'lock': True
                },
                status=status.HTTP_200_OK
            )

        try:
            email = request.data['email']
            password = request.data['password']

            password = decrypt_password(password).decode("utf-8")

            user = AuthUser.objects.get(email=email)

            if user.is_superuser:
                current_authority = 'admin'
            else:
                if user.is_corporate:
                    if user.is_partner:
                        current_authority = 'corporate_sales_partner'
                    else:
                        current_authority = 'corporate'
                elif user.is_partner:
                    current_authority = 'partner'
                elif user.is_customer_supporter:
                    current_authority = 'customer_supporter'
                elif user.is_underwriter:
                    current_authority = 'underwriter'
                elif user.is_farmer:
                    current_authority = 'farmer'
                elif user.is_broker:
                    current_authority = 'broker'
                else:
                    current_authority = 'guest'

            two_factor = user.is_two_factor

            t_c_status = user.accepted_t_c

            if not two_factor:
                if user.check_password(password):
                    return Response(
                        {
                            'two_factor': two_factor,
                            't_c_status': t_c_status,
                            'current_authority': current_authority,
                            'success': 'success'
                        },
                        status=status.HTTP_200_OK
                    )

                else:
                    cache.set(auth_failure_key, auth_failures + 1, 180)
                    if cache.get(auth_failure_key) >= 3:
                        return Response(
                            {
                                'error': 'Locked out, too many authentication failures',
                                'lock': True
                            },
                            status=status.HTTP_200_OK
                        )
                    return Response(
                        'The details you have provided are incorrect. Please check and try again.',
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR
                    )
            else:
                if user.check_password(password):
                    phone = user.phone
                    if phone:
                        code = randint(100000, 999999)
                        twilio_response = twilio_client.messages.create(
                            to=phone,
                            from_="447400329427",
                            body="Hello from Stable! Your 2 Factor Authentication code is: " + str(code)
                        )

                        if twilio_response.sid:
                            if TwoFactorTokenHistory.objects.filter(user=user).exists():
                                token = TwoFactorTokenHistory.objects.get(user=user)
                                token.code = code
                                token.save()
                            else:
                                token = TwoFactorTokenHistory(
                                    user=user,
                                    code=code
                                )
                                token.save()

                        return Response({'t_c_status': t_c_status, 'current_authority': current_authority, 'success': 'success'}, status=status.HTTP_200_OK)
                    else:
                        return Response(
                            'We can\'t get your phone number from your profile,'
                            ' Please Contact Stable Admin to get further more details',
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR
                        )
                else:
                    cache.set(auth_failure_key, auth_failures + 1, 180)
                    if cache.get(auth_failure_key) >= 3:
                        return Response(
                            {
                                'error': 'Locked out, too many authentication failures',
                                'lock': True
                            },
                            status=status.HTTP_200_OK
                        )
                    return Response(
                        'You can not login with provided credentials',
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR
                    )
        except Exception as e:
            raise StableAPIException(e)


class VerifyCodeView(APIView):
    @staticmethod
    def post(request):
        try:
            code = request.data['security_code'] if 'security_code' in request.data else None
            if not code:
                return Response(
                    {'error': 'Please enter security code that we sent you!'},
                    status=status.HTTP_200_OK
                )
            email = request.data['email'] if 'email' in request.data else None
            if not email:
                return Response(
                    {'error': 'Email is not defined!'},
                    status=status.HTTP_200_OK
                )

            user = AuthUser.objects.get(email=email)
            token = TwoFactorTokenHistory.objects.get(user=user)

            if token.code == code:
                return Response({'success': 'success'}, status=status.HTTP_200_OK)
            else:
                return Response(
                    {
                        'error': 'Code is not correct! Please login again to get new token'
                    },
                    status=status.HTTP_200_OK
                )

        except Exception as e:
            raise StableAPIException(e)
