from django.conf import settings
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from aox_dr.utils import send_provider_agnostic_email


class CustomPasswordResetForm(PasswordResetForm):

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):

        if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
            template_id = ''
            merge_vars = {
                'first_name': context['user'].first_name,
                'link': f'{settings.DASHBOARD_URL}/login?uid={context["uid"]}&token={context["token"]}'
            }
        else:
            template_id = 'reset-password-template'
            merge_vars = {
                'FIRST_NAME': context['user'].first_name,
                'LINK': f'{settings.DASHBOARD_URL}/login?uid={context["uid"]}&token={context["token"]}'
            }
        send_provider_agnostic_email("Reset Password", to_email, template_id=template_id, merge_vars=merge_vars, attachments=[])

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='auth/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):
        """
        Generate a one-use only link for resetting password and send it to the
        user.
        """
        email = self.cleaned_data["email"]

        for user in self.get_users(email):
            if not domain_override:
                current_site = get_current_site(request)
                domain = current_site.domain.replace(':8000', '')
            else:
                domain = domain_override

            context = {
                'email': email,
                'domain': domain,
                'site_name': "Stable Price",
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
            }
            if extra_email_context is not None:
                context.update(extra_email_context)
            self.send_mail(
                subject_template_name, email_template_name, context, from_email,
                email, html_email_template_name=html_email_template_name,
            )
