from rest_framework import pagination


class StableCustomPagination(pagination.LimitOffsetPagination):
    offset_query_param = "start"
    limit_query_param = "length"
