from django.conf import settings
from django.conf.urls import (
    include,
    url,
)
from django.conf.urls.static import static
from django.contrib import admin
from rest_auth.views import PasswordResetConfirmView
from rest_framework import routers

from .authenticate import (
    CheckCredentialsView,
    CustomLoginView,
    CustomPasswordResetView,
    VerifyCodeView,
)

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^api/v1/rest-auth/login/', CustomLoginView.as_view(), name='rest_login'),
    url(r'^api/v1/rest-auth/check_credentials/', CheckCredentialsView.as_view(), name='check_credentials'),
    url(r'^api/v1/rest-auth/verify_code/', VerifyCodeView.as_view(), name='verify_token'),
    url(r'^api/v1/rest-auth/password/reset/', CustomPasswordResetView.as_view(), name='rest_login'),
    url(r'^api/v1/rest-auth/password/confirm', PasswordResetConfirmView.as_view(), name='rest_password_change'),
    url(r'^api/v1/rest-auth/', include('rest_auth.urls')),

    url(r'^api/v1/pricing/', include(('pricing.urls', 'pricing'), namespace='pricing')),
    url(r'^api/v1/corporate/', include(('Corporate.urls', 'Corporate'), namespace='corporate')),
    url(r'^api/v1/overview/', include(('overview.urls', 'overview'), namespace='overview')),
    url(r'^api/v1/administration/', include(('administration.urls', 'administration'), namespace='administration')),
    url(r'^api/v1/cb_control/', include(('CBcontrol.urls', 'CBcontrol'), namespace='CBcontrol')),
    url(r'^api/v1/crm/', include(('CRM.urls', 'CRM'), namespace='CRM')),
    url(r'^api/v1/partner/', include(('Partner.urls', 'Partner'), namespace='Partner')),
    url(r'^api/v1/borderaux/', include(('Borderaux.urls', 'Borderaux'), namespace='Borderaux')),
    url(r'^api/v1/broker/', include(('Broker.urls', 'Broker'), namespace='Broker')),
    url(r'^api/v1/sales_pipeline/', include(('SalesPipeline.urls', 'SalesPipeline'), namespace='SalesPipeline')),

    url(r'^cbapi/', include(('CBAPIs.urls', 'CBAPIs'), namespace='CBAPIs')),

    url(r'^', include(router.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
