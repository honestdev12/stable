import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'aox_dr.settings.local')

app = Celery('aox_dr')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
