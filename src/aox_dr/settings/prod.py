from aox_dr.settings.base import *

DEBUG = False

ENV = 'Prod'

PAGE_CACHE_SECONDS = 1

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'aox_backend',
        'PASSWORD': '31.aox_stable',
        'HOST': 'aox-be.cz06q2uo7ybm.eu-west-1.rds.amazonaws.com',
        'PORT': 5432,
    }
}

CACHES = {
    'default': {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/0",
    }
}

# Keep this secret! Maybe as environmental variable.
AWS_ACCESS_KEY_ID = 'AKIAJFBGLNO6XVXJ2KCQ'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'stable--prod'
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_LOCATION = 'static'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]
STATIC_URL = 'https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

DEFAULT_FILE_STORAGE = 'aox_dr.settings.storage_backends.MediaStorage'
MEDIA_URL_CUSTOM = 'https://%s/%s' % (AWS_S3_CUSTOM_DOMAIN, 'media')

# mandrill api key configuration

MANDRILL_API_KEY = '3AB-J9XHBMsL7RuojgfuSQ'  # Live Mandrill API Key.
SENDGRID_API_KEY = "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"

# stripe api configuration

STRIPE_API_KEY = 'sk_live_39wrPr2pcFkLpj6euLIzlYZT'

# Go CardLess api configuration

GO_CARD_LESS_ACCESS_TOKEN = 'live_6rhkzp7YWasdElFPP2hrpncf_j3nshEE_xgl_l7t'
GO_CARD_LESS_ENVIRONMENT = 'live'

# Go CardLess Redirect URL

GO_CARD_LESS_REDIRECT_URL = 'https://cb.stableprice.com/p/{chatbot_id}/payment/processing'

# Celery Configuration

CELERY_BROKER_URL = 'sqla+postgresql://aox_backend:31.aox_stable' \
                    '@aox-be.cz06q2uo7ybm.eu-west-1.rds.amazonaws.com/postgres'

CELERY_BROKER_URL = 'redis://localhost:6379/1'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'

# Websites url configurations

CHAT_BOT_URL = 'https://cb.stableprice.com'
DASHBOARD_URL = 'https://dash.stableprice.com'

CORS_ORIGIN_REGEX_WHITELIST = (
    r'^(https?://)?(\w+\.)?stableprice\.com$',
)


RAVEN_CONFIG['environment'] = ENV
