from aox_dr.settings.base import *  # NOQA (ignore all errors on this line)

DEBUG = False

ENV = 'Dev'

PAGE_CACHE_SECONDS = 1

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'stable_dev',
        'USER': 'stable_dev',
        'PASSWORD': 'hello_stable',
        'HOST': 'dev-server.cz06q2uo7ybm.eu-west-1.rds.amazonaws.com',
        'PORT': 5432,
    }
}

CACHES = {
    'default': {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/0",
    }
}

# Keep this secret! Maybe as environmental variable.
AWS_ACCESS_KEY_ID = 'AKIAJFBGLNO6XVXJ2KCQ'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'stable--dev'
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_LOCATION = 'static'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]
STATIC_URL = 'https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

DEFAULT_FILE_STORAGE = 'aox_dr.settings.storage_backends.MediaStorage'
MEDIA_URL_CUSTOM = 'https://%s/%s' % (AWS_S3_CUSTOM_DOMAIN, 'media')

# mandrill api key configuration

MANDRILL_API_KEY = '3AB-J9XHBMsL7RuojgfuSQ'  # Live Mandrill API Key.
SENDGRID_API_KEY = "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"

# stripe api configuration

STRIPE_API_KEY = 'sk_test_4gnS8VHbAWuZqh1brpLoz5hM'

# Go CardLess api configuration

GO_CARD_LESS_ACCESS_TOKEN = 'sandbox_OWlsmcUqFCLC2q8dxGrgsi44SsJ99mZOoHv0z1BM'
GO_CARD_LESS_ENVIRONMENT = 'sandbox'

# Go CardLess Redirect URL

GO_CARD_LESS_REDIRECT_URL = 'https://dev.stableprice.com/p/{chatbot_id}/payment/processing'

# Celery Configuration

CELERY_BROKER_URL = 'redis://localhost:6379/1'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'

CHAT_BOT_URL = 'https://dev.stableprice.com'
DASHBOARD_URL = 'https://dev.stableprice.com:4200'

REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = ('rest_framework.authentication.SessionAuthentication',) + \
                                                   REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES']

RAVEN_CONFIG['environment'] = ENV
