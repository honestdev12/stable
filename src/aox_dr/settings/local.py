from aox_dr.settings.base import *

DEBUG = True

ENV = 'Local'

PAGE_CACHE_SECONDS = 1

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': os.environ.get('DB_PASS', 'a'),
        'HOST': os.environ.get('DB_HOST', 'localhost'),
        'PORT': 5432,
    }
}

# static files configuration

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static_dist'),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL_CUSTOM = 'http://localhost:8000/media'

# mandrill api configuration

MANDRILL_API_KEY = 'cIVv66RwFNbqdQR4FWM63w'

# stripe api configuration

STRIPE_API_KEY = 'sk_test_4gnS8VHbAWuZqh1brpLoz5hM'

# Go CardLess api configuration

GO_CARD_LESS_ACCESS_TOKEN = 'sandbox_OWlsmcUqFCLC2q8dxGrgsi44SsJ99mZOoHv0z1BM'
GO_CARD_LESS_ENVIRONMENT = 'sandbox'

# Go CardLess Redirect URL

GO_CARD_LESS_REDIRECT_URL = 'http://localhost:3000/p/{chatbot_id}/payment/processing'

# Celery Configuration

CELERY_BROKER_URL = 'sqla+postgresql://postgres:a@localhost/postgres'

# Websites url configurations

CHAT_BOT_URL = 'http://localhost:3000'
DASHBOARD_URL = 'http://localhost:4200'

REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] + (
    'rest_framework.authentication.SessionAuthentication',
)

RAVEN_CONFIG['environment'] = ENV

CELERY_TASK_ALWAYS_EAGER = True
