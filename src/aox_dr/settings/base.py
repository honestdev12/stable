"""Django settings for aox_dr project."""

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))  # remove /sswmain/settings to get base folder

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ajsdgas7&*kosdsa21[]jaksdhlka-;kmcv8l$#diepsm8&ah^'

DEBUG = True

MAX_FILE_UPLOAD_SIZE = "104857600"

DATA_UPLOAD_MAX_NUMBER_FIELDS = 10240

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'django.contrib.sessions',
    # 'django.contrib.admin',
    'administration.apps.OTPAdminConfig',
    'django.contrib.humanize',

    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',

    'django_twilio',
    'djrill',
    'corsheaders',
    'storages',
    'after_response',
    'django_extensions',
    'cacheback',
    'django_filters',
    'django_fsm',
    'fsm_admin',
    'django_fsm_log',
    'django_celery_beat',
    'django_otp',
    'django_otp.plugins.otp_totp',
    'django_otp.plugins.otp_hotp',
    'django_otp.plugins.otp_static',

    'pricing',
    'CBAPIs',
    'Corporate',
    'administration',
    'overview',
    'CBcontrol',
    'CRM',
    'Partner',
    'Borderaux',
    'Broker',
    'SalesPipeline'
)

MIDDLEWARE = (
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'corsheaders.middleware.CorsPostCsrfMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

ROOT_URLCONF = 'aox_dr.urls'

WSGI_APPLICATION = 'aox_dr.wsgi.application'

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

ACCOUNT_ACTIVATION_DAYS = 7  # days

# ############# Django cache configuration ###################

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'STABLE_CACHE',
    }
}

# ############# REST FRAMEWORK ###################

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
    {
        'NAME': 'aox_dr.validators.CustomPasswordValidator',
    },
]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'aox_dr.paginator.StableCustomPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
    ),
    'COERCE_DECIMAL_TO_STRING': False,
    'EXCEPTION_HANDLER': 'aox_dr.exception.custom_exception_handler'
}

REST_AUTH_SERIALIZERS = {
    'USER_DETAILS_SERIALIZER': 'administration.serializer.UserSerializer',
    'PASSWORD_RESET_SERIALIZER': 'aox_dr.serializer.CustomPasswordResetSerializer'
}

AUTH_USER_MODEL = 'administration.AuthUser'

OLD_PASSWORD_FIELD_ENABLED = True

# ################# Product List ####################

CURRENCY_CONVERSION_RULE = {
    'UK': {
        'AN Fertiliser': {
            'multiplier': 100,
            'unit': 'Pound',
            'symbol': '£'
        },
        'Red Diesel': {
            'multiplier': 1,
            'unit': 'Pence',
            'symbol': '£'
        },
        'Lamb': {
            'multiplier': 1,
            'unit': 'Pence',
            'symbol': 'p'
        },
        'Beef': {
            'multiplier': 1,
            'unit': 'Pence',
            'symbol': 'p'
        },
        'Pork': {
            'multiplier': 1,
            'unit': 'Pence',
            'symbol': 'p'
        },
        'Rapeseed': {
            'multiplier': 100,
            'unit': 'Pound',
            'symbol': '£'
        },
        'Feed Wheat': {
            'multiplier': 100,
            'unit': 'Pound',
            'symbol': '£'
        },
        'Feed Barley': {
            'multiplier': 100,
            'unit': 'Pound',
            'symbol': '£'
        },
        'Milling Wheat': {
            'multiplier': 100,
            'unit': 'Pound',
            'symbol': '£'
        },
        'Milk': {
            'multiplier': 1,
            'unit': 'Pence',
            'symbol': 'p'
        }
    }
}

PRODUCT_NAME_JSON = {
    'Pig': 'Pork',
    'OSR': 'Rapeseed',
    'Lamb Deadweight': 'Lamb',
    'Deadweight Cattle': 'Beef'
}

# ################### Third Party Credentials #########################

TWILIO_ACCOUNT_SID = 'ACb2f949397fbf28e019ae376c487fa5bc'

TWILIO_AUTH_TOKEN = 'ab34bce7d063580b5b0376405f7efc86'

EMAIL_BACKEND = 'djrill.mail.backends.djrill.DjrillBackend'

DEFAULT_FROM_MAIL = 'stable@stableprice.com'
STABLE_SUPPORT_MAIL = 'support@stableprice.com'

# ################# CORS Configuration ###################

CORS_ORIGIN_ALLOW_ALL = True

# ######## Raven Confs ###########

RAVEN_CONFIG = {
    # 'dsn': 'https://18e3d0860f924027bbc71aef003a6ba3:f544e64cd3794e73a9db2df05a04e6ac@sentry.io/1272352',
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry', 'console'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'tags': {'custom-tag': 'x'},
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': True,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
    },
}

INSTALLED_APPS = INSTALLED_APPS + ('raven.contrib.django.raven_compat',)

MIDDLEWARE = MIDDLEWARE + (
    'raven.contrib.django.raven_compat.middleware.Sentry404CatchMiddleware',
    'raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware',
)

STABLE_COMMISSION_PERCENT = 20

UNIQUE_MARKET_REFERRENCE = 'B0750RARSP1900807'
