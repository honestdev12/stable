from aox_dr.settings.base import *  # NOQA (ignore all errors on this line)

DEBUG = False

ENV = 'Staging'

PAGE_CACHE_SECONDS = 1

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'aox_backend',
        'PASSWORD': '31.aox_stable',
        'HOST': 'aox-be.cz06q2uo7ybm.eu-west-1.rds.amazonaws.com',
        'PORT': 5432,
    }
}

# Keep this secret! Maybe as environmental variable.
AWS_ACCESS_KEY_ID = 'AKIAJFBGLNO6XVXJ2KCQ'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'stable--prod'
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_LOCATION = 'static'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]
STATIC_URL = 'https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

DEFAULT_FILE_STORAGE = 'aox_dr.settings.storage_backends.MediaStorage'
MEDIA_URL_CUSTOM = 'https://%s/%s' % (AWS_S3_CUSTOM_DOMAIN, 'media')

# mandrill api key configuration

MANDRILL_API_KEY = '3AB-J9XHBMsL7RuojgfuSQ'  # Live Mandrill API Key.

# stripe api configuration

STRIPE_API_KEY = 'sk_test_4gnS8VHbAWuZqh1brpLoz5hM'

# Go CardLess api configuration

GO_CARD_LESS_ACCESS_TOKEN = 'sandbox_OWlsmcUqFCLC2q8dxGrgsi44SsJ99mZOoHv0z1BM'
GO_CARD_LESS_ENVIRONMENT = 'sandbox'

# Go CardLess Redirect URL

GO_CARD_LESS_REDIRECT_URL = 'https://cb.stableprice.com/p/{chatbot_id}/payment/processing'

# Celery Configuration

CELERY_BROKER_URL = 'sqla+postgresql://aox_backend:31.aox_stable' \
                    '@aox-be.cz06q2uo7ybm.eu-west-1.rds.amazonaws.com/postgres'

# Websites url configurations

CHAT_BOT_URL = 'https://cb.stableprice.com'
DASHBOARD_URL = 'https://dash.stableprice.com'

RAVEN_CONFIG['environment'] = ENV
