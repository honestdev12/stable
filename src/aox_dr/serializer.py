from rest_auth.serializers import PasswordResetSerializer
from django.conf import settings
from .forms import CustomPasswordResetForm


class CustomPasswordResetSerializer(PasswordResetSerializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    password_reset_form_class = CustomPasswordResetForm

    def get_email_options(self):
        """Override this method to change default e-mail options"""
        return {
            'from_email': getattr(settings, 'DEFAULT_FROM_MAIL')
        }