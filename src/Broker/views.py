import json
from decimal import Decimal

from django.utils import timezone
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from administration.models import AuthUser
from aox_dr.exception import StableAPIException
from CBAPIs.models import (
    Category,
    Market,
)
from CBAPIs.utils import send_broker_quote_mail
from CRM.models import (
    Farmer,
    Transaction,
)
from CRM.serializer import FarmerSerializer
from CRM.tasks import set_quote_timeout
from Partner.models import Partner
from pricing.models import PricingResults
from pricing.utils import (
    convert_string_to_date,
)


# Create your views here.
class BrokerBuyView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        if not request.user.is_broker:
            return Response({'error': 'You are not able to request this data'}, status=status.HTTP_200_OK)

        policy = json.loads(request.data.get('policy'))
        farmer = json.loads(request.data.get('farmer'))
        user = json.loads(request.data.get('user'))

        try:
            if AuthUser.objects.filter(email=user['email']).exists():
                return Response({'error': 'Farmer with this email address already exist'}, status=status.HTTP_200_OK)

            partner = Partner.objects.get(company_admin=request.user)

            farmer_name = user['full_name'].split(' ')

            new_user = AuthUser(
                first_name=farmer_name[0],
                last_name=farmer_name[1] if len(farmer_name) > 1 else "",
                email=user['email'],
                username=user['email'],
                password=user['email'],
                is_active=False,
                is_farmer=True,
                phone=user['phone']
            )
            new_user.save()

            new_farmer = Farmer(
                country='UK',
                user=new_user
            )
            new_farmer.save()

            new_farmer = FarmerSerializer(new_farmer, data=farmer, partial=True)
            if new_farmer.is_valid():
                new_farmer.save()
            else:
                Farmer.objects.filter(user=new_user).delete()
                AuthUser.objects.filter(email=new_user.email).delete()
                return Response(
                    {
                        'error': 'Something went wrong. Please try again later or contact Stable Admin'
                    },
                    status=status.HTTP_200_OK
                )

            price_obj = PricingResults.objects.filter(
                prod_name=policy['categoryName'],
                country=partner.country,
                style=policy['protectionStyle'],
                stop=Decimal(str(policy['stopPrice'])),
                strike=Decimal(str(policy['startPrice'])),
                policy_start_date=policy['startDate'],
                policy_end_date=policy['endDate']
            ).first()

            price_per_unit = Decimal(price_obj.price)
            premium = price_per_unit * Decimal(policy['quantity'])
            total_amount = premium * Decimal(partner.tax) + premium
            diff_start_stop_price = abs(Decimal(policy['stopPrice']) - Decimal(policy['startPrice']))
            potential_loss = diff_start_stop_price * Decimal(policy['quantity'])

            quote_end_time = timezone.now() + timezone.timedelta(hours=72)

            category = Category.objects.get(country=partner.country, name=policy['categoryName'])
            market = Market.objects.get(id=category.market_id)
            purchase = Transaction(
                farmer=new_user,
                partner=partner,
                start_date=convert_string_to_date(policy['startDate']),
                stop_date=convert_string_to_date(policy['endDate']),
                market=market,
                category=policy['categoryName'],
                quantity=policy['quantity'],
                metric=policy['unit'],
                protection_duration=policy['protectionDuration'],
                protection_style=policy['protectionStyle'],
                protection_direction=policy['protectionDirection'],
                start_price=policy['startPrice'],
                stop_price=policy['stopPrice'],
                price_per_unit=price_per_unit,
                tax=partner.tax,
                farmer_saving=0,
                monthly_cost=total_amount / int(policy['protectionDuration']),
                total_cost=total_amount,
                premium=policy['premium'],
                commission=100,
                potential_loss=potential_loss,
                payment_style='Payment on Account',
                country=partner.country,
                quote_expiration_date=quote_end_time
            )

            purchase.save()

            set_quote_timeout.apply_async(args=[purchase.id], eta=quote_end_time)

            send_broker_quote_mail(
                user=new_user,
                purchase_id=purchase.id,
                quantity=purchase.quantity,
                unit=purchase.metric,
                market=purchase.market.name,
                category=purchase.category,
                index_owner=purchase.category.index.owner,
                direction=purchase.protection_direction,
                closing_date=policy['endDate'],
                current_cost=total_amount,
                price_per_quantity=price_per_unit,
                start_price=purchase.start_price,
                stop_price=purchase.stop_price,
            )

            return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)
