from django.conf.urls import url

from Broker.views import BrokerBuyView

urlpatterns = [
    # APIs for buy policy
    url(r'^buy/', BrokerBuyView.as_view()),
]
