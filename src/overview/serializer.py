from rest_framework import serializers
from .models import AvailableCountries


class AvailableCountriesSerializer(serializers.ModelSerializer):

    class Meta:
        model = AvailableCountries
        fields = '__all__'
