import collections
import datetime
import gc
import itertools
import json
import urllib
from collections import Counter
from datetime import timedelta
from decimal import Decimal
from os import cpu_count

from django.core.cache import cache
from django.db.models import (
    Count,
    DecimalField,
    F,
    Func,
    Q,
    Sum,
)
from django.db.models.functions import TruncMonth
from raven.contrib.django.models import client
from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from aox_dr.exception import StableAPIException
from CBAPIs.models import (
    Category,
    purchase_log,
    reg_log,
)
from CRM.models import (
    ComplaintLogs,
    Farmer,
    Transaction,
)
from CRM.serializer import TransactionSerializer
from Partner.models import (
    Agents,
    Partner,
)
from sdk.back_end_sdk import (
    get_dash_board_cards_KPIs,
    get_risk_pools,
    get_RMS,
    how_much_left_list,
)
from .models import AvailableCountries
from .serializer import AvailableCountriesSerializer
from .utils import risk_pool_to_FE_converter


def prod_zip_par(qset, all_purchase):
    zip_tuples = []
    for user in qset:
        transactions = filter(lambda x: x["userID"] == user.userID, all_purchase)
        for e in transactions:

            zip_tuples.append((user.zip, e["indexName"]))
    return zip_tuples


def call(url, data):
    req = urllib.request.Request(url)
    req.add_header('Content-Type', 'application/json')

    response = urllib.request.urlopen(req, data=json.dumps(data).encode('utf8'))

    return response.read().decode('utf8')


class IndexView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        now = datetime.datetime.now()
        twelve_month_ago = now - timedelta(days=365)

        zip_prod = {}
        step = 500
        qset = reg_log.objects.filter(created_at__gte=twelve_month_ago)[:200]

        zip_list = [e.zip for e in qset]
        zip_dict = Counter(zip_list)

        size_list = [float(e.farmSize) for e in qset]
        size_dict = Counter([n - n % step + step for n in size_list])

        zip_tuples = []
        chunk_size = 100
        qset_list = [qset[i:i + chunk_size] for i in range(0, len(qset), chunk_size)]

        from multiprocessing import Pool
        pool = Pool(cpu_count())

        purchase_all = list(purchase_log.objects.values("userID", "indexName"))

        zip_tuples_par = pool.starmap(prod_zip_par, itertools.product(qset_list, [purchase_all]))
        pool.close()
        pool.join()
        for i in zip_tuples_par:
            zip_tuples += i

        for data in Counter(zip_tuples).items():

            zip_prod[data[0][0]] = {}
            zip_prod[data[0][0]][data[0][1]] = data[1]

        gc.collect()

        map_data = zip_dict
        size_data = collections.OrderedDict(sorted(size_dict.items()))
        zip_prod = zip_prod

        postcodes = list(map_data.keys())

        postcode_list = [postcodes[i:i + 60] for i in range(0, len(postcodes), 60)]
        results = []  # result from the postcode API
        for postcode_chunk in postcode_list:
            coords = call(url='https://api.postcodes.io/postcodes', data={"postcodes": postcode_chunk})
            results_chunk = json.loads(coords)['result']
            results = results + results_chunk
        dist_dict = {}
        country_dict = {}  # dict of country
        zip_map = {}

        for place in results:
            try:
                zip_map[place['result']['postcode']] = [place['result']["european_electoral_region"]]
                # coord of each ciry
                country = place['result']["european_electoral_region"]
                try:
                    country_dict[country] += 1
                except Exception as e:
                    client.captureException()
                    country_dict[country] = 1

                dist_dict[place['result']['admin_district']] = [place['result']["longitude"],
                                                                place['result']["latitude"],
                                                                map_data[place['result']['postcode']]]

            except Exception as e:
                client.captureException()

        zip_map = {k: v for k, v in zip_map.items() if v[0]}

        location_chart_data = []
        for key, value in country_dict.items():
            if key is None:
                key = "Other"
            location_chart_data.append(
                {
                    "name": key,
                    "value": value,
                }
            )

        farm_size_chart_data = []
        for key, value in size_data.items():
            farm_size_chart_data.append(value)

        context = {
            "dist_dict": dist_dict,
            "location_chart_data": location_chart_data,
            'farm_size_chart_data': farm_size_chart_data,
            'zip_prod': zip_prod,
            'zip_map': zip_map
        }

        return Response(context, status=status.HTTP_200_OK)


class CardsDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            total_user_chart_data = []
            contracts_chart_data = []

            today = datetime.datetime.today()
            year = today.year
            month = today.month

            first = today.replace(day=1)
            last_month = first - datetime.timedelta(days=1)

            if request.user.is_superuser or request.user.is_underwriter or request.user.is_customer_supporter:

                purchased = Transaction.objects.filter(purchased=True)
                total_users_count = purchased.distinct('farmer_id').count()

                total_user_chart_query = purchased.annotate(
                    month=TruncMonth('quote_date')
                ).values(
                    'month'
                ).annotate(r=Count('farmer__id', distinct=True)).values('month',
                                                                        'r').order_by(
                    '-month')[:12]
                total_user_chart_query = reversed(total_user_chart_query)

                earned_commission = purchased.aggregate(Sum('commission'))

                earned_commission_query = purchased.annotate(
                    month=TruncMonth('quote_date')
                ).values(
                    'month'
                ).annotate(r=Sum('commission')).values('month', 'r').order_by('-month')[
                                          :12]
                earned_commission_query = reversed(earned_commission_query)

                this_month_commission = purchased.filter(
                    quote_date__year__gte=year,
                    quote_date__month__gte=month
                ).aggregate(Sum('commission'))

                last_month_commission = purchased.filter(
                    quote_date__year__gte=last_month.year,
                    quote_date__month__gte=last_month.month
                ).aggregate(Sum('commission'))

                contracts_count = purchased.count()

                contracts_chart_query = purchased.annotate(
                    month=TruncMonth('quote_date')
                ).values('month').annotate(
                    r=Count('id')
                ).values(
                    'month',
                    'r'
                ).order_by('-month')[:12]

                contracts_chart_query = reversed(contracts_chart_query)

                live_contracts_count = purchased.filter(
                    owed_status='Running',
                ).count()

                this_month_live_contracts = purchased.filter(
                    owed_status='Running',
                    quote_date__year__gte=year,
                    quote_date__month__gte=month
                ).count()
            else:
                partner = Partner.objects.get(company_admin=request.user)
                partner_purchased = Transaction.objects.filter(purchased=True, partner=partner)

                total_users_count = partner_purchased.distinct('farmer__id').count()

                total_user_chart_query = partner_purchased.annotate(
                    month=TruncMonth('quote_date')
                ).values(
                    'month'
                ).annotate(
                    r=Count('farmer__id', distinct=True)
                ).values(
                    'month',
                    'r'
                ).order_by('-month')[:12]

                total_user_chart_query = reversed(total_user_chart_query)

                earned_commission = partner_purchased.aggregate(Sum('commission'))
                earned_commission_query = partner_purchased.annotate(
                    month=TruncMonth('quote_date')
                ).values(
                    'month'
                ).annotate(
                    r=Sum('commission')
                ).values(
                    'month',
                    'r'
                ).order_by('-month')[:12]
                earned_commission_query = reversed(earned_commission_query)

                this_month_commission = partner_purchased.filter(
                    quote_date__year__gte=year,
                    quote_date__month__gte=month
                ).aggregate(Sum('commission'))
                last_month_commission = partner_purchased.filter(
                    quote_date__year__gte=last_month.year,
                    quote_date__month__gte=last_month.month
                ).aggregate(Sum('commission'))

                contracts_count = partner_purchased.count()

                contracts_chart_query = partner_purchased.annotate(
                    month=TruncMonth('quote_date')
                ).values(
                    'month'
                ).annotate(
                    r=Count('id')
                ).values(
                    'month',
                    'r'
                ).order_by('-month')[:12]
                contracts_chart_query = reversed(contracts_chart_query)

                live_contracts_count = partner_purchased.filter(
                    owed_status='Running'
                ).count()

                this_month_live_contracts = partner_purchased.filter(
                    owed_status='Running',
                    quote_date__year__gte=year,
                    quote_date__month__gte=month
                ).count()

            total_user = 0
            for user_month in total_user_chart_query:
                total_user += int(user_month['r'])
                total_user_chart_data.append({
                    'x': user_month['month'].strftime('%B %Y'),
                    'y': user_month['r']
                })

            total_contracts = 0
            for contract in contracts_chart_query:
                total_contracts += int(contract['r'])
                contracts_chart_data.append({
                    'x': contract['month'].strftime('%B %Y'),
                    'y': contract['r']
                })

            total_commission = 0
            for contract in earned_commission_query:
                total_commission += int(contract['r'])

            this_month_commission = Decimal(this_month_commission['commission__sum']) \
                if this_month_commission['commission__sum'] else 0
            last_month_commission = Decimal(last_month_commission['commission__sum']) \
                if last_month_commission['commission__sum'] else 0

            if last_month_commission == 0:
                commission_percent_diff = 0
                commission_status = ''
            else:
                commission_percent_diff = int(this_month_commission / last_month_commission * 100 - 100)
                if commission_percent_diff > 0:
                    commission_status = 'up'
                else:
                    commission_status = 'down'

            context = {
                'overview_cards_data': {
                    'total_users_count': str(total_users_count),
                    'total_users_chart_data': total_user_chart_data,
                    'average_users_count': str(int(int(total_user) / 12)) if total_user else 0,
                    'earned_commission': str(earned_commission['commission__sum'])
                    if earned_commission['commission__sum'] else 0,
                    'average_commission': int(int(total_commission) / 12) if total_commission else 0,
                    'commission_percent_diff': commission_percent_diff,
                    'commission_status': commission_status,
                    'contracts_count': str(contracts_count),
                    'contracts_chart_data': contracts_chart_data,
                    'average_contracts': str(int(int(total_contracts) / 12)) if total_contracts else 0,
                    'live_contracts_count': live_contracts_count,
                    'this_month_live_contracts': this_month_live_contracts
                }
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class CommissionChartDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            partner = Partner.objects.get(company_admin=request.user)
            if request.user.is_superuser or request.user.is_underwriter \
                    or request.user.is_customer_supporter:
                context = {
                    'commission_chart_data': {
                        'axis_data': [],
                        'value': []
                    }
                }
                return Response(context, status=status.HTTP_200_OK)

            chart_data_query = Transaction.objects.filter(
                partner=partner,
                purchased=True
            ).annotate(
                month=TruncMonth('quote_date')
            ).values('month') \
                .annotate(c=Sum('commission'), r=Sum('premium'), s=Sum('farmer_saving')).values('month', 'c', 'r', 's')

            commission_chart_data_query = chart_data_query.order_by('month')
            commission_ranking_query = chart_data_query.order_by('-c')[:7]
            revenue_ranking_query = chart_data_query.order_by('-r')[:7]

            commission_chart_data = []
            revenue_chart_data = []
            commission_ranking_data = []
            revenue_ranking_data = []

            for data in commission_chart_data_query:
                commission_chart_data.append(
                    {
                        'month': data['month'].strftime('%b %Y'),
                        'commission': data['c'] / 100,
                        'farmer_save': data['s'] / 100,
                    }
                )
                revenue_chart_data.append(
                    {
                        'x': data['month'].strftime('%B %Y'),
                        'y': data['r']
                    }
                )

            for data in commission_ranking_query:
                commission_ranking_data.append(
                    {
                        'month': data['month'].strftime('%B %Y'),
                        'value': data['c']
                    }
                )

            for data in revenue_ranking_query:
                revenue_ranking_data.append(
                    {
                        'month': data['month'].strftime('%B %Y'),
                        'value': data['r']
                    }
                )

            context = {
                'chart_data': {
                    'commission_chart_data': commission_chart_data,
                    'revenue_chart_data': revenue_chart_data,
                    'commission_ranking_data': commission_ranking_data,
                    'revenue_ranking_data': revenue_ranking_data
                }
            }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class ProductAvailabilityChartDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            country = request.GET.get('country')
            if not country:
                return Response({'error': 'Country not defined'}, status=status.HTTP_200_OK)

            # Get a list of all commodities in the country
            products = Category.objects.filter(country=country)

            # Query SDK to get list of all Risk Pools
            risk_pools = get_risk_pools()
            availability = how_much_left_list(risk_pools)

            availabilityArray = []

            for counter, commodity in enumerate(availability):
                divider = 1
                if availability[counter][0]['Currency'] == "Pence":
                    divider = 100

                availabilityArray.append({
                    'Commodity': availability[counter][0]['Risk Pool Name'],
                    'Supply': availability[counter][0]['Current Total Risk Capital'] / divider,
                    'Demand': (availability[counter][0]['Current Total Risk Capital'] -
                               availability[counter][0]['Available Risk Capitals']) / divider,
                    'DS': (availability[counter][0]['Current Total Risk Capital'] -
                           availability[counter][0]['Available Risk Capitals']) /
                          availability[counter][0]['Current Total Risk Capital']
                })

            for commodity in availabilityArray:
                commodity['Commodity'] = risk_pool_to_FE_converter(commodity['Commodity'])

            context = {
                'product_availability_chart_data': {
                    'demand_supply_data': availabilityArray
                }
            }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class AgentsChartDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if not request.user.is_partner:
                context = {
                    'agents_chart_data': {
                        'axis_data': [],
                        'value': []
                    }
                }
            else:
                chart_axis = []
                chart_data = []

                today = datetime.datetime.now()
                year = today.year
                month = today.month

                transactions = Agents.objects.annotate(
                    month_commission=Sum(
                        'transaction__commission',
                        filter=Q(quote_date__year__gte=year) | Q(quote_date__month__gte=month)
                    )
                ).order_by('month_commission')[:10]

                for transaction in transactions:
                    chart_axis.append(transaction.name)
                    chart_data.append(transaction.month_commission)

                context = {
                    'agents_chart_data': {
                        'axis_data': chart_axis,
                        'value': chart_data
                    }
                }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class ComplaintsInfoView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if not request.user.is_superuser and not request.user.is_customer_supporter:
                return Response(
                    {'error': 'Stable Admin or Customer Staff User can see this data'},
                    status=status.HTTP_200_OK
                )

            complaints_data = ComplaintLogs.objects.aggregate(
                outstanding=Count(
                    'id',
                    filter=Q(status='New Complaint'),
                    distinct=True
                ),
                in_progress=Count(
                    'id',
                    filter=Q(status='In Progress'),
                    distinct=True
                ),
                resolved=Count(
                    'id',
                    filter=Q(status='Resolved'),
                    distinct=True
                ),
                total=Count(
                    'id',
                    distinct=True
                ))

            context = {
                'data': complaints_data,
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PartnerLastFiveQuotes(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if not request.user.is_superuser and not request.user.is_partner and not request.user.is_broker:
                return Response(
                    {'error': 'Only a Stable Admin or Partner can view this information'},
                    status=status.HTTP_200_OK
                )

            partner = get_object_or_404(Partner, company_admin=request.user)
            quote_histories = Transaction.objects.filter(partner=partner, purchased=False).order_by('-id')[:5]
            policy_histories = Transaction.objects.filter(partner=partner, purchased=True).order_by('-id')[:5]

            last_five_quotes = TransactionSerializer(quote_histories, many=True)
            last_five_policies = TransactionSerializer(policy_histories, many=True)

            context = {
                'last_five_quotes': last_five_quotes.data,
                'last_five_policies': last_five_policies.data,
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class ContractsSoldQuotesGeneratedView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if not request.user.is_superuser and not request.user.is_partner and not request.user.is_broker:
                return Response(
                    {'error': 'Only a Stable Admin or Partner can view this information'},
                    status=status.HTTP_200_OK
                )

            partner = Partner.objects.get(company_admin=request.user)

            transaction_history = Transaction.objects.filter(
                partner=partner
            ).annotate(
                month=TruncMonth('quote_date')
            ).values('month').annotate(
                policy=Count(
                    'purchased', filter=Q(purchased=True)
                ),
                quote=Count('purchased', filter=Q(purchased=False))
            ).values('month', 'policy', 'quote')

            transaction_data = transaction_history.order_by('month')

            transaction_chart_data = []

            for data in transaction_data:
                transaction_chart_data.append(
                    {
                        'month': data['month'].strftime('%b %Y'),
                        'purchased': data['policy'],
                        'quote': data['quote'],
                    }
                )

            context = {
                'chart_data': {
                    'transaction_chart_data': transaction_chart_data,
                }
            }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class AvailableCountriesView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            context = {
                'data': AvailableCountriesSerializer(AvailableCountries.objects.all(), many=True).data
            }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class CommodityLiabilityView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            data = Transaction.objects.values('category').annotate(
                value=Sum(
                    Func((
                                 F('start_price') - F('stop_price')
                         ) * F('quantity') / 100, function="ABS"),
                    output_field=DecimalField(),
                    filter=Q(purchased=True, owed_status='Running')
                )
            ).values('category__name', 'value')

            liability_per_product_data = []
            liability_per_product_value = []
            for product in data:
                liability_per_product_data.append(product['category__name'])
                if not product['value']:
                    liability_per_product_value.append(0)
                else:
                    liability_per_product_value.append(product['value'])

            context = {
                'liability_per_product_data': liability_per_product_data,
                'liability_per_product_value': liability_per_product_value
            }

            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class GetHotProductView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            data = Transaction.objects.filter(
                purchased=True,
                owed_status="Running"
            ).annotate(
                value=Count('id')
            ).values('category__name', 'value').distinct()

            context = {
                'hot_product_data': data
            }

            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class UnderWriterCardView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        """
        SDK to get values from Simon's system.
        """
        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        # Need to change these to correct values after they are changed.
        gross_premium_written = {
            "all_time": dash_kpi['gross_premium_written']['all_time'],
            "current_year": dash_kpi['gross_premium_written']['current_year'],
            "current_month": dash_kpi['gross_premium_written']['current_month']
        }

        context = {
            "dash_kpi": dash_kpi,
            "number_of_farmers": dash_kpi['number_of_farmers'],
            "number_of_contracts": dash_kpi['number_of_contracts'],
            "gross_premium_written": gross_premium_written,
            "claims_paid": dash_kpi['claims_paid'],
            "products_available": dash_kpi['number_of_products_available'],
            "total_risk_capital": dash_kpi['total_risk_capital'],
            "risk_capital_utilised": dash_kpi['risk_capital_utilised'],
            "free_risk_capital": dash_kpi['free_risk_capital']
        }

        return Response(context, status=status.HTTP_200_OK)


class SalesPartnerCardView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            partner = Partner.objects.get(company_admin=request.user)

            farmers_all_time = Farmer.objects.filter(user__company_name=partner.name).values('id').distinct().count()
            if not farmers_all_time:
                farmers_all_time = 0

            total_commission_earned = Transaction.objects.filter(purchased=True, partner=partner).aggregate(
                value=Sum(
                    F('commission') / 100,
                    output_field=DecimalField(),
                )
            )

            contracts_all_time = Transaction.objects.filter(
                purchased=True,
                partner=partner
            ).values('id').distinct().count()
            if not contracts_all_time:
                contracts_all_time = 0

            live_contracts = Transaction.objects.filter(
                purchased=True,
                partner=partner,
                owed_status="Running"
            ).values('id').distinct().count()
            if not live_contracts:
                live_contracts = 0

            context = {
                'farmers_all_time': farmers_all_time,
                'total_commission_earned': round(
                    total_commission_earned['value'], 2
                ) if total_commission_earned['value'] else 0,
                'contracts_all_time': contracts_all_time,
                'live_contracts': live_contracts
            }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class ROIView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='ROI_all')

        context = dash_kpi['ROI'][commodity][1].get(period, 0)

        return Response(context, status=status.HTTP_200_OK)


class LossRatioView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='Loss_ratio_all')

        context = dash_kpi['loss_ratios'][commodity][1].get(period, 0)

        return Response(context, status=status.HTTP_200_OK)


class PremiumRatioView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='premium_ratio_all')

        context = dash_kpi['premium_ratio'][commodity][1][period]

        return Response(context, status=status.HTTP_200_OK)


class GWPAllocationByContractLength(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='all')

        data = dash_kpi['gross_premium_written']['details']['by_maturity'][commodity]
        # index: data[f'Maturity_at_{index}'][period] if index <= 12 else 0 for index, data in enumerate(test.values())

        context = [
            {"months": 1, "value": data['Maturity_at_1'][period]},
            {"months": 2, "value": data['Maturity_at_2'][period]},
            {"months": 3, "value": data['Maturity_at_3'][period]},
            {"months": 4, "value": data['Maturity_at_4'][period]},
            {"months": 5, "value": data['Maturity_at_5'][period]},
            {"months": 6, "value": data['Maturity_at_6'][period]},
            {"months": 7, "value": data['Maturity_at_7'][period]},
            {"months": 8, "value": data['Maturity_at_8'][period]},
            {"months": 9, "value": data['Maturity_at_9'][period]},
            {"months": 10, "value": data['Maturity_at_10'][period]},
            {"months": 11, "value": data['Maturity_at_11'][period]},
            {"months": 12, "value": data['Maturity_at_12'][period]}
        ]

        return Response(context, status=status.HTTP_200_OK)


class GWPAllocationByProduct(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='all')

        data = dash_kpi['gross_premium_written']['details']['by_products']
        # index: data[f'Maturity_at_{index}'][period] if index <= 12 else 0 for index, data in enumerate(test.values())

        context = [
            {"commodity": 'GB_ANFertiliser', "value": data['GB_ANFertiliser'][period]},
            {"commodity": 'GB_DeadweightCattle', "value": data['GB_DeadweightCattle'][period]},
            {"commodity": 'GB_FeedBarley', "value": data['GB_FeedBarley'][period]},
            {"commodity": 'GB_FeedWheat', "value": data['GB_FeedWheat'][period]},
            {"commodity": 'GB_LambDeadweight', "value": data['GB_LambDeadweight'][period]},
            {"commodity": 'GB_Milk', "value": data['GB_Milk'][period]},
            {"commodity": 'GB_MillingWheat', "value": data['GB_MillingWheat'][period]},
            {"commodity": 'GB_OSR', "value": data['GB_OSR'][period]},
            {"commodity": 'GB_Pig', "value": data['GB_Pig'][period]},
            {"commodity": 'GB_RedDiesel', "value": data['GB_RedDiesel'][period]},
        ]

        return Response(context, status=status.HTTP_200_OK)


class GWPAllocationByOptionType(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='all')

        data = dash_kpi['gross_premium_written']['details']['by_style']
        # index: data[f'Maturity_at_{index}'][period] if index <= 12 else 0 for index, data in enumerate(test.values())

        context = [
            {"commodity": 'Asian Call BullSpread', "value": data['Asian Call BullSpread'][period]},
            {"commodity": 'Asian Put BearSpread', "value": data['Asian Put BearSpread'][period]},
            {"commodity": 'European Call BullSpread', "value": data['European Call BullSpread'][period]},
            {"commodity": 'European Put BearSpread', "value": data['European Put BearSpread'][period]},
        ]

        return Response(context, status=status.HTTP_200_OK)


class GWPAllocationByStrikeRatiosPut(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='all')

        data = dash_kpi['gross_premium_written']['details']['by_strike_ratios_put'][commodity]
        # index: data[f'Maturity_at_{index}'][period] if index <= 12 else 0 for index, data in enumerate(test.values())

        context = [
            {
                "strike_ratio": float(ratio.replace('Strike_at_', '').replace('_', '.')) * 100,
                "value": value[period]
            } for ratio, value in data.items()
        ]
        #
        # context = [
        #     {"strike_ratio": '60', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '61', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '62', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '63', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '64', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '65', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '66', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '67', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '68', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '69', "value": data['Strike_at_0_6'][period]},
        #     {"strike_ratio": '70', "value": data['Strike_at_0_6'][period]},
        # ]

        return Response(context, status=status.HTTP_200_OK)


class GWPAllocationByStrikeRatiosCall(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='all')

        data = dash_kpi['gross_premium_written']['details']['by_strike_ratios_call'][commodity]

        # This is not a very nice oneliner, we need to strip extra text, then turn the embedded decimal to a real
        # decimal value to then multiply by 100 for display
        context = [
            {
                "strike_ratio": str(Decimal(ratio.replace('Strike_at_', '').replace('_', '.')) * 100),
                "value": value.get(period, None)
            } for ratio, value in data.items()
        ]

        return Response(context, status=status.HTTP_200_OK)


class noContractsByMaximumLiabilityBucket(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):

        dash_kpi = cache.get(key='dashboard_kpis')
        if dash_kpi is None:
            dash_kpi = get_dash_board_cards_KPIs()
            cache.set(key='dashboard_kpis', value=dash_kpi, timeout=300)

        commodity = request.GET.get('commodity', default='all')
        period = request.GET.get('period', default='all')

        data = dash_kpi['gross_premium_written']['details']['by_max_liability'][commodity]
        # index: data[f'Maturity_at_{index}'][period] if index <= 12 else 0 for index, data in enumerate(test.values())

        context = [
            {"bucket": '0-50000', "value": data['0-50000'][period]},
            {"bucket": '50000-100000', "value": data['50000-100000'][period]},
            {"bucket": '100000-150000', "value": data['100000-150000'][period]},
            {"bucket": '150000-200000', "value": data['150000-200000'][period]},
        ]

        return Response(context, status=status.HTTP_200_OK)


class riskCapitalUsage(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        commodity = request.GET.get('commodity', default='')
        period = request.GET.get('period', default='')

        rms_data = get_RMS(commodity, period)

        # rms_data = cache.get(key='rms_data')
        # if rms_data is None:
        #     rms_data = get_RMS()
        #     cache.set(key='rms_data', value=rms_data, timeout=300)
        rms_data = rms_data[1]

        return Response(rms_data, status=status.HTTP_200_OK)
