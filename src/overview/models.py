from django.db import models

from administration.models import AuthUser
from Partner.models import Partner


class TwoFactorTokenHistory(models.Model):
    user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)
    code = models.CharField(max_length=6, null=True)
    date = models.DateTimeField(auto_now_add=True)


class AvailableCountries(models.Model):
    name = models.CharField(max_length=255, unique=True)
    currency = models.CharField(max_length=255)
    tax = models.DecimalField(max_digits=24, decimal_places=8)
