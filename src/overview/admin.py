from django.contrib import admin
from .models import AvailableCountries
# Register your models here.


class AvailableCountriesAdmin(admin.ModelAdmin):
    list_display = ['name', 'currency', 'tax']

admin.site.register(AvailableCountries, AvailableCountriesAdmin)
