"""
Converts returned values from Simon's system to our FE values.
Please note we need to add future countries here as well!
Such as AU_ GE_ FR_ etc etc
(Should get a config file from simon for this perhaps)
"""
def risk_pool_to_FE_converter(name):
    if name == "GB_DeadweightCattle":
        name = "Beef"
    if name == "GB_ANFertiliser":
        name = "AN Fertiliser"
    if name == "GB_FeedWheat":
        name = "Feed Wheat"
    if name == "GB_RedDiesel":
        name = "Red Diesel"
    if name == "GB_FeedBarley":
        name = "Feed Barley"
    if name == "GB_OSR":
        name = "Rapeseed"
    if name == "GB_Pig":
        name = "Pork"
    if name == "GB_LambDeadweight":
        name = "Lamb"
    if name == "GB_MillingWheat":
        name = "Milling Wheat"
    if name == "GB_Milk":
        name = "Milk"

    return name
