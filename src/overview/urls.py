from django.conf.urls import url
from overview import views as OverviewView

urlpatterns = [
    url(r'^$', OverviewView.IndexView.as_view()),
    url(r'^cards/', OverviewView.CardsDataView.as_view()),
    url(r'^get_commission_chart_data/', OverviewView.CommissionChartDataView.as_view()),
    url(r'^get_product_availability_chart_data/', OverviewView.ProductAvailabilityChartDataView.as_view()),
    url(r'^get_top_agents_chart_data/', OverviewView.AgentsChartDataView.as_view()),
    url(r'^get_complaints_info/', OverviewView.ComplaintsInfoView.as_view()),
    url(r'^get_partner_last_five_quotes/', OverviewView.PartnerLastFiveQuotes.as_view()),
    url(r'^get_contracts_sold_quotes_generated/', OverviewView.ContractsSoldQuotesGeneratedView.as_view()),
    url(r'^get_commodity_liability/', OverviewView.CommodityLiabilityView.as_view()),
    url(r'^get_hot_product/', OverviewView.GetHotProductView.as_view()),
    url(r'^get_underwriter_cards/', OverviewView.UnderWriterCardView.as_view()),
    url(r'^get_salespartner_cards/', OverviewView.SalesPartnerCardView.as_view()),

    url(r'^get_roi/', OverviewView.ROIView.as_view()),
    url(r'^get_lossratio/', OverviewView.LossRatioView.as_view()),
    url(r'^get_premiumratio/', OverviewView.PremiumRatioView.as_view()),
    url(r'^get_gwp_allocation_by_contract_length/', OverviewView.GWPAllocationByContractLength.as_view()),
    url(r'^get_gwp_allocation_by_product/', OverviewView.GWPAllocationByProduct.as_view()),
    url(r'^get_gwp_allocation_by_option_type/', OverviewView.GWPAllocationByOptionType.as_view()),
    url(r'^get_gwp_allocation_by_strike_ratios_put/', OverviewView.GWPAllocationByStrikeRatiosPut.as_view()),
    url(r'^get_gwp_allocation_by_strike_ratios_call/', OverviewView.GWPAllocationByStrikeRatiosCall.as_view()),
    url(r'^get_gwp_allocation_by_liability_bucket/', OverviewView.noContractsByMaximumLiabilityBucket.as_view()),
    url(r'^get_risk_capital_usage/', OverviewView.riskCapitalUsage.as_view()),


    # API for available countries and currencies
    url(r'^available_countries/', OverviewView.AvailableCountriesView.as_view()),
]
