from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig


class AdministrationConfig(AppConfig):
    name = 'administration'


class OTPAdminConfig(AdminConfig):
    default_site = 'django_otp.admin.OTPAdminSite'
