from django.conf.urls import url

from administration import views as admin_views

urlpatterns = [
    # Api for Faq
    url(r'^faq/section/', admin_views.FaqSectionView.as_view()),
    url(r'^faq/faq/', admin_views.FaqFaqView.as_view()),
    url(r'^faq/add/', admin_views.FaqAddView.as_view()),
    url(r'^faq/delete/(?P<faq_id>\d+)/$', admin_views.FaqDeleteView.as_view()),

    # Api for profile
    url(r'^enable_two_factor/', admin_views.TwoFactorView.as_view()),
    url(r'^check_newsletter_status/', admin_views.NewsletterView.as_view()),
    url(r'^update_newsletter_status/', admin_views.UpdateNewsletterView.as_view()),
    url(r'^profile/update/', admin_views.ProfileView.as_view()),
    url(r'^farmer/', admin_views.FarmerView.as_view()),
    url(r'^profile/upload_avatar/', admin_views.AvatarView.as_view()),
    url(r'^accept_t_c/', admin_views.AcceptsTermsAndConditions.as_view()),
    url(r'^broker_accept_t_c/', admin_views.BrokerAcceptsTermsAndConditions.as_view()),
    url(r'^get_profile_info/', admin_views.GetProfileInformation.as_view()),

    # API for User Type Lookup
    url(r'^customer_service/', admin_views.CustomerServiceUserListView.as_view()),

]
