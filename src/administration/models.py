from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.db import models
from django.utils import timezone


class AuthUserManager(BaseUserManager):
    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class AuthUser(AbstractBaseUser, PermissionsMixin):
    """
    Used for all authenticated user
    """

    first_name = models.CharField(db_column='first name', max_length=30, blank=True)
    last_name = models.CharField(db_column='last name', max_length=30, blank=True)
    username = models.CharField(db_column='username', max_length=254, unique=True,
                                help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.')
    email = models.EmailField(db_column='email address', max_length=254, unique=True)
    phone = models.CharField(db_column='phone number', max_length=30, null=True, blank=True)
    company_name = models.CharField(db_column='company name', max_length=254, null=True, blank=True)
    office_address = models.CharField(db_column='office address', max_length=254, null=True, blank=True)
    partner_option = models.CharField(db_column='partner option', max_length=254, null=True, blank=True)
    broker_id = models.CharField(db_column='broker id', max_length=254, null=True, blank=True)
    orders = models.IntegerField(db_column='order', null=True, blank=True)

    date_of_birth = models.DateField(default=timezone.now)

    avatar = models.ImageField(upload_to='user_avatar/', null=True, blank=True)

    address_1 = models.CharField(db_column='address 1', max_length=254, null=True, blank=True)
    address_2 = models.CharField(db_column='address 2', max_length=254, null=True, blank=True)
    town = models.CharField(db_column='town', max_length=254, null=True, blank=True)
    county = models.CharField(db_column='county', max_length=254, null=True, blank=True)
    post_code = models.CharField(db_column='post code', max_length=254, null=True, blank=True)

    is_staff = models.BooleanField(db_column='staff status', default=False,
                                   help_text='Designates whether the user can log into this admin site.')
    is_active = models.BooleanField(db_column='active status', default=True,
                                    help_text='Designates whether this user should be treated as active. '
                                              'Unselect this instead of deleting accounts.')
    is_two_factor = models.BooleanField(db_column='two_factor status', default=True,
                                        help_text='Designates whether this user has 2 Factor Authentication active. '
                                                  'True == active, false == not-active')
    is_newsletter = models.BooleanField(db_column='news_letter status', default=False,
                                        help_text='Designates whether this user has signed up for the news letter. '
                                                  'True == active, false == not-active')
    is_session_out = models.BooleanField(db_column='session timeout status', default=False,
                                         help_text='Designates whether this user has session timeout func. '
                                                   'True == active, false == not-active')
    accepted_t_c = models.BooleanField(db_column='terms and cookies accepted status', default=False,
                                       help_text='Designates whether this user accepted terms and cookies policy')

    is_underwriter = models.BooleanField(db_column='underwriter status', default=False,
                                         help_text='Designates whether the user can log into underwriter view.')

    is_customer_supporter = models.BooleanField(db_column='customer supporter status', default=False,
                                                help_text='Designates whether the user can log '
                                                          'into customer supporter view.')

    is_corporate = models.BooleanField(db_column='Corporate Status', default=False,
                                       help_text='Designates whether the user can log into Corporate ChartBot.')

    is_partner = models.BooleanField(db_column='Partner Status', default=False,
                                     help_text='Designates whether the user can log into Sales Partner View.')

    is_farmer = models.BooleanField(db_column='Farmer Status', default=False,
                                    help_text='Designates whether the user can log into Farmer View.')

    is_broker = models.BooleanField(db_column='Broker Status', default=False,
                                    help_text='Designates whether the user can log into Broker View.')

    registered_at = models.DateField(db_column='registered date', auto_now_add=True, null=True)
    objects = AuthUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    class Meta:
        db_table = 'dashboard_user'


class Section(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Faq(models.Model):
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    question = models.CharField(max_length=255)
    answer = models.TextField()
