from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token

from administration.models import (
    AuthUser,
    Faq,
    Section,
)


class UserAdmin(BaseUserAdmin):
    list_display = ['id', 'first_name', 'last_name', 'company_name', 'email', 'is_superuser', 'is_partner',
                    'is_underwriter', 'is_customer_supporter', 'is_farmer', 'is_broker',
                    'is_active', 'registered_at']
    ordering = ('id',)

    fieldsets = (
        (None, {
            'fields': ('username', 'password', 'is_active', 'is_two_factor', 'is_newsletter',
                       'accepted_t_c', 'is_session_out'
                       )
        }),
        ('Personal info', {
            'fields': ('avatar', 'first_name', 'last_name', 'email', 'phone', 'company_name',
                       'partner_option', 'orders', 'broker_id')
        }),
        ('Location info', {
            'fields': ('address_1', 'address_2', 'town', 'county', 'post_code')
        }),
        ('Permissions', {
            'fields': ('is_staff', 'is_superuser', 'is_partner', 'is_underwriter', 'is_customer_supporter',
                       'is_corporate', 'is_farmer', 'is_broker')
        }),
    )


class SectionAdmin(admin.ModelAdmin):
    list_display = ['name']


class FaqAdmin(admin.ModelAdmin):
    list_display = ['section', 'question', 'answer']


admin.site.register(AuthUser, UserAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(Faq, FaqAdmin)
admin.site.unregister(Group)
admin.site.unregister(Token)
