import datetime
import json
import uuid
from hashlib import md5

import requests
from django.contrib.auth.tokens import default_token_generator
from django.db.models import Q
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode as uid_decoder
from requests.auth import HTTPBasicAuth
from rest_condition import Or
from rest_framework import status
from rest_framework.generics import (
    get_object_or_404,
    ListAPIView,
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from aox_dr.exception import StableAPIException
from CBAPIs.utils import (
    confirm_to_sell_wrapper,
    sell_this_wrapper,
    send_broker_policy_mail,
)
from CRM.models import (
    Transaction,
)
from CRM.permissions import (
    IsBroker,
    IsCorporate,
    IsFarmer,
    IsPartner,
    IsSuperuser,
    IsUnderwriter,
)
from CRM.serializer import FarmerSerializer
from CRM.tasks import set_policy_runs_out
from Partner.models import Partner
from payment.tasks import success_payment
from .models import (
    AuthUser,
    Faq,
    Section,
)
from .serializer import (
    FaqSerializer,
    SectionSerializer,
    UserSerializer,
)


def signupNewsletter(user_email):
    data = {
        "email_address": user_email,
        "status": "subscribed",
    }
    requests.post(
        "https://us13.api.mailchimp.com/3.0/lists/1349b8956d/members/",
        auth=HTTPBasicAuth('anystring', '6d82be129741ebeb26f77432a5094bd7-us13'),
        data=json.dumps(data)
    )


def subscribeNewsletter(user_email):
    data = {
        "status": "subscribed",
    }
    try:
        requests.patch(
            "https://us13.api.mailchimp.com/3.0/lists/1349b8956d/members/" + user_email.hexdigest(),
            auth=HTTPBasicAuth('anystring', '6d82be129741ebeb26f77432a5094bd7-us13'),
            data=json.dumps(data)
        )
    except Exception as e:
        raise StableAPIException(e)


def unsubscribeNewsletter(user_email):
    data = {
        "status": "unsubscribed",
    }
    try:
        requests.patch(
            "https://us13.api.mailchimp.com/3.0/lists/1349b8956d/members/" + user_email.hexdigest(),
            auth=HTTPBasicAuth('anystring', '6d82be129741ebeb26f77432a5094bd7-us13'),
            data=json.dumps(data)
        )
    except Exception as e:
        raise StableAPIException(e)


class FaqSectionView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            section = SectionSerializer(
                Section.objects.all(),
                many=True
            )
            return Response(section.data, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class FaqFaqView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            section_id = request.GET['section_id'] if 'section_id' in request.GET else None
            if not section_id or section_id == 'undefined':
                return Response(
                    {'error': 'We did not receive a valid FAQ section with your request'},
                    status=status.HTTP_200_OK
                )

            section = get_object_or_404(Section, pk=section_id)

            faq = FaqSerializer(Faq.objects.filter(section=section), many=True)

            return Response(faq.data, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class FaqAddView(APIView):
    permission_classes = (IsAuthenticated, IsSuperuser)

    @staticmethod
    def post(request):
        try:
            section_id = request.data['section'] if 'section' in request.data else None
            if not section_id or section_id == 'undefined':
                return Response(
                    {'error': 'We did not receive a valid FAQ section with your request'},
                    status=status.HTTP_200_OK
                )

            question = request.data['question'] if 'question' in request.data else None
            if not question:
                return Response(
                    {'error': 'We did not receive a valid question with your request'},
                    status=status.HTTP_200_OK
                )

            answer = request.data['answer'] if 'answer' in request.data else None
            if not question:
                return Response(
                    {'error': 'We did not receive a valid answer with your request'},
                    status=status.HTTP_200_OK
                )

            faq = Faq(
                section=Section.objects.get(id=section_id),
                question=question,
                answer=answer
            )
            faq.save()

            section = SectionSerializer(
                Section.objects.all(),
                many=True
            )

            return Response(section.data, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class FaqDeleteView(APIView):
    permission_classes = (IsAuthenticated, IsSuperuser)

    @staticmethod
    def get(request, faq_id):
        try:

            Faq.objects.get(id=faq_id).delete()

            section = SectionSerializer(
                Section.objects.all(),
                many=True
            )

            return Response(section.data, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class TwoFactorView(APIView):
    permission_classes = (IsAuthenticated, Or(IsFarmer, IsPartner, IsCorporate, IsUnderwriter, IsBroker))

    @staticmethod
    def get(request):
        user = request.user
        user.is_two_factor = not user.is_two_factor
        user.save()
        return Response({'success': 'success'}, status=status.HTTP_200_OK)


class NewsletterView(APIView):
    permission_classes = (IsAuthenticated, Or(IsFarmer, IsPartner, IsCorporate, IsUnderwriter, IsBroker))

    @staticmethod
    def get(request):
        try:
            user_email = md5()
            user_email.update(request.user.email.encode('utf-8'))

            r = requests.get(
                "https://us13.api.mailchimp.com/3.0/lists/1349b8956d/members/" + user_email.hexdigest(),
                auth=HTTPBasicAuth('anystring', '6d82be129741ebeb26f77432a5094bd7-us13'),
            )
            response = r.content.decode("utf-8")
            json_response = json.loads(response)
            newsletter_status = json_response.get('status')

            # checkNewsletterStatus(request.user.email)

            return Response({'newsletter_status': newsletter_status}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UpdateNewsletterView(APIView):
    permission_classes = (IsAuthenticated, Or(IsFarmer, IsPartner, IsCorporate, IsUnderwriter, IsBroker))

    @staticmethod
    def get(request):
        requested_status = request.data.get('new_status', None)
        if not requested_status:
            return Response({'error': 'No new status was supplied'},
                            status=status.HTTP_200_OK)
        if requested_status not in ['subscribed', 'unsubscribed']:
            return Response({'error': 'An invalid new status was supplied'}, status=status.HTTP_200_OK)

        user_email = md5()
        user_email.update(request.user.email.encode('utf-8'))

        if requested_status == 'subscribed':
            newsletter_status = True
            subscribeNewsletter(user_email)
        else:
            newsletter_status = False
            unsubscribeNewsletter(user_email)

        user = request.user
        user.is_newsletter = newsletter_status
        user.save()

        return Response({'newsletter_status': newsletter_status}, status=status.HTTP_200_OK)


class ProfileView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            user = UserSerializer(request.user, data=request.data, partial=True)
            if user.is_valid():
                user.save()
            else:
                return Response({'error': user.errors}, status=status.HTTP_200_OK)
            return Response(UserSerializer(request.user).data, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class FarmerView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            user = FarmerSerializer(request.user.farmer)
            return Response(user.data, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    @staticmethod
    def post(request):
        try:
            user = FarmerSerializer(request.user.farmer, data=request.data, partial=True)
            if user.is_valid():
                user.save()
            else:
                return Response({'error': user.errors}, status=status.HTTP_200_OK)
            return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class AvatarView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if request.user.avatar:
                return Response(request.user.avatar, status=status.HTTP_200_OK)
            return Response({'error': 'Unable to locate this users avatar'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    @staticmethod
    def post(request):
        try:
            avatar = request.data.get('filepond')
            if not avatar:
                return Response({'error': 'No avatar was supplied'}, status=status.HTTP_200_OK)

            user = request.user
            if user.avatar:
                user.avatar.delete()

            user.avatar = avatar
            user.save()
            return Response(UserSerializer(request.user).data, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class AcceptsTermsAndConditions(APIView):

    @staticmethod
    def post(request):
        try:
            email = request.data['email']
            user = AuthUser.objects.get(email=email)
            user.accepted_t_c = True
            user.save()
            return Response({"success": "success"}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class BrokerAcceptsTermsAndConditions(APIView):

    @staticmethod
    def post(request):
        try:
            email = request.data.get('email')
            broker = request.data.get('broker')
            token = request.data.get('token_pass')
            policy = request.data.get('policy')

            try:
                broker_uid = force_text(uid_decoder(broker))
                user = AuthUser.objects.get(pk=broker_uid)
            except (TypeError, ValueError, OverflowError, AuthUser.DoesNotExist):
                return Response({'error': 'Invalid Broker'}, status=status.HTTP_200_OK)

            try:
                policy_uid = force_text(uid_decoder(policy))
                purchase = Transaction.objects.get(pk=policy_uid)
            except (TypeError, ValueError, OverflowError, Transaction.DoesNotExist):
                return Response({'error': 'Invalid Policy'}, status=status.HTTP_200_OK)

            if not default_token_generator.check_token(user, token):
                return Response({'error': 'Invalid Token'}, status=status.HTTP_200_OK)

            if purchase.farmer != user or user.email != email:
                return Response(
                    {
                        'error': 'You are trying to buy a policy that does not belong to you or maybe '
                                 'the email address you provide us is incorrect'
                    },
                    status=status.HTTP_200_OK
                )

            if purchase.purchased:
                return Response({'error': 'This policy already purchased'}, status=status.HTTP_200_OK)

            purchase.purchased = True
            purchase.payment_style = 'Paid'
            purchase.paid_at = datetime.datetime.now()

            response = sell_this_wrapper(purchase.id, request.user.id)
            if response['if_confirmed']:
                """
                Tell Simon's system that payment is going ahead.
                """
                confirm_to_sell_wrapper(response['_unique_trade_id'], purchase.pk)
                policy_end_date = purchase.stop_date + timezone.timedelta(days=31)
                task_response = set_policy_runs_out.apply_async(args=[purchase.id], eta=policy_end_date)
                purchase.unique_trade_id = response['_unique_trade_id']

                if task_response.task_id:
                    purchase.owed_status = 'Running'
                    purchase.contract_running_task_id = task_response.task_id

                purchase.save()

                # Farmer create new password and send Broker Policy Email
                user.is_active = True
                password = str(uuid.uuid4())
                user.set_password(password)
                user.save()
                send_broker_policy_mail(request, user.first_name, email, password)

                success_payment(request.user.id, purchase.id)
                return Response("success", status=status.HTTP_200_OK)
            else:
                return Response({"error": response['comment']}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GetProfileInformation(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if request.user.is_farmer:
                live_quotes = Transaction.objects.filter(
                    farmer=request.user,
                    purchased=False,
                    quote_expired=False
                ).count()
                live_policies = Transaction.objects.filter(
                    farmer=request.user,
                    purchased=True,
                    owed_status="Running"
                ).count()
            elif request.user.is_partner or request.user.is_broker:
                partner = Partner.objects.get(company_admin=request.user)
                if request.user.is_corporate:
                    live_quotes = Transaction.objects.filter(
                        Q(partner=partner) |
                        Q(corporate=request.user),
                        purchased=False,
                        quote_expired=False
                    ).count()
                    live_policies = Transaction.objects.filter(
                        Q(partner=partner) |
                        Q(corporate=request.user),
                        purchased=True,
                        owed_status="Running"
                    ).count()
                else:
                    live_quotes = Transaction.objects.filter(
                        partner=partner,
                        purchased=False,
                        quote_expired=False
                    ).count()
                    live_policies = Transaction.objects.filter(
                        partner=partner,
                        purchased=True,
                        owed_status="Running"
                    ).count()
            elif request.user.is_corporate:
                live_quotes = Transaction.objects.filter(
                    corporate=request.user,
                    purchased=False,
                    quote_expired=False
                ).count()
                live_policies = Transaction.objects.filter(
                    corporate=request.user,
                    purchased=True,
                    owed_status="Running"
                ).count()
            else:
                live_quotes = Transaction.objects.filter(
                    purchased=False,
                    quote_expired=False
                ).count()
                live_policies = Transaction.objects.filter(
                    purchased=True,
                    owed_status="Running"
                ).count()

            member_since = request.user.registered_at

            context = {
                'live_quotes': live_quotes,
                'live_policies': live_policies,
                'member_since': member_since
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class CustomerServiceUserListView(ListAPIView):
    queryset = AuthUser.objects.filter(is_customer_supporter=True)
    serializer_class = UserSerializer
    pagination_class = None
