# Generated by Django 2.0 on 2018-10-20 09:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0009_authuser_office_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='authuser',
            name='avatar',
            field=models.ImageField(blank=True, null=True, upload_to='user_avatar/'),
        ),
    ]
