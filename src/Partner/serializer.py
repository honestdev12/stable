import datetime

from django.db.models import Sum
from rest_framework import serializers

from administration.serializer import UserFarmerSerializer as UserSerializer
from CBAPIs.models import ChatBot
from CRM.models import Transaction
from .models import (
    Agents,
    DueDiligence,
    Farmers,
    Partner,
    Statement,
)


class PartnerSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    company_admin = UserSerializer(required=True)
    chat_bots = serializers.SerializerMethodField('get_all_chat_bots')

    @staticmethod
    def get_all_chat_bots(obj):
        chat_bots = list(ChatBot.objects.filter(sales_partner=obj).values_list('pk'))
        return chat_bots

    class Meta:
        model = Partner
        fields = '__all__'


class PartnerCBSerializer(serializers.ModelSerializer):

    class Meta:
        model = Partner
        fields = ['id', 'name', 'logo', 'is_active',
                  'is_pause', 'notes', 'country', 'currency', 'tax', 'voucher']


class StatementSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    partner = PartnerSerializer(required=True)

    class Meta:
        model = Statement
        fields = '__all__'


class DueDiligenceSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    partner = PartnerSerializer(required=True)

    class Meta:
        model = DueDiligence
        fields = '__all__'


class AgentSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    all_commission = serializers.SerializerMethodField('all_get_commission')
    month_commission = serializers.SerializerMethodField('month_get_commission')
    last_sales_date = serializers.SerializerMethodField('last_get_sales_date')
    company = PartnerSerializer(required=True)

    @staticmethod
    def all_get_commission(obj):
        affiliate_code = obj.affiliate_code

        all_commission = Transaction.objects.filter(
            purchased=True,
            affiliate__affiliate_code=affiliate_code,
        ).aggregate(Sum('commission'))
        return all_commission['commission__sum']

    @staticmethod
    def month_get_commission(obj):
        affiliate_code = obj.affiliate_code

        today = datetime.datetime.now()
        year = today.year
        month = today.month

        month_commission = Transaction.objects.filter(
            purchased=True,
            affiliate__affiliate_code=affiliate_code,
            quote_date__year__gte=year,
            quote_date__month__gte=month
        ).aggregate(Sum('commission'))
        return month_commission['commission__sum']

    @staticmethod
    def last_get_sales_date(obj):
        affiliate_code = obj.affiliate_code

        last_sales_date = Transaction.objects.filter(
            purchased=True,
            affiliate__affiliate_code=affiliate_code,
        ).order_by('-id')
        return last_sales_date[0].quote_date if last_sales_date else None

    class Meta:
        model = Agents
        fields = '__all__'


class CBMemberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Farmers
        fields = '__all__'
