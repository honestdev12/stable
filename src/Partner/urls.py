from django.conf.urls import url

from Partner.views import (
    AcceptCompanyView,
    ActivePartnersView,
    DeclineCompanyView,
    DeleteDueDiligenceView,
    DeletePartnerAgentsView,
    DeleteStatementFileView,
    DownloadPartnerMemberShipView,
    DownloadStatementFileView,
    GenerateChatBotView,
    GetCompanyDataView,
    GetCurrentLogoView,
    GetMaxSavings,
    PartnerAgentsView,
    PartnerMemberShipView,
    PartnerSignUpView,
    PartnerStatementView,
    PartnerValidationView,
    PauseCompanyView,
    SalesPartnerChatBot,
    SalesPartnerSignUpView,
    StatementView,
    UpdateCompanyDataView,
    UploadCompanyLogoView,
    UploadDueDiligenceView,
    UploadPartnerMemberShipView,
)

urlpatterns = [
    # Api for Partner Control
    url(r'^get_company_data/', GetCompanyDataView.as_view()),
    url(r'^accept_partner/', AcceptCompanyView.as_view()),
    url(r'^decline_partner/(?P<id>\d+)/$', DeclineCompanyView.as_view()),
    url(r'^pause_partner/(?P<partner_id>\d+)/$', PauseCompanyView.as_view()),
    url(r'^update_company_data/(?P<id>\d+)/$', UpdateCompanyDataView.as_view()),
    url(r'^upload_company_logo/', UploadCompanyLogoView.as_view()),
    url(r'^get_current_logo/', GetCurrentLogoView.as_view()),
    url(r'^partner_signup/', PartnerSignUpView.as_view()),
    url(r'^upload_due_diligence/', UploadDueDiligenceView.as_view()),
    url(r'^delete_due_diligence/(?P<due_diligence_file_id>\d+)/$', DeleteDueDiligenceView.as_view()),

    # Api for Sales Partner
    url(r'^become_sales_partner/', SalesPartnerSignUpView.as_view()),
    url(r'^generate_cb_sales_partner/', GenerateChatBotView.as_view()),
    url(r'^sales_partner_cb/', SalesPartnerChatBot.as_view()),

    # Api for Partner Performance
    url(r'^get_active_partners/(?P<country>[\w ]+)/$', ActivePartnersView.as_view()),

    # Api for Partner Statement
    url(r'^statement/', StatementView.as_view()),
    url(r'^get_partner_statements/(?P<partner_id>\d+)/$', PartnerStatementView.as_view()),
    url(r'^download_statement_file/(?P<statement_file_id>\d+)/$', DownloadStatementFileView.as_view()),
    url(r'^delete_statement_file/(?P<statement_file_id>\d+)/$', DeleteStatementFileView.as_view()),

    # Api for Partner Agents
    url(r'^partner_agents/', PartnerAgentsView.as_view()),
    url(r'^delete_partner_agent/(?P<agent_id>\d+)/$', DeletePartnerAgentsView.as_view()),

    # Api for CB partner validation
    url(r'^check/', PartnerValidationView.as_view()),

    # API for partner farmer csv upload
    url(r'^upload_member/', UploadPartnerMemberShipView.as_view()),
    url(r'^partner_member/', PartnerMemberShipView.as_view()),
    url(r'^download_member/', DownloadPartnerMemberShipView.as_view()),

    # API to get max_savings value for a partner
    url(r'^max_savings/', GetMaxSavings.as_view()),
]
