from django.contrib import admin

from .models import (
    Agents,
    DueDiligence,
    Farmers,
    Partner,
    Statement,
)


class PartnerCompanyAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'max_savings', 'is_active', 'is_pause']


class PartnerStatementAdmin(admin.ModelAdmin):
    list_display = ['name', 'upload_at', 'partner']


class DueDiligenceAdmin(admin.ModelAdmin):
    list_display = ['name', 'upload_at', 'partner', 'file']


class PartnerAgentsAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'company', 'affiliate_code']


class PartnerFarmersAdmin(admin.ModelAdmin):
    list_display = ['chatbot', 'reference_id', 'postcode', 'business_name']


admin.site.register(Partner, PartnerCompanyAdmin)
admin.site.register(Statement, PartnerStatementAdmin)
admin.site.register(Agents, PartnerAgentsAdmin)
admin.site.register(Farmers, PartnerFarmersAdmin)
admin.site.register(DueDiligence, DueDiligenceAdmin)
