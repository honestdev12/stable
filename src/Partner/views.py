import datetime
import json
import re
import uuid

from django.conf import settings
from django.core.files.storage import default_storage
from django.core.paginator import (
    EmptyPage,
    PageNotAnInteger,
    Paginator,
)
from django.db.models import Sum
from django.db.models.functions import TruncMonth
from django.utils.crypto import get_random_string
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from administration.models import AuthUser
from aox_dr.exception import StableAPIException
from aox_dr.utils import send_provider_agnostic_email
from CBAPIs.models import (
    Category,
    ChatBot,
    Market,
    PartnerCategory,
    PartnerMarket,
)
from CBAPIs.serializer import ChatBotSerializer
from CRM.models import (
    Message,
    Transaction,
)
from pricing.utils import decimal_format
from .models import (
    Agents,
    DueDiligence,
    Farmers,
    MembershipFile,
    Partner,
    Statement,
)
from .serializer import (
    AgentSerializer,
    CBMemberSerializer,
    DueDiligenceSerializer,
    PartnerCBSerializer,
    PartnerSerializer,
    StatementSerializer,
)


def send_accepted_partner(partner_email, partner_username, partner_password):
    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'username': partner_username,
            'password': partner_password,
        }
    else:
        template_id = 'Partner Approval'
        merge_vars = {
            'USERNAME': partner_username,
            'PASSWORD': partner_password,
        }

    send_provider_agnostic_email(
        subject="Your request to become a Stable partner was accepted",
        to_email=partner_email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=[]
    )


def send_paused_partner_email(partner, partner_email):
    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'partner': partner,
        }
    else:
        template_id = 'Partner Pause'
        merge_vars = {
            'PARTNER': partner,
        }

    send_provider_agnostic_email(
        subject="You have been paused as a partner on Stable",
        to_email=partner_email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=[]
    )


class PartnerSignUpView(APIView):

    @staticmethod
    def post(request):
        try:
            name = request.data['Your Name'] if 'Your Name' in request.data else None
            if not name:
                return Response({'error': 'Name not defined'}, status=status.HTTP_200_OK)
            name = name.split('\n')
            first_name = name[0].replace('first = ', '')
            last_name = name[1].replace('last = ', '')

            company_name = request.data['Your Organisation'] if 'Your Organisation' in request.data else None
            if not company_name:
                return Response({'error': request.data}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            country = request.data['Country'] if 'Country' in request.data else None
            if not country:
                return Response({'error': 'Country not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            email = request.data['Email'] if 'Email' in request.data else None
            if not email:
                return Response({'error': 'Email not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            phone = request.data['Mobile Phone'] if 'Mobile Phone' in request.data else None
            if not phone:
                return Response({'error': 'Mobile Phone not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            notes = request.data['How did you hear about Stable?'] if \
                'How did you hear about Stable?' in request.data else None
            if not notes:
                return Response({'error': 'Notes not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            if not AuthUser.objects.filter(username=email, email=email).exists():
                company_admin = AuthUser(
                    first_name=first_name,
                    last_name=last_name,
                    username=email,
                    email=email,
                    phone=phone,
                    is_active=False,
                    is_partner=True,
                    company_name=company_name
                )
                temp_password = get_random_string(length=15)
                company_admin.set_password(temp_password)
                company_admin.save()
            else:
                return Response(
                    {
                        "error": 'User with this email address "" already exists.'.format(email)
                    },
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )

            if not Partner.objects.filter(name=company_name).exists():
                partner = Partner(
                    name=company_name,
                    notes=notes,
                    register_country=country,
                    company_admin=company_admin
                )
                partner.save()
            else:
                return Response(
                    {
                        'error': 'Partner: {} Already Exist'.format(company_name)
                    },
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )

            return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class SalesPartnerSignUpView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if not request.user.is_corporate:
                return Response({'error': 'Corporate can become a Sales Partner'}, status=status.HTTP_200_OK)

            company_name = request.user.company_name
            if not company_name:
                return Response(
                    {'error': 'Please update your profile company name field'},
                    status=status.HTTP_200_OK
                )

            phone = request.user.phone
            if not phone:
                return Response(
                    {'error': 'Please update your profile phone number field'},
                    status=status.HTTP_200_OK
                )

            if not Partner.objects.filter(name=company_name).exists():
                partner = Partner(
                    name=company_name,
                    company_admin=request.user,
                    notes='Become a Sales Partner'
                )
                partner.save()
            else:
                return Response(
                    {
                        'error': 'Partner: {} Already Exist'.format(company_name)
                    },
                    status=status.HTTP_200_OK
                )

            return Response({'success': 'success'}, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class GenerateChatBotView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            selected_markets = json.loads(request.data['markets']) if 'markets' in request.data else None
            if not selected_markets:
                return Response({'error': 'Markets are not selected'}, status=status.HTTP_200_OK)

            selected_categories = json.loads(request.data['categories']) if 'categories' in request.data else None
            if not selected_categories:
                return Response({'error': 'Categories are not selected'}, status=status.HTTP_200_OK)

            country = request.data['country']
            name = request.data['name']
            membership_id_display = request.data['membership_id_display'] == 'true'

            default_commission = request.data.get('default_commission', None)
            if default_commission == 'null':
                default_commission = None
            default_farmer_savings = request.data.get('default_farmer_savings', None)
            if default_farmer_savings == 'null':
                default_farmer_savings = None

            phone = None
            if membership_id_display:
                phone = request.data.get('phone')
                if not phone:
                    return Response({'error': 'Contact Phone is not defined'}, status=status.HTTP_200_OK)

            if request.user.is_partner or request.user.is_broker:
                partner = Partner.objects.get(company_admin=request.user)
                if partner.is_active and not partner.is_pause:
                    if ChatBot.objects.filter(country=country, sales_partner=partner).exists():
                        return Response(
                            {
                                'error': 'We are sorry, but you have already generated a Quote Engine for the {}, and you can only generate one quote engine per country for your account.'.format(country)
                            },
                            status=status.HTTP_200_OK
                        )
                    else:
                        chatbot = ChatBot(
                            name=name,
                            sales_partner=partner,
                            country=country,
                            membership_id=membership_id_display,
                            partner_phone=phone,
                            default_commission=default_commission,
                            default_farmer_savings=default_farmer_savings
                        )
                        chatbot.save()

                        if PartnerMarket.objects.filter(chatbot=chatbot).exists() \
                                or PartnerCategory.objects.filter(chatbot=chatbot).exists():
                            return Response(
                                {
                                    'error': 'You have generated ChatBot'
                                             ' already. Your ChatBot - {}'.format(chatbot.pk)
                                },
                                status=status.HTTP_200_OK
                            )

                        for market in selected_markets:
                            market = Market.objects.get(id=market['id'])
                            partner_market = PartnerMarket(
                                market=market,
                                name=market.name,
                                image=market.image,
                                order_number=market.order_number,
                                display=market.display,
                                chatbot=chatbot
                            )
                            partner_market.save()

                        for commodity in selected_categories:
                            category = Category.objects.get(id=commodity['id'])
                            origin_market_name = Market.objects.get(id=category.market_id).name
                            new_market = PartnerMarket.objects.get(
                                name=origin_market_name,
                                chatbot=chatbot
                            )
                            partner_category = PartnerCategory(
                                name=category.name,
                                image=category.image,
                                market=new_market,
                                currency=category.currency,
                                percent=category.percent,
                                category=category,
                                unit=category.unit,
                                index_id=category.index_id,
                                amount=category.amount,
                                available=category.available,
                                spent=category.spent,
                                display=category.display,
                                single_month=category.single_month,
                                multi_month=category.multi_month,
                                min_contract_length=category.min_contract_length,
                                max_contract_length=category.max_contract_length,
                                chatbot=chatbot,
                                commission=commodity['commission'],
                                farmers_save=commodity['farmers_save']
                            )
                            partner_category.save()

                        if request.user.is_broker and not request.user.broker_id:
                            user = request.user
                            user.broker_id = str(uuid.uuid4())
                            user.save()
                else:
                    return Response(
                        {'error': 'Partner not activated yet, please request to Stable'},
                        status=status.HTTP_200_OK
                    )
            else:
                return Response(
                    {'error': 'Sales Partner Can Generate ChatBot'},
                    status=status.HTTP_200_OK
                )

            context = {
                'success': 'ChatBot generated successfully! '
                           'You are able to control Market and Categories for your own ChatBot now.',
                'chat_bot': chatbot.pk
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class SalesPartnerChatBot(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if not request.user.is_partner and not request.user.is_broker:
                return Response({'error': 'Sales Partner Can see this data'}, status=status.HTTP_200_OK)

            partner = Partner.objects.get(company_admin=request.user)
            chatbots = ChatBot.objects.filter(sales_partner=partner)

            context = ChatBotSerializer(chatbots, many=True).data
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GetCompanyDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            companies = PartnerSerializer(Partner.objects.order_by('id').all(), many=True)
            context = {
                'company_data': companies.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class AcceptCompanyView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'Super User can accept Partner'}, status=status.HTTP_200_OK)

            partner_id = request.data.get('id')
            partner_type = request.data.get('type')

            if not partner_id or not partner_type:
                return Response({'error': 'Partner not defined'}, status=status.HTTP_200_OK)

            partner = Partner.objects.get(id=partner_id)
            if partner.is_active:
                return Response({'error': 'Partner already approved'}, status=status.HTTP_200_OK)
            partner.is_active = True
            partner.save()

            company_admin = partner.company_admin

            temp_password = get_random_string(length=15)
            company_admin.set_password(temp_password)

            if not company_admin.is_corporate:
                company_admin.is_active = True
                if partner_type == 'broker':
                    company_admin.is_partner = False
                    company_admin.is_broker = True
                else:
                    company_admin.is_partner = True
                    company_admin.is_broker = False
            else:
                company_admin.is_partner = True
            company_admin.save()

            send_accepted_partner(
                partner_email=company_admin.email, partner_username=company_admin.email, partner_password=temp_password,
            )

            companies = PartnerSerializer(Partner.objects.order_by('id').all(), many=True)
            context = {
                'company_data': companies.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class DeclineCompanyView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, id):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'Super User can Decline Partner'}, status=status.HTTP_200_OK)

            partner = Partner.objects.get(id=id)
            if partner.is_active and not partner.is_pause:
                return Response({'error': 'Can\'t Decline this Partner for now'}, status=status.HTTP_200_OK)

            company_admin = partner.company_admin

            chatbots = ChatBot.objects.filter(sales_partner=partner)

            PartnerMarket.objects.filter(chatbot__in=chatbots).delete()
            PartnerCategory.objects.filter(chatbot__in=chatbots).delete()
            chatbots.delete()

            if not company_admin.is_corporate:
                company_admin.delete()
                partner.delete()
            else:
                partner.delete()
                company_admin.is_partner = False
                company_admin.save()

            companies = PartnerSerializer(Partner.objects.order_by('id').all(), many=True)
            context = {
                'company_data': companies.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PauseCompanyView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, partner_id):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'Super User can Pause Partner'}, status=status.HTTP_200_OK)

            partner = Partner.objects.get(id=partner_id)
            if not partner.is_active:
                return Response({'error': 'Partner not Approved yet.'}, status=status.HTTP_200_OK)

            company_admin = partner.company_admin
            if not company_admin.is_corporate:
                company_admin.is_active = not company_admin.is_active
            else:
                company_admin.is_partner = not company_admin.is_partner
            company_admin.save()

            partner.is_pause = not partner.is_pause
            partner.save()

            if partner.is_pause:
                send_paused_partner_email(partner=partner.name, partner_email=company_admin.email)
                message = Message(
                    company=partner,
                    content="Your Account has been put on hold and your ChatBots will be temporarily "
                            "unavailable to your farmers until everything has been resolved",
                    sender=request.user.get_full_name(),
                    read_users=[]
                )
                message.save()

            companies = PartnerSerializer(Partner.objects.order_by('id').all(), many=True)
            context = {
                'company_data': companies.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UpdateCompanyDataView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, id):
        try:
            company = Partner.objects.get(id=id)
            company.max_savings = request.data['max_savings']
            company.country = request.data['country'] if 'country' in request.data else None
            company.currency = request.data['currency'] if 'currency' in request.data else None
            company.tax = request.data['tax'] if 'tax' in request.data else None
            company.support_number = request.data['support_number'] if 'support_number' in request.data else None
            company.save()

            companies = PartnerSerializer(Partner.objects.order_by('id').all(), many=True)
            context = {
                'company_data': companies.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UploadCompanyLogoView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            if not request.user.is_partner:
                return Response({'error': 'User should be Company Admin role.'}, status=status.HTTP_200_OK)
            else:
                company_name = request.user.company_name
                company = Partner.objects.get(name=company_name)
                if not company:
                    return Response({'error': 'Company does not exist.'}, status=status.HTTP_200_OK)

            logo = request.data['logo'] if 'logo' in request.data else None
            if not logo:
                return Response({'error': 'Not defined Company Logo.'}, status=status.HTTP_200_OK)

            if company.logo:
                if default_storage.exists(str(company.logo)):
                    default_storage.delete(str(company.logo))

            company.logo = logo
            company.save()

            context = {
                'success': 'success',
                'current_logo': PartnerSerializer(company).data['logo'],
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GetCurrentLogoView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if not request.user.is_partner:
                return Response({'error': 'User should be Company Admin role.'}, status=status.HTTP_200_OK)
            else:
                company_name = request.user.company_name
                company = Partner.objects.get(name=company_name)
                if not company:
                    return Response({'error': 'Company does not exist.'}, status=status.HTTP_200_OK)

            context = {
                'current_logo': PartnerSerializer(company).data['logo'],
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class ActivePartnersView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, country):
        try:
            if country == 'all':
                active_partners = Partner.objects.filter(is_active=True, is_pause=False).order_by('id')
            else:
                active_partners = Partner.objects.filter(
                    is_active=True, is_pause=False, country=country
                ).order_by('id')

            today = datetime.datetime.now()
            year = today.year
            month = today.month

            partner_performance = []

            for partner in active_partners:
                company_name = partner.name
                month_commission = Transaction.objects.filter(
                    purchased=True,
                    partner__name=company_name,
                    quote_date__year__gte=year,
                    quote_date__month__gte=month
                ).aggregate(Sum('commission'))

                all_sales = Transaction.objects.filter(
                    purchased=True, partner__name=company_name
                ).aggregate(Sum('premium'))

                month_sales = Transaction.objects.filter(
                    purchased=True,
                    partner__name=company_name,
                    quote_date__year__gte=year,
                    quote_date__month__gte=month
                ).aggregate(Sum('premium'))

                sales_chart_query = Transaction.objects.filter(
                    purchased=True,
                    partner__name=company_name,
                ).annotate(
                    month=TruncMonth('quote_date')
                ).values('month').annotate(r=Sum('premium')).values('month', 'r').order_by(
                    'month')

                sales_chart_axis = []
                sales_chart_value = []
                for sales_data in sales_chart_query:
                    sales_chart_axis.append(sales_data['month'].strftime('%B %Y'))
                    sales_chart_value.append(decimal_format(sales_data['r']))

                partner_performance.append(
                    {
                        'partner_id': partner.id,
                        'month_commission': decimal_format(
                            month_commission['commission__sum']
                        ) if month_commission['commission__sum'] else 0,
                        'all_sales': decimal_format(all_sales['premium__sum']) if all_sales['premium__sum'] else 0,
                        'month_sales': decimal_format(month_sales['premium__sum']) if month_sales['premium__sum'] else 0,
                        'sales_chart_axis': sales_chart_axis,
                        'sales_chart_value': sales_chart_value
                    }
                )

            context = {
                'partners': PartnerSerializer(active_partners, many=True).data,
                'partner_performance': partner_performance
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class StatementView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if request.user.is_superuser:
                statements = StatementSerializer(
                    Statement.objects.all(),
                    many=True
                )
            elif request.user.is_partner:
                statements = StatementSerializer(
                    Statement.objects.filter(partner=Partner.objects.get(company_admin=request.user)),
                    many=True
                )
            elif request.user.is_broker:
                statements = StatementSerializer(
                    Statement.objects.filter(partner=Partner.objects.get(company_admin=request.user)),
                    many=True
                )
            else:
                return Response({'error': 'User must SuperUser or Company Admin'}, status=status.HTTP_200_OK)

            context = {
                'statement_files': statements.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    def post(self, request):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'User must SuperUser'}, status=status.HTTP_200_OK)

            file = request.data['file'] if 'file' in request.data else None
            if not file:
                return Response({'error': 'Statement File not defined'}, status=status.HTTP_200_OK)

            partner = request.data['partner'] if 'partner' in request.data else None
            if not partner:
                return Response({'error': 'Partner not defined'}, status=status.HTTP_200_OK)
            else:
                partner = Partner.objects.get(id=partner)

            name = file.name
            if re.search(r'\.pdf$', str(file)):
                statement = Statement(
                    file=file,
                    name=name,
                    partner=partner
                )
                statement.save()
            else:
                return Response(
                    {
                        'error': 'Statement File not PDF, please update correct statement file'
                    },
                    status=status.HTTP_200_OK
                )

            statements = StatementSerializer(
                Statement.objects.all(),
                many=True
            )

            context = {
                'statement_files': statements.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PartnerStatementView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, partner_id):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'User must SuperUser'}, status=status.HTTP_200_OK)

            statements = StatementSerializer(
                Statement.objects.filter(partner=Partner.objects.get(id=partner_id)),
                many=True
            )

            context = {
                'statements': statements.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class DownloadStatementFileView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, statement_file_id):
        try:
            if not request.user.is_superuser and not request.user.is_partner:
                return Response(
                    {'error': 'Super User or Company Admin can download Statement.'},
                    status=status.HTTP_200_OK
                )

            statement_file = Statement.objects.get(id=statement_file_id)
            if default_storage.exists(str(statement_file.file)):
                url = statement_file.file.url if 'http' in str(statement_file.file.url) \
                    else '{}/{}'.format(settings.MEDIA_URL_CUSTOM, str(statement_file.file))
                response = Response({'data': url}, content_type='application/pdf', status=status.HTTP_200_OK)
                response['Content-Disposition'] = 'attachment; filename={}'.format(statement_file.name)
                return response
            else:
                return Response({'error': 'File does not exist!'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class DeleteStatementFileView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, statement_file_id):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'User must SuperUser'}, status=status.HTTP_200_OK)

            statement_file = str(Statement.objects.get(id=statement_file_id).file)
            if default_storage.exists(statement_file):
                default_storage.delete(statement_file)
                Statement.objects.filter(id=statement_file_id).delete()

                statements = StatementSerializer(
                    Statement.objects.all(),
                    many=True
                )

                context = {
                    'statement_files': statements.data
                }
                return Response(context, status=status.HTTP_200_OK)
            else:
                return Response({'error': 'File does not exist!'})
        except Exception as e:
            raise StableAPIException(e)


class PartnerAgentsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if request.user.is_partner:
                agents = AgentSerializer(
                    Agents.objects.filter(company=Partner.objects.get(name=request.user.company_name)),
                    many=True
                )
            else:
                return Response({'error': 'User must Company Admin'}, status=status.HTTP_200_OK)

            context = {
                'agents': agents.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    def post(self, request):
        try:
            if request.user.is_partner:
                name = request.data['name'] if 'name' in request.data else None
                if not name:
                    return Response({'error': 'Name not defined.'}, status=status.HTTP_200_OK)

                email = request.data['email'] if 'email' in request.data else None
                if not email:
                    return Response({'error': 'Email not defined.'}, status=status.HTTP_200_OK)

                phone_number = request.data['phoneNumber'] if 'phoneNumber' in request.data else None
                if not phone_number:
                    return Response({'error': 'phoneNumber not defined.'}, status=status.HTTP_200_OK)

                agent = Agents(
                    name=name,
                    email=email,
                    phone_number=phone_number,
                    company=Partner.objects.get(name=request.user.company_name),
                )

                agent.save()
                agent.affiliate_code = agent.id
                agent.save()

                agents = AgentSerializer(
                    Agents.objects.filter(company=Partner.objects.get(name=request.user.company_name)),
                    many=True
                )
            else:
                return Response({'error': 'User must Company Admin'}, status=status.HTTP_200_OK)

            context = {
                'agents': agents.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class DeletePartnerAgentsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, agent_id):
        try:
            if request.user.is_partner:
                Agents.objects.get(id=agent_id).delete()
                agents = AgentSerializer(
                    Agents.objects.filter(company=Partner.objects.get(name=request.user.company_name)),
                    many=True
                )
            else:
                return Response({'error': 'User must Company Admin'}, status=status.HTTP_200_OK)

            context = {
                'agents': agents.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PartnerValidationView(APIView):

    @staticmethod
    def post(request):
        try:
            partner_name = request.data['PartnerName'] if 'PartnerName' in request.data else None
            if not partner_name:
                return Response({'error': 'Partner not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            partner = Partner.objects.get(name=partner_name)
            if partner.is_pause:
                partner_status = 'Paused'
            elif partner.is_active:
                partner_status = 'Active'
            else:
                partner_status = 'Not Approved'

            context = {
                'status': partner_status,
                'partner': PartnerCBSerializer(partner).data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PartnerMemberShipView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            members = Farmers.objects.filter(chatbot_id=request.GET.get('chat_bot'))

            page = request.GET['pagination[page]'] if 'pagination[page]' in request.GET else 1
            per_page = request.GET['pagination[perpage]'] if 'pagination[perpage]' in request.GET else 20

            total = members.count()
            paginator = Paginator(members, per_page)

            try:
                members = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                members = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                members = paginator.page(paginator.num_pages)

            total_members = CBMemberSerializer(members, many=True)

            context = {
                'data': total_members.data,
                'meta': {
                    'page': page,
                    'perpage': per_page,
                    'total': total
                }
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UploadPartnerMemberShipView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        if not request.user.is_partner:
            return Response({'error': 'Only a Stable Partner may upload this information'}, status=status.HTTP_200_OK)
        try:
            chat_bot_id = request.data.get('chatbot')
            if not chat_bot_id:
                return Response({'error': 'Chat Bot is not defined'}, status=status.HTTP_200_OK)

            chat_bot = ChatBot.objects.get(pk=chat_bot_id)
            if not chat_bot.membership_id:
                return Response({'error': 'This ChatBot is available to all users'}, status=status.HTTP_200_OK)

            file = request.data['file']

            if re.search(r'\.csv$', str(file)) or re.search(r'\.xlsx$', str(file)):
                file_data = file.read().decode("utf-8")
                lines = file_data.split("\n")
                if len(lines):
                    for idx, line in enumerate(lines):
                        if line:
                            data = line.split(',')
                            if idx == 0:
                                if data[0].replace('\r', '') == 'Business Name' \
                                        and data[1].replace('\r', '') == 'Postcode' \
                                        and data[2].replace('\r', '') == 'Membership ID':
                                    if not MembershipFile.objects.filter(chatbot_id=chat_bot_id).exists():
                                        MembershipFile(
                                            chatbot_id=chat_bot_id,
                                            file=file
                                        ).save()
                                    else:
                                        member_file = MembershipFile.objects.get(chat_bot_id=chat_bot_id)
                                        if default_storage.exists(str(member_file.file)):
                                            default_storage.delete(str(member_file.file))
                                        member_file.delete()
                                        MembershipFile(
                                            chat_bot_id=chat_bot_id,
                                            file=file
                                        ).save()
                                    Farmers.objects.filter(chatbot_id=chat_bot_id).delete()
                                else:
                                    return Response(
                                        {
                                            'error': 'The CSV should have the following headings:'
                                                     ' 1. Business Name 2.Postcode 3. Membership ID'
                                        },
                                        status=status.HTTP_200_OK
                                    )
                            else:
                                business_name = data[0]
                                postcode = data[1]
                                reference_id = data[2]
                                member = Farmers(
                                    chatbot=chat_bot,
                                    reference_id=reference_id,
                                    postcode=postcode,
                                    business_name=business_name
                                )
                                member.save()
                    return Response({'success': 'success'}, status=status.HTTP_200_OK)
                else:
                    return Response(
                        {'error': 'File of incorrect format. '
                                  'Please upload a CSV file.'},
                        status=status.HTTP_200_OK
                    )
            else:
                return Response(
                    {'error': 'File of incorrect format. Please upload a CSV file.'},
                    status=status.HTTP_200_OK
                )
        except Exception as e:
            raise StableAPIException(e)


class DownloadPartnerMemberShipView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            chat_bot_id = request.GET.get('chat_bot')
            if not chat_bot_id:
                return Response({'error': 'ChatBot ID not defined'}, status=status.HTTP_200_OK)

            member_file = MembershipFile.objects.get(chat_bot_id=chat_bot_id).file
            if not default_storage.exists(str(member_file)):
                return Response(
                    {
                        'error': 'You have uploaded membership but File removed or not saved. '
                                 'Please contact stable admin.'
                    },
                    status=status.HTTP_200_OK
                )
            context = {
                'data': member_file.url if 'http' in str(member_file.url)
                else '{}/{}'.format(settings.MEDIA_URL_CUSTOM, str(member_file))
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GetMaxSavings(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        if not request.user.is_partner:
            return Response({'error': 'You must be a partner with stable to access this information.'}, status=status.HTTP_200_OK)

        try:
            partner_id = request.user.id
            if not partner_id:
                return Response({'error': 'Partner not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            partner = Partner.objects.get(company_admin=partner_id)
            partner_max_savings = partner.max_savings

            context = {
                'max_savings': partner_max_savings
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UploadDueDiligenceView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if request.user.is_superuser:
                # This is what i need to fix tomorrow, work on the correct foreign key to pass through as a value
                # because currently this partner is not working and i think it should probably be something
                # similar to the value above in max commission.!
                due_diligence = DueDiligenceSerializer(
                    DueDiligence.objects.filter(partner= Partner.objects.get(id=request.GET.get('partner'))),
                    many=True
                )
            # elif request.user.is_partner:
            #     due_diligence = DueDiligenceSerializer(
            #         DueDiligence.objects.filter(partner=Partner.objects.get(company_admin=request.user)),
            #         many=True
            #     )
            else:
                return Response({'error': 'User must SuperUser'}, status=status.HTTP_200_OK)

            context = {
                'due_diligence_files': due_diligence.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    def post(self, request):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'User must SuperUser'}, status=status.HTTP_200_OK)

            file = request.data['file'] if 'file' in request.data else None
            if not file:
                return Response({'error': 'Due Diligence File not defined'}, status=status.HTTP_200_OK)

            partner = request.data['partner'] if 'partner' in request.data else None
            if not partner:
                return Response({'error': 'Partner not defined'}, status=status.HTTP_200_OK)
            else:
                partner = Partner.objects.get(id=partner)

            name = file.name
            if re.search(r'\.pdf$', str(file)) or re.search(r'\.jpg$', str(file)) or re.search(r'\.png$', str(file)):
                due_diligence = DueDiligence(
                    file=file,
                    name=name,
                    partner=partner
                )
                due_diligence.save()
            else:
                return Response(
                    {
                        'error': 'Due Diligence File not PDF, please update correct statement file'
                    },
                    status=status.HTTP_200_OK
                )

            due_diligence = DueDiligenceSerializer(
                DueDiligence.objects.filter(partner=partner),
                many=True
            )

            context = {
                'due_diligence_files': due_diligence.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class DeleteDueDiligenceView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, due_diligence_file_id):
        try:
            if request.user.is_superuser:
                partner = DueDiligence.objects.get(id=due_diligence_file_id).partner
                due_diligence_file = str(DueDiligence.objects.get(id=due_diligence_file_id).file)
                if default_storage.exists(due_diligence_file):
                    default_storage.delete(due_diligence_file)
                    DueDiligence.objects.filter(id=due_diligence_file_id).delete()

                    due_diligence = DueDiligenceSerializer(
                        DueDiligence.objects.filter(partner=partner),
                        many=True
                    )

                    context = {
                        'due_diligence': due_diligence.data
                    }
                    return Response(context, status=status.HTTP_200_OK)
                else:
                    return Response({'error': 'File does not exist!'})

        except Exception as e:
            raise StableAPIException(e)
