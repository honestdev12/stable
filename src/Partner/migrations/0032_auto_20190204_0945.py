# Generated by Django 2.0 on 2019-02-04 09:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Partner', '0031_auto_20190131_1235'),
    ]

    operations = [
        migrations.AlterField(
            model_name='farmers',
            name='business_name',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='farmers',
            name='postcode',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='farmers',
            name='reference_id',
            field=models.TextField(),
        ),
    ]
