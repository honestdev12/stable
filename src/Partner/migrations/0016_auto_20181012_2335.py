# Generated by Django 2.0 on 2018-10-12 19:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Partner', '0015_partner_register_country'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='max_savings',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
