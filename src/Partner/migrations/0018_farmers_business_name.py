# Generated by Django 2.0 on 2018-10-30 08:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Partner', '0017_auto_20181018_2011'),
    ]

    operations = [
        migrations.AddField(
            model_name='farmers',
            name='business_name',
            field=models.CharField(default='', max_length=255),
        ),
    ]
