from django.db import models

from administration.models import AuthUser


def company_logo_directory_path(instance, filename):
    # file will be uploaded to a hidden directory at
    return 'company_logo/' + str(instance.name) + '/' + filename


def partner_statement_upload_path(instance, filename):
    return 'company_statement/' + str(instance.partner.name) + '/' + filename


def partner_due_diligence_upload_path(instance, filename):
    return 'company_due_diligence/' + str(instance.partner.name) + '/' + filename


class Partner(models.Model):
    name = models.CharField(max_length=255, unique=True)
    max_savings = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    logo = models.FileField(blank=True, null=True, upload_to=company_logo_directory_path)
    is_active = models.BooleanField(default=False)
    is_pause = models.BooleanField(default=False)
    notes = models.TextField(blank=True, null=True)
    company_admin = models.ForeignKey(AuthUser, null=True, blank=True, on_delete=models.CASCADE)
    register_country = models.CharField(max_length=255, null=True, blank=True)
    country = models.CharField(max_length=255, null=True, blank=True)
    currency = models.CharField(max_length=255, null=True, blank=True)
    tax = models.DecimalField(default=0, max_digits=24, decimal_places=8)
    support_number = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Statement(models.Model):
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to=partner_statement_upload_path)
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)
    upload_at = models.DateField(auto_now_add=True)


class DueDiligence(models.Model):
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to=partner_due_diligence_upload_path)
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)
    upload_at = models.DateField(auto_now_add=True)


class Agents(models.Model):
    name = models.CharField(max_length=255, default='')
    email = models.EmailField(default='')
    phone_number = models.CharField(max_length=255, default='')
    company = models.ForeignKey(Partner, default=0, on_delete=models.CASCADE)
    affiliate_code = models.CharField(max_length=255, default='')
    registered_at = models.DateField(auto_now_add=True)


# Model is to store CSV data from sales partners regarding their farmers!
class Farmers(models.Model):
    chatbot = models.ForeignKey('CBAPIs.ChatBot', on_delete=models.CASCADE)
    reference_id = models.TextField()
    postcode = models.TextField(default="")
    business_name = models.TextField(default="")

    class Meta:
        unique_together = ('chatbot', 'reference_id')


class MembershipFile(models.Model):
    file = models.FileField(upload_to='membership')
    chatbot = models.ForeignKey('CBAPIs.ChatBot', on_delete=models.CASCADE)
