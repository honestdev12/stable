from datetime import (
    datetime,
)
from decimal import Decimal

import pendulum
from django.contrib.humanize.templatetags.humanize import intcomma, ordinal


def str_to_date(s):
    try:
        d = datetime.strptime(s, '%b-%y')
    except Exception as e:
        try:
            d = datetime.strptime(s, '%b %y')
        except Exception as e:
            try:
                d = datetime.strptime(s, '%B %Y')
            except Exception as e:
                d = datetime.strptime(s, '%B-%Y')

    d = datetime.strftime(d, "%Y/%m/%d")
    return d


def str_to_str(s):
    d = convert_string_to_date(s)
    return datetime.strftime(d, "%B %Y")


def convert_string_to_date(s):
    try:
        d = datetime.strptime(s, '%b-%y')
    except ValueError:
        try:
            d = datetime.strptime(s, '%y-%b')
        except ValueError:
            try:
                d = datetime.strptime(s, '%b %y')
            except ValueError:
                try:
                    d = datetime.strptime(s, '%B %Y')
                except ValueError:
                    try:
                        d = datetime.strptime(s, '%B-%Y')
                    except ValueError:
                        try:
                            d = datetime.strptime(s, '%Y-%B')
                        except ValueError:
                            d = datetime.strptime(s, '%Y-%m-%d')

    return d


def get_stop_date(s):
    date = convert_string_to_date(s)
    datetime = pendulum.instance(date)
    return datetime.end_of('month')


def set_to_15th(date):
    datetime = pendulum.instance(date)
    return datetime.set(day=15)


def set_to_next_month(date):
    datetime = pendulum.instance(date)
    return datetime.add(months=1)


def decimal_format(value, decimal_places=2, comma_format=False):
    _decimal_places = Decimal(1)/(Decimal('10') ** decimal_places)
    value = value.quantize(_decimal_places)
    if not comma_format:
        return str(value)
    return intcomma(value)


def custom_strftime(date_format, t):
    return t.strftime(date_format).replace('{S}', ordinal(t.day))
