from __future__ import unicode_literals
from django.contrib import admin
from . import models


class IndexPriceDataAdmin(admin.ModelAdmin):
    list_display = ['prod_name', 'price', 'date', 'is_active', 'country']


class PricingResultsAdmin(admin.ModelAdmin):
    list_display = ['prod_name', 'maturity', 'policy_start_date', 'policy_end_date', 'country']


class PaymentStatusChangesLogAdmin(admin.ModelAdmin):
    list_display = ['user', 'content', 'date', 'policy']


class IndexPriceChangesLogAdmin(admin.ModelAdmin):
    list_display = ['category', 'previous_value', 'new_value', 'colour', 'user', 'country']


class FinalPricingFileAdmin(admin.ModelAdmin):
    list_display = ['name', 'file_status', 'uploaded_at']


admin.site.register(models.IndexPriceData, IndexPriceDataAdmin)
admin.site.register(models.PricingResults, PricingResultsAdmin)
admin.site.register(models.PaymentStatusLog, PaymentStatusChangesLogAdmin)
admin.site.register(models.IndexPriceChangesLog, IndexPriceChangesLogAdmin)
admin.site.register(models.FinalPricingFile, FinalPricingFileAdmin)

