from datetime import datetime

from celery import shared_task
from django.conf import settings
from django.core.mail import send_mass_mail
from django.db.models import Sum

from Borderaux.models import ClaimBorderaux
from CBAPIs.utils import settle_this_wrapper
from CRM.models import Transaction
from CRM.tasks import set_paid_month_indemnity_zero
from pricing.models import (
    IndexPriceData,
    PaymentStatusLog,
)


@shared_task()
def check_for_contracts_to_settle(country, month, price, product_name):
    contracts = Transaction.objects.filter(
        stop_date=month,
        category__name=product_name,
        owed_status='Finished',
        purchased=True,
        country=country
    )
    request_bank_details_emails = ()
    for contract in contracts:
        protection_style = contract.protection_style
        protection_direction = contract.protection_direction

        if protection_style == 'Asian':

            index_price_sum = IndexPriceData.objects.filter(
                is_active='Live',
                prod_name=product_name,
                date__range=(contract.start_date, contract.stop_date),
                country=country
            ).aggregate(Sum('price'))

            end_asset_average_price = float(
                index_price_sum['price__sum']
            ) / int(contract.protection_duration) if index_price_sum['price__sum'] else 0
            asset_price = end_asset_average_price
        else:
            asset_price = price

        if protection_direction == 'FALL':
            start_price_difference = (contract.start_price - asset_price)
            start_price_difference = start_price_difference if start_price_difference >= 0 else 0

            stop_price_difference = (contract.stop_price - asset_price)
            stop_price_difference = stop_price_difference if stop_price_difference >= 0 else 0
        else:
            start_price_difference = (asset_price - contract.start_price)
            start_price_difference = start_price_difference if start_price_difference >= 0 else 0

            stop_price_difference = (asset_price - contract.stop_price)
            stop_price_difference = stop_price_difference if stop_price_difference >= 0 else 0

        unit_payoff = start_price_difference - stop_price_difference

        total_payoff = unit_payoff * contract.quantity

        if total_payoff and total_payoff != 0:
            contract.owed_amount = total_payoff
            contract.owed_status = 'Awaiting Details'

            """
            Send Simon information about settlement.
            """
            _unique_trade_id = contract.unique_trade_id

            settle_this_wrapper.apply_async(args=[_unique_trade_id, total_payoff])

            request_bank_details_email = (
                'Request Bank Details',
                'Dear {}, \nPlease fill this form to provide us your bank details, '
                '\nLink: '
                'https://stable.formstack.com/forms/farmer_payment_details'
                '?policyid={}&paymenttype=Owed'
                '\nso we can pay for owed amount (£{}) of money. \n \nThanks, '
                '\nStablePrice'.format(contract.farmer.first_name, contract.id, float(total_payoff)),
                settings.DEFAULT_FROM_MAIL,
                [contract.farmer.email])
            request_bank_details_emails = \
                (request_bank_details_email,) + request_bank_details_emails

            claim_borderaux = ClaimBorderaux.objects.get(transaction=contract)
            claim_borderaux.state = 'due'
            claim_borderaux.paid_month_indemnity = total_payoff
            claim_borderaux.save()
            set_paid_month_indemnity_zero.apply_async(
                args=[claim_borderaux.id],
            )
            payment_status_log = PaymentStatusLog(
                content='Bank details requested',
                date=datetime.now(),
                policy=contract
            )
            payment_status_log.save()
        else:
            contract.owed_amount = 0
            contract.owed_status = 'None'
    send_mass_mail(request_bank_details_emails, fail_silently=False)
