import ast
class Para:
    def __init__(self,mat1,mat2,prod,s1,s2,s3,s4,threshold,sim_len,forecast,Engs,IV):
        self.maturity_begin = mat1
        self.maturity_end = mat2
        self.product = prod
        self.sr1 = s1#strike ratio
        self.sr2 = s2
        self.sr3 = s3
        self.sr4 = s4
        self.threshold = threshold
        self.sim_length = sim_len
        self.forecast_power = forecast
        self.Engs = Engs
        self.IV = IV
    def __str__(self):
        return("Maturity Begin: " + str(self.maturity_begin) + "\n" +
               "Maturity Eng: " + str(self.maturity_end) + "\n" +
               "Product: " + str(self.product) + "\n" +
               "SR1: " + str(self.sr1) + "\n" +
               "SR2: " + str(self.sr2) + "\n" +
               "SR3: " + str(self.sr3) + "\n" +
               "SR4: " + str(self.sr4) + "\n" +
               "Threshold: " + str(self.threshold) + "\n" +
               "Sim Length: " + str(self.sim_length) + "\n" +
               "Forecast Power: " + str(self.forecast_power) + "\n" +
               "Engines: " + str(self.Engs) + "\n" +
               "IV: " + str(self.IV) + "\n")




class Version:
    def __init__(self,string):

        string = string.replace('v','')
        string = string.replace('.',', ')
        (First,Last) = ast.literal_eval(string)

        self.First = First
        self.Last = Last

    def __str__(self):
        return 'v'+str(self.First)+'.'+str(self.Last)

    def small_update(self):
        self.Last += 1

    def big_update(self):
        self.First += 1


class smoothingPar:
    def __init__(self,sr_from,sr_to,cl_from,cl_to,threshold,limitation):
        self.sr_from = sr_from
        self.sr_to = sr_to
        self.cl_from = cl_from
        self.cl_to = cl_to
        self.threshold = threshold
        self.limitation = limitation

    def isValid(self,sr_ext_from,sr_ext_to,mm_from,mm_to):
        return sr_ext_from>self.sr_from and mm_from>self.cl_from
