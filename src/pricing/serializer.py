from rest_framework import serializers

from .models import (
    File,
    FinalPricingFile,
    IndexPriceChangesLog,
    PaymentStatusLog,
    PricingResults,
)


class IndexFileSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = File
        fields = '__all__'


class PricingFileSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = FinalPricingFile
        fields = ['key', 'name', 'uploaded_at', 'file_status']


class PricingResultsSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')

    class Meta:
        model = PricingResults
        fields = ['key', 'prod_name', 'total_style', 'maturity', 'strike',
                  'price', 'stop', 'policy_start_date', 'policy_end_date', 'issuing_date', 'withdraw_date']


class IndexPriceChangesLogSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    user = serializers.SerializerMethodField()

    class Meta:
        model = IndexPriceChangesLog
        fields = '__all__'

    def get_user(self, obj):
        return {
            'first_name': obj.user.first_name,
            'last_name': obj.user.last_name,
        }


class PaymentStatusLogSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    user = serializers.SerializerMethodField('get_user_name')

    @staticmethod
    def get_user_name(obj):
        return obj.user.get_full_name()

    class Meta:
        model = PaymentStatusLog
        fields = '__all__'
