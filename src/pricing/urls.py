from django.conf.urls import url
from pricing import views as pricing_views

urlpatterns = [
    url(r'^get_index_price_data/(?P<country>[\w ]+)/$', pricing_views.IndexPriceDataView.as_view()),
    url(r'^modify_index_price_data/', pricing_views.ModifyIndexPriceDataView.as_view()),

    url(r'^upload_existing_results/(?P<country>[\w ]+)/$', pricing_views.UploadExistingResultsView.as_view()),
    url(r'^load_uploaded_results/(?P<results_file_id>\d+)/$', pricing_views.LoadUploadedResultsView.as_view()),
    url(r'^download_uploaded_results/(?P<results_file_id>\d+)/$', pricing_views.DownloadUploadedResultsView.as_view()),
    url(r'^delete_uploaded_results/(?P<results_file_id>\d+)/$', pricing_views.DeleteUploadedResultsView.as_view()),

    url(r'^indexprices', pricing_views.PricingPageView.as_view()),
    url(r'^current_performance/', pricing_views.CurrentPerformanceView.as_view()),
]
