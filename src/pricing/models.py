import datetime

import boto3
from django.conf import settings
from django.db import models

from administration.models import AuthUser as User

if not settings.DEBUG:
    session = boto3.Session(
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
    )
    s3 = session.resource('s3')


def index_date():
    return datetime.datetime.now()


class File(models.Model):
    BOOL_CHOICES = ((True, 'Yes'), (False, 'No'))
    has_additional_attr = models.BooleanField(choices=BOOL_CHOICES, default=False)
    additional_attr = models.CharField(max_length=20, default='', null=True, blank=True)
    file = models.FileField(upload_to='index/')
    name = models.CharField(max_length=200)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    index_config = models.IntegerField(default=0)


class FinalPricingFile(models.Model):
    name = models.CharField(max_length=200)
    file = models.FileField(upload_to='pricing/')
    file_status = models.CharField(max_length=20, default='None', null=True, blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    country = models.CharField(max_length=255, default='UK')

    def save(self, *args, **kwargs):
        super(FinalPricingFile, self).save(*args, **kwargs)
        if self.file and not settings.DEBUG:
            s3.Object(settings.AWS_STORAGE_BUCKET_NAME, 'media/' + self.file.name).put(
                Body=self.file, ACL='private'
            )


class IndexPriceData(models.Model):
    prod_name = models.CharField(max_length=200, null=False, blank=False, default='')
    price = models.DecimalField(db_column="Price", default=0.0, max_digits=24, decimal_places=8)
    date = models.DateTimeField(db_column="Date", default=index_date)
    is_active = models.CharField(max_length=200, default='Live')
    person_one = models.ForeignKey(User, null=True, blank=True, related_name='person_one', on_delete=models.SET_NULL)
    person_two = models.ForeignKey(User, null=True, blank=True, related_name='person_two', on_delete=models.SET_NULL)
    country = models.CharField(max_length=255, null=False, blank=False, default='UK')

    class Meta:
        ordering = ('-date',)
        get_latest_by = ('date',)


class PricingResultManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(
            withdraw_date__isnull=True
        )


class PricingResults(models.Model):
    prod_name = models.CharField(db_column="Product Name", max_length=40, default='None')
    style = models.CharField(db_column="Style", max_length=40, default='None')
    total_style = models.CharField(db_column="Total Style", max_length=40, default='None')
    direction = models.CharField(db_column="Price Direction", max_length=40, default='None')
    maturity = models.IntegerField(db_column="Maturity", default=0)

    strike = models.DecimalField(db_column="Strike", max_digits=15, decimal_places=10, default=0)
    strike_ratio = models.DecimalField(db_column="Strike_Ratio", max_digits=4, decimal_places=2, default=0)
    price = models.DecimalField(db_column="Price", max_digits=15, decimal_places=10, default=0)
    start_asset_price = models.DecimalField(max_digits=15, decimal_places=10, default=0)
    stop = models.DecimalField(db_column="Stop", max_digits=15, decimal_places=10, default=0)

    policy_start_date = models.CharField(db_column="Policy Start Date", max_length=40, default='')
    policy_end_date = models.CharField(db_column="Policy End Date", max_length=40, default='')

    issuing_date = models.DateField(db_index=True)
    withdraw_date = models.DateField(db_index=True, null=True)

    country = models.CharField(max_length=255, null=False, blank=False, default='UK')

    pricing_results_file = models.ForeignKey('pricing.FinalPricingFile', on_delete=models.CASCADE)

    objects = PricingResultManager()

    def __str__(self):
        return str(self.prod_name)

    class Meta:
        get_latest_by = 'issuing_date'


class IndexPriceChangesLog(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    month = models.CharField(max_length=255)
    category = models.CharField(max_length=255)
    previous_value = models.DecimalField(decimal_places=8, max_digits=24)
    new_value = models.DecimalField(max_digits=24, decimal_places=8)
    colour = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    country = models.CharField(max_length=255, default='UK')


class PaymentStatusLog(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    content = models.CharField(max_length=255)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    policy = models.ForeignKey('CRM.Transaction', on_delete=models.PROTECT)
