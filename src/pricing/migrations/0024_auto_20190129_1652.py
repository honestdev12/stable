# Generated by Django 2.0 on 2019-01-29 16:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pricing', '0023_merge_20190128_0948'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='indexpricedata',
            options={'ordering': ('-date',)},
        ),
        migrations.AddField(
            model_name='pricingresults',
            name='start_asset_price',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=15),
        ),
    ]
