# Generated by Django 2.0 on 2018-12-05 17:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pricing', '0018_auto_20181205_1824'),
    ]

    operations = [
        migrations.AddField(
            model_name='pricingresults',
            name='strike_ratio',
            field=models.DecimalField(db_column='Strike_Ratio', decimal_places=2, default=0.0, max_digits=4),
        ),
    ]
