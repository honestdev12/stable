# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-06-21 11:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pricing', '0003_indexpricedata_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='indexpricedata',
            name='is_active',
            field=models.CharField(default='Live', max_length=200),
        ),
    ]
