import codecs
import csv
import re
from datetime import (
    datetime,
)
from decimal import Decimal

import boto3
import pandas as pd
import pytz
from django.conf import settings
from django.core.files.storage import default_storage
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from aox_dr.exception import StableAPIException
from CBAPIs.models import Category
from pricing.tasks import check_for_contracts_to_settle
from .models import (
    FinalPricingFile,
    IndexPriceChangesLog,
    IndexPriceData,
    PricingResults,
)
from .serializer import (
    IndexPriceChangesLogSerializer,
    PricingFileSerializer,
    PricingResultsSerializer,
)
from .utils import (
    convert_string_to_date,
    decimal_format,
    set_to_15th,
    str_to_str,
)

if not settings.DEBUG:
    s3_client = boto3.client(
        's3',
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
    )

utc = pytz.UTC


def sum_range(price_list, start, stop):
    price_sum = 0

    for i in range(start, stop, 1):
        price_sum += price_list[i]['price']

    return price_sum / stop


def get_index_price_data(country=None):
    index_prods = IndexPriceData.objects.values('prod_name').order_by().distinct('prod_name')

    index_months = IndexPriceData.objects.values('date', 'id').order_by('-date').distinct('date')
    if country:
        index_months = index_months.filter(country=country)

    index_price_data_query = IndexPriceData.objects.all()
    if country:
        index_price_data_query = index_price_data_query.filter(country=country)

    index_price_log = IndexPriceChangesLog.objects.order_by('-id')
    if country:
        index_price_log = index_price_log.filter(country=country)

    index_price_data = []
    index_price_csv_data = []
    for idx, month in enumerate(index_months):
        index_price_dict = {
            'date': datetime.strftime(month['date'], "%B %Y"),
            'key': datetime.strftime(month['date'], "%B %Y"),
            'id': idx
        }
        index_price_csv_dict = {
            'key': datetime.strftime(month['date'], "%B %Y"),
            'date': datetime.strftime(month['date'], "%B %Y"),
        }
        for data in index_price_data_query:
            if data.date == month['date']:
                if data.is_active == 'Staging':
                    price = '{}_yellow'.format(decimal_format(data.price, decimal_places=6))
                elif data.is_active == 'Danger':
                    price = '{}_red'.format(decimal_format(data.price, decimal_places=6))
                else:
                    price = decimal_format(data.price, decimal_places=6)
                index_price_dict[data.prod_name] = price
                index_price_csv_dict[data.prod_name] = data.price
        index_price_data.append(index_price_dict)
        index_price_csv_data.append(index_price_csv_dict)

    context = {
        'index_price_data': index_price_data,
        'index_prods': index_prods,
        'index_price_csv_data': index_price_csv_data,
        'index_price_log': IndexPriceChangesLogSerializer(
            index_price_log,
            many=True
        ).data,
    }
    return context


class PricingPageView(APIView):

    @staticmethod
    def get(request):
        try:
            category = request.GET.get('category')
            if not category:
                return Response({'error': 'Category not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            month = request.GET.get('numberofmonths')
            if not month:
                return Response(
                    {'error': 'Month Count not defined'},
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )

            currency = settings.CURRENCY_CONVERSION_RULE.get('UK').get(category).get('unit')
            metric = Category.objects.get(name=category, country="UK").unit

            index_price = IndexPriceData.objects.filter(prod_name=category).order_by('-date')[:int(month)]
            raw_price_chart_data = []
            raw_price_chart_axis = []

            for raw in index_price:
                date_string = datetime.strftime(raw.date, "%b %Y")
                raw_price_chart_axis.append(date_string)
                raw_price_chart_data.append(decimal_format(raw.price))

            context = {
                "Months": reversed(raw_price_chart_axis),
                "IndexPrices": reversed(raw_price_chart_data),
                "currency": currency,
                "metric": metric
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class CurrentPerformanceView(APIView):

    @staticmethod
    def post(request):
        try:
            category_id = request.data.get('category')
            if not category_id:
                return Response({'error': 'Category not defined'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            start_date = request.data.get('start_date')
            if not start_date:
                return Response(
                    {'error': 'Start Date not defined'},
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )

            start_price = request.data.get('start_price')
            if not start_date:
                return Response(
                    {'error': 'Start Price not defined'},
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )

            # Get most recent index month, then can subtract from start to get duration.
            category = Category.objects.get(pk=category_id)
            end_date = IndexPriceData.objects.filter(prod_name=category.name).order_by('-date')[0].date
            cur_date = datetime.strptime(start_date, '%Y-%m-%d')
            cur_date = cur_date.replace(tzinfo=utc)

            prices_array = IndexPriceData.objects.filter(
                prod_name=category.name, country="UK", date__range=(cur_date, end_date)
            ).values('date', 'price')

            if not prices_array:
                return Response(
                    {
                        'error': 'The {category} index price for the month your cover starts '
                                 'is yet to be published. We will display it as soon as '
                                 'it is available.'.format(category=category.name)
                    },
                    status=status.HTTP_200_OK
                )

            running_averages = []
            start_prices = []
            label = []
            prices = []

            for i, item in enumerate(prices_array):
                start_prices.append(decimal_format(start_price))
                label.append(item['date'])
                prices.append(decimal_format(item['price']))
                running_averages.append(decimal_format(sum_range(prices_array, 0, i + 1)))

            current_running_average = sum(item['price'] for item in prices_array) / len(prices_array)

            context = {
                'label': label,
                'prices': prices,
                "start_prices": start_prices,
                "running_averages": running_averages,
                "current_running_average": current_running_average
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class IndexPriceDataView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, country):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'User must SuperUser'}, status=status.HTTP_200_OK)

            context = get_index_price_data(country)
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class ModifyIndexPriceDataView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'User must SuperUser'}, status=status.HTTP_200_OK)

            prod_name = request.data['prod_name'] if 'prod_name' in request.data else None
            if not prod_name:
                return Response({'error': 'Product Name not defined'}, status=status.HTTP_200_OK)

            country = request.data['country'] if 'country' in request.data else None
            if not country:
                return Response({'error': 'Country not defined'}, status=status.HTTP_200_OK)

            month = convert_string_to_date(request.data['date']) if 'date' in request.data else None
            if not month:
                return Response({'error': 'Month not defined'}, status=status.HTTP_200_OK)

            price = request.data['price'] if 'price' in request.data else None
            if not price or price == 'undefined':
                if not IndexPriceData.objects.filter(date=month, prod_name=prod_name, country=country).exists():
                    return Response({'error': 'Index Price Not Modified!'}, status=status.HTTP_200_OK)
                else:
                    price = Decimal(0)
            else:
                price = Decimal(price)

            if IndexPriceData.objects.filter(date=month, prod_name=prod_name, country=country).exists():
                index_price = IndexPriceData.objects.get(date=month, prod_name=prod_name, country=country)
            else:
                index_price = IndexPriceData(
                    prod_name=prod_name,
                    date=month,
                    price=price,
                    country=country
                )
                index_price.save()

            if index_price.is_active == 'Live' or index_price.is_active == 'Danger':
                previous_price = index_price.price
                index_price.price = price
                index_price.is_active = 'Staging'
                index_price.person_one = request.user
                index_price.save()
                index_price_changes_log = IndexPriceChangesLog(
                    user=request.user,
                    previous_value=previous_price,
                    new_value=price,
                    month=datetime.strftime(month, '%B %Y'),
                    category=prod_name,
                    colour='Yellow',
                    country=country
                )
                index_price_changes_log.save()
            elif index_price.is_active == 'Staging':
                # If the index price we're replacing is in the staging state already, then we've just gone live with it
                # So we should start processing them

                if index_price.person_one == request.user:
                    return Response(
                        {'error': 'You can not modify this value again.'},
                        status=status.HTTP_200_OK
                    )
                modified_price = index_price.price
                if Decimal(modified_price) == Decimal(price):
                    if Decimal(modified_price) == Decimal(0):
                        index_price.delete()
                        index_price_changes_log = IndexPriceChangesLog(
                            user=request.user,
                            previous_value=modified_price,
                            new_value=price,
                            month=datetime.strftime(month, '%B %Y'),
                            category=prod_name,
                            colour='Green',
                            country=country
                        )
                        index_price_changes_log.save()
                    else:
                        index_price.is_active = 'Live'
                        index_price.person_two = request.user
                        index_price.save()
                        index_price_changes_log = IndexPriceChangesLog(
                            user=request.user,
                            previous_value=modified_price,
                            new_value=price,
                            month=datetime.strftime(month, '%B %Y'),
                            category=prod_name,
                            colour='Green',
                            country=country
                        )
                        index_price_changes_log.save()

                        # Settlement Algorithm Part
                        check_for_contracts_to_settle.delay(country, month, price, prod_name)
                else:
                    index_price.is_active = 'Danger'
                    index_price.price = price
                    index_price.person_two = request.user
                    index_price.save()
                    index_price_changes_log = IndexPriceChangesLog(
                        user=request.user,
                        previous_value=modified_price,
                        new_value=price,
                        month=datetime.strftime(month, '%B %Y'),
                        category=prod_name,
                        colour='Red',
                        country=country
                    )
                    index_price_changes_log.save()

            context = get_index_price_data(country=country)
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UploadExistingResultsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, country):
        if not request.user.is_superuser:
            return Response({'error': 'Stable Admin can see price data'}, status=status.HTTP_200_OK)
        uploaded_file_list = PricingFileSerializer(FinalPricingFile.objects.filter(country=country), many=True)
        loaded_pricing_data = PricingResultsSerializer(PricingResults.objects.filter(country=country), many=True)
        context = {
            'success': 'success',
            'uploaded_file_list': uploaded_file_list.data,
            'loaded_pricing_data': loaded_pricing_data.data,
        }
        return Response(context, status=status.HTTP_200_OK)

    @staticmethod
    def post(request, country):
        if not request.user.is_superuser:
            return Response({'error': 'Stable Admin can upload price data'}, status=status.HTTP_200_OK)

        for file_key in request.data:
            file = request.data[file_key]
            name = file.name
            if re.search(r'\.csv$', str(file)):
                file_data = file.read().decode("utf-8")
                lines = file_data.split("\n")
                if len(lines):
                    pass
                else:
                    return Response(
                        {'error': 'File not Pricing data. '
                                  'Please upload correct Existing Pricing File.'},
                        status=status.HTTP_200_OK
                    )

                pricing_file = FinalPricingFile(
                    file=file, name=name, country=country
                )
                pricing_file.save()
            else:
                return Response({'error': name + ' is Wrong file format. Should be CSV'})

        uploaded_file_list = PricingFileSerializer(FinalPricingFile.objects.filter(country=country), many=True)
        loaded_pricing_data = PricingResultsSerializer(PricingResults.objects.filter(country=country), many=True)
        context = {
            'success': 'success',
            'uploaded_file_list': uploaded_file_list.data,
            'loaded_pricing_data': loaded_pricing_data.data,
        }
        return Response(context, status=status.HTTP_200_OK)


class LoadUploadedResultsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, results_file_id):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'Stable Admin can load price data'}, status=status.HTTP_200_OK)

            pricing_file = FinalPricingFile.objects.get(id=results_file_id)
            file_dir = str(pricing_file.file)
            file_status = pricing_file.file_status
            country = pricing_file.country

            if file_status == 'Loaded':
                return Response({'error': 'File already loaded'})

            if default_storage.exists(file_dir):
                with default_storage.open(file_dir, 'r') as validate_file:
                    df = pd.read_csv(validate_file)
                    maturity_column = list(df['Shown Maturity'].unique())
                    for i in range(3, 12):
                        if i not in maturity_column:
                            return Response({'error': 'File does not include all maturity value, '
                                                      'please delete and upload correct Pricing file.'})

            if default_storage.exists(file_dir):
                with default_storage.open(file_dir, 'rb') as csv_file:
                    reader = csv.DictReader(codecs.iterdecode(csv_file, 'utf-8'))
                    prod_name_json = settings.PRODUCT_NAME_JSON

                    if FinalPricingFile.objects.filter(file_status='Loaded', country=country):
                        prev_loaded_file = FinalPricingFile.objects.get(file_status='Loaded', country=country)
                        prev_loaded_file.file_status = 'None'
                        prev_loaded_file.save()

                    issuing_date = None
                    new_pricing_data = []

                    for idx, row in enumerate(reader):
                        prod_name = row['Product']
                        if prod_name in prod_name_json:
                            prod_name = prod_name_json[prod_name]

                        multiplier = Decimal(1)
                        multiplier_defined = settings.CURRENCY_CONVERSION_RULE.get(
                            country
                        ).get(
                            prod_name
                        ).get('multiplier')

                        if not multiplier_defined:
                            multiplier_defined = multiplier
                        else:
                            multiplier_defined = Decimal(multiplier_defined)

                        try:
                            style__split = str(row['Style']).split(' ')
                            style = style__split[0]
                            direction = style__split[1]
                            total_style = str(row['Style'])
                            maturity = int(row['Shown Maturity'])
                            strike_ratio = Decimal(row['Strike Ratio'])
                            strike = Decimal(row['Strike']) * multiplier_defined
                            stop = Decimal(row['Stop']) * multiplier_defined
                            start_asset_price = Decimal(row['Start Asset Price']) * multiplier_defined
                            price = Decimal(row['Spread Price']) * multiplier_defined
                            policy_start_date = str_to_str(str(row['Policy Shown Start Date']))
                            policy_end_date = str_to_str(str(row['Policy End Date']))

                            # Dumb system to store the date as the 15th
                            issuing_date = set_to_15th(convert_string_to_date(row['Issuing Date']))
                        except (TypeError, ValueError):
                            continue

                        new_pricing_data.append(PricingResults(
                            prod_name=prod_name,
                            style=style,
                            total_style=total_style,
                            direction=direction,
                            maturity=maturity,
                            strike=strike,
                            strike_ratio=strike_ratio,
                            start_asset_price=start_asset_price,
                            stop=stop,
                            price=price,
                            policy_start_date=policy_start_date,
                            policy_end_date=policy_end_date,
                            issuing_date=issuing_date,
                            pricing_results_file=pricing_file,
                            country=country
                        ))
                    if issuing_date is not None:
                        PricingResults.objects.filter(country=country).update(withdraw_date=issuing_date)
                    PricingResults.objects.bulk_create(new_pricing_data)
            else:
                return Response({'error': 'File does not exist'})

            pricing_file.file_status = 'Loaded'
            pricing_file.save()
            uploaded_file_list = PricingFileSerializer(FinalPricingFile.objects.filter(country=country), many=True)
            loaded_pricing_data = PricingResultsSerializer(PricingResults.objects.filter(country=country), many=True)
            context = {
                'success': 'success',
                'uploaded_file_list': uploaded_file_list.data,
                'loaded_pricing_data': loaded_pricing_data.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class DownloadUploadedResultsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, results_file_id):
        if not request.user.is_superuser:
            return Response({'error': 'Stable Admin can download price data'}, status=status.HTTP_200_OK)

        from_file = FinalPricingFile.objects.get(id=results_file_id).file
        if default_storage.exists(str(from_file)):
            if not settings.DEBUG:
                url = s3_client.generate_presigned_url(
                    ClientMethod='get_object',
                    Params={
                        'Bucket': settings.AWS_STORAGE_BUCKET_NAME,
                        'Key': 'media/' + str(from_file)
                    }
                )
                context = {
                    'data': url
                }
            else:
                context = {
                    'data': from_file.url if 'http' in str(from_file.url)
                    else '{}/{}'.format(settings.MEDIA_URL_CUSTOM, str(from_file))
                }
            return Response(context)
        else:
            return Response({'error': 'File does not exist!'})


class DeleteUploadedResultsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, results_file_id):
        if not request.user.is_superuser:
            return Response({'error': 'Stable Admin can delete price data'}, status=status.HTTP_200_OK)

        final_pricing_file = FinalPricingFile.objects.get(id=results_file_id)
        from_file = final_pricing_file.file
        country = final_pricing_file.country
        if default_storage.exists(from_file.name):
            from_file.delete()
            final_pricing_file.delete()
            uploaded_file_list = PricingFileSerializer(FinalPricingFile.objects.filter(country=country), many=True)
            loaded_pricing_data = PricingResultsSerializer(PricingResults.objects.filter(country=country), many=True)
            context = {
                'success': 'success',
                'uploaded_file_list': uploaded_file_list.data,
                'loaded_pricing_data': loaded_pricing_data.data,
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'File does not exist!'})
