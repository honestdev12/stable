import datetime
import logging
from decimal import Decimal

import gocardless_pro
import stripe
from django.conf import settings
from django.utils import timezone
from raven.contrib.django.models import client
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from aox_dr.exception import StableAPIException
from CBAPIs.models import (
    Category,
    ChatBot,
    Index,
    Market,
    PartnerCategory,
)
from CBAPIs.utils import (
    cal_max_availability,
    cancel_this_wrapper,
    confirm_to_sell_wrapper,
    get_availability_quantity,
    get_category_info_view,
    sell_this_wrapper,
    send_quote_mail,
)
from CRM.models import (
    Farmer,
    Transaction as Purchase,
)
from CRM.tasks import set_policy_runs_out
from Partner.models import (
    Agents,
    Farmers,
    Partner,
)
from payment.tasks import success_payment
from pricing.models import PricingResults
from pricing.utils import (
    convert_string_to_date,
    decimal_format,
    get_stop_date,
    str_to_date,
)
from sdk.utils import get_availability_percent


class CategoriesView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            country = request.GET.get('country') if 'country' in request.GET else None
            if not country:
                return Response({'error': 'Country not defined'}, status=status.HTTP_200_OK)

            chat_bot = True

            availability = get_availability_percent(country)

            if request.user.is_farmer:
                # check all farmer tagged partners
                partners = Farmer.objects.get(user=request.user).tagged_partners
                for partner_id in partners:
                    if not ChatBot.objects.filter(
                            sales_partner=Partner.objects.get(id=partner_id),
                            country=country
                    ).exists():
                        chat_bot = False
                    else:
                        chat_bot = True
                        break

            category_arr = Category.objects.filter(country=country, display=True)

            category_arr_new = []
            for item in category_arr:
                index_item = Index.objects.get(id=item.index_id)
                for available in availability:

                    if available['Commodity'] == 'GB_' + index_item.name.replace(" ", ""):
                        available_amount = available['available_amount']
                        available_percent = available['available_percent']

                new_index_item = {
                    'id': index_item.id,
                    'name': index_item.name,
                    'definition': index_item.definition,
                    'url': index_item.url,
                    'percent': index_item.percent,
                    'owner': index_item.owner,
                    'note': index_item.note,
                    'last_index_date': index_item.last_index_date,
                    'last_index_price': index_item.last_index_price,
                    'low_index_price': index_item.low_index_price,
                    'high_index_price': index_item.high_index_price,
                }
                new_item = {
                    "id": str(item.id),
                    "image": item.image,
                    "name": item.name,
                    "currency": item.currency,
                    "percent": item.percent,
                    "unit": item.unit,
                    "amount": item.amount,
                    "available_amount": available_amount,
                    "available_percent": available_percent,
                    "spent": item.spent,
                    "index": new_index_item,
                    'single_month': item.single_month,
                    'multi_month': item.multi_month,
                    'min_contract_length': item.min_contract_length,
                    'max_contract_length': item.max_contract_length,
                }
                category_arr_new.append(new_item)
            context = {
                'data': category_arr_new,
                'chat_bot': chat_bot,
                'message': '{} does not support markets in this country, '
                           'but you can still purchase insurance '
                           'directly through Stable.'.format(request.user.company_name) if not chat_bot else ''
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class CategoryInfoView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            country = request.data.get('country')
            market_name = request.data.get('marketName')
            index_name = request.data.get('indexName')
            policy_start_date = request.data.get('policyStartDate')
            policy_end_date = request.data.get('policyEndDate')
            option_type = request.data.get('optionType')
            quantity = request.data.get('quantity')

            if not index_name or not policy_start_date or not policy_end_date or not option_type or not quantity:
                return Response({'error': 'Please provide all required params'}, status=status.HTTP_200_OK)

            context = get_category_info_view(
                index_name,
                option_type,
                market_name,
                policy_end_date,
                policy_start_date,
                quantity,
                request.user,
                country=country
            )

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class CategoryPolicyDateView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            index_name = request.data['indexName'] if "indexName" in request.data else None
            if not index_name:
                return Response({'error': 'IndexName not defined'}, status=status.HTTP_200_OK)

            country = request.data['country'] if 'country' in request.data else None
            if not country:
                return Response({'error': 'Country not defined.'}, status=status.HTTP_200_OK)

            pricing_results = PricingResults.objects.filter(
                prod_name=index_name,
                country=country,
            )
            if not pricing_results.exists():
                logger = logging.getLogger()
                logger.warning(f"No pricing data found for {index_name} in {country} when requested")
                return Response(
                    {'error': 'No pricing data could be found for this commodity.'},
                    status=status.HTTP_200_OK
                )
            policy_start_date = pricing_results[0].policy_start_date
            policy_end_date = pricing_results.values('policy_end_date').distinct()
            policy_end_date_list = [date['policy_end_date'] for date in policy_end_date]
            policy_end_date_list.sort(key=lambda date: str_to_date(date))
            policy_end_date_list = [date for date in policy_end_date_list]
            context = {
                'policy_start_date': policy_start_date,
                'policy_end_date': policy_end_date_list
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PrepareView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            user = request.user

            if not user.is_farmer:
                partner = Partner.objects.get(company_admin=user)
                if not partner.is_active or partner.is_pause:
                    return Response(
                        {
                            'error': '{} - Company does not exist or '
                                     'Maybe not accepted by Admin yet.'.format(partner.name)
                        },
                        status=status.HTTP_200_OK
                    )

            affiliate_code = request.data['Affiliate_ID'] if "Affiliate_ID" in request.data else None
            if affiliate_code:
                if not Agents.objects.filter(affiliate_code=affiliate_code).exists():
                    return Response({'error': 'Incorrect Affiliate Code: {}'.format(affiliate_code)},
                                    status=status.HTTP_200_OK)

            country = request.data['country'] if 'country' in request.data else None
            if not country:
                return Response({'error': 'Country not defined.'}, status=status.HTTP_200_OK)

            category_name = request.data['categoryName'] if "categoryName" in request.data else None
            if not category_name:
                return Response({'error': 'Category Name should provided.'}, status=status.HTTP_200_OK)
            else:
                category = Category.objects.get(name=category_name, country=country)

            protection_style = request.data['protectionStyle'] \
                if "protectionStyle" in request.data else None
            if not protection_style:
                return Response({'error': 'Protection Style should provided.'}, status=status.HTTP_200_OK)

            protection_duration = request.data['protectionDuration'] if "protectionDuration" in request.data else None
            if not protection_duration:
                return Response({'error': 'Protection Duration should provided.'}, status=status.HTTP_200_OK)

            stop_price = Decimal(request.data['stopPrice']) if "stopPrice" in request.data else None
            if not stop_price:
                return Response({'error': 'Stop Price should provided.'}, status=status.HTTP_200_OK)

            start_price = Decimal(request.data['startPrice']) if "startPrice" in request.data else None
            if not start_price:
                return Response({'error': 'Start Price should provided.'}, status=status.HTTP_200_OK)

            price_per_unit = Decimal(request.data['pricePerQuantity']) if "pricePerQuantity" in request.data else None
            if not price_per_unit:
                return Response({'error': 'Price per unit should provided.'}, status=status.HTTP_200_OK)

            quantity = Decimal(request.data['quantity']) if "quantity" in request.data else None
            if not quantity:
                return Response({'error': 'Quantity should provided.'}, status=status.HTTP_200_OK)

            reference_id = request.data['reference_id'] if "reference_id" in request.data else None

            start_date = request.data['startDate'] if "startDate" in request.data else None
            if not start_date:
                return Response({'error': 'startDate should provided.'}, status=status.HTTP_200_OK)

            closing_date = request.data['endDate'] if "endDate" in request.data else None
            if not closing_date:
                return Response({'error': 'endDate should provided.'}, status=status.HTTP_200_OK)

            closing_date = get_stop_date(closing_date)

            if category_name in ["Red Diesel", "AN Fertiliser"]:
                protection_direction = "RISE"
            else:
                protection_direction = "FALL"

            farmer = Farmer.objects.get(user=request.user)
            # EPIC farmers workflow sales partner commission on platform
            if request.user.is_farmer:
                tagged_partners = farmer.tagged_partners[::-1]

                chat_bots = ChatBot.objects.filter(sales_partner_id__in=tagged_partners, country=country)

                commission = 100
                farmer_savings = 100

                partner = Partner.objects.get(name='Stable')

                if chat_bots:
                    if PartnerCategory.objects.filter(name=category_name, chatbot__in=chat_bots).exists():
                        # Multiple Sales Partners chat bots used for same commodity
                        farmers_save_list = PartnerCategory.objects.filter(
                            name=category_name,
                            chatbot__in=chat_bots
                        ).values_list('farmers_save', flat=True)
                        farmers_save_dict = PartnerCategory.objects.filter(
                            name=category_name,
                            chatbot__in=chat_bots
                        ).values('farmers_save', 'chatbot__uuid')
                        if farmers_save_list.count() == len(farmers_save_list):
                            # If there is no difference, the sales partner most recently tagged will be used.
                            chat_bot_list = []
                            partner_list = []
                            for farmers_save in farmers_save_dict:
                                chat_bot_list.append(farmers_save['chatbot__uuid'])
                            for chat_bot in chat_bot_list:
                                partner_list.append(ChatBot.objects.get(pk=chat_bot).sales_partner.id)
                            for partner_id in tagged_partners:
                                if partner_id in partner_list:
                                    partner = Partner.objects.get(id=partner_id)
                                    partner_category = PartnerCategory.objects.get(
                                        chatbot=ChatBot.objects.get(
                                            sales_partner=partner,
                                            country=country
                                        ),
                                        name=category_name
                                    )
                                    commission = partner_category.commission
                                    farmer_savings = partner_category.farmers_save
                                    break
                        else:
                            # The Sales Partner who is the biggest discount to the farmer will be used for quote
                            partner_list = []
                            max_farmers_save = PartnerCategory.objects.filter(
                                name=category_name,
                                chatbot__in=chat_bots
                            ).order_by('farmers_save').last().farmers_save
                            chat_bot_list = PartnerCategory.objects.filter(
                                name=category_name,
                                chatbot__in=chat_bots,
                                farmers_save=max_farmers_save
                            ).values_list('chatbot__uuid', flat=True)
                            for chat_bot in chat_bot_list:
                                partner_list.append(ChatBot.objects.get(pk=chat_bot).sales_partner.id)
                            for partner_id in tagged_partners:
                                if partner_id in partner_list:
                                    partner = Partner.objects.get(id=partner_id)
                                    partner_category = PartnerCategory.objects.get(
                                        chatbot=ChatBot.objects.get(
                                            sales_partner=partner,
                                            country=country
                                        ),
                                        name=category_name
                                    )
                                    commission = partner_category.commission
                                    farmer_savings = partner_category.farmers_save
                                    break
                    else:
                        # Farmer Purchases Commodity insurance not offered by Sales Partner's chat bot i.e.
                        # different commodity but same country
                        partners = Partner.objects.filter(pk__in=tagged_partners).order_by('max_savings')
                        chatbot = ChatBot.objects.filter(
                            country=country,
                            sales_partner__in=partners
                        ).order_by('-default_farmer_savings').first()
                        if chatbot is not None:
                            commission = chatbot.default_commission
                            farmer_savings = chatbot.default_farmer_savings
                        else:
                            partner = partner.first()
                            if partner is not None:
                                commission = partner.max_savings
                                farmer_savings = 0
                            else:
                                # No idea if this is right, but need some kind of value
                                commission = 0
                                farmer_savings = 0

            else:
                commission = 100
                farmer_savings = 100
                partner = Partner.objects.get(name='Stable')

            if ChatBot.objects.filter(sales_partner=partner, country=country).exists():
                chat_bot = ChatBot.objects.get(sales_partner=partner, country=country)
                if chat_bot.membership_id and request.user.is_farmer:
                    farmer = Farmer.objects.get(user=request.user)
                    membership_id = farmer.membership_id

                    memberships = Farmers.objects.filter(chatbot=chat_bot, reference_id=membership_id)

                    if not memberships.exists():
                        return Response(
                            {
                                'error': '<div>You no longer seem to qualify for the {partner} offers and discounts.</b'
                                         'r><ul style="text-align: left;"><li>If you believe this is due to an error '
                                         'please contact the Sales Partner Membership team.'
                                         '<small>(Their number is <b>{enquiry_number}</b>)</small></li> '
                                         '<li>Follow this <a href="https://stableprice.com">StablePrice.com</a> to '
                                         'see what other partners there are available. '
                                         'Otherwise please click continue to '
                                         'proceed at full price</li></ul>'.format(partner=chat_bot.sales_partner.name,
                                                                                  enquiry_number=chat_bot.partner_phone
                                                                                  ),
                                'member_valid': True
                            },
                            status=status.HTTP_200_OK
                        )

            multiplier = Decimal(1)
            multiplier_defined = settings.CURRENCY_CONVERSION_RULE.get(
                country
            ).get(category_name).get('multiplier')

            if not multiplier_defined:
                multiplier_defined = multiplier
            else:
                multiplier_defined = Decimal(multiplier_defined)

            start_price = start_price * multiplier_defined
            stop_price = stop_price * multiplier_defined

            if protection_style == 'Asian' and protection_direction == 'FALL':
                style = 'Asian Put BearSpread'
            elif protection_style == 'Asian' and protection_direction == 'RISE':
                style = 'Asian Call BullSpread'
            elif protection_style == 'European' and protection_direction == 'FALL':
                style = 'European Put BearSpread'
            else:
                style = 'European Call BullSpread'

            price_obj = PricingResults.objects.filter(
                prod_name=category_name,
                country=country,
                style=protection_style,
                stop=Decimal(str(stop_price)),
                strike=Decimal(str(start_price)),
                policy_start_date=start_date,
                policy_end_date=datetime.datetime.strftime(closing_date, "%B %Y"),
            )

            if not price_obj.exists():
                return Response(
                    {
                        'error', 'Does not exist Prices for this case'
                    },
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )

            price_obj = price_obj.first()
            strike_ratio = price_obj.strike_ratio

            price_per_unit_raw = price_obj.price
            if farmer_savings == 100:
                price_per_unit_discount = 0
            else:
                price_per_unit_discount = price_per_unit_raw * farmer_savings / 100
            price_per_unit = (price_per_unit_raw - price_per_unit_discount) * (1 + partner.tax)

            total_cost = quantity * price_per_unit
            monthly_cost = total_cost / int(protection_duration)
            premium = quantity * (price_per_unit_raw - price_per_unit_discount)
            if commission == 100:
                commission = 0
            else:
                commission = premium * commission / 100
            farmer_saving = price_per_unit_discount * quantity
            potential_loss = abs(start_price - stop_price) * quantity

            purchase = Purchase(
                farmer=user,
                corporate=user if user.is_corporate else None,
                partner=partner,
                start_date=convert_string_to_date(start_date),
                stop_date=closing_date,
                market=category.market,
                category=category,
                quantity=quantity,
                metric=category.unit,
                protection_duration=protection_duration,
                protection_style=protection_style,
                protection_direction=protection_direction,
                strike_ratio=strike_ratio,
                start_price=start_price,
                stop_price=stop_price,
                price_per_unit=price_per_unit,
                price_per_unit_raw=price_per_unit_raw,
                tax=partner.tax,
                farmer_saving=farmer_saving,
                monthly_cost=monthly_cost,
                total_cost=total_cost,
                premium=premium,
                commission=commission,
                potential_loss=potential_loss,
                payment_style='Invoice' if user.is_corporate else None,
                affiliate=Agents.objects.get(affiliate_code=affiliate_code) if affiliate_code else None,
                country=country,
                reference_id=reference_id
            )

            purchase.save()

            send_quote_mail(
                first_name=request.user.first_name,
                farmer_email=request.user.email,
                quantity=int(quantity),
                unit=category.unit,
                market=category.market.name,
                category=category,
                index_owner=category.index.owner,
                direction=protection_direction,
                start_date=start_date,
                closing_date=closing_date,
                quote_date=purchase.quote_date,
                current_cost=total_cost / 100,
                monthly_cost=purchase.monthly_cost / 100,
                price_per_quantity=purchase.price_per_unit,
                start_price=start_price,
                stop_price=stop_price,
                premium=purchase.premium,
                tax=Decimal(purchase.tax) * Decimal(purchase.premium),
                potential_loss=purchase.potential_loss,
                protection_duration=purchase.protection_duration,
                transaction=purchase,
            )

            res_obj = {
                "purchaseId": str(purchase.id),
                "amountToPay": purchase.total_cost / 100,
                "quantity": purchase.quantity,
                "quote": purchase.id,
                "closingDate": closing_date,
                "startPrice": purchase.start_price / multiplier_defined,
                "pricePerQuantity": purchase.price_per_unit_raw / 100,
                "monthlyCost": purchase.monthly_cost / 100,
                "monthCount": purchase.protection_duration,
                "potentialLoss": purchase.potential_loss / 100,
                'premium': purchase.premium / 100,
                'tax': purchase.tax,
                'country': country,
                'reference_id': reference_id,
                'amount_saved': farmer_saving / 100,
                'address_line_1': farmer.user.address_1,
                'address_line_2': farmer.user.address_2,
                'town': farmer.user.town,
                'county': farmer.user.county,
                'postcode': farmer.zip,
                'farm_name': farmer.farm_name,
                'farm_postcode': farmer.user.post_code,
                'farm_size': farmer.farm_size,
                'farm_produce': {
                    'Barley': farmer.barley,
                    'Oats': farmer.oats,
                    'Lamb': farmer.sheep,
                    'Calves': farmer.suckler_herd_size,
                    'Potatoes': farmer.potatoes,
                    'Rapeseed': farmer.rapeseed,
                    'Cattle': farmer.cattle,
                    'Wheat': farmer.wheat,
                    'Dairy': farmer.dairy_herd_size,
                    'Other Crops': farmer.other_crops,
                    'Other Livestock': farmer.other_livestock,
                    'Breeding Sows': farmer.breeding_sows,
                },
                'tax_value': (purchase.total_cost - purchase.premium) * multiplier_defined
            }

            return Response(res_obj, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class StripeView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_farmer:
                return Response({'error': 'You are not a farmer'}, status=status.HTTP_200_OK)
            purchase_id = request.data['purchaseId'] if "purchaseId" in request.data else None
            if not purchase_id:
                return Response({'error': 'purchase ID is not defined'}, status=status.HTTP_200_OK)

            token = request.data['token'] if "token" in request.data else None
            if not token:
                return Response({'error': 'Token not defined'}, status=status.HTTP_200_OK)

            purchase = Purchase.objects.get(id=purchase_id)
            if purchase.purchased:
                return Response({'error': 'This policy already purchased'}, status=status.HTTP_200_OK)

            if int(request.user.id) != int(purchase.farmer.id):
                return Response(
                    {
                        'error': 'You are trying to buy a policy that does not belong to you'
                    },
                    status=status.HTTP_200_OK
                )

            index = purchase.category.index

            """
            Check contract size does not exceed allowed amount
            This is especially important when they purchase through manage page.
            """
            if request.user.is_farmer:
                max_availability = cal_max_availability(
                    request.user,
                    purchase.category.name,
                    purchase.country,
                    purchase.stop_date.strftime('%Y-%B'),
                    purchase.start_date.strftime('%Y-%B'),
                    purchase.protection_style,
                    purchase.quantity
                ).get('max_availability', 0)

                max_reason = 'Sorry, it seems you are trying to insure more than you produce, this is calculated ' \
                             'based of the size of your farm and any running contracts you may currently have.'
                if max_availability < purchase.quantity:
                    return Response({"error": max_reason}, status=status.HTTP_200_OK)
            else:
                max_availability = 100000000000
                max_reason = 'You have exceeded the maximum contract size we allow.'
                if max_availability < purchase.quantity:
                    return Response({"error": max_reason}, status=status.HTTP_200_OK)

            availability = get_availability_quantity(purchase.category.name, purchase.market, purchase.country)

            if availability[0]['available_amount'] < max_availability:
                max_availability = round(Decimal(availability[0]['available_amount']), 0)
                max_reason = 'The amount you wish to insure exceeds our ' \
                             'current availability for ' + purchase.category.name + '.'

                if max_availability < purchase.quantity:
                    return Response({"error": max_reason}, status=status.HTTP_200_OK)

            purchase.purchased = True
            purchase.paid_at = datetime.datetime.now()
            purchase.payment_provider = 'stripe'

            stripe.api_key = settings.STRIPE_API_KEY

            """
            Query Simon's Systems to make sure purchase is okay!
            """
            response = sell_this_wrapper(purchase.id, request.user.id)
            if response['if_confirmed']:
                """
                Tell Simon's system that payment is going ahead.
                """
                confirm_to_sell_wrapper(response['_unique_trade_id'], purchase.pk)
                try:
                    stripe_res = stripe.Charge.create(
                        amount=int(purchase.total_cost),
                        currency="gbp",
                        source=token,
                        description="New payment from Dashboard "
                                    "for user " + str(request.user.id) + " and policy " + str(purchase_id),
                        idempotency_key=str(purchase.idempotency_key),
                        receipt_email=request.user.email
                    )
                except Exception as e:
                    """
                    Tell Simon's system that payment failed so they can free up capital.
                    """
                    purchase.purchased = False
                    cancel_this_wrapper(response['_unique_trade_id'])
                    confirm_to_sell_wrapper(response['_unique_trade_id'], purchase.pk)
                    raise StableAPIException(e)

                purchase.payment_id = stripe_res.id
                purchase.unique_trade_id = response['_unique_trade_id']

                policy_end_date = purchase.stop_date + timezone.timedelta(days=31)
                task_response = set_policy_runs_out.apply_async(args=[purchase.id], eta=policy_end_date)

                if task_response.task_id:
                    purchase.owed_status = 'Running'
                    purchase.contract_running_task_id = task_response.task_id

                purchase.underwriters = {"underwriter": "Ascot", "amount": "100%"}

                purchase.save()

                success_payment(request.user.id, purchase.id)
                return Response("success", status=status.HTTP_200_OK)
            else:
                # Here we should alert our system to the problem isolated from users end experience.
                # response['comment']
                # After we can return the following error to the user.
                return Response({"error": "Sorry, but our system has flagged the following transaction as "
                                          "incorrect. If you believe this to be an error please "
                                          "contact us.",
                                 "detail": response['if_confirmed']}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error": "Sorry, something went wrong with your payment, don't worry you have"
                                      " not been charged for any transaction. "
                                      "Please check your details and try again",
                             "detail": str(e)}, status=status.HTTP_200_OK)


class CommodityTreeView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            country = request.GET.get('country')
            if not country:
                return Response({'error': 'Country not defined.'}, status=status.HTTP_200_OK)

            markets = Market.objects.filter(country=country, display=True)

            commodity_array = [
                {
                    'text': 'All Markets and Categories',
                    'children': [],
                    'state': {
                        'opened': True
                    },
                    'type': 'default'
                }
            ]

            commodity_tree_type = {}

            for i, market in enumerate(markets):
                market_id = market.id
                commodity_array[0]['children'].append(
                    {
                        'id': 'market_{}'.format(market.id),
                        'text': market.name,
                        'children': [],
                        'state': {
                            'opened': True
                        },
                        'type': market.name
                    }
                )
                commodity_tree_type[market.name] = {
                    'icon': '{}/{}'.format(settings.MEDIA_URL_CUSTOM, market.image)
                }
                categories = Category.objects.filter(market_id=market_id, display=True)
                for category in categories:
                    commodity_array[0]['children'][i]['children'].append(
                        {
                            'id': 'category_{}'.format(category.id),
                            'text': category.name,
                            'type': category.name
                        }
                    )
                    commodity_tree_type[category.name] = {
                        'icon': '{}/{}'.format(settings.MEDIA_URL_CUSTOM, category.image)
                    }

            context = {
                'commodity_array': commodity_array,
                'commodity_tree_type': commodity_tree_type
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GoCardLessPrepareView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if not request.user.is_farmer:
                return Response({'error': 'You are not a farmer'}, status=status.HTTP_200_OK)
            session_id = request.META['HTTP_AUTHORIZATION']
            policy_id = request.GET.get('id')
            if not policy_id or policy_id == 'undefined':
                return Response({'error': 'Policy ID not defined'}, status=status.HTTP_200_OK)
            user = request.user
            farmer = Farmer.objects.get(user=user) if request.user.is_farmer else None

            redirect_url = '{}/accounts?policy_id={}'.format(settings.DASHBOARD_URL, policy_id)
            go_client = gocardless_pro.Client(
                access_token=settings.GO_CARD_LESS_ACCESS_TOKEN,
                environment=settings.GO_CARD_LESS_ENVIRONMENT
            )
            redirect_flow = go_client.redirect_flows.create(
                params={
                    "description": "Insurance for " + user.first_name + " " + user.last_name,
                    "session_token": session_id,
                    "success_redirect_url": redirect_url,
                    "prefilled_customer": {
                        "given_name": user.first_name,
                        "family_name": user.last_name,
                        "email": user.email,
                        "postal_code": farmer.zip if farmer else '',
                    }
                }
            )

            res_obj = {
                "redirectUrl": redirect_flow.redirect_url,
            }

            return Response(res_obj, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GoCardLessMakeView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        if not request.user.is_farmer:
            return Response({'error': 'You are not a farmer'}, status=status.HTTP_200_OK)
        session_id = request.META['HTTP_AUTHORIZATION']
        policy_id = request.data.get('policy_id')
        go_flow_id = request.data.get('flow_id')

        purchase = Purchase.objects.get(id=policy_id)
        if purchase.purchased:
            return Response({'error': 'This policy already purchased'}, status=status.HTTP_200_OK)

        if request.user != purchase.farmer:
            return Response(
                {
                    'error': 'You are trying to buy a policy that does not belong to you'
                },
                status=status.HTTP_200_OK
            )

        """
        Check contract size does not exceed allowed amount
        This is especially important when they purchase through manage page.
        """
        if request.user.is_farmer:
            max_availability = cal_max_availability(
                request.user,
                purchase.category.name,
                purchase.country,
                purchase.stop_date.strftime('%Y-%B'),
                purchase.start_date.strftime('%Y-%B'),
                purchase.protection_style,
                purchase.quantity
            ).get('max_availability', 0)

            max_reason = 'Sorry, it seems you are trying to insure more than you produce, this is calculated based ' \
                         'of the size of your farm and any running contracts you may currently have.'
            if max_availability < purchase.quantity:
                return Response({"error": max_reason}, status=status.HTTP_200_OK)
        else:
            max_availability = 100000000000
            max_reason = 'You have exceeded the maximum contract size we allow.'
            if max_availability < purchase.quantity:
                return Response({"error": max_reason}, status=status.HTTP_200_OK)

        availability = get_availability_quantity(purchase.category.name, purchase.market, purchase.country)

        if availability[0]['available_amount'] < max_availability:
            max_availability = round(Decimal(availability[0]['available_amount']), 0)
            max_reason = 'The amount you wish to insure exceeds our ' \
                         'current availability for ' + purchase.category.name + '.'

            if max_availability < purchase.quantity:
                return Response({"error": max_reason}, status=status.HTTP_200_OK)

        purchase.purchased = True
        purchase.paid_at = datetime.datetime.now()
        purchase.payment_style = 'Monthly'
        purchase.payment_provider = 'gocardless'

        try:
            go_client = gocardless_pro.Client(
                access_token=settings.GO_CARD_LESS_ACCESS_TOKEN,
                environment=settings.GO_CARD_LESS_ENVIRONMENT
            )
            redirect_flow = go_client.redirect_flows.complete(
                go_flow_id,
                params={
                    "session_token": session_id
                }
            )
            """
            Query Simon's Systems to make sure purchase is okay!
            """
            response = sell_this_wrapper(purchase.id, request.user.id)
            if response['if_confirmed']:
                """
                Tell Simon's system that payment is going ahead.
                """
                purchase.unique_trade_id = response['_unique_trade_id']
                response, error = confirm_to_sell_wrapper(purchase.unique_trade_id, purchase.pk)
                if error is not None:
                    return error
                try:
                    subscription = go_client.subscriptions.create(
                        params={
                            "amount": int(purchase.monthly_cost),
                            "currency": "GBP",
                            "interval_unit": "monthly",
                            "day_of_month": "-1",
                            "count": int(purchase.protection_duration),
                            "links": {
                                "mandate": redirect_flow.links.mandate
                            },
                        },
                        headers={
                            'Idempotency-Key': str(purchase.idempotency_key)
                        }
                    )
                except Exception as e:
                    """
                    Tell Simon's system that payment failed so they can free up capital.
                    """
                    purchase.purchased = False
                    client.captureException()
                    cancel_this_wrapper(response['_unique_trade_id'])
                    return Response({
                        "error": "Sorry, something went wrong with your payment, don't worry you have"
                                              " not been charged for any transaction. "
                                              "Please check your details and try again",
                        "detail": str(e)
                                    }, status=status.HTTP_200_OK)
                purchase.payment_id = subscription.id

                policy_end_date = purchase.stop_date + timezone.timedelta(days=31)
                task_response = set_policy_runs_out.apply_async(args=[purchase.id], eta=policy_end_date)

                if task_response.task_id:
                    purchase.owed_status = 'Running'
                    purchase.contract_running_task_id = task_response.task_id

                purchase.underwriters = {"underwriter": "Ascot", "amount": "100%"}

                purchase.save()

                success_payment(request.user.id, purchase.id, monthly=True)
                return Response("success", status=status.HTTP_200_OK)
            else:
                print(response["comment"])
                purchase.purchased = False
                return Response({
                    "error": "Sorry, something went wrong with our system, don't worry you have"
                             " not been charged for any transaction. We are aware of the problem and working to fix it",
                    "detail": response.get('comment', 'No comment from the SDK')
                }, status=status.HTTP_200_OK)
        except Exception as e:
            client.captureException()
            return Response({
                "error": "Sorry, something went wrong with your payment, don't worry you have"
                         " not been charged for any transaction. Please check your details and try again",
                "detail": str(e)

            }, status=status.HTTP_200_OK)


class LoyaltyCommodityView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if not request.user.is_corporate and not request.user.is_broker:
                return Response({'error': 'You are not able to see this data'}, status=status.HTTP_200_OK)

            if request.user.is_broker:
                partner = Partner.objects.get(company_admin=request.user)
                country = partner.country
            else:
                country = 'UK'

            commodities = Category.objects.filter(country=country, display=True).values('name', 'unit').distinct()

            return Response(commodities, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class LoyaltyPriceView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_corporate:
                return Response({'error': 'You are not a Corporate'}, status=status.HTTP_200_OK)

            commodity = request.data.get('commodity')
            maturity = request.data.get('maturity')

            prices = PricingResults.objects.filter(
                prod_name=commodity,
                maturity=maturity,
                country='UK',
            ).values('strike').distinct()
            return Response(prices, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class LoyaltyCalculateView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_corporate:
                return Response({'error': 'You are not a Corporate'}, status=status.HTTP_200_OK)

            customer = request.data.get('customer')
            strike_price = request.data.get('strike_price')
            maturity = request.data.get('maturity')
            commodity = request.data.get('commodity')
            target_spend = request.data.get('target_spend')

            commodity_obj = Category.objects.get(country='UK', name=commodity)

            price_per_unit = PricingResults.objects.filter(
                maturity=maturity,
                strike=strike_price,
                prod_name=commodity,
                country='UK',
            ).first().price
            loyalty = Decimal(target_spend) / Decimal(price_per_unit)

            if commodity in ['Rapeseed', 'Milling Wheat', 'Feed Barley', 'Feed Wheat']:
                strike_price = '£ {}'.format(strike_price)
                target_spend = '£ {}'.format(target_spend)
            else:
                strike_price = '{} p'.format(strike_price)
                target_spend = '{} p'.format(target_spend)

            context = {
                'success': 'success',
                'message': "Based on a spend of {target_spend} <b>{loyalty}{unit}</b> of {customer}'s {commodity} "
                           "enterprise can be covered for {maturity} months at a strike price "
                           "of {strike_price}".format(target_spend=target_spend, loyalty=decimal_format(loyalty),
                                                      unit=commodity_obj.unit, customer=customer,
                                                      commodity=commodity, maturity=maturity,
                                                      strike_price=strike_price)
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)
