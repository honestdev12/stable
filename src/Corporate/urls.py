from django.conf.urls import url
from Corporate import views as corporate_view

urlpatterns = [
    # APIs for buy policy
    url(r'^categories/categoryInfo', corporate_view.CategoryInfoView.as_view()),
    url(r'^categories/policyDate', corporate_view.CategoryPolicyDateView.as_view()),
    url(r'^categories', corporate_view.CategoriesView.as_view()),

    url(r'^purchases/prepare/', corporate_view.PrepareView.as_view()),
    url(r'^purchases/makePaymentStripe/', corporate_view.StripeView.as_view()),

    url(r'^purchases/preparePaymentGoCardless/', corporate_view.GoCardLessPrepareView.as_view()),
    url(r'^purchases/makePaymentGoCardless/', corporate_view.GoCardLessMakeView.as_view()),

    # APIs for commodity Tree
    url(r'^commodity/tree', corporate_view.CommodityTreeView.as_view()),

    # APIs for Loyalty calculator
    url(r'^loyalty/commodity', corporate_view.LoyaltyCommodityView.as_view()),
    url(r'^loyalty/price/', corporate_view.LoyaltyPriceView.as_view()),
    url(r'^loyalty/calculate/', corporate_view.LoyaltyCalculateView.as_view()),
]
