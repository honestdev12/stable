from decimal import Decimal

from django.db.models import (
    Max,
    Min,
)

from CBAPIs.models import (
    Category,
    Index,
)
from pricing.models import PricingResults
# backend sdk import from simon's team
from sdk.back_end_sdk import (
    get_risk_pools,
    how_much_left,
    name_this_risk_pool,
)


def get_availability_percent():
    risk_pools = get_risk_pools()

    availability = []

    for risk_pool in risk_pools:
        api_result = how_much_left(risk_pool)
        available_percent = (
                                Decimal(api_result[0]['Available Risk Capitals']) / Decimal(api_result[0]['Current Total Risk Capital'])
                            ) * 100
        availability.append(
            {
                'risk_pool': risk_pool,
                'available_amount': str(api_result[0]['Available Risk Capitals']),
                'available_percent': available_percent
            }
        )

    return availability


def get_availability_quantity(category_name, market_name, country):
    # API fetching Availability for commodity from Simon's DB

    risk_pools = []
    availability = []

    index_id = Category.objects.get(name=category_name, country=country).index_id

    if country == 'UK':
        country = 'GB'

    index_name = Index.objects.get(id=index_id).name
    risk_pool = name_this_risk_pool(market_name, index_name, country, '')

    """
    Call + Put options are different for working max_quantity.
    Price Rise and Price Fall.
    """

    if market_name == 'Input Costs':
        price = PricingResults.objects.filter(
            prod_name=category_name,
        ).aggregate(
            Min('strike'),
            Max('stop'),
        )
        maximum_liability = abs(price['strike__min'] - price['stop__max'])
    else:
        price = PricingResults.objects.filter(
            prod_name=category_name,
        ).aggregate(
            Max('strike'),
            Max('stop'),
        )
        maximum_liability = abs(price['strike__max'] - price['stop__max'])

    # maximum_liability = abs(73.7531666630 - 100.5724999950)

    max_quantity = (Decimal(how_much_left(risk_pool)[0]['Available Risk Capitals']) / maximum_liability)
    availability.append({'risk_pool': risk_pool, 'available_amount': max_quantity})

    return availability
