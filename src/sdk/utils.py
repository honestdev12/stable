from decimal import Decimal

from raven.contrib.django.models import client

from sdk.back_end_sdk import (
    AlgoSDKErrors,
    cancel_this,
    confirm_to_sell,
    get_risk_pools,
    get_RMS,
    how_much_left,
    how_much_left_list,
    name_this_risk_pool,
    sell_this,
    settle_this,
)


def get_availability_percent(country: str):
    if country == 'UK':
        country = 'GB'

    """
    SDK method to get all Risk Pools for Country (UK) for now.
    This needs future proofing for other countries, country codes etc
    """
    # Get all Risk Pools (When SDK updated can substitute country code here as parameter)
    risk_pools = get_risk_pools_wrapper()
    availability = how_much_left_list_wrapper(risk_pools)

    availability_array = []

    for commodity in availability:
        commodity = commodity[0]
        risk_pool_name = commodity.get('Risk Pool Name', None)
        if not risk_pool_name:
            continue
        commodity_data = {
                'risk_pool': risk_pool_name,
                'Commodity': risk_pool_name,
                'available_amount': Decimal(commodity['Available Risk Capitals']),
                'available_percent': (
                        Decimal(commodity['Available Risk Capitals']) /
                        Decimal(commodity['Current Total Risk Capital']) *
                        100
                )
            }

        availability_array.append(commodity_data)

    return availability_array


def get_risk_pools_wrapper():
    try:
        response = get_risk_pools()
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response


def how_much_left_list_wrapper(risk_pool_list: list):
    try:
        response = how_much_left_list(list_of_risk_pool_name=risk_pool_list)
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)

    # [[{}]]
    return response


def cancel_this_wrapper(trade_id: str):
    try:
        response = cancel_this(_unique_trade_id=trade_id)
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response


def confirm_to_sell_wrapper(trade_id: str):
    try:
        response = confirm_to_sell(_unique_trade_id=trade_id)
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response


def how_much_left_wrapper(risk_pool_name: str):
    try:
        response = how_much_left(risk_pool_name=risk_pool_name)
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response


def name_this_risk_pool_wrapper(market: str, product: str, country_code: str, style: str = ""):
    try:
        response = name_this_risk_pool(Market=market, Product=product, Country_Code=country_code, Style=style)
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response


def sell_this_wrapper(sell_data: dict, source_data_version_month: str):
    try:
        response = sell_this(this_sell=sell_data, SOURCE_DATA_VERSION_MONTH=source_data_version_month)
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response


def settle_this_wrapper(trade_id: str, payoff_amount: Decimal):
    try:
        response = settle_this(_unique_trade_id=trade_id, payoff_amount=payoff_amount)
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response


def get_dash_board_cards_KPIs_wrapper():
    try:
        response = get_dash_board_cards_KPIs_wrapper()
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response


def get_RMS_wrapper():
    try:
        response = get_RMS()
    except Exception as e:
        client.captureException()
        raise AlgoSDKErrors(e)
    return response
