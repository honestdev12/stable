# this is the backend SDK for the risk engine, current supported functions are:

import datetime
import json
import os
import pprint
import smtplib
import time
import uuid
import warnings
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pymongo
from pymongo import IndexModel

'''
# PLEASE SET UP SERVER PURPOSE HERE!!! # PLEASE SET UP SERVER PURPOSE HERE!!! # PLEASE SET UP SERVER PURPOSE HERE!!!
'''
# PLEASE SET UP SERVER PURPOSE HERE!!! # PLEASE SET UP SERVER PURPOSE HERE!!! # PLEASE SET UP SERVER PURPOSE HERE!!!
# SERVER_PURPOSE = 'PROD' # OR 'DEV' OR 'TEST'

SERVER_PURPOSE = 'DEV'

SERVER_PURPOSE = os.environ.get('SDK_ENV', SERVER_PURPOSE)  # OR 'PROD' OR 'DEV' or 'DEMO'
# PLEASE SET UP SERVER PURPOSE HERE!!! # PLEASE SET UP SERVER PURPOSE HERE!!! # PLEASE SET UP SERVER PURPOSE HERE!!!
print('Using ', SERVER_PURPOSE, ' server here...')


# supporting function
class AlgoSDKErrors(Exception):

    def __init__(self, more_details=''):

        # Call the base class constructor with the parameters it needs
        super().__init__('Something is wrong with the AlgoSDK, and it has been reported to the Quant team.')

        # Deal with more complicated cases
        self.errors = more_details
        this_time_now = datetime.datetime.now().strftime("%d-%b-%Y %H:%M:%S:%f")
        # Sending error details via email

        email_title = 'Algorithm SDK Error! - ' + this_time_now
        email_message = more_details

        self.send_email(email_title, email_message, attachment=[],
                        from_address='AlgoSDK@stableprice.com', to_address=REPORTING_EMAILS,
                        user_name='algolib.alerts', pass_word='oxjyzhyvnveiobzs',
                        smtp_server='smtp.gmail.com', smtp_server_port=587)

    def send_email(self, email_title, email_message, attachment,
                   from_address, to_address,
                   user_name, pass_word,
                   smtp_server, smtp_server_port):
        try:
            # setting up parameters
            # create the message
            msg = MIMEMultipart()
            msg['From'] = from_address
            msg['To'] = ", ".join(to_address)
            msg['Subject'] = email_title

            body = email_message

            msg.attach(MIMEText(body, 'plain'))

            # attach files
            if len(attachment) > 0:
                for this_attach in attachment:
                    filename = str(this_attach.split('/')[-1])
                    attachment = open(this_attach, "rb")

                    part = MIMEBase('application', 'octet-stream')
                    part.set_payload((attachment).read())
                    encoders.encode_base64(part)
                    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

                    msg.attach(part)

            server = smtplib.SMTP(smtp_server, smtp_server_port)
            server.starttls()
            server.login(user_name, pass_word)
            text = msg.as_string()
            server.sendmail(from_address, to_address, text)
            server.quit()
            print('Error reporting email sent.')
            return True
        except:
            print('Something went wrong in sending the error reporting email.')
            return False


if SERVER_PURPOSE == 'PROD':
    # global parameters
    FLAG_MESSAGE = True
    SSH_ADDRESS = 'prod.riskengine.stableprice.com'
    DB_PORT = 33310
    AUTH_DB = 'admin'
    DB_USER = 'stable_product'
    DB_PW = 'JXp9ai2gxkm6stq707VpS10Fo9S6PQgqNgTQUamJ3PM9Y1NMgm5okE5HRp7nbbZD'
    # initiate error tracking and reporting
    REPORTING_EMAILS = ['simon@stableprice.com', 'rachel@stableprice.com']
elif SERVER_PURPOSE == 'DEMO':
    # global parameters
    FLAG_MESSAGE = True
    SSH_ADDRESS = '34.243.172.252'
    DB_PORT = 33310
    AUTH_DB = 'admin'
    DB_USER = 'stable_product'
    DB_PW = 'JXp9ai2gxkm6stq707VpS10Fo9S6PQgqNgTQUamJ3PM9Y1NMgm5okE5HRp7nbbZD'
    # initiate error tracking and reporting
    REPORTING_EMAILS = ['rachel@stableprice.com']
elif SERVER_PURPOSE == 'DEV':
    # global parameters
    FLAG_MESSAGE = True
    SSH_ADDRESS = '63.34.57.58'
    DB_PORT = 33310
    AUTH_DB = 'admin'
    DB_USER = 'stable_product'
    DB_PW = 'JXp9ai2gxkm6stq707VpS10Fo9S6PQgqNgTQUamJ3PM9Y1NMgm5okE5HRp7nbbZD'
    # initiate error tracking and reporting
    REPORTING_EMAILS = ['rachel@stableprice.com']
elif SERVER_PURPOSE == 'TEST':
    # global parameters
    FLAG_MESSAGE = True
    SSH_ADDRESS = '3.88.26.233'
    DB_PORT = 33310
    AUTH_DB = 'admin'
    DB_USER = 'stable_product'
    DB_PW = 'JXp9ai2gxkm6stq707VpS10Fo9S6PQgqNgTQUamJ3PM9Y1NMgm5okE5HRp7nbbZD'
    # initiate error tracking and reporting
    REPORTING_EMAILS = ['']
else:
    print('No such server_purpose in Algo SDK. Please use PROD or DEV, thanks! ')
    raise AlgoSDKErrors('No such server_purpose in Algo SDK. Please use PROD or DEV, thanks! ')

# Ignored parameters
SSH_PORT = 22
SSH_USER_NAME = 'ubuntu'
REMOTE_BIND_ADDRESS = '127.0.0.1'
WAIT_TIME = 2  # seconds
SSH_PRIVATE_KEY = 'ignored'
# the month where we have the newest AHDB data
SOURCE_DATA_VERSION_MONTH = 'January 2019'
# maximum single contract size in term of GBP
MAX_SINGLE_SELL = 500000
# global price comparison tolerance
PRICE_TOLERANCE = max(0, 0.005)
# SDK version number
SDK_VERSION = 1.3
# limit the print depth for better output when testing
pprint = pprint.PrettyPrinter(depth=2)

###
### Note, currencies in risk pool or other per product functions use its own currency, e.g. pence
### use currency_factor function to deal with any income date to transfer to the right currency
###


''' 
----------------------------
SYSTEM FUNCTIONS
----------------------------
'''


class db:
    def __init__(self,
                 ssh_address=SSH_ADDRESS,
                 ssh_port=SSH_PORT,
                 ssh_user_name=SSH_USER_NAME,
                 ssh_private_key=SSH_PRIVATE_KEY,
                 remote_bind_address=REMOTE_BIND_ADDRESS,
                 db_port=DB_PORT,
                 auth_db=AUTH_DB,
                 db_user=DB_USER,
                 db_pw=DB_PW):
        try:
            # establish db collection, url, connection and used db
            mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (db_user, db_pw, SSH_ADDRESS, DB_PORT, auth_db)
            self.client = pymongo.MongoClient(mongo_uri, connectTimeoutMS=30000000, serverSelectionTimeoutMS=30000000)
        except:
            raise ConnectionError('Algo: Cannot connect to the db server!')

    def close_db(self):
        self.client.close()
        return None


# function to load index / final price and config file to db and indexing them
# web end, please DO NOT call this function!
def __load_finalprice_config(index_prices, final_price_df, config_file_df, version_month, db_client="I_AM_NOT_HERE"):
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client

    # Insert Index Price
    # convert numeric to fit BSON standard
    index_prices = index_prices.apply(pd.to_numeric, errors='ignore')
    # use df.to_dict('records') to transfer df to dict row by row
    index_prices_dict = index_prices.to_dict('records')

    # write file to database
    if client['IndexPrice'][version_month].count({}) > 0:
        print('Final Price for ', str(version_month), ' is already in db, please check.')
    else:
        print('Final Price for ', str(version_month), ' is inserted.')
        client['IndexPrice'][version_month].insert_many(index_prices_dict)

    # Insert Final Price
    # convert numeric to fit BSON standard
    final_price_df = final_price_df.apply(pd.to_numeric, errors='ignore')
    # use df.to_dict('records') to transfer df to dict row by row
    final_price_dict = final_price_df.to_dict('records')

    # write file to database
    if client['FinalPrice'][version_month].count({}) > 0:
        print('Final Price for ', str(version_month), ' is already in db, please check.')
    else:
        print('Final Price for ', str(version_month), ' is inserted.')
        client['FinalPrice'][version_month].insert_many(final_price_dict)

    # Insert Config file
    # convert numeric to fit BSON standard
    config_file_df = config_file_df.apply(pd.to_numeric, errors='ignore')
    # use df.to_dict('records') to transfer df to dict row by row
    config_file_dict = config_file_df.to_dict('records')

    # write file to database
    if client['ConfigFile'][version_month].count({}) > 0:
        print('Config File for ', str(version_month), ' is already in db, please check.')
    else:
        print('Config File for ', str(version_month), ' is inserted.')
        client['ConfigFile'][version_month].insert_many(config_file_dict)

    # create indices
    print('Creating indices for database...')
    client['FinalPrice'][version_month].create_indexes([IndexModel([("Product", pymongo.DESCENDING)]),
                                                        IndexModel([("Style", pymongo.ASCENDING)]),
                                                        IndexModel([("Shown Maturity", pymongo.ASCENDING)])])

    print('Indices for database created...')

    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return None


# push new version number to db
def __new_version_db_update(version_number, readme='', db_client="I_AM_NOT_HERE"):
    time_now = datetime.datetime.now()
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    # insert new version info on db
    new_version = {
        "Publish_Time": time_now,
        "SDK_VERSION_NUMBER": version_number,
        "SOURCE_DATA_VERSION_MONTH": SOURCE_DATA_VERSION_MONTH,
        "Changes": readme
    }
    client['_SERVER']['Version'].insert_one(new_version)
    print('\n\n\nAlgo Engine - New version updated!\n\n\n')
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return None


# testing to connect to db and check script version
def __link_and_version_check():
    # make db connection
    this_db = db(ssh_address=SSH_ADDRESS,
                 ssh_port=SSH_PORT,
                 ssh_user_name=SSH_USER_NAME,
                 ssh_private_key=SSH_PRIVATE_KEY,
                 remote_bind_address=REMOTE_BIND_ADDRESS,
                 db_port=DB_PORT,
                 auth_db=AUTH_DB,
                 db_user=DB_USER,
                 db_pw=DB_PW)
    client = this_db.client
    # find the newest version on db
    if_OK_to_go = False
    try:
        SERVER_NEWEST_VERSION = list(client['_SERVER']['Version'].find().sort('Publish_Time', -1).limit(1))[0]
        if SERVER_NEWEST_VERSION:
            if SERVER_NEWEST_VERSION['SDK_VERSION_NUMBER'] == SDK_VERSION:
                if_OK_to_go = True
                print('\033[1m', 'Algo Engine SDK is up to date.', '\033[0m')
            else:
                if_OK_to_go = False
                print('\033[1m', 'Algo Engine SDK is not up to date.', '\033[0m')
                warnings.warn('Algo Engine SDK is not up to date.')
                print('\033[1m', 'Algo Engine SDK is not up to date, the current version is: ', \
                      str(SDK_VERSION), '. However, the newest one on server is: ', \
                      str(SERVER_NEWEST_VERSION['SDK_VERSION_NUMBER']), '. Please check!', '\033[0m')
        else:
            if_OK_to_go = False
            print('\n\n\n', '\033[1m', 'Algo Engine SDK needs update.', '\033[0m')
            print('\n\n\n', '\033[1m', 'Algo Engine SDK needs update.', '\033[0m')
            print('\n\n\n', '\033[1m', 'Algo Engine SDK needs update.', '\033[0m')

    except:
        if_OK_to_go = False
        print('\n\n\n', '\033[1m', 'Cannot connect to Algo SDK server!!! Please Check!', '\033[0m')
        print('\n\n\n', '\033[1m', 'Cannot connect to Algo SDK server!!! Please Check!', '\033[0m')
        print('\n\n\n', '\033[1m', 'Cannot connect to Algo SDK server!!! Please Check!', '\033[0m')
        print('\n\n\n', '\033[1m', 'Cannot connect to Algo SDK server!!! Please Check!', '\033[0m')
        print('\n\n\n', '\033[1m', 'Cannot connect to Algo SDK server!!! Please Check!', '\033[0m')

    this_db.close_db()
    return if_OK_to_go


# making the checks
print("If the system is ready to go: ", str(__link_and_version_check()))

''' 
----------------------------
SUPPORTING FUNCTIONS
----------------------------
'''


# compare dates at different timescale
def if_same_day(date_to_be_checked, benchmark_date):
    # function to compare two dates
    day_check = date_to_be_checked.day == benchmark_date.day
    month_check = date_to_be_checked.month == benchmark_date.month
    year_check = date_to_be_checked.year == benchmark_date.year
    if day_check and month_check and year_check:
        return True
    else:
        return False


def if_same_month(date_to_be_checked, benchmark_date):
    # function to compare two dates
    month_check = date_to_be_checked.month == benchmark_date.month
    year_check = date_to_be_checked.year == benchmark_date.year
    if month_check and year_check:
        return True
    else:
        return False


def if_same_year(date_to_be_checked, benchmark_date):
    # function to compare two dates
    year_check = date_to_be_checked.year == benchmark_date.year
    if year_check:
        return True
    else:
        return False


# support function to make sure string is db name friendly
def good_db_name(string_name):
    new_string_name_works_with_db = str(string_name).replace(" ", "_")
    new_string_name_works_with_db = new_string_name_works_with_db.replace(".", "_")
    return new_string_name_works_with_db


# currency factor changer - a function to calculate the right currency values: e.g. 1 pence = 0.01 GBP
def currency_factor(face_value, from_currency_type, to_currency_type='GBP'):
    from_currency_type = str(from_currency_type).lower()
    to_currency_type = str(to_currency_type).lower()

    if from_currency_type == 'gbp' and to_currency_type == 'gbp':
        true_value = face_value
    elif from_currency_type == 'pence' and to_currency_type == 'gbp':
        true_value = 0.01 * face_value
    elif from_currency_type == 'gbp' and to_currency_type == 'pence':
        true_value = 100 * face_value
    else:
        print('Cannot make the currency converting!')
        raise AlgoSDKErrors('Cannot make the currency converting!')
    return true_value


''' 
----------------------------
LOW LEVEL FUNCTIONS
----------------------------
'''


class risk_pool:
    def __init__(self, db_client='', risk_pool_db='Risk_pools'):
        self.client = db_client
        self.db_name = risk_pool_db

    # adding risk capital for risk pool
    def add_rc(self, time_now, unique_trade_id,
               underwriter_name, underwriter_risk_pool_list):

        value_for_return = []
        for this_new_pool in underwriter_risk_pool_list:
            # check if this pool is already here
            pool_already_in_db = list(self.client[self.db_name][good_db_name(this_new_pool["Risk Pool Name"])]
                                      .find().sort('Counter', -1).limit(1))
            # if a pool is already here, make the adjustment
            if pool_already_in_db:
                pool_already_in_db = pool_already_in_db[0]
                # drop id for db
                pool_already_in_db.pop("_id", "")
                pool_already_in_db['Tracking_id'] = unique_trade_id
                # check current rp size and adjust the allocations
                current_available_uws_rc = pool_already_in_db['Allocations']
                # check if the current UW is in already in the risk capital
                if underwriter_name in current_available_uws_rc.keys():
                    # calculate the new weights
                    pool_already_in_db['Time'] = time_now
                    pool_already_in_db['Available Risk Capitals'] = pool_already_in_db['Available Risk Capitals'] + \
                                                                    max(0, this_new_pool['This Pool Size'])
                    pool_already_in_db['Total Injected Risk Capital'] = pool_already_in_db[
                                                                            'Total Injected Risk Capital'] + \
                                                                        max(0, this_new_pool['This Pool Size'])
                    pool_already_in_db['Current Total Risk Capital'] = pool_already_in_db[
                                                                           'Current Total Risk Capital'] + \
                                                                       max(0, this_new_pool['This Pool Size'])
                    # update the "Current Capping Size"
                    pool_already_in_db['Current Capping Size'] = pool_already_in_db['Available Risk Capitals'] * \
                                                                 pool_already_in_db['Current Capping Ratio']
                    pool_already_in_db['This trade size'] = max(0, this_new_pool['This Pool Size'])

                    current_available_total_rc = pool_already_in_db['Available Risk Capitals']
                    # update all old UWs
                    for UW in pool_already_in_db['Allocations'].keys():
                        if pool_already_in_db['Allocations'][UW]['UW'] == underwriter_name:
                            # update this underwriter
                            pool_already_in_db['Allocations'][underwriter_name]['Time_of_change'] = time_now

                            # adjust static values
                            pool_already_in_db['Allocations'][underwriter_name]['UW_total_injected_rc'] += \
                                max(0, this_new_pool['This Pool Size'])
                            pool_already_in_db['Allocations'][underwriter_name]['UW_current_total_rc'] += \
                                max(0, this_new_pool['This Pool Size'])
                            pool_already_in_db['Allocations'][underwriter_name]['UW_available_rc'] += \
                                max(0, this_new_pool['This Pool Size'])

                            # adjust share percentages
                            pool_already_in_db['Allocations'][underwriter_name]['UW_share_percentage'] = \
                                pool_already_in_db['Allocations'][underwriter_name]['UW_available_rc'] \
                                / current_available_total_rc

                        else:
                            pool_already_in_db['Allocations'][UW]['UW_share_percentage'] = \
                                pool_already_in_db['Allocations'][UW][
                                    'UW_available_rc'] / current_available_total_rc

                else:

                    # this UW is a new comer, work out the weights
                    pool_already_in_db['Time'] = time_now
                    pool_already_in_db['Available Risk Capitals'] += max(0, this_new_pool['This Pool Size'])
                    pool_already_in_db['Total Injected Risk Capital'] += max(0, this_new_pool['This Pool Size'])
                    pool_already_in_db['Current Total Risk Capital'] += max(0, this_new_pool['This Pool Size'])
                    # update the "Current Capping Size"
                    pool_already_in_db['Current Capping Size'] = pool_already_in_db['Available Risk Capitals'] * \
                                                                 pool_already_in_db['Current Capping Ratio']
                    current_available_total_rc = pool_already_in_db['Available Risk Capitals']

                    pool_already_in_db['This trade size'] = max(0, this_new_pool['This Pool Size'])

                    # update all old UWs
                    for UW in pool_already_in_db['Allocations'].keys():
                        pool_already_in_db['Allocations'][UW]['UW_share_percentage'] = \
                            pool_already_in_db['Allocations'][UW][
                                'UW_available_rc'] / current_available_total_rc
                    # build this new underwriter
                    pool_already_in_db['Allocations'][underwriter_name] = \
                        {
                            'Time_of_change': time_now,
                            'UW': underwriter_name,
                            'UW_share_percentage': max(0, this_new_pool['This Pool Size']) / current_available_total_rc,
                            'UW_total_injected_rc': max(0, this_new_pool['This Pool Size']),
                            'UW_total_removed_rc': 0,
                            'UW_total_lost_rc': 0,
                            'UW_current_total_rc': max(0, this_new_pool['This Pool Size']),
                            'UW_available_rc': max(0, this_new_pool['This Pool Size']),
                            'UW_pending_rc': 0,
                            'UW_locked_rc': 0
                        }

                # update db by push a new row
                pool_already_in_db['Counter'] += 1
                pool_already_in_db['Action'] = "Adding risk capital to risk pool"
                pool_already_in_db['SDK_VERSION_NUMBER'] = SDK_VERSION
                self.client[self.db_name][good_db_name(this_new_pool["Risk Pool Name"])].insert(pool_already_in_db)
                value_for_return.append(pool_already_in_db)
            else:
                # create as a new pool
                first_row = {}
                # get the top up amount
                this_top_up = max(0, this_new_pool["This Pool Size"])
                # setting up new risk pool record
                first_row['Counter'] = 1
                first_row['Time'] = time_now
                first_row['Risk Pool Name'] = this_new_pool['Risk Pool Name']
                first_row['Definition'] = this_new_pool['Definition']
                first_row['Units'] = this_new_pool['Units']
                first_row['Currency'] = this_new_pool['Currency']
                first_row['Market'] = this_new_pool['Market']

                # High Level Risk Capitals Views # <- always in its own currency
                first_row['Total Injected Risk Capital'] = this_top_up  # all risk capital ever been put here
                first_row['Total Removed Risk Capital'] = 0  # all risk capital that has been withdrawn
                first_row['Total Lost Risk Capital'] = 0
                first_row['Current Total Risk Capital'] = this_top_up

                # Availabilities # <- always in its own currency
                first_row['Available Risk Capitals'] = this_top_up  # risk capital still left to use
                first_row['Pending Risk Capitals'] = 0  # risk capital deployed to live contracts

                # Capping
                first_row['Pool Status'] = "Normal"  # for sell load, like if 80% full
                first_row['Current Capping Ratio'] = 0.2  # % left when to start purchase capping
                first_row['Current Capping Size'] = this_top_up * first_row[
                    'Current Capping Ratio']  # £ left when to start purchase capping

                # Others
                first_row['Locked Risk Capitals'] = 0  # for events like a tmp rc adj. or stop sell tmp
                # first_row['Lost Risk Capitals'] = 0

                # this trade
                first_row['This trade size'] = max(0, this_new_pool['This Pool Size'])
                first_row['Action'] = "Adding risk capital to risk pool"
                first_row['Tracking_id'] = unique_trade_id
                first_row['Allocations'] = {
                    underwriter_name:
                        {
                            'Time_of_change': time_now,
                            'UW': underwriter_name,
                            'UW_share_percentage': 1,
                            'UW_total_injected_rc': this_top_up,
                            'UW_total_removed_rc': 0,
                            'UW_total_lost_rc': 0,
                            'UW_current_total_rc': this_top_up,
                            'UW_available_rc': this_top_up,
                            'UW_pending_rc': 0,
                            'UW_locked_rc': 0
                        }
                }
                # insert this to the db
                first_row['SDK_VERSION_NUMBER'] = SDK_VERSION
                self.client[self.db_name][good_db_name(this_new_pool["Risk Pool Name"])].insert(first_row)
                value_for_return.append(first_row)
        return value_for_return

    # function check how much to sell for a given this_sell
    def purchase(self, Market, Product, Country_Code, Style,
                 current_requested_liability, Strike, Stop, Currency, Quantity_Requested):

        # TODO: Update the doc to reflect the new changes

        # check if enough risk cap is available
        ## find this risk pool
        rp_name = self.get_pool_name(Market=Market,
                                     Product=Product,
                                     Country_Code=Country_Code,
                                     Style=Style)
        rp_this_product = self.check_rc(risk_pool_name=rp_name)
        if rp_this_product == 'No such risk  pool':
            print('Error! Cannot find this risk pool!')
            raise AlgoSDKErrors('Error! Cannot find this risk pool!')

        ## making the checks
        available_rc = rp_this_product['Available Risk Capitals']
        rp_status = rp_this_product['Pool Status']
        ### if this is a normal sell: enough rc and pool is normal - sell what is purchased
        if available_rc > current_requested_liability and rp_status == "Normal":
            this_sale_liability = current_requested_liability
            this_sale_quantity = Quantity_Requested
            sale_condition = "Can sell in full."
        ### if left rc is not enough, but status is normal
        #### sell to the minimum of the capping ratio
        elif available_rc <= current_requested_liability and rp_status == "Normal":
            this_sale_liability = available_rc * rp_this_product['Current Capping Ratio']
            this_sale_quantity = int(this_sale_liability / abs(Strike - Stop))
            # update this_sale_liability based on int quantity
            this_sale_liability = this_sale_quantity * abs(Strike - Stop)
            sale_condition = "Single trade larger than rc left, sell partially."
        ### if rc cap has already be reached, sell 20%(capping ratio) of whatever is smaller
        elif rp_status == "Capping":
            this_sale_liability = min(available_rc * 0.2, current_requested_liability)
            this_sale_quantity = int(this_sale_liability / abs(Strike - Stop))
            # update this_sale_liability based on int quantity
            this_sale_liability = this_sale_quantity * abs(Strike - Stop)
            if abs(this_sale_quantity - Quantity_Requested) < PRICE_TOLERANCE:
                sale_condition = "Can sell in full."
            else:
                sale_condition = "RC capping reached, sell partially."
        ### if no rc left or cases where unclear what is rc status
        else:
            this_sale_liability = 0
            this_sale_quantity = 0
            sale_condition = "RC empty or unknown condition, no sale."

        # Note, no freezing of risk pool is conducted, this wil be in the confirm sell step
        # Note, no risk pool records will be logged, this will be in the confirm sell step as well

        return this_sale_liability, this_sale_quantity, sale_condition, rp_name

    # function to request risk capital for purchase,
    # risk capital are taken following the weights for each underwriters
    def request_rc(self, time_now, _unique_trade_id,
                   Market, Product, Country_Code, Style,
                   Strike, Stop, Currency,
                   this_sale_liability, this_sale_quantity, sale_condition):

        # TODO: Update the doc to reflect the new changes

        # check if enough risk cap is available
        ## find this risk pool
        rp_name = self.get_pool_name(Market=Market,
                                     Product=Product,
                                     Country_Code=Country_Code,
                                     Style=Style)
        rp_this_product = self.check_rc(risk_pool_name=rp_name)
        if rp_this_product == 'No such risk  pool':
            print('Error! Cannot find this risk pool!')
            raise AlgoSDKErrors('Error! Cannot find this risk pool!')
        pass

        rp_this_product['Counter'] += 1
        rp_this_product['Time'] = time_now
        rp_this_product['Tracking_id'] = _unique_trade_id

        # before write to risk pool, check again is enough is left
        available_rc = rp_this_product['Available Risk Capitals']
        if available_rc >= this_sale_liability:
            # ok to sell, up date the risk pool record
            rp_this_product['Available Risk Capitals'] = rp_this_product['Available Risk Capitals'] - max(0,
                                                                                                          this_sale_liability)
            rp_this_product['Pending Risk Capitals'] = rp_this_product['Pending Risk Capitals'] + max(0,
                                                                                                      this_sale_liability)
            rp_this_product['Action'] = "OK! Purchase made for: " + str(Market) + ' ' + str(Product) + ' ' \
                                        + str(Country_Code) + ' ' + str(Style)
            rp_this_product['This trade size'] = -max(0, this_sale_liability)

            # now, figure how much to take from each UW
            UW_distributions = {}
            UW_distributions['Time_of_change'] = time_now
            UW_distributions['Tracking_id'] = _unique_trade_id
            for this_UW in rp_this_product['Allocations'].keys():
                # for risk pools
                this_UW_amount = max(0, this_sale_liability) * rp_this_product['Allocations'][this_UW][
                    'UW_share_percentage']
                rp_this_product['Allocations'][this_UW]['Time_of_change'] = time_now
                rp_this_product['Allocations'][this_UW]['UW_available_rc'] -= this_UW_amount
                rp_this_product['Allocations'][this_UW]['UW_pending_rc'] += this_UW_amount
                # to record as UWs
                UW_distributions[this_UW] = {}
                UW_distributions[this_UW]['UW_share_percentage'] = \
                    rp_this_product['Allocations'][this_UW]['UW_share_percentage']
                UW_distributions[this_UW]['UW_this_sale_freeze'] = this_UW_amount

            if_sold = "Sold"
        else:
            # sorry mate, you are too slow, the risk capital is gone
            print('sorry mate, you are too slow, the risk capital is gone!')
            if_sold = "Insufficient RC"
            UW_distributions = {}
            # update the risk pool
            rp_this_product['Action'] = "Failed! Not enough risk capital for purchasing: " + str(Market) + ' ' + str(
                Product) + ' ' + str(Country_Code) + ' ' + str(Style)

        # update the risk pool status
        rp_this_product = self.update_pool_status(rp_this_product)
        # insert to DB
        rp_this_product['SDK_VERSION_NUMBER'] = SDK_VERSION
        self.client[self.db_name][rp_name].insert(rp_this_product)

        return if_sold, UW_distributions

    # return of risk capital when settle
    def settlement(self, time_now, _unique_trade_id, risk_pool_name, pay_off_to_farmers, db_client="I_AM_NOT_HERE"):
        ## find this risk pool record based on tracking id
        # make db connection
        if db_client == "I_AM_NOT_HERE":
            # make db connection
            this_db = db(ssh_address=SSH_ADDRESS,
                         ssh_port=SSH_PORT,
                         ssh_user_name=SSH_USER_NAME,
                         ssh_private_key=SSH_PRIVATE_KEY,
                         remote_bind_address=REMOTE_BIND_ADDRESS,
                         db_port=DB_PORT,
                         auth_db=AUTH_DB,
                         db_user=DB_USER,
                         db_pw=DB_PW)
            client = this_db.client
        else:
            client = db_client
        # find the newest record from the risk pool db
        current_pool = self.check_rc(risk_pool_name=risk_pool_name)
        # find this trade from risk pool
        this_trade = list(client[self.db_name][risk_pool_name].
                          find({"Tracking_id": _unique_trade_id}).
                          sort('Counter', -1).
                          limit(1))[0]
        # work out amount to return
        this_trade_liability = abs(this_trade['This trade size'])
        amount_to_return = abs(this_trade['This trade size']) - max(0, abs(pay_off_to_farmers))
        lost_risk_capital = max(0, abs(pay_off_to_farmers))

        # check if liability and amount to return make sense
        if pay_off_to_farmers < this_trade_liability + PRICE_TOLERANCE:
            # putting capital to back to risk pool
            current_pool['Counter'] += 1
            current_pool['Time'] = time_now
            current_pool['Available Risk Capitals'] += amount_to_return
            current_pool['Pending Risk Capitals'] -= this_trade_liability
            current_pool['Total Lost Risk Capital'] += lost_risk_capital
            current_pool['Current Total Risk Capital'] -= lost_risk_capital
            current_pool['Action'] = "OK! Risk capital returned to risk pool."
            current_pool['This trade size'] = amount_to_return
            current_pool['Tracking_id'] = _unique_trade_id
            # distribute all returned amount to the current risk pool based on the previous traded percentage
            for this_UW in current_pool['Allocations'].keys():
                # TODO: if new UW is here or just solder to some of the UWs
                # amount need to return to this UW
                this_UW_amount_to_return = amount_to_return * this_trade['Allocations'][this_UW][
                    'UW_share_percentage']
                this_UW_amount_lost = lost_risk_capital * this_trade['Allocations'][this_UW][
                    'UW_share_percentage']
                current_pool['Allocations'][this_UW]['Time_of_change'] = time_now
                current_pool['Allocations'][this_UW]['UW_available_rc'] += this_UW_amount_to_return
                current_pool['Allocations'][this_UW]['UW_pending_rc'] -= this_trade_liability * \
                                                                         this_trade['Allocations'][this_UW][
                                                                             'UW_share_percentage']
                current_pool['Allocations'][this_UW]['UW_total_lost_rc'] += this_UW_amount_lost
                current_pool['Allocations'][this_UW]['UW_current_total_rc'] -= this_UW_amount_lost
                # update the UW share percentages
                current_pool['Allocations'][this_UW]['UW_share_percentage'] = \
                    current_pool['Allocations'][this_UW]['UW_available_rc'] / \
                    current_pool['Available Risk Capitals']
            # update the risk pool status
            rp_this_product = self.update_pool_status(current_pool)
            # insert to DB
            rp_this_product['SDK_VERSION_NUMBER'] = SDK_VERSION
            self.client[self.db_name][risk_pool_name].insert(rp_this_product)

        else:
            msg = 'It seems the pay off to farmers is larger than the policy liability.' \
                  ' Of course this is not right and need some checks.' \
                  ' Please forward the tracking id to the Quant team. ' \
                  ' Thank you! _unique_trade_id: ' + str(_unique_trade_id)
            print(msg)
            raise AlgoSDKErrors(msg)

        return rp_this_product

    # remove risk capital when cost in payoff or UW ask to remove
    def remove_rc(self, _unique_trade_id, remove_amount):
        pass

    # cancel a policy - can cancel only if before settlement
    def cancel_policy(self, time_now, _unique_trade_id, risk_pool_name, db_client="I_AM_NOT_HERE"):
        ## find this risk pool record based on tracking id
        # make db connection
        if db_client == "I_AM_NOT_HERE":
            # make db connection
            this_db = db(ssh_address=SSH_ADDRESS,
                         ssh_port=SSH_PORT,
                         ssh_user_name=SSH_USER_NAME,
                         ssh_private_key=SSH_PRIVATE_KEY,
                         remote_bind_address=REMOTE_BIND_ADDRESS,
                         db_port=DB_PORT,
                         auth_db=AUTH_DB,
                         db_user=DB_USER,
                         db_pw=DB_PW)
            client = this_db.client
        else:
            client = db_client
        # find the newest record from the risk pool db
        current_pool = self.check_rc(risk_pool_name=risk_pool_name)
        # find this trade from risk pool
        this_trade = list(client[self.db_name][risk_pool_name].
                          find({"Tracking_id": _unique_trade_id}).
                          sort('Counter', -1).
                          limit(1))[0]

        # work out amount to return
        this_trade_liability = abs(this_trade['This trade size'])
        amount_to_return = abs(this_trade['This trade size'])
        lost_risk_capital = 0

        # check if liability and amount to return make sense
        if amount_to_return < this_trade_liability + PRICE_TOLERANCE:
            # putting capital to back to risk pool
            current_pool['Counter'] += 1
            current_pool['Time'] = time_now
            current_pool['Available Risk Capitals'] += amount_to_return
            current_pool['Pending Risk Capitals'] -= amount_to_return
            current_pool['Total Lost Risk Capital'] += lost_risk_capital
            current_pool['Action'] = "OK! Policy cancelled and risk capital returned to risk pool."
            current_pool['This trade size'] = amount_to_return
            current_pool['Tracking_id'] = _unique_trade_id
            # distribute all returned amount to the current risk pool based on the previous traded percentage
            for this_UW in current_pool['Allocations'].keys():
                # TODO: if new UW is here or just solder to some of the UWs
                # amount need to return to this UW
                this_UW_amount_to_return = amount_to_return * this_trade['Allocations'][this_UW][
                    'UW_share_percentage']
                current_pool['Allocations'][this_UW]['Time_of_change'] = time_now
                current_pool['Allocations'][this_UW]['UW_available_rc'] += this_UW_amount_to_return
                current_pool['Allocations'][this_UW]['UW_pending_rc'] -= this_UW_amount_to_return
                # update the UW share percentages
                current_pool['Allocations'][this_UW]['UW_share_percentage'] = \
                    current_pool['Allocations'][this_UW]['UW_available_rc'] / \
                    current_pool['Available Risk Capitals']
            # update the risk pool status
            rp_this_product = self.update_pool_status(current_pool)
            # insert to DB
            rp_this_product['SDK_VERSION_NUMBER'] = SDK_VERSION
            self.client[self.db_name][risk_pool_name].insert(rp_this_product)

        else:
            msg = 'It seems the pay off to farmers is larger than the policy liability. ' \
                  'Of course this is not right and need some checks. ' \
                  'Please forward the tracking id to the Quant team. ' \
                  'Thank you! _unique_trade_id: ' + str(_unique_trade_id)
            print(msg)
            raise AlgoSDKErrors(msg)

        return rp_this_product

    # function to check if 'Current Capping Size' is reached
    def update_pool_status(self, pool_already_in_db):
        # check if available rc is above capping size
        if pool_already_in_db['Available Risk Capitals'] > pool_already_in_db['Current Capping Size']:
            # where more than capping size risk capital is available, sell as normal
            pool_already_in_db['Pool Status'] = "Normal"
        elif pool_already_in_db['Available Risk Capitals'] <= pool_already_in_db['Current Capping Size']:
            # risk capital reached capping size, reduce sales ratio
            pool_already_in_db['Pool Status'] = "Capping"
        else:
            # Not sure if here will be reached or not, but stop selling if here
            pool_already_in_db['Pool Status'] = "Empty"

        return pool_already_in_db

    # check hwo much risk cap is there for this pool
    def check_rc(self, risk_pool_name):
        # check if this pool exists
        pool_already_in_db = list(
            self.client[self.db_name][risk_pool_name].find().sort('Counter', -1).limit(
                1))
        if pool_already_in_db:
            pool_already_in_db[0].pop('_id', '')
            return pool_already_in_db[0]
        else:
            return 'No such risk  pool'

    # function to find the right risk pool for this product
    def get_pool_name(self, Market, Product, Country_Code, Style):
        risk_pool_name = (str(Country_Code) + '_' + str(Product)).replace(" ", "")
        return risk_pool_name

    # lock risk capital in certain events
    def lock_risk_capital(self):
        pass


''' 
----------------------------
SDK FUNCTIONS
----------------------------
'''

''' 
----------------------------
SDK Function list:

* v 1.0 release Jan 2019 *

Exceptions
--
# AlgoSDKErrors - fun: AlgoSDKErrors(msg_to_report)


Config and Prices
--
# get most recent configure file
# get most recent index prices
# get most recent spread prices
# update the most recent index prices


Underwriter
--
# get a list of current underwriters - fun: get_underwriters()
# check the details for underwriter - fun: get_this_underwriter(underwriter_name)
# add underwriter and risk capital - fun: add_underwriter(underwriter_name, risk_pool_list)


Risk pool
--
# how to name a risk pool - fun: name_this_risk_pool(Market, Product, Country_Code, Style)
# get the current risk pool names - fun: get_risk_pools()
# check the details for risk pool - fun: get_this_risk_pool(risk_pool_name)
# check availability (most recent risk pool) - fun: how_much_left(risk_pool_name) 


Policy
--
# pre-purchase a policy - fun: sell_this(this_sell, SOURCE_DATA_VERSION_MONTH)
# confirm of a purchase - fun: confirm_to_sell(_unique_trade_id)
# settle a policy - fun: settle_this(_unique_trade_id, payoff_amount)
# cancel a policy - fun: cancel_this(_unique_trade_id)


Key Performance Indicators (Alivelator)
--
# Loss ratios - product(risk pool) / Underwriter / over-all  - fun: get_loss_ratio(risk_pool_name, underwriter_name)
# Premium ratio - product(risk pool) / Underwriter / over-all  - fun: get_premium_ratio(risk_pool_name, underwriter_name)
# ROI - product(risk pool) / Underwriter / over-all  - fun: get_roi(risk_pool_name, underwriter_name)


Real Money Simulator(Alivelator)
--
# Real time allocations - product(risk pool) / Underwriter / over-all - show who is owning what at what percentage
# fun: get_allocations(risk_pool_name, underwriter_name)
# Real time (cumulative) liabilities - product(risk pool) / Underwriter / over-all
# Real time premiums - product(risk pool) / Underwriter / over-all
# Real time (cumulative) payoff - product(risk pool) / Underwriter / over-all
# fun: get_RMS(risk_pool_name, underwriter_name)


* v 2.0 release August 2019 *
# pricing SDKs
# remove an underwriter
# adding time and date supprot for getting KPI values
# checking in the settlement function if the right payoff has been calculated
# tracking underwriter trading in their own db records
# remove risk capitals
# etc.

----------------------------
'''

### ---------------------------- START OF THE FUNCTIONS

'''
Underwriter
--
# get a list of current underwriters - fun: get_underwriters()
# check the details for underwriter - fun: get_this_underwriter(underwriter_name)
# add underwriter and risk capital - fun: add_underwriter(underwriter_name, risk_pool_list)
'''


# function to get current underwriters
def get_underwriters(db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client

    # obtain the all underwriters
    all_underwriters = client['Underwriter'].list_collection_names()
    # close the collection
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return all_underwriters


# function to get details of an underwriter
def get_this_underwriter(underwriter_name, db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client

    # get UW details
    this_underwriter_details = list(client['Underwriter'][underwriter_name].find({}))
    # close the collection
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return this_underwriter_details


# function to test get_underwriters and get_this_underwriter
def get_underwriter_tester():
    # get all current UWs
    all_underwriters = get_underwriters(db_client="I_AM_NOT_HERE")
    print('\033[1m', '\n\nCurrently we have the following Underwriters: ', '\033[0m')
    if all_underwriters:
        print([str(x) for x in all_underwriters])
        # return full details of one UW
        print('\033[1m', '\n\nFor ', str(all_underwriters[-1]), ' we have: ', '\033[0m')
        pprint.pprint(get_this_underwriter(all_underwriters[-1]))
    return None


# function to setup underwriter and add risk capital
def add_underwriter(underwriter_name, risk_pool_list, db_client="I_AM_NOT_HERE"):
    time_now = datetime.datetime.now()
    # build the unique trading identification (time + unique code)
    unique_trade_id = datetime.datetime.strftime(time_now, "%d%b%Y%H%M%S%f") + \
                      str(uuid.uuid4())[-6:]
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client

    # remove common naming errors
    underwriter = good_db_name(underwriter_name)
    # add UW and risk pool
    # check if we already had this underwriter
    if not underwriter in client['Underwriter'].list_collection_names():
        # build this UW
        this_UW = {
            "Underwriter Name": underwriter,
            "Time of Change": time_now,
            "Changes": risk_pool_list,
            "Counter": 1
        }

    else:
        # if we have already got this UW, push a new line
        # build this UW
        this_UW = {
            "Time of Change": time_now,
            "Changes": risk_pool_list,
            "Counter": list(client['Underwriter'][underwriter].find().sort('Counter', -1).limit(1))[0]['Counter'] + 1
        }

    # push the change to a new row
    this_UW['SDK_VERSION_NUMBER'] = SDK_VERSION
    client['Underwriter'][underwriter].insert(this_UW)

    # update the risk pools
    # create risk pool project
    rp = risk_pool(db_client=client)
    rp_added = rp.add_rc(time_now=time_now, unique_trade_id=unique_trade_id,
                         underwriter_name=underwriter, underwriter_risk_pool_list=risk_pool_list)
    # close the collection
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return this_UW, rp_added


# function to test add_underwriter
def add_underwriter_tester():
    # risk pool list is a sub list for the following list
    risk_pool_list = [
        {
            "Risk Pool Name": "GB_FeedWheat",
            "Definition": "UK Ex Farm",
            "Units": "Ton",
            "This Pool Size": 2000000,
            "Currency": "GBP",
            "Market": "Arable"
        },

        {
            "Risk Pool Name": "GB_FeedBarley",
            "Definition": "UK Ex Farm",
            "Units": "Ton",
            "This Pool Size": 2000000,
            "Currency": "GBP",
            "Market": "Arable"
        },

        {
            "Risk Pool Name": "GB_MillingWheat",
            "Definition": "UK Ex Farm",
            "Units": "Ton",
            "This Pool Size": 2000000,
            "Currency": "GBP",
            "Market": "Arable"
        },

        {
            "Risk Pool Name": "GB_OSR",
            "Definition": "UK Delivered (Erith OSR)",
            "Units": "Ton",
            "This Pool Size": 2000000,
            "Currency": "GBP",
            "Market": "Arable"
        },

        {
            "Risk Pool Name": "GB_Milk",
            "Definition": "UK Farmgate Milk Price (excluding bonuses)",
            "Units": "Litre",
            "This Pool Size": 2000000 * 100,
            "Currency": "Pence",
            "Market": "Dairy"
        },

        {
            "Risk Pool Name": "GB_DeadweightCattle",
            "Definition": "GB Deadweight Steers (All Grades)",
            "Units": "Kg",
            "This Pool Size": 2000000 * 100,
            "Currency": "Pence",
            "Market": "Livestock"
        },

        {
            "Risk Pool Name": "GB_LambDeadweight",
            "Definition": "GB Deadweight Lambs",
            "Units": "Kg",
            "This Pool Size": 2000000 * 100,
            "Currency": "Pence",
            "Market": "Livestock"
        },

        {
            "Risk Pool Name": "GB_Pig",
            "Definition": "SPP EU Spec",
            "Units": "Kg",
            "This Pool Size": 2000000 * 100,
            "Currency": "Pence",
            "Market": "Livestock"
        },

        {
            "Risk Pool Name": "GB_ANFertiliser",
            "Definition": "AN Fertiliser",
            "Units": "Ton",
            "This Pool Size": 2000000,
            "Currency": "GBP",
            "Market": "Input"
        },

        {
            "Risk Pool Name": "GB_RedDiesel",
            "Definition": "Red Diesel pence/litre",
            "Units": "Litre",
            "This Pool Size": 2000000 * 100,
            "Currency": "Pence",
            "Market": "Input"
        },
    ]

    # underwriter is a unique string of the UW name
    # call to add UW and risk cap
    print('\033[1m', '\n\nAdding Underwriters...', '\033[0m')
    add_underwriter(underwriter_name='Ascot_UK', risk_pool_list=risk_pool_list)
    print('\033[1m', 'Added Ascot_UK...', '\033[0m')
    add_underwriter(underwriter_name='Nephila_US', risk_pool_list=risk_pool_list)
    print('\033[1m', 'Added Nephila_US...', '\033[0m')
    risk_pool_list = [
        {
            "Risk Pool Name": "GB_FeedWheat",
            "Definition": "UK Ex Farm",
            "Units": "Ton",
            "This Pool Size": 0,
            "Currency": "GBP",
            "Market": "Arable"
        },

        {
            "Risk Pool Name": "GB_FeedBarley",
            "Definition": "UK Ex Farm",
            "Units": "Ton",
            "This Pool Size": 0,
            "Currency": "GBP",
            "Market": "Arable"
        },

        {
            "Risk Pool Name": "GB_MillingWheat",
            "Definition": "UK Ex Farm",
            "Units": "Ton",
            "This Pool Size": 0,
            "Currency": "GBP",
            "Market": "Arable"
        },

        {
            "Risk Pool Name": "GB_OSR",
            "Definition": "UK Delivered (Erith OSR)",
            "Units": "Ton",
            "This Pool Size": 0,
            "Currency": "GBP",
            "Market": "Arable"
        },

        {
            "Risk Pool Name": "GB_Milk",
            "Definition": "UK Farmgate Milk Price (excluding bonuses)",
            "Units": "Litre",
            "This Pool Size": 2000000 * 100,  # <- note the input of value at its currency
            "Currency": "Pence",
            "Market": "Dairy"
        },

        {
            "Risk Pool Name": "GB_DeadweightCattle",
            "Definition": "GB Deadweight Steers (All Grades)",
            "Units": "Kg",
            "This Pool Size": 2000000 * 100,  # <- note the input of value at its currency
            "Currency": "Pence",
            "Market": "Livestock"
        },

        {
            "Risk Pool Name": "GB_LambDeadweight",
            "Definition": "GB Deadweight Lambs",
            "Units": "Kg",
            "This Pool Size": 2000000 * 100,  # <- note the input of value at its currency
            "Currency": "Pence",
            "Market": "Livestock"
        },

        {
            "Risk Pool Name": "GB_Pig",
            "Definition": "SPP EU Spec",
            "Units": "Kg",
            "This Pool Size": 2000000 * 100,  # <- note the input of value at its currency
            "Currency": "Pence",
            "Market": "Livestock"
        },

        {
            "Risk Pool Name": "GB_ANFertiliser",
            "Definition": "AN Fertiliser",
            "Units": "Ton",
            "This Pool Size": 2000000,
            "Currency": "GBP",
            "Market": "Input"
        },

        {
            "Risk Pool Name": "GB_RedDiesel",
            "Definition": "Red Diesel pence/litre",
            "Units": "Litre",
            "This Pool Size": 2000000 * 100,  # <- note the input of value at its currency
            "Currency": "Pence",
            "Market": "Input"
        },
    ]
    added_UW = add_underwriter(underwriter_name='Redpanda', risk_pool_list=risk_pool_list)
    print('\033[1m', 'Added Redpanda...', '\033[0m')
    print('\033[1m', 'Underwriters added.', '\033[0m')
    return added_UW


'''
Risk pool
--
# how to name a risk pool - fun: name_this_risk_pool(Market, Product, Country_Code, Style)
# get the current risk pool names - fun: get_risk_pools()
# check the details for risk pool - fun: get_this_risk_pool(risk_pool_name)
# check availability (most recent risk pool) - fun: how_much_left(risk_pool_name) or a list of them fun: how_much_left_list(list_of_risk_pool_name)
'''


# function to name a risk pool
def name_this_risk_pool(Market, Product, Country_Code, Style):
    rp = risk_pool()
    return rp.get_pool_name(Market, Product, Country_Code, Style)


# function to get all current risk pool names
def get_risk_pools(client_already_here='I_am_not_here'):
    # make db connection
    if client_already_here == 'I_am_not_here':
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = client_already_here
    # obtain the all underwriters
    all_rps = client['Risk_pools'].list_collection_names()
    # close the collection
    if client_already_here == 'I_am_not_here':
        # close the collection
        this_db.close_db()
    return all_rps


# function to get the full details of one risk pool
def get_this_risk_pool(risk_pool_name, client_already_here='I_am_not_here'):
    # make db connection
    if client_already_here == 'I_am_not_here':
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = client_already_here
    # get UW details
    this_rp = list(client['Risk_pools'][risk_pool_name].find({}))
    # close the collection
    if client_already_here == 'I_am_not_here':
        # close the collection
        this_db.close_db()
    return this_rp


# function to get the most current availability
def how_much_left(risk_pool_name, client_already_here='I_am_not_here'):
    # make db connection
    if client_already_here == 'I_am_not_here':
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = client_already_here
    # get UW details
    this_rp_left = list(client['Risk_pools'][risk_pool_name].find({}).sort('Time', -1).limit(1))
    # close the collection
    if client_already_here == 'I_am_not_here':
        # close the collection
        this_db.close_db()
    return this_rp_left


# function to get the most current availability for a list of risk pool
def how_much_left_list(list_of_risk_pool_name, client_already_here='I_am_not_here'):
    if client_already_here == 'I_am_not_here':
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = client_already_here
    this_rp_left_list = []
    for this_rp in list_of_risk_pool_name:
        # get UW details
        this_rp_left_list.append(list(client['Risk_pools'][this_rp].find({}).sort('Time', -1).limit(1)))
    if client_already_here == 'I_am_not_here':
        # close the collection
        this_db.close_db()
    return this_rp_left_list


# function to test all risk pool functions
def risk_pool_tester():
    # return a risk pool name
    print(name_this_risk_pool('Dairy', 'Milk', 'AU', 'Cool_style'))
    # find all current risk pool
    current_pools = get_risk_pools()
    print('\033[1m', '\n\nCurrently we have the following risk pools: ', '\033[0m')
    print([str(x) for x in current_pools])
    # return full details of one pool
    print('\033[1m', '\n\nFor ', str(current_pools[-1]), ' we have: ', '\033[0m')
    pprint.pprint(get_this_risk_pool(current_pools[-1]))
    # return availability
    print('\033[1m', '\n\nThe availability for GB_DeadweightCattle  is: ', '\033[0m')
    pprint.pprint(how_much_left('GB_DeadweightCattle'))
    pprint.pprint(how_much_left_list(['GB_DeadweightCattle', 'GB_Milk']))
    return None


'''
Policy
--
# pre-purchase a policy - fun: sell_this(this_sell, SOURCE_DATA_VERSION_MONTH)
# confirm of a purchase - fun: confirm_to_sell(_unique_trade_id)
# settle a policy - fun: settle_this(_unique_trade_id, payoff_amount)
# cancel a policy - fun: cancel_this(_unique_trade_id)
'''


# function try to confirm a sell of one policy
def sell_this(this_sell, SOURCE_DATA_VERSION_MONTH, db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client

    # write this trade to db as request no matter what - just to be sure we can go back and check
    this_sell['Call Received Time'] = datetime.datetime.now()
    # build the unique trading identification (time + unique code)
    unique_trade_id = datetime.datetime.strftime(this_sell['Call Received Time'], "%d%b%Y%H%M%S%f") + \
                      str(uuid.uuid4())[-6:]
    this_sell['_unique_trade_id'] = unique_trade_id
    client['Trade']['Received'].insert_one(this_sell)

    # check if user id and policy number combination is already registered
    if client['Trade']['Trades'].count({
                                           "user_id": this_sell["user_id"],
                                           "policy_id": this_sell["policy_id"]
                                       }) > 0:
        this_sell['if_confirmed'] = False
        this_sell['comment'] = 'User_id and trade_id combination already exists in database.'

        # Note the exit here
        # finished and close db
        if db_client == "I_AM_NOT_HERE":
            this_db.close_db()
        return this_sell

    # check if all variable make sense
    version_month = SOURCE_DATA_VERSION_MONTH
    # check if we can find the contract
    this_policy = client['FinalPrice'][version_month].find_one({
                                                                   "Product": this_sell["Product"],
                                                                   "Style": this_sell["Style"],
                                                                   "Shown Maturity": this_sell["Shown Maturity"],
                                                                   "Strike Ratio": this_sell["Strike Ratio"],
                                                                   "Stop Ratio": this_sell["Stop Ratio"],
                                                                   "Policy Shown Start Date": this_sell[
                                                                       "Policy Shown Start Date"]
                                                               })
    # if we do have this policy
    if this_policy:
        # confirm prices:
        this_sell['if_confirmed'] = 1
        this_sell['comment'] = []
        # strike price
        if abs(this_policy["Strike"] - this_sell["Strike"]) < PRICE_TOLERANCE:
            this_sell['if_confirmed'] = 1
        else:
            this_sell['if_confirmed'] = 0
            this_sell['comment'].append('Strike price does not match.')

        # stop price
        if abs(this_policy["Stop"] - this_sell["Stop"]) < PRICE_TOLERANCE:
            this_sell['if_confirmed'] *= 1
        else:
            this_sell['if_confirmed'] *= 0
            this_sell['comment'].append('Stop price does not match.')

        # spread price
        if abs(this_policy["Spread Price"] - this_sell["Spread Price"]) < PRICE_TOLERANCE:
            this_sell['if_confirmed'] = True
        else:
            this_sell['if_confirmed'] = False
            this_sell['comment'].append('Spread price does not match.')

        # Total Amount to Pay
        should_charge_as_total = (this_policy["Spread Price"] * this_sell["Quantity Requested"] - this_sell[
            "Discount Amount From Voucher"]) * \
                                 (this_sell["IPT Rate"] + 1)
        if abs(should_charge_as_total - this_sell["Total Amount to Pay"]) < PRICE_TOLERANCE:
            this_sell['if_confirmed'] *= 1
        else:
            this_sell['if_confirmed'] *= 0
            this_sell['comment'].append('Total Amount to Pay does not match.')

        # work out the current max liability
        current_max_liability = abs(this_sell["Strike"] - this_sell["Stop"]) * this_sell["Quantity Requested"]
        # check if over the single max liability
        if abs(current_max_liability) <= currency_factor(MAX_SINGLE_SELL,
                                                         from_currency_type='GBP',
                                                         to_currency_type=this_sell["Currency"]):
            this_sell['if_confirmed'] *= 1
        else:
            this_sell['if_confirmed'] *= 0
            this_sell['comment'].append('Single contract size (' + \
                                        str(current_max_liability) +
                                        ') too large, max allowed is: ' + str(MAX_SINGLE_SELL))

        # check availability and risk capital
        rp = risk_pool(db_client=client)
        # find current availability for this product

        this_sale_liability, this_sale_quantity, sale_condition, rp_name = rp.purchase(
            Market=this_sell['Market'],
            Product=this_sell['Product'],
            Country_Code=this_sell['Country Code'],
            Style=this_sell['Style'],
            current_requested_liability=current_max_liability,
            Strike=this_sell['Strike'],
            Stop=this_sell['Stop'],
            Currency=this_sell['Currency'],
            Quantity_Requested=this_sell['Quantity Requested'])

        if sale_condition == "Can sell in full.":
            this_sell['if_confirmed'] *= 1
        else:
            this_sell['if_confirmed'] *= 0
            this_sell['comment'].append(sale_condition)

        # update this_sell record
        this_sell['this_sale_liability'] = this_sale_liability
        this_sell['this_sale_quantity'] = this_sale_quantity
        this_sell['sale_condition'] = sale_condition
        this_sell['Risk Pool Name'] = rp_name

    else:
        this_sell['if_confirmed'] = 0
        this_sell['comment'] = ['Cannot find this policy in database.']

    # change data type of if confirmed
    this_sell['if_confirmed'] = ((this_sell['if_confirmed']) == 1)
    this_sell['if_sold'] = "Just Queried"  # note, in this stage, we do not sell any thing

    # insert to db into the queue and wait to be confirmed
    client['Trade']['Queue'].insert_one(this_sell)
    # Note the exit here
    # finished and close db
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return this_sell


# testing function for sell_this
def sell_this_tester(SOURCE_DATA_VERSION_MONTH, db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client

    # test normal sell for 10 times
    for i in range(10):
        # make collections and choose random product from db to sell

        # get two random collections from the final price
        valid_prices = \
            list(client['FinalPrice'][SOURCE_DATA_VERSION_MONTH].aggregate([{"$sample": {"size": 1}}]))[0]
        config_file = list(client['ConfigFile'][SOURCE_DATA_VERSION_MONTH].find())

        def find_config(this_product, config_file_list):
            this_config = [x for x in config_file_list if x['Commodity Name as in the CSV file'] == str(this_product)]
            return this_config[0]

        time_now = datetime.datetime.now()
        this_sell = {
            # basic policy info
            "Quotation Time": time_now,
            "Market": find_config(valid_prices['Product'], config_file)['Market'],
            "Product": valid_prices['Product'],
            "Country Code": "GB",
            "Product Index Price Quoted Date": valid_prices['Policy Real Start Date'],
            "Style": valid_prices['Style'],
            "Shown Maturity": valid_prices['Shown Maturity'],
            "Strike Ratio": valid_prices['Strike Ratio'],
            "Strike": valid_prices['Strike'],
            "Stop": valid_prices['Stop'],
            "Stop Ratio": valid_prices['Stop Ratio'],
            "Spread Price": valid_prices['Spread Price'],
            "Start Asset Price": valid_prices['Start Asset Price'],
            "Policy End Date": valid_prices['Policy End Date'],
            "Policy Shown Start Date": valid_prices['Policy Shown Start Date'],
            "Units": find_config(valid_prices['Product'], config_file)['Units'],
            "Currency": find_config(valid_prices['Product'], config_file)['Currency'],

            # basic sells info
            "IPT Rate": 0.12,
            "IPT Amount": valid_prices['Spread Price'] * 100 * 0.12,
            "Quantity Requested": 100,
            "Total Price With IPT": (valid_prices['Spread Price'] * 100 - 500) * 1.12,
            "Total Price Without IPT": valid_prices['Spread Price'] * 100,

            # vouchers
            "Discount Amount From Voucher": 500,

            # payment
            "Total Amount to Pay": (valid_prices['Spread Price'] * 100 - 500) * 1.12,

            # Client and policy info
            "user_id": time.time(),
            "policy_id": time.time(),
        }

        # everything is all right
        this_sell_done = sell_this(this_sell, SOURCE_DATA_VERSION_MONTH)
        print('Just tried to sell: ', str(this_sell['Product']))

    # finished and close db
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    # testing the errors
    time_now = datetime.datetime.now()

    this_sell = {
        # basic policy info
        "Quotation Time": time_now,
        "Market": "Arable",
        "Product": "Feed Wheat",
        "Country Code": "GB",
        "Product Index Price Quoted Date": "September 2018",
        "Style": "European Put BearSpread",
        "Shown Maturity": 12,
        "Strike Ratio": 0.91,
        "Strike": 888,
        "Stop": 888,
        "Stop Ratio": 0.5,
        "Spread Price": 888,
        "Start Asset Price": 30.63436173,
        "Policy End Date": "November 2019",
        "Policy Shown Start Date": "October 2018",
        "Units": "Litre",
        "Currency": "Pence",

        # basic sells info
        "IPT Rate": 0.12,
        "IPT Amount": 7951.4514561391,
        "Quantity Requested": 50000,
        "Total Price With IPT": 888,
        "Total Price Without IPT": 66262.0954678262,

        # vouchers
        "Discount Amount From Voucher": 4213.5469239654,

        # payment
        "Total Amount to Pay": 8888888888888888888,

        # Client and policy info
        "user_id": time.time(),
        "policy_id": time.time(),
    }

    this_sell_error = sell_this(this_sell, SOURCE_DATA_VERSION_MONTH)
    pprint.pprint(this_sell_error)

    return this_sell_done, this_sell_error


# function to confirm a quoted sell
def confirm_to_sell(_unique_trade_id, db_client="I_AM_NOT_HERE"):
    time_now = datetime.datetime.now()
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client

    # find this quote from database
    this_sell = client['Trade']['Queue'].find_one({'_unique_trade_id': _unique_trade_id})

    if this_sell and this_sell['if_confirmed']:
        this_sell.pop('_id', '')
        this_sell['Call Received Time'] = time_now
        # find the risk pool
        rp = risk_pool(db_client=client)
        # request to use risk pool - frozen the rc amount
        if_sold, UW_distributions = rp.request_rc(time_now=time_now,
                                                  _unique_trade_id=_unique_trade_id,
                                                  Market=this_sell['Market'],
                                                  Product=this_sell['Product'],
                                                  Country_Code=this_sell['Country Code'],
                                                  Style=this_sell['Style'],
                                                  Strike=this_sell['Strike'],
                                                  Stop=this_sell['Stop'],
                                                  Currency=this_sell['Currency'],
                                                  this_sale_liability=this_sell['this_sale_liability'],
                                                  this_sale_quantity=this_sell['this_sale_quantity'],
                                                  sale_condition=this_sell['sale_condition'])
        this_sell['Risk Pool Confirmation Time'] = time_now
        this_sell['if_sold'] = if_sold
        this_sell['UW_allocations'] = UW_distributions
        # update the queue list as well for if_sold
        client['Trade']['Queue'].find_one_and_update(
            {"_unique_trade_id": _unique_trade_id},
            {"$set": {"if_sold": "Sold"}}
        )
        # write this sell to trading db
        client['Trade']['Trades'].insert(this_sell)
    else:
        # cannot find this tracking_id in db
        msg = 'Cannot find this unique tracking id in the database, please make sure the followings are right. \n' \
              '1. The right _unique_trade_id is used. \n' \
              '2. You have called the sell_this function first. \n' \
              '3. sell_this function does not given any error message. \n' \
              '4. You are sure the passed sell has if_confrimed is True.  \n' \
              '5. Please contact the Quant team for details if you have no idea why this happens.' \
              '\nThe tracking id is: ' + _unique_trade_id
        print(msg)
        raise AlgoSDKErrors(msg)

    # Note the exit here
    # finished and close db
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return this_sell


# function to test confirm sell
def confirm_to_sell_tester(tracking_id_works, tracking_id_not_working):
    done_sell = confirm_to_sell(tracking_id_works)
    print('A normail sell is normal, now comes a wrong one: ')
    try:
        error_sell = confirm_to_sell(tracking_id_not_working)
    except AlgoSDKErrors:
        print('Normal error found.')
    return done_sell


# settle a contract when it ends
def settle_this(_unique_trade_id, payoff_amount, db_client="I_AM_NOT_HERE"):
    time_now = datetime.datetime.now()
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    # try to find this tracking_id from the trade db
    this_settle = list(client['Trade']['Trades'].find({'_unique_trade_id': _unique_trade_id}))
    # and check if is_sold is true
    if len(this_settle) == 1:
        this_settle = this_settle[0]
        this_settle.pop('_id', '')
        this_settle['Call Received Time'] = time_now
        if this_settle['if_sold'] == "Sold":
            # return risk capital
            # find the risk pool
            rp = risk_pool(db_client=client)
            # reduce cost risk capital
            rp.settlement(time_now=time_now,
                          _unique_trade_id=_unique_trade_id,
                          risk_pool_name=this_settle['Risk Pool Name'],
                          pay_off_to_farmers=payoff_amount, db_client=client)
            # write to database
            this_settle['Settlement Time'] = time_now
            this_settle['Payoff Amount'] = payoff_amount
            this_settle['if_sold'] = 'Settled'
            this_settle['UW_allocations']['Time_of_change'] = time_now

            for this_UW in this_settle['UW_allocations'].keys():
                if this_UW in ['Time_of_change', 'Tracking_id']:
                    pass
                else:
                    this_settle['UW_allocations'][this_UW].pop('UW_this_sale_freeze', '')
                    # calculate what each UW need to pay
                    this_settle['UW_allocations'][this_UW]['Underwriter_to_pay'] = \
                        this_settle['UW_allocations'][this_UW]['UW_share_percentage'] * payoff_amount

            # insert to db
            client['Trade']['Trades'].insert_one(this_settle)

        # if is_sold is wrong, some series checking is needed
        else:
            this_settle = {
                'unique_tracking_id': _unique_trade_id,
                'settlement': 'if_sold is not True, cannot process the settlement.'
            }
    else:
        this_settle = {
            'unique_tracking_id': _unique_trade_id,
            'settlement': 'more than one record found for this tracking_id, cannot process the settlement.'
        }

    # Note the exit here
    # finished and close db
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return this_settle


# function to test settle_this
def settle_this_tester(_unique_trade_id, payoff_amount):
    # normal settlements
    settle_doc = settle_this(_unique_trade_id, payoff_amount)
    pprint.pprint(settle_doc)
    # error: try to settle again
    pprint.pprint(settle_this(_unique_trade_id, 888))
    # error: try to settle with an non-id
    pprint.pprint(settle_this('you cannot find me', payoff_amount))


# function to cancel a policy
def cancel_this(_unique_trade_id, db_client="I_AM_NOT_HERE"):
    time_now = datetime.datetime.now()
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    # try to find this tracking_id from the trade db
    trades = this_cancellation = list(client['Trade']['Trades'].find({'_unique_trade_id': _unique_trade_id}))
    # and check if is_sold is true
    if len(this_cancellation) == 1:
        this_cancellation = this_cancellation[0]
        this_cancellation['Call Received Time'] = time_now
        this_cancellation.pop('_id', '')
        if this_cancellation['if_sold'] == "Sold":
            # return risk capital
            # find the risk pool
            rp = risk_pool(db_client=client)
            # reduce cost risk capital
            rp.cancel_policy(time_now=time_now,
                             _unique_trade_id=_unique_trade_id,
                             risk_pool_name=this_cancellation['Risk Pool Name'], db_client=client)
            # write to database
            this_cancellation['Settlement Time'] = time_now
            this_cancellation['Payoff Amount'] = 0
            this_cancellation['if_sold'] = 'Cancelled'
            this_cancellation['UW_allocations']['Time_of_change'] = time_now

            for this_UW in this_cancellation['UW_allocations'].keys():
                if this_UW in ['Time_of_change', 'Tracking_id']:
                    pass
                else:
                    # calculate what each UW need to pay
                    this_cancellation['UW_allocations'][this_UW]['UW_this_cancel_release'] = \
                        this_cancellation['UW_allocations'][this_UW]['UW_this_sale_freeze']
                    this_cancellation['UW_allocations'][this_UW].pop('UW_this_sale_freeze', '')

            # insert to db
            client['Trade']['Trades'].insert_one(this_cancellation)

        # if is_sold is wrong, some series checking is needed
        else:
            this_cancellation = {
                'unique_tracking_id': _unique_trade_id,
                'settlement': 'if_sold is not True, cannot process the cancellation request.',
                'trades': trades
            }
    else:
        this_cancellation = {
            'unique_tracking_id': _unique_trade_id,
            'settlement': 'more than one record found for this tracking_id, cannot process the cancellation request.',
            'trades': trades
        }

    # Note the exit here
    # finished and close db
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    print(this_cancellation)
    return this_cancellation


# function to test the cancel_this
def cancel_this_tester(_unique_trade_id):
    # normal cancellation
    settle_doc = cancel_this(_unique_trade_id)
    pprint.pprint(settle_doc)
    # error: try to cancellation again
    pprint.pprint(cancel_this(_unique_trade_id))
    # error: try to cancellation with an non-id
    pprint.pprint(cancel_this('you cannot find me'))


# function to test the full policy loop
def policy_tester():
    # Try to purchase a policy
    # Confirm of the purchase
    # Settle this policy
    # Try to purchase a policy but with huge contract size
    # Confrim to purchase just portion of it
    # Cancel the policy
    pass


'''
Key Performance Indicators (Alivelator)
--
# Loss ratios - product(risk pool) / Underwriter / over-all  - fun: get_loss_ratio(risk_pool_name, underwriter_name)
# Premium ratio - product(risk pool) / Underwriter / over-all  - fun: get_premium_ratio(risk_pool_name, underwriter_name)
# ROI - product(risk pool) / Underwriter / over-all  - fun: get_roi(risk_pool_name, underwriter_name)
'''

# function to get all indicators and KPIs on the dashboard (new version) (cached)
# number of farmers (live contract and all)
# number of contracts (live, all, month, year)
# gross premium written (all, month, year)
# claims paid (all, month, year)
# number of product available
# total risk capital <- Total Risk Capital = Risk Capital Utilised + Free Risk Capital + Risk Capital Lost for Claims
# Risk Capital Lost for Claims
# Risk capital utilised <- Eri: maximum liability of contracts currently live
# Free risk capital
# list_of_all_risk_pools, list_of_all_availabilities
'''
;param string or datetime obj. benchmark_date: can be "today" or a datetime obj like: datetime.datetime.now()
;param string or pymongo obj. db_client: can be string "I_AM_NOT_HERE" or a db connection for better performance
'''


def get_dash_board_cards_KPIs(benchmark_date="today", db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client

    # set time date  bench mark
    if benchmark_date == 'today':
        benchmark_date = datetime.datetime.now()
    # read the cache from db
    dash_board_cards_KPIs_cached = \
    list(client['Cache']['get_dash_board_cards_KPIs'].find().limit(1).sort('Time_of_cache', -1))[0]
    dash_board_cards_KPIs = dash_board_cards_KPIs_cached['par_dash_board_cards_KPIs']
    # finished and close db
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return dash_board_cards_KPIs


# function to get loss ratio
def get_loss_ratio(risk_pool_name='', underwriter_name='', benchmark_date='today', db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    # build the right db name to search for
    db_name = 'get_loss_ratio' + good_db_name(risk_pool_name) + good_db_name(underwriter_name)
    # read the cache from db
    get_loss_ratio_cached = list(client['Cache'][db_name].find().limit(1).sort('Time_of_cache', -1))[0]
    loss_ratio, loss_ratio_full = get_loss_ratio_cached['par_loss_ratio'], get_loss_ratio_cached['par_loss_ratio_full']

    # close the db link
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return loss_ratio, loss_ratio_full


# function to get Premium Ratio
def get_premium_ratio(risk_pool_name='', underwriter_name='', benchmark_date='today', db_client="I_AM_NOT_HERE"):
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    # build the right db name to search for
    db_name = 'get_premium_ratio' + good_db_name(risk_pool_name) + good_db_name(underwriter_name)
    # read the cache from db
    get_premium_ratio_cached = list(client['Cache'][db_name].find().limit(1).sort('Time_of_cache', -1))[0]
    premium_ratio, premium_ratio_full = get_premium_ratio_cached['par_premium_ratio'], get_premium_ratio_cached[
        'par_premium_ratio_full']

    # close the db link
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return premium_ratio, premium_ratio_full


# function to get ROI
def get_ROI(risk_pool_name='', underwriter_name='', benchmark_date='today', db_client="I_AM_NOT_HERE"):
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    # build the right db name to search for
    db_name = 'get_ROI' + good_db_name(risk_pool_name) + good_db_name(underwriter_name)
    # read the cache from db
    get_ROI_cached = list(client['Cache'][db_name].find().limit(1).sort('Time_of_cache', -1))[0]
    ROI, ROI_full = get_ROI_cached['par_ROI'], get_ROI_cached['par_ROI_full']

    # close the db link
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return ROI, ROI_full


# function to test to get all KPI values
def get_KPIs_tester():
    print('\n\nKPI values obtainer tests:')
    # LR
    tic = time.time()
    print('Current overall LossRatio is: ', str(get_loss_ratio()))
    print('Time cost for query is: ', str(time.time() - tic), ' s.')
    tic = time.time()
    print('Current Redpanda LossRatio is: ', str(get_loss_ratio(underwriter_name='Redpanda')))
    print('Time cost for query is: ', str(time.time() - tic), ' s.')
    tic = time.time()
    print('Current Milk LossRatio is: ', str(get_loss_ratio(risk_pool_name='GB_Milk')))
    print('Time cost for query is: ', str(time.time() - tic), ' s.')
    tic = time.time()
    print('Current Milk LossRatio for Redpanda is: ',
          str(get_loss_ratio(risk_pool_name='GB_Milk', underwriter_name='Redpanda')))
    print('Time cost for query is: ', str(time.time() - tic), ' s.')
    # PR
    tic = time.time()
    print('Current overall Premium Ratio is: ', str(get_premium_ratio()))
    print('Time cost for query is: ', str(time.time() - tic), ' s.')
    # ROI
    tic = time.time()
    print('Current overall ROI is: ', str(get_ROI()))
    print('Time cost for query is: ', str(time.time() - tic), ' s.')
    # get dashboard data
    tic = time.time()
    this_results = get_dash_board_cards_KPIs()
    pprint.pprint(this_results)
    with open('Dashboard.json', 'w') as outfile:
        json.dump(this_results, outfile, indent=3, sort_keys=True, default=str)
    print('Time cost for dashboard query is: ', str(time.time() - tic), ' s.')


'''
Index Price and Settlement
--
# add/update/modify indices - fun: push_new_index(new_price_dict)
# return the current average price (for run off and settlement) - fun: current_prices(this_month = SOURCE_DATA_VERSION_MONTH)
# calculate the run off capital - fun: run_off_cap(this_policy)
# calculate the payoff for a matured policy - fun: pay_off(this_policy)

'''


def push_new_index(new_price_dict, db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    if_changed = False
    # note, this is a very dangour step, it may trigger settlement and re-pricing in the future

    # close the db link
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return if_changed


def current_prices(this_month=SOURCE_DATA_VERSION_MONTH):
    pass


'''
Real Money Simulator(Alivelator)
--
# Real time allocations - product(risk pool) / Underwriter / over-all - show who is owning what at what percentage
# fun: get_allocations(risk_pool_name, underwriter_name)
# Real time (cumulative) liabilities - product(risk pool) / Underwriter / over-all
# Real time premiums - product(risk pool) / Underwriter / over-all
# Real time (cumulative) payoff - product(risk pool) / Underwriter / over-all
# fun: get_RMS(risk_pool_name, underwriter_name)

'''


# function to get everything for RMS (cached)
def get_RMS(risk_pool_name='', underwriter_name='', db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    # build the right db name to search for
    db_name = 'get_RMS' + good_db_name(risk_pool_name) + good_db_name(underwriter_name)
    # read the cache from db
    get_RMS_cached = list(client['Cache'][db_name].find().limit(1).sort('Time_of_cache', -1))[0]
    this_RMS, this_month_liability = get_RMS_cached['par_this_RMS'], get_RMS_cached['par_this_month_liability']

    # close the db link
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return this_RMS, this_month_liability


# function to get allocations (rp weights in different products)
def get_allocations(risk_pool_name='', underwriter_name='', db_client="I_AM_NOT_HERE"):
    # make db connection
    if db_client == "I_AM_NOT_HERE":
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
    else:
        client = db_client
    # get the corresponding values
    allocations_dict = {}
    if risk_pool_name:
        if underwriter_name:
            # cases where both rp and UW are give,
            # and for allocations, this means 100%
            allocations_dict = {underwriter_name: {risk_pool_name: 1}}
            client.list_databases()
        else:
            # cases where only rp is give but UW is not
            # thus, we show the rc allocations for all UW in this risk pool
            all_risk_pool_records_fit = list(
                client['Risk_pools'][risk_pool_name].find().sort('Counter', -1).limit(1))
            # get loss ratio
            if all_risk_pool_records_fit:
                for this_UW in all_risk_pool_records_fit[0]['Allocations'].keys():
                    allocations_dict[this_UW] = \
                        {risk_pool_name: all_risk_pool_records_fit[0]['Allocations'][this_UW]['UW_share_percentage']}
            else:
                allocations_dict = {}
    else:
        if underwriter_name:
            # cases where only UW is given and rp is not
            # loop over all risk pool with this UW
            list_of_current_rps = get_risk_pools()
            allocations_dict[underwriter_name] = {}
            for this_RP in list_of_current_rps:
                all_risk_pool_records_fit = list(client['Risk_pools'][this_RP]. \
                                                 find({"Allocations." + underwriter_name: {"$exists": True}}).sort(
                    'Counter', -1).limit(1))
                if all_risk_pool_records_fit:
                    allocations_dict[underwriter_name][this_RP] = \
                        all_risk_pool_records_fit[0]['Allocations'][underwriter_name]['UW_share_percentage']
                else:
                    allocations_dict[underwriter_name][this_RP] = 0
        else:
            # loop over all rp for all UWs
            list_of_current_rps = get_risk_pools()
            list_of_current_UWs = get_underwriters()

            for this_UW in list_of_current_UWs:
                allocations_dict[this_UW] = {}
                print('checking ', str(this_UW))
                for this_RP in list_of_current_rps:
                    all_risk_pool_records_fit = list(client['Risk_pools'][this_RP]. \
                                                     find({"Allocations." + this_UW: {"$exists": True}}).sort('Counter',
                                                                                                              -1).limit(
                        1))
                    if all_risk_pool_records_fit:
                        allocations_dict[this_UW][this_RP] = \
                            all_risk_pool_records_fit[0]['Allocations'][this_UW]['UW_share_percentage']
                    else:
                        allocations_dict[this_UW][this_RP] = 0
    # close the db link
    if db_client == "I_AM_NOT_HERE":
        this_db.close_db()
    return allocations_dict


def rms_tester():
    print('Test for allocation starts: ')
    # testing allocations
    print('Current over-all allocation is: ')
    print(get_allocations())
    print('Allocation for risk_pool_name = GB_Milk')
    print(get_allocations(risk_pool_name='GB_Milk'))
    print('Allocation for underwriter_name = Redpanda')
    print(get_allocations(underwriter_name='Redpanda'))
    print('Allocation for risk_pool_name = GB_Milk,  underwriter_name = Redpanda')
    print(get_allocations(risk_pool_name='GB_Milk', underwriter_name='Redpanda'))
    # test different setting for real money simulator
    print('RMS for risk_pool_name = GB_Milk')
    print(get_RMS(risk_pool_name='GB_Milk'))
    print('RMS for underwriter_name = Redpanda')
    print(get_RMS(underwriter_name='Redpanda'))
    print('RMS for risk_pool_name = GB_Milk,  underwriter_name = Redpanda')
    print(get_RMS(risk_pool_name='GB_Milk', underwriter_name='Redpanda'))
    print('Current over-all RMS is: ')
    print(get_RMS())
    return 0


if __name__ == "__main__":

    # add new final price and config from db
    # IGNORE THIS at production
    if False:
        __new_version_db_update(version_number=SDK_VERSION)
        import pandas as pd

        index_prices = pd.read_csv('./Source_data/Dec2018_AHDB.csv')
        final_price_file = pd.read_csv('./Source_data/Dec18_final_price.csv')
        config_file = pd.read_csv('./Source_data/Dec_2018_config.csv')
        version_month = SOURCE_DATA_VERSION_MONTH
        # updating db
        __load_finalprice_config(index_prices, final_price_file, config_file, version_month)

    # underwriter function tests
    if False:
        # get_underwriter_tester
        get_underwriter_tester()
        # add new UWs
        newest_UW = add_underwriter_tester()
        print('\033[1m', '\n\nFor newest_UW we have.', '\033[0m')
        pprint.pprint(newest_UW)

    # risk pool function tester
    if False:
        risk_pool_tester()

    # test sell_this
    if False:
        this_sell_done, this_sell_error = sell_this_tester(SOURCE_DATA_VERSION_MONTH)

    # test confirm_to_sell
    if False:
        done_sell = confirm_to_sell_tester(tracking_id_works=this_sell_done['_unique_trade_id'],
                                           tracking_id_not_working=this_sell_error['_unique_trade_id'])
        pprint.pprint(done_sell)

    # test settlement
    if False:
        settle_this_tester(this_sell_done['_unique_trade_id'], 50)

    # test cancellation
    if False:
        time_now = datetime.datetime.now().strftime('%H:%M:%S %B-%d-%Y %Z')
        # make collections and choose randon product from db to sell
        # make db connection
        this_db = db(ssh_address=SSH_ADDRESS,
                     ssh_port=SSH_PORT,
                     ssh_user_name=SSH_USER_NAME,
                     ssh_private_key=SSH_PRIVATE_KEY,
                     remote_bind_address=REMOTE_BIND_ADDRESS,
                     db_port=DB_PORT,
                     auth_db=AUTH_DB,
                     db_user=DB_USER,
                     db_pw=DB_PW)
        client = this_db.client
        # get two random collections from the final price
        valid_prices = \
            list(client['FinalPrice'][SOURCE_DATA_VERSION_MONTH].aggregate([{"$sample": {"size": 1}}]))[0]
        config_file = list(client['ConfigFile'][SOURCE_DATA_VERSION_MONTH].find())
        # finished and close db
        this_db.close_db()


        def find_config(this_product, config_file_list):
            this_config = [x for x in config_file_list if x['Commodity Name as in the CSV file'] == str(this_product)]
            return this_config[0]


        time_now = datetime.datetime.now()
        this_sell = {
            # basic policy info
            "Quotation Time": time_now,
            "Market": find_config(valid_prices['Product'], config_file)['Market'],
            "Product": valid_prices['Product'],
            "Country Code": "GB",
            "Product Index Price Quoted Date": valid_prices['Policy Real Start Date'],
            "Style": valid_prices['Style'],
            "Shown Maturity": valid_prices['Shown Maturity'],
            "Strike Ratio": valid_prices['Strike Ratio'],
            "Strike": valid_prices['Strike'],
            "Stop": valid_prices['Stop'],
            "Stop Ratio": valid_prices['Stop Ratio'],
            "Spread Price": valid_prices['Spread Price'],
            "Start Asset Price": valid_prices['Start Asset Price'],
            "Policy End Date": valid_prices['Policy End Date'],
            "Policy Shown Start Date": valid_prices['Policy Shown Start Date'],
            "Units": find_config(valid_prices['Product'], config_file)['Units'],
            "Currency": find_config(valid_prices['Product'], config_file)['Currency'],

            # basic sells info
            "IPT Rate": 0.12,
            "IPT Amount": valid_prices['Spread Price'] * 100 * 0.12,
            "Quantity Requested": 100,
            "Total Price With IPT": (valid_prices['Spread Price'] * 100 - 500) * 1.12,
            "Total Price Without IPT": valid_prices['Spread Price'] * 100,

            # vouchers
            "Discount Amount From Voucher": 500,

            # payment
            "Total Amount to Pay": (valid_prices['Spread Price'] * 100 - 500) * 1.12,

            # Client and policy info
            "user_id": time.time(),
            "policy_id": time.time(),
        }

        # make a sell first
        this_sell_done = sell_this(this_sell, SOURCE_DATA_VERSION_MONTH)
        # confirm it
        confirm_to_sell(this_sell_done['_unique_trade_id'])
        # and cancel it
        cancel_this_tester(this_sell_done['_unique_trade_id'])

    # real trading simulation
    if False:
        number_of_trades = 1000
        from pressure_trading_testing import start_trading_testing

        start_trading_testing(number_of_trades)

    # Test the KPI value calculations
    if False:
        get_KPIs_tester()

    # Test the caching results
    if True:
        # fun:dash_board_cards_KPIs
        tic = time.time()
        results = get_dash_board_cards_KPIs()
        toc = time.time()
        # print(results)
        print('Time cost for fun:dash_board_cards_KPIs (cached) is: ', str(toc - tic), ' s.')

        # fun:get_RMS
        this_RMS, this_month_liability = get_RMS(risk_pool_name='', underwriter_name='')
        print(this_RMS)
        this_RMS, this_month_liability = get_RMS(risk_pool_name='GB_Milk', underwriter_name='')
        this_RMS, this_month_liability = get_RMS(risk_pool_name='', underwriter_name='Ascot_UK')
        tic = time.time()
        this_RMS, this_month_liability = get_RMS(risk_pool_name='GB_FeedWheat', underwriter_name='Ascot_UK')
        toc = time.time()
        # print(this_month_liability)
        print('Time cost for fun:dash_board_cards_KPIs (cached) is: ', str(toc - tic), ' s.')

        # fun:KPIs
        tic = time.time()
        loss_ratio, loss_ratio_full = get_loss_ratio(risk_pool_name='', underwriter_name='')
        loss_ratio, loss_ratio_full = get_loss_ratio(risk_pool_name='', underwriter_name='Ascot_UK')
        loss_ratio, loss_ratio_full = get_loss_ratio(risk_pool_name='GB_FeedWheat', underwriter_name='Ascot_UK')

        premium_ratio, premium_ratio_full = get_premium_ratio(risk_pool_name='', underwriter_name='')
        premium_ratio, premium_ratio_full = get_premium_ratio(risk_pool_name='', underwriter_name='Ascot_UK')
        premium_ratio, premium_ratio_full = get_premium_ratio(risk_pool_name='GB_FeedWheat',
                                                              underwriter_name='Ascot_UK')

        ROI, ROI_full = get_ROI(risk_pool_name='', underwriter_name='')
        ROI, ROI_full = get_ROI(risk_pool_name='', underwriter_name='Ascot_UK')
        ROI, ROI_full = get_ROI(risk_pool_name='GB_FeedWheat', underwriter_name='Ascot_UK')

        toc = time.time()
        # print(this_month_liability)
        print('Time cost for fun:KPIs (cached) is: ', str(toc - tic), ' s.')

    # Test the RMS
    if False:
        rms_tester()
