import json
from datetime import (
    date,
    datetime,
)
from decimal import Decimal

from celery.task.control import revoke
from django.conf import settings
from django.core.paginator import (
    EmptyPage,
    PageNotAnInteger,
    Paginator,
)
from django.db.models import Q
from django.utils import timezone
from raven.contrib.django.models import client
from rest_condition import Or
from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from aox_dr.exception import StableAPIException
from aox_dr.utils import send_provider_agnostic_email
from Borderaux.models import RiskBorderaux
from CBAPIs.utils import (
    cancel_this_wrapper
)
from CRM.permissions import (
    IsCustomerSupport,
    IsNotDeletion,
    IsOwner,
    IsSuperuser,
    IsUnderwriter,
)
from CRM.tasks import (
    go_card_less_cancellation,
    set_policy_runs_out,
)
from Partner.models import Partner
from payment.tasks import send_policy_cancel_mail
from payment.utils import (
    get_first_day,
    get_last_day,
)
from pricing.models import (
    IndexPriceData,
    PaymentStatusLog,
)
from pricing.serializer import PaymentStatusLogSerializer
from pricing.utils import custom_strftime
from .models import (
    AmountInvestSettings,
    ComplaintLogs,
    Farmer,
    Message,
    SimulationData,
    SupportLogs,
    Transaction,
)
from .serializer import (
    AmountInvestSettingsSerializer,
    ComplaintsSerializer,
    FarmerCRMSerializer,
    MessageSerializer,
    SimulationDataSerializer,
    SupportLogsSerializer,
    TransactionSerializer,
)


class TransactionsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if request.user.is_superuser or request.user.is_customer_supporter:
                transaction_histories = Transaction.objects.order_by('-id').all()
            elif request.user.is_partner or request.user.is_broker:
                partner = Partner.objects.get(company_admin=request.user)
                transaction_histories = Transaction.objects.filter(partner=partner).order_by('-id')
            elif request.user.is_corporate:
                transaction_histories = Transaction.objects.filter(corporate=request.user).order_by('-id')
            elif request.user.is_farmer:
                transaction_histories = Transaction.objects.filter(farmer=request.user).order_by('-id')
            else:
                return Response({'error': 'You have not access to this data'}, status=status.HTTP_401_UNAUTHORIZED)

            page = request.data.get('pagination[page]') if 'pagination[page]' in request.data else 1
            per_page = request.data.get('pagination[perpage]') if 'pagination[perpage]' in request.data else 20
            search = request.data.get('query[contract_status]') if 'query[contract_status]' in request.data else None

            # Search
            if search == 'All':
                transaction_histories = transaction_histories

            elif search == 'Active Policies':
                transaction_histories = transaction_histories.filter(
                    Q(owed_status='Running') &
                    Q(purchased=True)
                ).order_by('-id')

            elif search == 'Expired Policies':
                transaction_histories = transaction_histories.filter(
                    Q(owed_status='Terminated') &
                    Q(purchased=True)
                ).order_by('-id')

            elif search == 'Active Quotes':
                transaction_histories = transaction_histories.filter(
                    Q(owed_status='None') &
                    Q(purchased=False) &
                    Q(quote_expired=False)
                ).order_by('-id')

            elif search == 'Expired Quotes':
                transaction_histories = transaction_histories.filter(
                    Q(owed_status='None') &
                    Q(purchased=False) &
                    Q(quote_expired=True)
                ).order_by('-id')

            total = transaction_histories.count()
            paginator = Paginator(transaction_histories, per_page)

            try:
                transaction_histories = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                transaction_histories = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                transaction_histories = paginator.page(paginator.num_pages)

            all_transactions = TransactionSerializer(transaction_histories, many=True)

            context = {
                'data': all_transactions.data,
                'meta': {
                    'page': page,
                    'perpage': per_page,
                    'total': total
                }
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GetAllPolicyIDView(APIView):
    permission_classes = (IsAuthenticated, Or(IsCustomerSupport, IsSuperuser, IsUnderwriter))

    @staticmethod
    def get(request):
        policy_ids = Transaction.objects.order_by('id').values_list('id', flat=True)
        context = {
            'data': policy_ids
        }
        return Response(context, status=status.HTTP_200_OK)


class GetPolicyHolderView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, transaction_id):
        try:
            if not request.user.is_superuser and not request.user.is_customer_supporter:
                return Response(
                    {
                        'error': 'Stable Admin or Customer Supporter can view this data'
                    },
                    status=status.HTTP_200_OK
                )
            policy_holder = Transaction.objects.get(id=transaction_id).farmer
            farmer = None
            if policy_holder.is_farmer:
                farmer = Farmer.objects.get(user=policy_holder)

            phone = policy_holder.phone
            name = policy_holder.get_full_name()
            email = policy_holder.email
            address = farmer.zip if farmer else None

            context = {
                'name': name,
                'email': email,
                'phone': phone,
                'address': address
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PaymentStatusChangesLogsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, transaction_id):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'Super User can see this data'}, status=status.HTTP_200_OK)

            payment_changes_logs = PaymentStatusLogSerializer(
                PaymentStatusLog.objects.filter(policy=transaction_id),
                many=True
            )

            context = {
                'success': 'success',
                'payment_changes_logs': payment_changes_logs.data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class PolicyCancellationView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            policy_id = request.data['policyID'] if 'policyID' in request.data else None
            if not policy_id:
                return Response({'error': 'Policy ID not defined'}, status=status.HTTP_200_OK)

            cancel_status = request.data['cancelStatus'] if 'cancelStatus' in request.data else None
            if not cancel_status:
                return Response({'error': 'Cancel Status not defined'}, status=status.HTTP_200_OK)

            cancellation_reason = request.data.get('cancellation_reason', 'unknown')

            if request.user.is_superuser or request.user.is_customer_supporter:
                policy = get_object_or_404(Transaction, id=policy_id)
                notes = "This policy was cancelled by a Stable Employee"
            elif request.user.is_corporate:
                policy = get_object_or_404(Transaction, id=policy_id, corporate=request.user)
                notes = "This Policy was cancelled by the corporate responsible"
            elif request.user.is_farmer:
                policy = get_object_or_404(Transaction, id=policy_id, farmer=request.user)
                notes = "This Policy was cancelled by the farmer that owns the policy"
            else:
                return Response(
                    {'error': 'Your user role is not allowed to cancel policies'},
                    status=status.HTTP_200_OK
                )

            if policy.owed_status in ['Paid', 'Finished']:
                return Response(
                    {'error': 'Policy already {}'.format(policy.owed_status)},
                    status=status.HTTP_200_OK
                )

            if cancel_status == 'Purchased':
                policy.purchased = True
                policy.payment_style = 'Corporate Invoice'
                policy.paid_at = datetime.now()

                policy_end_date = policy.stop_date + timezone.timedelta(days=31)
                task_response = set_policy_runs_out.apply_async(args=[policy.id], eta=policy_end_date)

                if task_response.task_id:
                    policy.owed_status = 'Running'
                    policy.contract_running_task_id = task_response.task_id
            else:
                policy.cancel_status = cancel_status
                if cancel_status == 'Cancelled':
                    policy.cancelled = datetime.now()
                    policy.cancellation_period = str((date.today() - policy.quote_date).days)
                    policy.owed_status = 'Terminated'
                    task_id = policy.contract_running_task_id
                    if task_id:
                        revoke(task_id, terminate=True)

                    # Cancellation for GoCardLess monthly subscription after 7 days
                    if policy.payment_style == 'Monthly' and policy.cancel_status != 'Cancelled' and \
                            policy.owed_status != 'Terminated':
                        cancel_date = timezone.now() + timezone.timedelta(days=7)
                        notes += f"The gocardless subscription is to be cancelled on {cancel_date}"
                        go_card_less_cancellation.apply_async(args=[policy.id], eta=cancel_date)

            policy.save()

            # Let Simon know policy is cancelled.
            if cancel_status == 'Cancelled':
                cancel_this_wrapper.apply_async(args=[policy.unique_trade_id])
                send_policy_cancel_mail.delay(
                    first_name=policy.farmer.first_name,
                    email=policy.farmer.email,
                    policy_id=policy.id,
                    cancelled=custom_strftime("{S} %B %Y", policy.cancelled),
                    market=policy.market.name,
                    category=policy.category.name,
                    index=policy.category.index.name,
                    protection_direction=policy.protection_direction,
                    start_date=date.strftime(policy.start_date, "%B %Y"),
                    stop_date=date.strftime(policy.stop_date, "%B %Y"),
                    start_price=policy.start_price,
                    stop_price=policy.stop_price,
                    quantity=policy.quantity,
                    total_cost=policy.total_cost,
                    monthly_cost=policy.monthly_cost,
                    cancellation_period=policy.cancellation_period,
                    unit=policy.metric,
                    premium=policy.premium,
                    tax=Decimal(policy.tax) * Decimal(policy.premium),
                    potential_loss=policy.potential_loss,
                    protection_duration=policy.protection_duration
                )
                RiskBorderaux.objects.create(
                    reporting_period_start=get_first_day(),
                    reporting_period_end=get_last_day(),
                    transaction=policy,
                    risk_inception_date=policy.get_risk_inception_date(),
                    risk_expiry_date=policy.stop_date,
                    transaction_risk_type='cancellation',
                    installment_basis='monthly' if policy.payment_provider == 'gocardless' else 'in_full',
                    installments=policy.protection_duration if policy.payment_provider == 'gocardless' else 1,
                    notes=notes,
                    risk_cancellation_date=date.today(),
                    reason_for_cancellation=cancellation_reason
                )

            PaymentStatusLog.objects.create(
                content=cancel_status,
                date=datetime.now(),
                user=request.user,
                policy=policy
            )

            if request.user.is_corporate:
                transaction_histories = TransactionSerializer(
                    Transaction.objects.filter(corporate=request.user),
                    many=True
                )
                context = {
                    'success': 'success',
                    'accounts_table_data': transaction_histories.data
                }
            else:
                context = {
                    'success': 'success',
                }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            client.captureException()
            raise StableAPIException(e)


class PolicyOwedStatusView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            if not request.user.is_superuser:
                return Response(
                    {'error': 'SuperUser can edit this value'},
                    status=status.HTTP_200_OK
                )
            else:
                policy_id = request.data['policyID'] if 'policyID' in request.data else None
                if not policy_id:
                    return Response({'error': 'Policy ID not defined'}, status=status.HTTP_200_OK)

                owed_status = request.data['owedStatus'] if 'owedStatus' in request.data else None
                if not owed_status:
                    return Response({'error': 'Owed Status not defined'}, status=status.HTTP_200_OK)

                policy = Transaction.objects.get(id=policy_id)
                policy.owed_status = owed_status
                policy.save()

                if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
                    template_id = ''
                    merge_vars = {
                        'first_name': policy.farmer.first_name,
                    }
                else:
                    template_id = 'Payment Confirmation'
                    merge_vars = {
                        'FIRST_NAME': policy.farmer.first_name,
                    }

                send_provider_agnostic_email(
                    "Stable's Volatility Insurance Confirmation",
                    to_email=policy.farmer.email,
                    template_id=template_id,
                    merge_vars=merge_vars,
                    attachments=[]
                )

                payment_status_log = PaymentStatusLog(
                    content='Payment Sent',
                    date=datetime.now(),
                    user=request.user,
                    policy=int(policy_id)
                )
                payment_status_log.save()

                context = {
                    'success': 'success',
                }
                return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class RequestBankDetailsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_superuser:
                return Response(
                    {'error': 'SuperUser can edit this value'},
                    status=status.HTTP_200_OK
                )
            else:
                policy_id = request.data['policyID'] if 'policyID' in request.data else None
                if not policy_id:
                    return Response({'error': 'Policy ID not defined'}, status=status.HTTP_200_OK)

                refunded_amount = request.data['refundedAmount'] if 'refundedAmount' in request.data else None
                if not refunded_amount:
                    return Response({'error': 'Refunded Amount not defined'}, status=status.HTTP_200_OK)

                policy = Transaction.objects.get(id=policy_id)

                if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
                    template_id = ''
                    merge_vars = {
                        'first_name': policy.farmer.first_name,
                        'policy_id': policy.pk,
                        'refunded_amount': refunded_amount,
                    }
                else:
                    template_id = 'Request Bank Details for Policy Cancellation'
                    merge_vars = {
                        'FIRST_NAME': policy.farmer.first_name,
                        'POLICY_ID': policy.pk,
                        'REFUNDED_AMOUNT': refunded_amount,
                    }

                send_provider_agnostic_email(
                    "Request Bank Details for Policy Cancellation",
                    to_email=policy.farmer.email,
                    template_id=template_id,
                    merge_vars=merge_vars,
                    attachments=[]
                )

                policy.cancel_status = 'Cancellation Awaiting Details'
                policy.save()

                payment_status_log = PaymentStatusLog(
                    content=policy.cancel_status,
                    date=datetime.now(),
                    user=request.user,
                    policy=int(policy.id)
                )
                payment_status_log.save()

                context = {
                    'success': 'success',
                }
                return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class RequestBankDetailsWebHookView(APIView):

    @staticmethod
    def post(request):
        try:
            policy_id = request.data['PolicyID'] if 'PolicyID' in request.data else None
            if not policy_id:
                return Response({'error': 'Policy ID not defined'}, status=status.HTTP_200_OK)

            payment_type = request.data['PaymentType'] if 'PaymentType' in request.data else None
            if not payment_type:
                return Response({'error': 'Payment Type not defined'}, status=status.HTTP_200_OK)

            policy = Transaction.objects.get(id=policy_id)

            if payment_type == 'Owed':
                if policy.owed_status == 'Awaiting Details':
                    policy.owed_status = 'Needs Paying'
                    policy.save()
                    payment_status_log = PaymentStatusLog(
                        content='Bank details received for Owed',
                        date=datetime.now(),
                        policy=int(policy_id)
                    )
                    payment_status_log.save()
                    context = {
                        'success': 'success',
                    }
                else:
                    context = {
                        'error': 'Not requested bank details for this policy yet.',
                    }
            else:
                if policy.cancel_status == 'Cancellation Awaiting Details':
                    policy.cancel_status = 'Needs Paying'
                    policy.save()
                    payment_status_log = PaymentStatusLog(
                        content='Bank details received for Cancellation',
                        date=datetime.now(),
                        policy=int(policy_id)
                    )
                    payment_status_log.save()
                    context = {
                        'success': 'success',
                    }
                else:
                    context = {
                        'error': 'Not requested bank details for this policy yet.',
                    }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            raise StableAPIException(e)


class MessageView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if request.user.is_superuser or request.user.is_underwriter \
                    or request.user.is_customer_supporter:
                messages = MessageSerializer(Message.objects.order_by('-id').all(), many=True).data
            else:
                partner = Partner.objects.get(company_admin=request.user)
                messages = MessageSerializer(
                    Message.objects.filter(company=partner).order_by('-id'),
                    many=True
                ).data

            data = []
            for message in messages:
                message_obj = Message.objects.get(id=message['id'])
                if request.user.id not in message_obj.read_users:
                    message_obj.read_users.append(request.user.id)
                    message_obj.save()
                data.append(
                    {
                        'company_name': message['company']['name'],
                        'sender': message['sender'],
                        'content': message['content'],
                        'create_date': message['create_date']
                    }
                )

            context = {
                'success': 'success',
                'messages': data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    def post(self, request):
        try:
            if not request.user.is_superuser:
                return Response({'error': 'User must SuperUser'}, status=status.HTTP_200_OK)

            company = request.data['company_name'] if 'company_name' in request.data else None
            if not company:
                return Response({'error': 'Company does not defined'}, status=status.HTTP_200_OK)

            content = request.data['content'] if 'content' in request.data else None
            if not content:
                return Response({'error': 'Message Content does not defined'}, status=status.HTTP_200_OK)

            sender = request.user.get_full_name()

            message = Message(
                company=Partner.objects.get(name=company),
                sender=sender,
                content=content,
                read_users=[]
            )
            message.save()

            messages = MessageSerializer(Message.objects.order_by('-id').all(), many=True).data
            data = []
            for message in messages:
                message_obj = Message.objects.get(id=message['id'])
                if request.user.id not in message_obj.read_users:
                    message_obj.read_users.append(request.user.id)
                    message_obj.save()
                data.append(
                    {
                        'company_name': message['company']['name'],
                        'sender': message['sender'],
                        'content': message['content'],
                        'create_date': message['create_date']
                    }
                )

            context = {
                'success': 'success',
                'messages': data
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class NewMessageView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if not request.user.is_partner:
                return Response({'error': 'You are not a partner'}, status=status.HTTP_200_OK)
            else:
                partner_company = Partner.objects.get(company_admin=request.user)
                messages = MessageSerializer(
                    Message.objects.filter(
                        company=partner_company
                    ),
                    many=True
                ).data

                new_messages = 0
                for message in messages:
                    if request.user.id not in message['read_users']:
                        new_messages = new_messages + 1

            context = {
                'success': 'success',
                'newMessages': new_messages
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class AllSupportLogsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if not request.user.is_superuser and not request.user.is_customer_supporter:
                return Response(
                    {'error': 'Stable Admin or Customer Staff User can see this data'},
                    status=status.HTTP_200_OK
                )

            support_logs = SupportLogs.objects.all()

            page = request.GET['pagination[page]'] if 'pagination[page]' in request.GET else 1
            perpage = request.GET['pagination[perpage]'] if 'pagination[perpage]' in request.GET else 20
            search = request.GET['query[generalSearch]'] if 'query[generalSearch]' in request.GET else None

            if search:
                search_split = search.split()
                if len(search_split) > 1:
                    support_logs = SupportLogs.objects.filter(
                        Q(log_type__contains=search) |
                        Q(log__contains=search) |
                        Q(customer_support_staff__first_name__contains=search.split()[0]) |
                        Q(customer_support_staff__last_name__contains=search.split()[1])
                    )
                else:
                    support_logs = SupportLogs.objects.filter(
                        Q(log_type__contains=search) |
                        Q(log__contains=search) |
                        Q(customer_support_staff__first_name__contains=search) |
                        Q(customer_support_staff__last_name__contains=search)
                    )

            total = support_logs.count()
            paginator = Paginator(support_logs, perpage)

            try:
                support_logs = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                support_logs = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                support_logs = paginator.page(paginator.num_pages)

            support_logs = SupportLogsSerializer(support_logs, many=True)

            context = {
                'data': support_logs.data,
                'meta': {
                    'page': page,
                    'perpage': perpage,
                    'total': total
                }
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class SupportLogsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, transaction_id):
        try:
            if not request.user.is_superuser and not request.user.is_customer_supporter:
                return Response(
                    {'error': 'Stable Admin or Customer Staff User can see this data'},
                    status=status.HTTP_200_OK
                )

            log_type = request.GET.get('log_type', None)
            if not log_type:
                return Response({'error': 'Log Type is not defined'}, status=status.HTTP_200_OK)

            logs = SupportLogs.objects.filter(transaction__id=transaction_id, log_type=log_type)

            page = request.GET['pagination[page]'] if 'pagination[page]' in request.GET else 1
            per_page = request.GET['pagination[perpage]'] if 'pagination[perpage]' in request.GET else 20

            total = logs.count()
            paginator = Paginator(logs, per_page)

            try:
                logs = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                logs = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                logs = paginator.page(paginator.num_pages)

            logs = SupportLogsSerializer(logs, many=True)

            context = {
                'data': logs.data,
                'meta': {
                    'page': page,
                    'perpage': per_page,
                    'total': total
                }
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class AddSupportLogView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_customer_supporter or request.user.is_superuser:
                return Response(
                    {'error': 'Only a member of the Stable support team or a Stable Admin may add support data.'},
                    status=status.HTTP_200_OK
                )

            policy_id = request.data['policyID'] if 'policyID' in request.data else None
            if not policy_id:
                return Response({'error': 'Policy ID must be defined.'}, status=status.HTTP_200_OK)
            else:
                transaction = Transaction.objects.get(id=policy_id)
                if not transaction:
                    return Response(
                        {
                            'error': 'Policy ID is not correct, Transaction '
                                     'with this policy ID is not exist: {}'.format(policy_id)
                        },
                        status=status.HTTP_200_OK
                    )

            log_content = request.data['log'] if 'log' in request.data else None
            if not log_content:
                return Response({'error': 'Please provide a log of your conversation'}, status=status.HTTP_200_OK)

            log_type = request.data['log_type'] if 'log_type' in request.data else None
            if not log_type:
                return Response({'error': 'Please define a log type (Email or Phone)'}, status=status.HTTP_200_OK)

            support_log = SupportLogs(
                log_type=log_type,
                log=log_content,
                customer_support_staff=request.user,
                transaction=transaction
            )
            support_log.save()

            transaction.phone_call_count = transaction.phone_call_count + 1
            transaction.save()

            context = {
                'success': 'success'
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class AddComplaintLogsView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, complaint_id):
        instance = ComplaintLogs.objects.get(id=complaint_id)
        complaint = ComplaintsSerializer(instance, data=request.data)
        if complaint.is_valid():
            pass
        else:
            return Response({"error": complaint.errors})

    @staticmethod
    def post(request):
        try:
            if not request.user.is_superuser and not request.user.is_customer_supporter:
                return Response(
                    {
                        'error': 'You must be a stable Admin to submit a complaint.'
                    }, status=status.HTTP_200_OK
                )

            data = request.data.copy()
            policy_id = int(data['policy_number'])
            policy = Transaction.objects.get(id=policy_id)

            date_resolved = request.data.get('date_resolved')
            if date_resolved:
                date_resolved = datetime.strptime(date_resolved, "%d/%m/%Y")
                data['date_resolved'] = date_resolved

            date_received = datetime.strptime(request.data.get('date_received', '01/01/1990'), "%d/%m/%Y")
            date_acknowledged = datetime.strptime(request.data.get('date_acknowledged', '01/01/1990'), "%d/%m/%Y")

            if policy.farmer.is_farmer:
                farmer = Farmer.objects.get(user=policy.farmer)
                post_code = farmer.zip
                company_name = farmer.farm_name
            else:
                farmer = policy.farmer
                post_code = ""
                company_name = farmer.company_name

            data['policy_number'] = str(policy_id)
            data['postcode'] = post_code
            data['policy_holder_company_name'] = company_name
            data['date_received'] = date_received
            data['date_acknowledged'] = date_acknowledged
            data['policy_holder_forename'] = policy.farmer.first_name
            data['policy_holder_surname'] = policy.farmer.last_name

            complaint_id = data.get('id', None)
            if complaint_id:
                complaint_log = ComplaintsSerializer(ComplaintLogs.objects.get(id=int(complaint_id)), data=data)
            else:
                complaint_log = ComplaintsSerializer(data=data)
            if complaint_log.is_valid():
                complaint_log.save()
            else:
                raise Response({"error": complaint_log.errors}, status=status.HTTP_400_BAD_REQUEST)

            logs = ComplaintsSerializer(ComplaintLogs.objects.all(), many=True)

            return Response({'data': logs.data}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class ComplaintViewSet(ModelViewSet):
    serializer_class = ComplaintsSerializer
    queryset = ComplaintLogs.objects.all()
    permission_classes = (
        IsAuthenticated,
        Or(IsCustomerSupport, IsUnderwriter, IsSuperuser),
        IsNotDeletion,
    )

    def create(self, request, *args, **kwargs):
        files = [{'original_filename': key, 'file': value} for key, value in self.request.FILES.items()]
        data = {}

        for key, value in request.data.items():
            if key not in self.request.FILES.keys():
                if len(request.data.getlist(key)) > 1:
                    data[key] = request.data.getlist(key)
                else:
                    data[key] = request.data.get(key)
        updated_data = {
            'file': files,
            **data
        }
        notes = updated_data.get('notes', None)
        if notes != '' and notes is not None:
            updated_data['notes'] = [{
                'note': notes,
                'user': request.user.pk
            }]
        serializer = self.get_serializer(data=updated_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        files = [{'original_filename': key, 'file': value} for key, value in self.request.FILES.items()]
        data = {}

        for key, value in request.data.items():
            if key not in self.request.FILES.keys():
                if len(request.data.getlist(key)) > 1:
                    data[key] = request.data.getlist(key)
                else:
                    data[key] = request.data.get(key)
        updated_data = {
            'file': files,
            **data
        }
        notes = updated_data.get('notes', None)
        if notes != '' and notes is not None:
            updated_data['notes'] = [{
                'note': notes,
                'user': request.user.pk
            }]
        serializer = self.get_serializer(instance, data=updated_data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class ComplaintLogsView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_superuser and not request.user.is_customer_supporter:
                return Response(
                    {'error': 'Only a member of the Customer Support team or a Stable Admin can see this data.'},
                    status=status.HTTP_200_OK
                )

            logs = ComplaintsSerializer(ComplaintLogs.objects.all(), many=True)

            return Response({'data': logs.data}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class ComplaintResponseView(APIView):
    @staticmethod
    def post(request):
        try:
            if not request.user.is_customer_supporter and not request.user.is_superuser:
                return Response(
                    {'error': 'Only a member of the Stable support team or a Stable Admin may respond to a complaint.'},
                    status=status.HTTP_200_OK
                )

            complaint_id = request.data['complaint_id'] if 'complaint_id' in request.data else None
            policy_id = request.data['policy_id'] if 'policy_id' in request.data else None
            if not complaint_id:
                return Response({'error': 'A Complaint ID needs to be assigned to this response log.'},
                                status=status.HTTP_200_OK)
            else:
                complaint = ComplaintLogs.objects.get(id=complaint_id)
                if not complaint:
                    return Response(
                        {
                            'error': 'Complaint ID is not correct, A Complaint '
                                     'with this Complaint ID does not exist: {}'.format(complaint_id)
                        },
                        status=status.HTTP_200_OK
                    )

            if policy_id:
                transaction = Transaction.objects.get(id=policy_id)
            else:
                transaction = None
            log_content = request.data['log'] if 'log' in request.data else None
            if not log_content:
                return Response({'error': 'Please provide a log of your conversation'}, status=status.HTTP_200_OK)

            log_type = request.data['log_type'] if 'log_type' in request.data else None
            if not log_type:
                return Response({'error': 'Please define a log type (Email or Phone)'}, status=status.HTTP_200_OK)

            complaint_status = request.data['complaint_status'] if 'complaint_status' in request.data else None
            if not complaint_status:
                return Response({'error': 'Please define a status for this complaint'}, status=status.HTTP_200_OK)

            support_log = SupportLogs(
                log_type=log_type,
                log=log_content,
                customer_support_staff=request.user,
                complaint=complaint
            )
            support_log.save()

            complaint.status = complaint_status
            complaint.last_reply = datetime.now()
            if complaint_status == 'Resolved':
                complaint.resolved_at = datetime.now()
            if complaint_status == 'In Progress':
                complaint.resolved_at = datetime.now()
            complaint.save()

            context = {
                'success': 'success'
            }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class AmountInvestDataView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if not request.user.is_underwriter:
                return Response({'error': 'UnderWriter can see this data'}, status=status.HTTP_200_OK)

            if AmountInvestSettings.objects.filter(underwriter=request.user).exists():
                amount_invest_data = AmountInvestSettingsSerializer(
                    AmountInvestSettings.objects.get(underwriter=request.user)
                )
                context = {
                    'success': 'success',
                    'settings_data': amount_invest_data.data
                }
            else:
                context = {
                    'success': 'success',
                    'settings_data': {}
                }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_underwriter:
                return Response({'error': 'UnderWriter can see this data'}, status=status.HTTP_200_OK)

            country = request.data['country'] if 'country' in request.data else None
            if not country:
                return Response({'error': 'Country not defined'}, status=status.HTTP_200_OK)

            product_data_json = {}
            total = 0
            for product in request.data:
                if product != 'country':
                    total += Decimal(request.data[product])
                    product_data_json[product] = request.data[product]

            amount_invest = AmountInvestSettings.objects.get(underwriter=request.user)
            amount_invest.country = country
            amount_invest.total_amount = total
            amount_invest.data = product_data_json
            amount_invest.save()

            amount_invest_data = AmountInvestSettingsSerializer(
                AmountInvestSettings.objects.get(underwriter=request.user)
            )
            context = {
                'success': 'success',
                'settings_data': amount_invest_data.data
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class FarmersView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if not request.user.is_superuser and not request.user.is_customer_supporter:
                return Response(
                    {'error': 'Stable Admin or Customer Staff User can see this data'},
                    status=status.HTTP_200_OK
                )

            farmers = FarmerCRMSerializer(
                Farmer.objects.all(),
                many=True
            )

            context = {
                'farmers': farmers.data
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class SimulationView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if not request.user.is_underwriter:
                return Response(
                    {'error': 'UnderWriter can see this data'},
                    status=status.HTTP_200_OK
                )

            simulations = SimulationData.objects.filter(underwriter=request.user)

            page = request.data.get('pagination[page]', 1)
            per_page = request.data.get('pagination[perpage]', 20)

            total = simulations.count()
            paginator = Paginator(simulations, per_page)

            try:
                simulations = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                simulations = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                simulations = paginator.page(paginator.num_pages)

            all_simulations = SimulationDataSerializer(simulations, many=True)

            context = {
                'data': all_simulations.data,
                'meta': {
                    'page': page,
                    'perpage': per_page,
                    'total': total
                }
            }

            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class AddSimulationView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_underwriter:
                return Response(
                    {'error': 'UnderWriter can see this data'},
                    status=status.HTTP_200_OK
                )

            data = request.data.get('data')
            if not data:
                return Response({'error': 'Data not defined'}, status=status.HTTP_200_OK)
            else:
                data = json.loads(data)

            simulation = SimulationDataSerializer(data=data)
            if simulation.is_valid():
                simulation.save(underwriter=request.user, type='New')
            else:
                return Response({'error': str(simulation.errors)}, status=status.HTTP_200_OK)

            return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class UpdateSimulationView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        try:
            if not request.user.is_underwriter:
                return Response(
                    {'error': 'UnderWriter can see this data'},
                    status=status.HTTP_200_OK
                )

            data = request.data.get('data')
            if not data:
                return Response({'error': 'Data not defined'}, status=status.HTTP_200_OK)
            else:
                data = json.loads(data)

            simulation = SimulationData.objects.get(id=data['id'])

            simulation_update = SimulationDataSerializer(simulation, data=data)
            if simulation_update.is_valid():
                simulation_update.save()
            else:
                return Response({'error': str(simulation_update.errors)}, status=status.HTTP_200_OK)

            return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class StartSimulationView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        try:
            if not request.user.is_underwriter:
                return Response(
                    {'error': 'UnderWriter can see this data'},
                    status=status.HTTP_200_OK
                )

            simulations = SimulationData.objects.filter(underwriter=request.user, type='New')
            for simulation in simulations:
                simulation.type = 'Running'
                simulation.save()

            return Response({'success': 'success'}, status=status.HTTP_200_OK)
        except Exception as e:
            raise StableAPIException(e)


class GenerateTermsAndConditionsView(APIView):
    permission_classes = (IsAuthenticated, IsOwner)

    def get(self, request, transaction_id):
        transaction = get_object_or_404(Transaction, pk=transaction_id)
        self.check_object_permissions(request, transaction)

        country = 'UK' if transaction.country == 'GB' else transaction.country
        multiplier = settings.CURRENCY_CONVERSION_RULE.get(
            country
        ).get(
            transaction.category.name
        ).get('multiplier')

        last_index = IndexPriceData.objects.filter(
            prod_name=transaction.category.name,
            is_active='Live',
            country='UK' if transaction.country == 'GB' else transaction.country
        ).latest()

        return Response({
            'farmer_first_name': transaction.farmer.first_name,
            'farmer_last_name': transaction.farmer.last_name,
            'address_line_1': transaction.farmer.address_1,
            'address_line_2': transaction.farmer.address_2,
            'town': transaction.farmer.town,
            'county': transaction.farmer.county,
            'postcode': transaction.farmer.farmer.zip,
            'farm_name': transaction.farmer.farmer.farm_name,
            'farm_postcode': transaction.farmer.post_code,
            'farm_size': transaction.farmer.farmer.farm_size,
            'total_cost': transaction.total_cost / 100,
            'ipt_value': (transaction.total_cost - transaction.premium) / 100,
            'policy_limit': transaction.potential_loss / 100,
            'insured_product': transaction.category.name,
            'start_price': transaction.start_price / multiplier,
            'stop_price': transaction.stop_price / multiplier,
            'currency': transaction.category.currency,
            'latest_index_price': last_index.price,
            'index_owner': transaction.category.index.owner,
            'quantity': transaction.quantity,
            'duration': transaction.get_protection_style_duration(),
            'start_date': transaction.get_risk_inception_date(),
            'end_date': transaction.stop_date,
            'metric': transaction.category.unit,
            'farm_produce': {
                'Barley': transaction.farmer.farmer.barley,
                'Oats': transaction.farmer.farmer.oats,
                'Lamb': transaction.farmer.farmer.sheep,
                'Calves': transaction.farmer.farmer.suckler_herd_size,
                'Potatoes': transaction.farmer.farmer.potatoes,
                'Rapeseed': transaction.farmer.farmer.rapeseed,
                'Cattle': transaction.farmer.farmer.cattle,
                'Wheat': transaction.farmer.farmer.wheat,
                'Dairy': transaction.farmer.farmer.dairy_herd_size,
                'Other Crops': transaction.farmer.farmer.other_crops,
                'Other Livestock': transaction.farmer.farmer.other_livestock,
                'Breeding Sows': transaction.farmer.farmer.breeding_sows,
            },
        })
