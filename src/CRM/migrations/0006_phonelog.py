# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-06-01 10:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CRM', '0005_transaction_invoice_end_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='PhoneLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_support_staff_id', models.IntegerField()),
                ('log', models.TextField()),
                ('logged_at', models.DateTimeField(auto_now_add=True)),
                ('transaction', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CRM.Transaction')),
            ],
        ),
    ]
