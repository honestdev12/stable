# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-09-27 15:30
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('CRM', '0027_user_membership_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='Farmer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('role', models.CharField(max_length=255, null=True)),
                ('membership_id', models.CharField(blank=True, max_length=255, null=True)),
                ('country', models.CharField(max_length=255)),
                ('birthday', models.DateField(null=True)),
                ('zip', models.CharField(max_length=255, null=True)),
                ('farm_size', models.IntegerField(null=True)),
                ('farm_name', models.CharField(max_length=255, null=True)),
                ('farm_type', models.CharField(default='mixed', max_length=255, null=True)),
                ('farm_number', models.IntegerField(blank=True, null=True)),
                ('dairy_herd_size', models.BigIntegerField(blank=True, null=True)),
                ('cattle', models.BigIntegerField(blank=True, null=True)),
                ('breeding_sows', models.BigIntegerField(blank=True, null=True)),
                ('barley', models.BigIntegerField(blank=True, null=True)),
                ('rapeseed', models.BigIntegerField(blank=True, null=True)),
                ('other_crops', models.BigIntegerField(blank=True, null=True)),
                ('other_livestock', models.BigIntegerField(blank=True, null=True)),
                ('potatoes', models.BigIntegerField(blank=True, null=True)),
                ('wheat', models.BigIntegerField(blank=True, null=True)),
                ('oats', models.BigIntegerField(blank=True, null=True)),
                ('sheep', models.BigIntegerField(blank=True, null=True)),
                ('suckler_herd_size', models.BigIntegerField(blank=True, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.DeleteModel(
            name='User',
        ),
    ]
