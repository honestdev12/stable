# Generated by Django 2.0 on 2018-11-15 11:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CRM', '0046_farmer_tagged_partners'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='farmer',
            name='birthday',
        ),
        migrations.RemoveField(
            model_name='farmer',
            name='farm_type',
        ),
    ]
