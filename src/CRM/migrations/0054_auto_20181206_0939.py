# Generated by Django 2.0 on 2018-12-06 09:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CRM', '0053_transaction_unique_trade_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='unique_trade_id',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
