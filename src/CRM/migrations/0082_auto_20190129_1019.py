# Generated by Django 2.0 on 2019-01-29 10:19

import django.utils.timezone
from django.db import (
    migrations,
    models,
)


def go_commit_die(apps, schema_editor):
    Transaction = apps.get_model('CRM.Transaction')
    for transaction in Transaction.objects.all():
        transaction.commission = transaction.commission if transaction.commission else 0
        transaction.discount = transaction.discount if transaction.discount else 0
        transaction.farmer_saving = transaction.farmer_saving if transaction.farmer_saving else 0
        transaction.monthly_cost = transaction.monthly_cost if transaction.monthly_cost else 0
        transaction.owed_amount = transaction.owed_amount if transaction.owed_amount else 0
        transaction.potential_loss = transaction.potential_loss if transaction.potential_loss else 0
        transaction.premium = transaction.premium if transaction.premium else 0
        transaction.price_per_unit = transaction.price_per_unit if transaction.price_per_unit else 0
        transaction.price_per_unit_raw = transaction.price_per_unit_raw if transaction.price_per_unit_raw else 0
        transaction.start_price = transaction.start_price if transaction.start_price else 0
        transaction.stop_price = transaction.stop_price if transaction.stop_price else 0
        transaction.tax = transaction.tax if transaction.tax else 0
        transaction.total_cost = transaction.total_cost if transaction.total_cost else 0
        transaction.save()


class Migration(migrations.Migration):

    dependencies = [
        ('CRM', '0081_merge_20190128_0948'),
    ]

    operations = [
        migrations.RunPython(go_commit_die),
        migrations.AddField(
            model_name='transaction',
            name='datetime_created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='commission',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='discount',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='farmer_saving',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='monthly_cost',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='owed_amount',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='potential_loss',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='premium',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='price_per_unit',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='price_per_unit_raw',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='start_price',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='stop_price',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='tax',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='total_cost',
            field=models.DecimalField(decimal_places=8, default=0, max_digits=24),
        ),
    ]
