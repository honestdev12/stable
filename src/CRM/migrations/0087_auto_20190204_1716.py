# Generated by Django 2.0 on 2019-02-04 17:16

from django.db import (
    migrations,
    models,
)

import CRM.models


class Migration(migrations.Migration):

    dependencies = [
        ('CRM', '0086_auto_20190204_1226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='stop_date',
            field=models.DateField(db_index=True, default=CRM.models.default_start_stop_date),
        ),
    ]
