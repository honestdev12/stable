from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from CRM import views as crm_views
from CRM.views import ComplaintViewSet

router = DefaultRouter()
router.register(r'complaints', ComplaintViewSet)

urlpatterns = router.urls + [
    url(r'^transactions/', crm_views.TransactionsView.as_view()),
    url(r'^generate_terms_and_conditions/(?P<transaction_id>\d+)/$', crm_views.GenerateTermsAndConditionsView.as_view()),
    url(r'^accounts/get_holder_info/(?P<transaction_id>\d+)/$', crm_views.GetPolicyHolderView.as_view()),
    url(r'^accounts/get_all_ids/', crm_views.GetAllPolicyIDView.as_view()),
    url(r'^accounts/set_cancellation_status/', crm_views.PolicyCancellationView.as_view()),
    url(r'^accounts/set_owed_status/', crm_views.PolicyOwedStatusView.as_view()),
    url(r'^accounts/request_bank_details/', crm_views.RequestBankDetailsView.as_view()),
    url(r'^get_payment_status_changes_logs/(?P<transaction_id>\d+)/$', crm_views.PaymentStatusChangesLogsView.as_view()),
    url(r'^web_hook_request_bank/', crm_views.RequestBankDetailsWebHookView.as_view()),

    url(r'^get_message_data/', crm_views.MessageView.as_view()),
    url(r'^get_new_messages/', crm_views.NewMessageView.as_view()),
    url(r'^send_message/', crm_views.MessageView.as_view()),

    url(r'^get_log_data/(?P<transaction_id>\d+)/$', crm_views.SupportLogsView.as_view()),
    url(r'^add_log_data/', crm_views.AddSupportLogView.as_view()),
    url(r'^get_support_logs/', crm_views.AllSupportLogsView.as_view()),

    url(r'^complaint/', crm_views.ComplaintLogsView.as_view()),
    url(r'^add_complaint/', crm_views.AddComplaintLogsView.as_view()),
    url(r'^complaint_response/', crm_views.ComplaintResponseView.as_view()),
    
    url(r'^get_amount_invest_data/', crm_views.AmountInvestDataView.as_view()),
    url(r'^update_amount_invest_data/', crm_views.AmountInvestDataView.as_view()),

    url(r'^get_all_farmers/', crm_views.FarmersView.as_view()),

    url(r'^simulation/start/', crm_views.StartSimulationView.as_view()),
    url(r'^simulation/update/', crm_views.UpdateSimulationView.as_view()),
    url(r'^add_simulation/', crm_views.AddSimulationView.as_view()),
    url(r'^simulation/', crm_views.SimulationView.as_view())
]
