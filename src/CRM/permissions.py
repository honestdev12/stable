from rest_framework import permissions


class IsSuperuser(permissions.BasePermission):
    """
    Permission check for customer support representatives.
    """

    def has_permission(self, request, view):
        return request.user.is_superuser


class IsCustomerSupport(permissions.BasePermission):
    """
    Permission check for customer support representatives.
    """

    def has_permission(self, request, view):
        return request.user.is_superuser or request.user.is_customer_supporter


class IsUnderwriter(permissions.BasePermission):
    """
    Permission check for customer support representatives.
    """

    def has_permission(self, request, view):
        return request.user.is_underwriter


class IsFarmer(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_farmer


class IsPartner(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_partner


class IsCorporate(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_corporate


class IsBroker(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_broker


class IsNotDeletion(permissions.BasePermission):
    """
    Permission check to disallow a user to delete items
    """

    def has_permission(self, request, view):
        return request.method != "DELETE"

    def has_object_permission(self, request, view, obj):
        return request.method != "DELETE"


class IsOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj.farmer == request.user
