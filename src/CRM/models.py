import datetime
from pathlib import Path
from uuid import uuid4

import pendulum
from django.conf import settings
from django.contrib.postgres.fields import (
    ArrayField,
    JSONField,
)
from django.db import models
from django.utils import timezone

from administration.models import AuthUser
from Partner.models import (
    Agents,
    Partner,
)


def default_start_stop_date():
    return datetime.date.today()


def complaint_date():
    return datetime.date.today()

TRANSACTION_PAYMENT_PROVIDER_CHOICES = (
    ('unknown', 'Provider not saved for this record'),
    ('stripe', 'Stripe'),
    ('gocardless', 'GoCardless'),
)


def get_expiry_datetime():
    return timezone.now() + timezone.timedelta(hours=72)


class Transaction(models.Model):
    farmer = models.ForeignKey(AuthUser, on_delete=models.CASCADE, related_name="Farmer", default=2)
    reference_id = models.CharField(max_length=255, null=True, blank=True)
    unique_trade_id = models.CharField(max_length=255, null=True, blank=True)  # String of datetime + UUID provided by the SDK

    partner = models.ForeignKey(Partner, on_delete=models.DO_NOTHING, default=1)
    corporate = models.ForeignKey(AuthUser, null=True, blank=True, on_delete=models.DO_NOTHING, related_name="Corporate")
    affiliate = models.ForeignKey(Agents, null=True, blank=True, on_delete=models.DO_NOTHING)
    underwriters = JSONField(default={"underwriter": "Ascot", "amount": "100%"}, null=False, blank=False)
    country = models.CharField(max_length=255, default='UK')

    datetime_created = models.DateTimeField(auto_now_add=True)

    start_date = models.DateField(default=default_start_stop_date)
    stop_date = models.DateField(default=default_start_stop_date, db_index=True)
    quote_date = models.DateField(auto_now_add=True)
    market = models.ForeignKey('CBAPIs.Market', on_delete=models.PROTECT)
    category = models.ForeignKey('CBAPIs.Category', on_delete=models.PROTECT)
    quantity = models.IntegerField()
    metric = models.CharField(max_length=255)
    protection_duration = models.CharField(max_length=255)
    protection_style = models.CharField(max_length=255)
    protection_direction = models.CharField(max_length=255)
    start_price = models.DecimalField(max_digits=24, decimal_places=8)
    stop_price = models.DecimalField(max_digits=24, decimal_places=8)
    strike_ratio = models.DecimalField(max_digits=5, decimal_places=2)
    price_per_unit = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    price_per_unit_raw = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    tax = models.DecimalField(default=0, max_digits=24, decimal_places=8)
    discount = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    monthly_cost = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    total_cost = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    premium = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    commission = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    farmer_saving = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    potential_loss = models.DecimalField(max_digits=24, decimal_places=8, default=0)

    quote_expired = models.BooleanField(default=False, db_index=True)
    quote_expiration_date = models.DateTimeField(db_index=True, default=get_expiry_datetime)

    purchased = models.BooleanField(default=False)
    payment_style = models.CharField(max_length=255, null=True, blank=True)
    cancel_status = models.CharField(max_length=255, null=True, blank=True)
    cancelled = models.DateField(null=True, blank=True)
    pdf_id = models.UUIDField(null=True, blank=True)  # UUID generated when creating the certificate PDF
    paid_at = models.DateField(null=True, blank=True)
    invoice_end_date = models.DateTimeField(null=True, blank=True)
    payment_id = models.CharField(max_length=255, null=True, blank=True)  # String given to us by payment providers
    payment_provider = models.TextField(choices=TRANSACTION_PAYMENT_PROVIDER_CHOICES, default='unknown')
    owed_status = models.CharField(max_length=255, default='None')
    owed_amount = models.DecimalField(max_digits=24, decimal_places=8, default=0)
    contract_running_task_id = models.CharField(null=True, blank=True, max_length=255)  # String given by celery for the contract cancellation task
    cancellation_period = models.CharField(null=True, blank=True, max_length=255)

    phone_call_count = models.IntegerField(default=0)
    email_log_count = models.IntegerField(default=0)

    idempotency_key = models.UUIDField(default=uuid4)

    # state = FSMField(default='quote', protected=True)

    def __str__(self):
        return "%s - %s %s" % (self.id, self.farmer.first_name, self.farmer.last_name)

    def get_risk_inception_date(self):
        if self.protection_style == 'Asian':
            return self.start_date
        else:
            stop_date = self.stop_date
            end_date = pendulum.instance(
                datetime.datetime.combine(
                    stop_date,
                    datetime.datetime.min.time()
                )
            )
            return end_date.start_of('month')

    def get_protection_style_duration(self):
        if self.protection_style == 'Asian':
            return self.protection_duration
        else:
            return 1

    # @transition(source='quote', target='expired', field='state')
    # def expire_quote(self):
    #     pass

    # @transition(source='quote', target='purchased', field='state')
    # def purchase(self, payment_provider):
    #     self.payment_provider = payment_provider


class Farmer(models.Model):
    # Personal Info
    user = models.OneToOneField(AuthUser, on_delete=models.CASCADE)
    role = models.CharField(max_length=255, null=True)
    membership_id = models.CharField(max_length=255, null=True, blank=True)  # deprecated
    memberships = models.ManyToManyField('Partner.Farmers')
    country = models.CharField(max_length=255, default="UK")    

    # Sales Partner
    tagged_partners = ArrayField(models.IntegerField(), default=list)  # deprecated
    partners = models.ManyToManyField('Partner.Partner')

    # Farm Info
    zip = models.CharField(max_length=255, null=True)
    farm_size = models.IntegerField(null=True)
    farm_name = models.CharField(max_length=255, null=True)

    dairy_herd_size = models.BigIntegerField(default=0)
    cattle = models.BigIntegerField(default=0)
    breeding_sows = models.BigIntegerField(default=0)
    barley = models.BigIntegerField(default=0)
    rapeseed = models.BigIntegerField(default=0)
    other_crops = models.BigIntegerField(default=0)
    other_livestock = models.BigIntegerField(default=0)
    potatoes = models.BigIntegerField(default=0)
    wheat = models.BigIntegerField(default=0)
    oats = models.BigIntegerField(default=0)
    sheep = models.BigIntegerField(default=0)
    suckler_herd_size = models.BigIntegerField(default=0)


class Message(models.Model):
    company = models.ForeignKey(Partner, on_delete=models.CASCADE, default=1)
    sender = models.CharField(max_length=255, default='')
    content = models.TextField(default='')
    create_date = models.DateField(default=datetime.date.today)
    read_users = ArrayField(models.IntegerField(), blank=True, null=True)


COMPLAINT_LOG_STATUS_CHOICES = (
    ('Pending', 'Pending'),
    ('Open', 'Open'),
    ('Resolved', 'Resolved'),
)

COMPLAINT_LOG_COMPLAINT_ESCALATION = (
    ('No', 'No'),
    ('Yes', 'Yes'),
)


class ComplaintLogs(models.Model):
    assignee = models.ForeignKey(AuthUser, on_delete=models.SET_NULL, null=True)
    type = models.CharField(max_length=255, default="")
    formal_informal = models.CharField(max_length=255, default="")
    submitting_company = models.CharField(max_length=255, default="Stable")
    cover_holder = models.CharField(max_length=255, default="Stable")

    policy_holder_title = models.CharField(max_length=255, default="")
    policy_holder_forename = models.CharField(max_length=255, default="")
    policy_holder_surname = models.CharField(max_length=255, default="")
    policy_holder_company_name = models.CharField(max_length=255, default="")
    policy_holder_email = models.CharField(max_length=255, default="")
    policy_holder_mobile = models.CharField(max_length=255, default="")
    address_line_1 = models.CharField(max_length=255, default="")
    address_line_2 = models.CharField(max_length=255, default="")
    address_line_3 = models.CharField(max_length=255, default="")

    policy = models.ForeignKey('CRM.Transaction', on_delete=models.CASCADE, null=True)

    county = models.CharField(max_length=255, default="")
    postcode = models.CharField(max_length=255, default="")

    date_received = models.DateField(default=complaint_date)
    date_acknowledged = models.DateField(null=True, blank=True)
    date_resolved = models.DateField(null=True, blank=True)

    complaint_code = models.CharField(max_length=255, default="")
    claims_related = models.CharField(max_length=255, default="")
    product_type = models.CharField(max_length=255, default="")
    placement = models.CharField(max_length=255, default="")
    syndicate_number = models.CharField(max_length=255, default="")

    note_type = models.CharField(max_length=255, default="")
    note = models.TextField(default="")

    summary_resolution_communication_date = models.CharField(max_length=255, null=True, blank=True)
    summary_resolution_communication_decision = models.CharField(max_length=255, null=True, blank=True)
    summary_resolution_communication_grounds_for_justification = models.CharField(max_length=255, null=True, blank=True)
    summary_resolution_communication_action_taken = models.CharField(max_length=255, null=True, blank=True)
    summary_resolution_communication_redress_amount = models.CharField(max_length=255, null=True, blank=True)
    summary_resolution_communication_redress_date_paid = models.CharField(max_length=255, null=True, blank=True)
    root_cause = models.CharField(max_length=255, null=True, blank=True)

    status = models.TextField(default="Pending", choices=COMPLAINT_LOG_STATUS_CHOICES)
    complaint_escalation = models.TextField(choices=COMPLAINT_LOG_COMPLAINT_ESCALATION, default='No')
    date_of_escalation = models.DateField(null=True)
    conclusion_date_of_escalation = models.DateField(null=True)
    fos_eligible_complainants = models.TextField(default="")

    class Meta:
        ordering = ('date_received',)


def complaint_upload_to_handler(instance, filename):
    path = Path(filename)
    return f'complaint_log/{uuid4()}{path.suffixes}'


class ComplaintLogFile(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    complaint = models.ForeignKey(ComplaintLogs, on_delete=models.CASCADE)
    original_filename = models.TextField()
    file = models.FileField(upload_to=complaint_upload_to_handler)
    created_date = models.DateTimeField(auto_now_add=True)


class ComplaintNote(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    complaint = models.ForeignKey(ComplaintLogs, on_delete=models.CASCADE)
    note = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)


class SupportLogs(models.Model):
    customer_support_staff = models.ForeignKey(AuthUser, on_delete=models.DO_NOTHING, default=1)
    log_type = models.CharField(max_length=255)
    log = models.TextField(default='')
    logged_at = models.DateTimeField(auto_now_add=True)
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE, null=True)
    complaint = models.ForeignKey(ComplaintLogs, blank=True, null=True, on_delete=models.CASCADE)


class AmountInvestSettings(models.Model):
    underwriter = models.ForeignKey(AuthUser, on_delete=models.CASCADE)
    data = JSONField()
    country = models.CharField(max_length=255, default='UK')
    total_amount = models.DecimalField(default=0, max_digits=24, decimal_places=8)


class SimulationData(models.Model):
    market = models.CharField(max_length=255)
    commodity = models.CharField(max_length=255)
    unit = models.CharField(max_length=255)
    currency = models.CharField(max_length=255)
    style = models.CharField(max_length=255)
    direction = models.CharField(max_length=255)
    strike_min = models.DecimalField(max_digits=24, decimal_places=8)
    strike_max = models.DecimalField(max_digits=24, decimal_places=8)
    strike_type = models.CharField(max_length=255)
    strike_mean = models.DecimalField(max_digits=24, decimal_places=8, null=True, blank=True)
    strike_std = models.DecimalField(max_digits=24, decimal_places=8, null=True, blank=True)
    stop = models.DecimalField(max_digits=24, decimal_places=8)
    maturity_min = models.IntegerField()
    maturity_max = models.IntegerField()
    maturity_type = models.CharField(max_length=255)
    maturity_mean = models.IntegerField(null=True, blank=True)
    maturity_std = models.IntegerField(null=True, blank=True)
    quantity_min = models.DecimalField(max_digits=24, decimal_places=8)
    quantity_max = models.DecimalField(max_digits=24, decimal_places=8)
    quantity_type = models.CharField(max_length=255)
    quantity_mean = models.DecimalField(max_digits=24, decimal_places=8, null=True, blank=True)
    quantity_std = models.DecimalField(max_digits=24, decimal_places=8, null=True, blank=True)
    type = models.CharField(max_length=255, null=True, blank=True)
    underwriter = models.ForeignKey(AuthUser, on_delete=models.CASCADE, null=True, blank=True)
