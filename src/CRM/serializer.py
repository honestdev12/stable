import datetime

from django.db.models import Q
from rest_framework import serializers

from administration.models import AuthUser
from administration.serializer import (
    UserFarmerSerializer,
    UserSerializer,
)
from Partner.serializer import (
    AgentSerializer,
    PartnerSerializer,
)
from .models import (
    AmountInvestSettings,
    ComplaintLogFile,
    ComplaintLogs,
    ComplaintNote,
    Farmer,
    Message,
    SimulationData,
    SupportLogs,
    Transaction,
)


class TransactionSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    farmer = serializers.SerializerMethodField('get_farmer_name')
    partner = serializers.CharField(source='partner.name')
    purchase_status = serializers.SerializerMethodField('get_paid_status')
    affiliate = AgentSerializer(required=True)
    index_owner = serializers.CharField(source='category.index.owner')
    category_name = serializers.CharField(source='category.name', read_only=True)
    market_name = serializers.CharField(source='market.name', read_only=True)

    total_cost = serializers.SerializerMethodField()
    premium = serializers.SerializerMethodField()
    monthly_cost = serializers.SerializerMethodField()

    def get_premium(self, value):
        return value.premium / 100

    def get_total_cost(self, value):
        return value.total_cost / 100

    def get_monthly_cost(self, value):
        return value.monthly_cost / 100

    @staticmethod
    def get_paid_status(obj):
        if obj.cancel_status:
            return obj.cancel_status
        else:
            if obj.payment_style == 'Invoice':
                return 'Pending'
            elif obj.payment_style == 'Void':
                return 'Void'
            elif obj.payment_style == 'Payment on Account':
                return 'Payment on Account'
            elif obj.payment_style == 'Paid':
                return 'Paid'
            else:
                if obj.purchased:
                    return "Purchased"
                else:
                    if obj.quote_expired:
                        return 'Quote Expired'
                    else:
                        return "Quote"

    @staticmethod
    def get_farmer_name(obj):
        return obj.farmer.get_full_name()

    @staticmethod
    def get_partner_name(obj):
        return obj.partner.name

    class Meta:
        model = Transaction
        fields = '__all__'


class FarmerCRMSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    user = UserFarmerSerializer(required=True)
    recent_purchases = serializers.SerializerMethodField('get_recent_transactions')
    other_purchases = serializers.SerializerMethodField('get_other_transactions')

    @staticmethod
    def get_recent_transactions(obj):
        today = datetime.datetime.now()
        year = today.year
        month = today.month

        recent = TransactionSerializer(
            Transaction.objects.filter(
                farmer=obj.user,
                quote_date__year__gte=year,
                quote_date__month__gte=month
            ),
            many=True
        )

        return recent.data

    @staticmethod
    def get_other_transactions(obj):
        today = datetime.datetime.now()
        year = today.year
        month = today.month

        recent = TransactionSerializer(
            Transaction.objects.filter(
                Q(farmer=obj.user),
                (Q(purchased=True) & Q(cancel_status=None)),
                (~Q(quote_date__year__gte=year) | ~Q(quote_date__month__gte=month))
            ),
            many=True
        )

        return recent.data

    class Meta:
        model = Farmer
        fields = '__all__'


class FarmerSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    user = UserSerializer()

    class Meta:
        model = Farmer
        fields = '__all__'


class AuthUserSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField('get_user_full_name')

    @staticmethod
    def get_user_full_name(obj):
        name = obj.first_name + " " + obj.last_name
        return name

    class Meta:
        model = AuthUser
        fields = ('id', 'full_name', 'email', 'username', 'is_partner', 'is_superuser',
                  'is_staff', 'is_active', 'company_name')


class MessageSerializer(serializers.ModelSerializer):
    key = serializers.CharField(source='id')
    company = PartnerSerializer(required=True)

    class Meta:
        model = Message
        fields = '__all__'


class ComplaintsFilesSerializer(serializers.ModelSerializer):

    class Meta:
        model = ComplaintLogFile
        fields = '__all__'
        extra_kwargs = {
            'complaint': {
                'required': False
            }
        }


class ComplaintNoteSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(read_only=True, required=False, source='user.first_name', default='Deleted')
    last_name = serializers.CharField(read_only=True, required=False, source='user.last_name', default='User')

    class Meta:
        model = ComplaintNote
        fields = '__all__'
        extra_kwargs = {
            'complaint': {
                'required': False
            }
        }


class ComplaintsSerializer(serializers.ModelSerializer):
    policy_number = serializers.PrimaryKeyRelatedField(queryset=Transaction.objects.all(), source="policy")
    assignee = serializers.PrimaryKeyRelatedField(queryset=AuthUser.objects.filter(is_customer_supporter=True))
    policy_holder_name = serializers.SerializerMethodField()
    date_acknowledged = serializers.DateField(required=False)
    date_resolved = serializers.DateField(required=False)
    date_received = serializers.DateField()
    file = ComplaintsFilesSerializer(many=True, source='complaintlogfile_set')
    notes = ComplaintNoteSerializer(many=True, source='complaintnote_set')

    def get_policy_holder_name(self, obj):
        try:
            return f"{obj.policy.farmer.first_name} {obj.policy.farmer.last_name}"
        except Exception:
            return "Deleted User"

    class Meta:
        model = ComplaintLogs
        exclude = ('policy', )
        extra_kwargs = {
            'address_line_2': {
                'required': False,
                'allow_blank': True
            },
            'address_line_3': {
                'required': False,
                'allow_blank': True
            },
            'county': {
                'required': False,
            },
            'policy_holder_address_line_3': {
                'required': False,
            },
            'complaint_escalation': {
                'required': False,
            },
            'date_of_escalation': {
                'required': False,
            },
            'conclusion_date_of_escalation': {
                'required': False,
            },
            'fos_eligible_complainants': {
                'required': False,
            },
            'file': {
                'required': False,
            },
            'notes': {
                'required': False
            }
        }

    def create(self, validated_data):
        print(validated_data)
        files_data = validated_data.pop('complaintlogfile_set')
        notes_data = validated_data.pop('complaintnote_set')
        complaint = super(ComplaintsSerializer, self).create(validated_data=validated_data)
        for file_data in files_data:
            if file_data.get('uuid', None) is None:
                ComplaintLogFile.objects.create(complaint=complaint, **file_data)
        for note in notes_data:
            if note.get('uuid', None) is None:
                ComplaintNote.objects.create(complaint=complaint, **note)
        return complaint

    def update(self, instance, validated_data):
        files_data = validated_data.pop('complaintlogfile_set')
        notes_data = validated_data.pop('complaintnote_set')
        complaint = super(ComplaintsSerializer, self).update(
            instance=instance,
            validated_data=validated_data
        )
        for file_data in files_data:
            if file_data.get('uuid', None) is None:
                ComplaintLogFile.objects.create(complaint=complaint, **file_data)

        for note in notes_data:
            if note.get('uuid', None) is None:
                ComplaintNote.objects.create(complaint=complaint, **note)
        return complaint


class SupportLogsSerializer(serializers.ModelSerializer):
    transaction = TransactionSerializer(required=False)
    complaint = ComplaintsSerializer(required=False)
    customer_support_staff = AuthUserSerializer(required=True)
    key = serializers.CharField(source='id')

    class Meta:
        model = SupportLogs
        fields = ('transaction', 'complaint', 'key', 'customer_support_staff', 'log_type', 'log', 'logged_at')


class AmountInvestSettingsSerializer(serializers.ModelSerializer):
    underwriter = AuthUserSerializer(required=True)
    key = serializers.CharField(source='id')

    class Meta:
        model = AmountInvestSettings
        fields = '__all__'


class SimulationDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = SimulationData
        fields = '__all__'
