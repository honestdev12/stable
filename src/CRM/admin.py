from django.contrib import admin
from .models import Transaction, Farmer, Message, SupportLogs, AmountInvestSettings, ComplaintLogs, SimulationData


class FarmerAdmin(admin.ModelAdmin):
    list_display = [
        'user', 'membership_id', 'country'
    ]

    fieldsets = (
        ('Personal Info', {
            'fields': (
                'user', 'role', 'membership_id', 'country', 'tagged_partners'
            )
        }),
        ('Farm Info', {
            'fields': ('zip', 'farm_size', 'farm_name')
        }),
        ('Product Info', {
            'fields': (
                'dairy_herd_size', 'cattle', 'breeding_sows', 'barley', 'rapeseed', 'other_crops', 'sheep',
                'other_livestock', 'potatoes', 'wheat', 'oats', 'suckler_herd_size'
            )
        })
    )


class TransactionAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'market', 'category', 'purchased', 'paid_at', 'quote_date', 'start_date', 'stop_date', 'phone_call_count'
    ]

    fieldsets = (
        ('Farmer Info', {
            'fields': ('farmer',)
        }),
        ('Partner Info', {
            'fields': ('partner', 'affiliate', 'country', 'corporate', 'underwriters')
        }),
        ('Quote Info', {
            'fields': ('quote_expired', 'quote_expiration_date', 'start_date', 'stop_date', 'market', 'category',
                       'quantity', 'metric', 'protection_duration', 'protection_style', 'protection_direction', 'strike_ratio',
                       'start_price', 'stop_price', 'price_per_unit', 'price_per_unit_raw', 'tax', 'discount',
                       'monthly_cost', 'total_cost', 'premium', 'commission', 'farmer_saving', 'potential_loss')
        }),
        ('Payment Info', {
            'fields': (
                'purchased', 'payment_id', 'payment_style', 'cancel_status', 'cancelled', 'cancellation_period',
                'paid_at', 'pdf_id', 'invoice_end_date', 'phone_call_count', 'owed_status', 'owed_amount',
                'contract_running_task_id', 'unique_trade_id'
            )
        })
    )


class MessageAdmin(admin.ModelAdmin):
    list_display = ['id', 'sender', 'company', 'content', 'create_date']


class SupportLogsAdmin(admin.ModelAdmin):
    list_display = ['id', 'customer_support_staff_id', 'log_type', 'log', 'logged_at', 'transaction']


class ComplaintLogsAdmin(admin.ModelAdmin):
    list_display = ['id', 'policy_holder_forename', 'policy_holder_surname', 'policy_holder_company_name',
                    'policy_holder_email', 'type', 'note_type']


class AmountInvestDataAdmin(admin.ModelAdmin):
    list_display = ['id', 'underwriter', 'country', 'total_amount']


class SimulationDataAdmin(admin.ModelAdmin):
    list_display = ['id', 'market', 'commodity', 'style', 'direction', 'type', 'underwriter']


admin.site.register(Farmer, FarmerAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(SupportLogs, SupportLogsAdmin)
admin.site.register(ComplaintLogs, ComplaintLogsAdmin)
admin.site.register(AmountInvestSettings, AmountInvestDataAdmin)
admin.site.register(SimulationData, SimulationDataAdmin)
