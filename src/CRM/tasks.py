from datetime import (
    datetime,
)

import gocardless_pro
from django.apps import apps
from django.conf import settings
from django.utils import timezone

from aox_dr.celery import app
from CRM.models import Transaction
from pricing.models import PaymentStatusLog


@app.task
def set_invoice_as_void(transaction_id):
    """
    This celery task sets the 'is_active' flag of the race object
    to False in the database after the race end time has elapsed.
    """
    print('Celery working')
    model = apps.get_model('CRM.Transaction')
    transaction = model.objects.get(id=transaction_id)

    transaction.payment_style = 'Void'
    transaction.save()


@app.task
def set_policy_runs_out(transaction_id):
    print('Celery working')
    model = apps.get_model('CRM.Transaction')
    transaction = model.objects.get(id=transaction_id)

    transaction.owed_status = 'Finished'
    transaction.save()

    payment_status_log = PaymentStatusLog(
        content='Policy Finished',
        date=datetime.now(),
        policy=transaction
    )

    payment_status_log.save()


@app.task
def end_policies():
    # Transactions which have been running up until today, are ended by this task and set as awaiting their index data.
    freshly_ended_transactions = Transaction.objects.filter(stop_date__lte=timezone.now())
    for transaction in freshly_ended_transactions:
        transaction.owed_status = 'Finished'
        transaction.save()
        PaymentStatusLog.objects.create(
            content='Policy Finished',
            date=timezone.now(),
            policy=transaction
        )


@app.task(autoretry_for=(Exception,), retry_backoff=True)
def go_card_less_cancellation(transaction_id):
    model = apps.get_model('CRM.Transaction')
    transaction = model.objects.get(id=transaction_id)

    subscription_id = transaction.payment_id
    go_client = gocardless_pro.Client(
        access_token=settings.GO_CARD_LESS_ACCESS_TOKEN,
        environment=settings.GO_CARD_LESS_ENVIRONMENT
    )
    go_client.subscriptions.cancel(str(subscription_id))


@app.task
def set_quote_timeout(transaction_id):
    print('Celery working')
    model = apps.get_model('CRM.Transaction')
    transaction = model.objects.get(id=transaction_id)

    transaction.quote_expired = True
    transaction.save()

@app.task()
def timeout_quotes():
    """
    Periodic tasks to expire all quotes that have gone past the expiry date
    :return:
    """
    # Not using update to allow triggering of save methods
    transactions = Transaction.objects.filter(
        quote_expired=False,
        quote_expiration_date__lte=timezone.now()
    )
    for transaction in transactions:
        transaction.quote_expired = True
        transaction.save()

@app.task
def set_paid_month_indemnity_zero(claim_id):
    print('Celery working')
    model = apps.get_model('Borderaux.ClaimBorderaux')
    claim = model.objects.get(id=claim_id)

    claim.paid_month_indemnity = 0
    claim.save()
