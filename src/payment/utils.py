import datetime

import pendulum
from django.conf import settings
from django.template.loader import get_template

from administration.models import AuthUser
from CBAPIs.serializer import CBFarmProdSerializer
from CRM.models import Transaction
from pricing.models import IndexPriceData
from pricing.utils import (
    decimal_format,
    custom_strftime,
)


def unit_string(unit, category, price):
    json = {
        'TONNES': '{}/t',
        'KILOS': '{}/kg',
        'LITRES': '{}/l'
    }

    symbol = settings.CURRENCY_CONVERSION_RULE.get(
        "UK"
    ).get(
        category
    ).get('symbol')

    if symbol == 'p':
        return json.get(unit).format('{a} {b}'.format(a=price, b=symbol))
    else:
        return json.get(unit).format('{a} {b}'.format(a=symbol, b=price))


def render_policy_schedule(
        user: AuthUser,
        transaction: Transaction,
):
    multiplier_symbol = settings.CURRENCY_CONVERSION_RULE.get(
        "UK" if transaction.category.country == "GB" else transaction.category.country,
    ).get(
        transaction.category.name,
    )

    multiplier = multiplier_symbol.get('multiplier')
    symbol = multiplier_symbol.get('symbol')

    index_price = IndexPriceData.objects.filter(prod_name=transaction.category.name).first().price

    if user.farmer:
        products = CBFarmProdSerializer(user.farmer).data
    else:
        products = {}

    # policy schedule pdf attachment part
    attachment_pdf_template = get_template("policy_schedule.html")
    return attachment_pdf_template.render(
        {
            "address": user.address_1,
            "zip": user.post_code,
            "farm_address": user.farmer.zip if user.farmer else None,
            "full_name": user.get_full_name(),
            "farm_name": user.farmer.farm_name if user.farmer else None,
            "farm_size": user.farmer.farm_size if user.farmer else None,
            "start_date": custom_strftime("{S} %B %Y", transaction.get_risk_inception_date()),
            "end_date": custom_strftime("{S} %B %Y", transaction.stop_date),
            "commodity": transaction.category.name,
            "quantity": transaction.quantity,
            "unit": transaction.category.unit,
            "start_price": unit_string(
                transaction.category.unit,
                transaction.category.name,
                decimal_format(transaction.start_price / multiplier)
            ),
            "stop_price": unit_string(
                transaction.category.unit,
                transaction.category.name,
                decimal_format(transaction.stop_price / multiplier)
            ),
            "index_name": transaction.category.index.owner,
            "premium_with_tax": decimal_format(transaction.total_cost / 100),
            "ipt": decimal_format((transaction.total_cost - transaction.premium) / 100),
            "potential_loss": decimal_format(transaction.potential_loss / 100),
            "protection_duration": '{} Months'.format(
                transaction.protection_duration
            ) if transaction.protection_style == "Asian" else '1 Month',
            "index_price": decimal_format(index_price),
            "products": products,
            "symbol": symbol
        }
    )


def get_first_day(dt: datetime.datetime = None):
    if dt is None:
        date_time = pendulum.today()
    else:
        date_time = pendulum.instance(dt)
    return date_time.start_of('month')


def get_last_day(dt: datetime.datetime = None):
    if dt is None:
        date_time = pendulum.today()
    else:
        date_time = pendulum.instance(dt)
    return date_time.end_of('month')
