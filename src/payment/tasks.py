import datetime
import io
from decimal import Decimal
from uuid import uuid4

import pdfkit
import stripe
from celery import shared_task
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.storage import default_storage
from django.template.loader import get_template

from administration.models import AuthUser
from aox_dr.utils import send_provider_agnostic_email
from Borderaux.models import RiskBorderaux
from CBAPIs.models import (
    Category,
    Market,
)
from CBAPIs.serializer import CBFarmProdSerializer
from CRM.models import (
    Farmer,
    Transaction,
)
from payment.utils import (
    get_first_day,
    get_last_day,
    render_policy_schedule,
)
from pricing.models import IndexPriceData
from pricing.utils import decimal_format


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 6}, retry_backoff=True)
def send_successful_payment_mail(
        first_name, last_name, post_code, business_name, quote, market_id, category_id, index, index_owner,
        direction, start_date, closing_date, quote_date, current_start_price, stop_price, quantity, unit, current_cost,
        monthly_cost, months, farmer_email, premium, tax, potential_loss, purchase_id, monthly, protection_duration):
    farmer = AuthUser.objects.get(email=farmer_email)
    farm = Farmer.objects.get(user=farmer) if farmer.is_farmer else None
    market = Market.objects.get(pk=market_id)
    category = Category.objects.get(pk=category_id)
    transaction = Transaction.objects.get(id=purchase_id)

    multiplier = settings.CURRENCY_CONVERSION_RULE.get('UK').get(category.name).get('multiplier')
    currency = settings.CURRENCY_CONVERSION_RULE.get('UK').get(category.name).get('unit')

    # Apply price conversion rules to start and stop prices
    current_start_price = decimal_format(Decimal(current_start_price) / Decimal(multiplier))
    stop_price = decimal_format(Decimal(stop_price) / Decimal(multiplier))

    # Convert specific prices to Pounds
    premium = decimal_format(Decimal(premium) / 100)
    current_cost = decimal_format(Decimal(current_cost) / 100)
    tax = decimal_format(Decimal(tax) / 100)
    monthly_cost = decimal_format(Decimal(monthly_cost) / 100)
    potential_loss = decimal_format(Decimal(potential_loss) / 100)

    if currency == 'Pound':
        pound = True
        penny = False
    else:
        pound = False
        penny = True

    # Payment Receipt pdf attachment part
    pdf_id = str(uuid4())
    attachment_pdf_template = get_template("payment_confirmation_pdf.html")
    attachment_pdf_html = attachment_pdf_template.render(
        {
            "start_date": start_date,
            "quote_date": quote_date,
            "full_name": first_name.title() + ' ' + last_name.title(),
            "post_code": post_code,
            "business_name": business_name,
            "policy_number": quote,
            "total_cost": current_cost,
            "premium": premium,
            "tax": tax,
        }
    )

    receipt_content = pdfkit.from_string(
        attachment_pdf_html,
        False
    )
    default_storage.save(f'email_attachment/{pdf_id}Payment Receipt.pdf', io.BytesIO(receipt_content))

    # Certificate pdf attachment part
    attachment_pdf_template = get_template("certificate.html")
    attachment_pdf_html = attachment_pdf_template.render(
        {
            "certificate_number": pdf_id,
            "policy_holder_address_1": farmer.address_1,
            "policy_holder_address_2": farmer.address_2,
            "policy_holder_address_3": farmer.county,
            "policy_holder_address_4": farmer.post_code,
            "policy_holder_name": farmer.get_full_name(),
            "farm_name": business_name,
            "start_date": transaction.get_risk_inception_date(),
            "end_date": transaction.stop_date,
            "commodity": category.name,
            "quantity": quantity,
            "start_price": current_start_price,
            "stop_price": stop_price,
            "index_name": index,
            "premium": premium,
            "premium_with_tax": tax,
            "total_cost": current_cost,
            "policy_id": quote,
            "potential_loss": potential_loss,
            "umr": "B0750RARSP1900807"
        }
    )

    certificate_content = pdfkit.from_string(attachment_pdf_html, False)
    default_storage.save(f'email_attachment/{pdf_id}Certificate Insurance.pdf', io.BytesIO(certificate_content))

    # policy schedule pdf attachment part

    attachment_pdf_html = render_policy_schedule(farmer, transaction)

    schedule_content = pdfkit.from_string(attachment_pdf_html, False)

    default_storage.save(f'email_attachment/{pdf_id}Policy Schedule.pdf', io.BytesIO(schedule_content))

    # attach stable policy insurance pdf
    policy_content = default_storage.open('email_attachment/Stable_Insurance_Policy.pdf').read()

    # attach IPID pdf
    ipid_content = default_storage.open('email_attachment/IPID.pdf').read()

    transaction.pdf_id = pdf_id
    transaction.save()

    attachments = [
        # ('Payment Receipt.pdf', receipt_content, 'application/pdf'),
        ('Certificate Insurance.pdf', certificate_content, 'application/pdf'),
        ('Policy Schedule.pdf', schedule_content, 'application/pdf'),
        ('Stable Insurance Policy.pdf', policy_content, 'application/pdf'),
        ('IPID.pdf', ipid_content, 'application/pdf')
    ]

    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'first_name': first_name.title(),
            'pound': pound,
            'penny': penny,
            'quote': quote,
            'market': market.name,
            'category': category.name,
            'index': index_owner,
            'direction': direction.title(),
            'closing_date': closing_date,
            'current_start_price': current_start_price,
            'stop_price': stop_price,
            'quantity': quantity,
            'unit': unit.title(),
            'current_cost': current_cost,
            'monthly_pay': monthly_cost if monthly else None,
            'months': months if monthly else None
        }
    else:
        template_id = "successful-monthly-payment-template" if monthly else "successful-payment-template"
        merge_vars = {
            'FIRST_NAME': first_name.title(),
            'POUND': pound,
            'PENNY': penny,
            'QUOTE': quote,
            'MARKET': market.name,
            'CATEGORY': category.name,
            'INDEX': index_owner,
            'DIRECTION': direction.title(),
            'CLOSING_DATE': closing_date,
            'CURRENT_START_PRICE': current_start_price,
            'STOP_PRICE': stop_price,
            'QUANTITY': quantity,
            'UNIT': unit.title(),
            'CURRENT_COST': current_cost,
            'MONTHLY_PAY': monthly_cost if monthly else None,
            'MONTHS': months if monthly else None
        }

    send_provider_agnostic_email(
        "Your Stable payment has been successful",
        to_email=farmer_email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=attachments
    )


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 6}, retry_backoff=True)
def send_policy_cancel_mail(first_name, email, policy_id, cancelled, market, category, index, protection_direction,
                            start_date, stop_date, start_price, stop_price, quantity, total_cost, monthly_cost,
                            cancellation_period, unit, premium, tax, potential_loss, protection_duration):
    farmer = AuthUser.objects.get(email=email)
    farm = Farmer.objects.get(user=farmer) if farmer.is_farmer else None
    index_price = IndexPriceData.objects.filter(prod_name=category).first().price
    transaction = Transaction.objects.get(pk=policy_id)

    if farm:
        products = CBFarmProdSerializer(farm).data
    else:
        products = {}

    multiplier = settings.CURRENCY_CONVERSION_RULE.get('UK').get(category).get('multiplier')
    currency = settings.CURRENCY_CONVERSION_RULE.get('UK').get(category).get('unit')

    # Apply price conversion rules to start and stop prices
    start_price = decimal_format(Decimal(start_price) / Decimal(multiplier))
    stop_price = decimal_format(Decimal(stop_price) / Decimal(multiplier))

    # Convert specific prices to Pounds
    total_cost = decimal_format(Decimal(total_cost) / 100)
    monthly_cost = decimal_format(Decimal(monthly_cost) / 100)
    premium = decimal_format(Decimal(premium) / 100)
    tax = decimal_format(Decimal(tax) / 100)
    potential_loss = decimal_format(Decimal(potential_loss) / 100)

    if currency == 'Pound':
        pound = True
        penny = False
    else:
        pound = False
        penny = True

    if int(cancellation_period) > 14:
        cancel_in = False
    else:
        cancel_in = True

    # policy schedule pdf attachment part

    attachment_pdf_html = render_policy_schedule(farmer, transaction)

    schedule_content = pdfkit.from_string(attachment_pdf_html, False)

    default_storage.save(f'email_attachment/{transaction.pdf_id}Policy Schedule.pdf', io.BytesIO(schedule_content))

    # attach stable policy insurance pdf
    policy_content = default_storage.open('email_attachment/Stable_Insurance_Policy.pdf').read()

    # attach IPID pdf
    ipid_content = default_storage.open('email_attachment/IPID.pdf').read()

    attachments = [
        ('Policy Schedule.pdf', schedule_content, 'application/pdf'),
        ('Stable Insurance Policy.pdf', policy_content, 'application/pdf'),
        ('IPID.pdf', ipid_content, 'application/pdf')
    ]

    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'first_name': first_name.title(),
            'pound': pound,
            'penny': penny,
            'quote': policy_id,
            'cancellation_date': cancelled,
            'category': category,
            'market': market,
            'index': index,
            'direction': protection_direction,
            'closing_date': stop_date,
            'current_start_price': start_price,
            'stop_price': stop_price,
            'quantity': quantity,
            'unit': unit.title(),
            'current_cost': total_cost,
            'monthly_pay': monthly_cost,
            'form_link': 'https://stable.formstack.com/forms/'
                         'farmer_reimbursement_details?policy_id={}'.format(policy_id) if cancel_in else None
        }
    else:
        template_id = "Policy_Cancellation_within_14days" if cancel_in else "Policy_Cancellation_outside_14days"
        merge_vars = {
            'FIRST_NAME': first_name.title(),
            'POUND': pound,
            'PENNY': penny,
            'QUOTE': policy_id,
            'Cancellation_date': cancelled,
            'CATEGORY': category,
            'MARKET': market,
            'INDEX': index,
            'DIRECTION': protection_direction,
            'CLOSING_DATE': stop_date,
            'CURRENT_START_PRICE': start_price,
            'STOP_PRICE': stop_price,
            'QUANTITY': quantity,
            'UNIT': unit.title(),
            'CURRENT_COST': total_cost,
            'MONTHLY_PAY': monthly_cost,
            'FORM_LINK': 'https://stable.formstack.com/forms/'
                         'farmer_reimbursement_details?policy_id={}'.format(policy_id) if cancel_in else None
        }

    send_provider_agnostic_email(
        "Cancellation of Stable insurance policy",
        to_email=email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=attachments
    )


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 6}, retry_backoff=True)
def send_payment_cancellation_email(transaction_id, first_name, email, quote, month_unpaid, months_over, date_3_days, market, category,
                                    index, protection_direction, start_date, stop_date, start_price, stop_price,
                                    quantity, unit, total_cost, monthly_cost, months, tax, premium,
                                    potential_loss, protection_duration):
    farmer = AuthUser.objects.get(email=email)
    farm = Farmer.objects.get(user=farmer) if farmer.is_farmer else None
    index_price = IndexPriceData.objects.filter(prod_name=category.name).first().price
    transaction = Transaction.objects.get(pk=transaction_id)

    if farm:
        products = CBFarmProdSerializer(farm).data
    else:
        products = {}

    multiplier = settings.CURRENCY_CONVERSION_RULE.get('UK').get(category).get('multiplier')
    currency = settings.CURRENCY_CONVERSION_RULE.get('UK').get(category).get('unit')

    # Apply price conversion rules to start and stop prices
    start_price = decimal_format(Decimal(start_price) / Decimal(multiplier))
    stop_price = decimal_format(Decimal(stop_price) / Decimal(multiplier))

    # Convert specific prices to Pounds
    total_cost = decimal_format(Decimal(total_cost) / 100)
    monthly_cost = decimal_format(Decimal(monthly_cost) / 100)
    tax = decimal_format(Decimal(tax) / 100)
    premium = decimal_format(Decimal(premium) / 100)
    potential_loss = decimal_format(Decimal(potential_loss) / 100)

    if currency == 'Pound':
        pound = True
        penny = False
    else:
        pound = False
        penny = True

    # policy schedule pdf attachment part
    attachment_pdf_html = render_policy_schedule(farmer, transaction)

    schedule_content = pdfkit.from_string(attachment_pdf_html, False)

    default_storage.save(f'email_attachment/{transaction.pdf_id}Policy Schedule.pdf', io.BytesIO(schedule_content))

    # attach stable policy insurance pdf
    policy_content = default_storage.open('email_attachment/Stable_Insurance_Policy.pdf').read()

    # attach IPID pdf
    ipid_content = default_storage.open('email_attachment/IPID.pdf').read()

    attachments = [
        ('Policy Schedule.pdf', schedule_content, 'application/pdf'),
        ('Stable Insurance Policy.pdf', policy_content, 'application/pdf'),
        ('IPID.pdf', ipid_content, 'application/pdf')
    ]

    if settings.EMAIL_BACKEND == 'sgbackend.SendGridBackend':
        template_id = ''
        merge_vars = {
            'first_name': first_name.title(),
            'pound': pound,
            'penny': penny,
            'quote': quote,
            'category': category,
            'market': market,
            'index': index,
            'direction': protection_direction,
            'closing_date': stop_date,
            'current_start_price': start_price,
            'stop_price': stop_price,
            'quantity': quantity,
            'unit': unit.title(),
            'current_cost': total_cost,
            'monthly_pay': monthly_cost,
            'months': months,
            'month_unpaid': month_unpaid,
            'date_3_days': date_3_days,
            'payment_left': monthly_cost,
            'months_overdue': months_over
        }
    else:
        template_id = "cancel-monthly-payment-template"
        merge_vars = {
            'FIRST_NAME': first_name.title(),
            'POUND': pound,
            'PENNY': penny,
            'QUOTE': quote,
            'CATEGORY': category,
            'MARKET': market,
            'INDEX': index,
            'DIRECTION': protection_direction,
            'CLOSING_DATE': stop_date,
            'CURRENT_START_PRICE': start_price,
            'STOP_PRICE': stop_price,
            'QUANTITY': quantity,
            'UNIT': unit.title(),
            'CURRENT_COST': total_cost,
            'MONTHLY_PAY': monthly_cost,
            'MONTHS': months,
            'MONTH_UNPAID': month_unpaid,
            'DATE_3_DAYS': date_3_days,
            'PAYMENT_LEFT': monthly_cost,
            'MONTHS_OVERDUE': months_over
        }

    send_provider_agnostic_email(
        "",
        to_email=email,
        template_id=template_id,
        merge_vars=merge_vars,
        attachments=attachments
    )


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 6}, retry_backoff=True)
def charge_with_stripe(amount, currency, source, description, idempotency_key):
    return stripe.Charge.create(
        amount=int(amount),
        currency=currency,
        source=source,
        description=description,
        idempotency_key=idempotency_key
    )


def success_payment(
        user_id: int,
        purchase_id: int,
        monthly: bool = False
):
    User = get_user_model()

    purchase = Transaction.objects.get(pk=purchase_id)
    user = User.objects.get(pk=user_id)
    farmer = Farmer.objects.get(user=user)

    RiskBorderaux.objects.create(
        reporting_period_start=get_first_day(),
        reporting_period_end=get_last_day(),
        transaction=purchase,
        risk_inception_date=purchase.get_risk_inception_date(),
        risk_expiry_date=purchase.stop_date,
        installments=purchase.protection_duration if purchase.payment_provider == 'gocardless' else 1,
        installment_basis='monthly' if purchase.payment_provider == 'gocardless' else '',
        notes='',
        sic_code='1500',
        sic_description='Mixed Farming'
    )

    send_successful_payment_mail.delay(
        first_name=user.first_name,
        last_name=user.last_name,
        post_code=farmer.zip if user.is_farmer else '',
        business_name=farmer.farm_name if user.is_farmer else '',
        farmer_email=purchase.farmer.email,
        quote=purchase.id,
        market_id=purchase.market.pk,
        category_id=purchase.category.pk,
        index=purchase.category.index.name,
        index_owner=purchase.category.index.owner,
        direction=purchase.protection_direction,
        start_date=datetime.datetime.strftime(purchase.start_date, "%B %Y"),
        closing_date=datetime.datetime.strftime(purchase.stop_date, "%B %Y"),
        quote_date=datetime.datetime.strftime(purchase.quote_date, "%B %Y"),
        current_start_price=purchase.start_price,
        stop_price=purchase.stop_price,
        quantity=purchase.quantity,
        unit=purchase.metric,
        current_cost=purchase.total_cost,
        monthly_cost=purchase.monthly_cost,
        months=purchase.protection_duration,
        premium=purchase.premium,
        tax=Decimal(purchase.tax) * Decimal(purchase.premium),
        potential_loss=purchase.potential_loss,
        purchase_id=purchase.id,
        monthly=monthly,
        protection_duration=purchase.protection_duration
    )
